//
// Created by jhrub on 13.03.2022.
//

#pragma once
#include "serialization/json.h"
#include "stream_data_formatter.h"

#include <serialization/json/support_data_stream.h>

using namespace spider::serialization::json_data_stream;
namespace json = spider::serialization::json;

namespace spider
{
   class json_formatter : public stream_data_formatter
   {
    public:
      json_formatter(fmt::memory_buffer &buffer) : json_state(buffer) {}

      void format_begin(commons::data::data_stream &rows) final
      {
         json_state.begin_array();
      }

      bool format_rows(commons::data::data_stream &rows, size_t count) final
      {
         if (write_data_stream_as_objects(json_state, rows, count) < count)
         {
            json_state.end_array();
            return false;
         }
         else
         {
            return true;
         }
      }

    private:
      spider::serialization::json::json_buffer json_state;
   };

} // namespace spider