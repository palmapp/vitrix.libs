//
// Created by jhrub on 13.03.2022.
//

#pragma once
#include "serialization/json.h"
#include "stream_data_formatter.h"
#include <boost/algorithm/string.hpp>
#include <spider/std_types.h>
namespace spider
{
   class csv_formatter : public stream_data_formatter
   {
    public:
      csv_formatter(fmt::memory_buffer &buffer, spider::optional<stream_formatter_options> options) : buffer_(buffer)
      {

         if (options.has_value())
         {
            delimiter = options.value().csv_record_delimiter;
            decimal = options.value().csv_decimal_point;
         }
      }

      void format_begin(commons::data::data_stream &rows) final
      {
         auto columns = rows.get_column_names();
         if (columns.has_value())
         {
            first_row_ = true;
            std::for_each(columns->begin(), columns->end(),
                          [&](auto &name)
                          {
                             delimit_field();
                             write_string_field(name);
                          });
         }
      }

      bool format_rows(commons::data::data_stream &rows, size_t count) final
      {
         size_t i = 0;
         for (; i != count; ++i)
         {
            auto row = rows.next_row();
            if (!row.has_value())
            {
               break;
            }

            delimit_row();
            write_row(row.value());
         }

         return i == count;
      }

    private:
      fmt::memory_buffer &buffer_;
      char delimiter = ',';
      char decimal = '.';
      bool first_col_ = false;
      bool first_row_ = false;

      void delimit_field()
      {
         if (first_col_)
         {
            buffer_.push_back(delimiter);
         }
         else
         {
            first_col_ = true;
         }
      }

      void delimit_row()
      {
         if (first_row_)
         {
            buffer_.push_back('\r');
            buffer_.push_back('\n');
         }
         else
         {
            first_row_ = true;
         }
         first_col_ = false;
      }

      void write_row(commons::basic_types::array_view<commons::basic_types::optional_field_value> row)
      {

         std::for_each(row.begin(), row.end(),
                       [&](commons::basic_types::optional_field_value const &field)
                       {
                          delimit_field();

                          if (field.has_value())
                          {
                             field.value().visit(
                                 [&](auto const &val)
                                 {
                                    using val_type = std::decay_t<decltype(val)>;
                                    if constexpr (std::is_same_v<std::string, val_type> ||
                                                  std::is_same_v<std::string_view, val_type>)
                                    {
                                       write_string_field(val);
                                    }
                                    else if constexpr (std::is_floating_point_v<val_type>)
                                    {
                                       if (decimal == '.')
                                       {
                                         fmt::format_to(std::back_inserter(buffer_), "{}", val);
                                       }
                                       else
                                       {
                                          auto number = fmt::format("{}", val);
                                          char decimal[2] = {this->decimal, 0};
                                          boost::replace_first(number, ".", decimal);

                                         fmt::format_to(std::back_inserter(buffer_), "{}", number);
                                       }
                                    }
                                    else
                                    {
                                      fmt::format_to(std::back_inserter(buffer_), "{}", val);
                                    }
                                 });
                          }
                       });
      }

      void write_string_field(std::string_view field)
      {
         using namespace boost;
         // https://datatracker.ietf.org/doc/html/rfc4180#section-2

         if (auto it = std::find_if(field.begin(), field.end(), is_any_of("\r\n,;\"")); it != field.end())
         {
            auto escaped = static_cast<std::string>(field);
            replace_all(escaped, "\"", "\"\"");

           fmt::format_to(std::back_inserter(buffer_), "\"{}\"", escaped);
         }
         else
         {
            buffer_.append(field);
         }
      }
   };

} // namespace spider