#include "react.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <commons/file_system.h>
#include <commons/time_utils.h>
#include <easylogging++.h>
#include <serialization/json.h>
#include <serialization/mustache.h>
#include <spider/auth/authorization_scope.h>

using namespace std::string_literals;
using namespace spider;
using namespace spider::react;
using namespace commons;
using namespace boost::filesystem;
using namespace boost::algorithm;

const std::string separator = commons::fs::get_separator();

react_app::react_app(std::string scope_path, filesystem_path react_index_html_path,
                     custom_script_generator_callback script_generator, std::vector<std::string> valid_scopes)
   : scoped_app_base(std::move(scope_path)), react_index_html_path_(std::move(react_index_html_path)),
     script_generator_(script_generator), valid_scopes_(std::move(valid_scopes))
{
   reload_template_if_changed();
}

bool react_app::is_current_scoped_path(string_view scope)
{
   if (scoped_app_base::is_current_scoped_path(scope) && (ends_with(scope, ".hot-update.json") ||
                                                          ends_with(scope, ".hot-update.js") || ends_with(
                                                             scope, ".hot-update.js.map")))
   {
      return true;
   }

   for (auto &scope_path : valid_scopes_)
   {
      if (boost::starts_with(scope, scope_path))
      {
         return true;
      }
   }

   // it allows to have react internal application routing
   return scoped_app_base::is_current_scoped_path(scope) && ends_with(scope, "/") && !starts_with(scope, "/api/");
}

void react_app::handle_request_internal(spider::request &request, spider::request_handler_callback callback)
{
   if (ends_with(request.path, ".hot-update.json") || ends_with(request.path, ".hot-update.js") ||
       ends_with(request.path, ".hot-update.js.map"))
   {
      auto ex = request.socket.get_executor();

      beast::tcp_stream stream(ex);
      boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address_v4::loopback(), 3000);
      stream.connect(ep);

      beast::http::request<beast::http::string_body> fwd_request{beast::http::verb::get,
                                                                 to_boost_string_view(request.path), 10};
      for (auto &field : request.message())
      {
         if (field.name() == beast::http::field::unknown)
         {
            continue;
         }
         fwd_request.set(field.name(), field.value());
      }

      beast::http::write(stream, fwd_request);
      beast::flat_buffer buffer;

      auto response = request.make_response();
      beast::http::read(stream, buffer, *response);

      request.write_response(response, callback);
   }
   else if (ends_with(request.path, "/"))
   {
      reload_template_if_changed();

      mstch::map context;
      context["custom_script"] = generate_custom_script(request);

      auto response = request.make_response();
      response->set("content-type", "text/html");
      response->body() = mstch::render(template_, context);

      request.write_response(response, callback);
   }
   else
   {
      request.return_redirect(fmt::format("{}/", request.path), callback);
   }
}

std::string react_app::load_react_app_template() const
{
   auto content = commons::read_file(react_index_html_path_);
   if (content.empty())
   {
      LOG(WARNING) << "react application is not present (path = '" << react_index_html_path_.string() << "'";
   }
   else
   {
      boost::replace_all(content, "<head>", "<head><script type='text/javascript'>{{{custom_script}}}</script>");
   }

   return content;
}

void react_app::reload_template_if_changed()
{
   boost::system::error_code ec;
   auto time_point = std::chrono::system_clock::from_time_t(last_write_time(react_index_html_path_, ec));

   if (ec)
   {
      LOG(ERROR) << "unable to check last_write_time : \"" << react_index_html_path_.string() << "\" [" << ec.value()
         << "] " << ec.message();
   }
   else if (time_point != last_change_)
   {
      LOG(DEBUG) << "realoading react template";

      template_ = load_react_app_template();
      last_change_ = time_point;
   }
   else
   {
      LOG(DEBUG) << "no react template reaload needed";
   }
}

std::string react_app::generate_custom_script(spider::request &request)
{
   return script_generator_(request);
}
