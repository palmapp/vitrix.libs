namespace spider::users
{
   using sql = const char *;
   struct dialect
   {
      const char *dialect_id;

      sql user_create;

      // first_name, last_name, email_address, login, id
      sql user_update;

      // id
      sql user_delete;
      // is_active,id
      sql user_set_active;

      // password, id
      sql user_reset_password;

      // new_password, id, old_password
      sql user_change_password;

      // login, password
      sql user_verify_password;

      // id
      sql user_get_by_id;

      // <no args>
      sql user_list;
      sql user_list_with_filter;

      //(fmt: %1%_roles) id, user_id, roles
      sql set_scope_roles;

      //(fmt: %1%_roles) id, user_id
      sql get_scope_roles;

      //(fmt: %1%_roles) id, user_id
      sql get_all_scope_roles;

      // (fmt: %1%_roles) user_id
      sql clear_scope_by_user_id;

      // (fmt: %1%_roles) id, user_id
      sql clear_scope_by_scope_and_user_id;

      // <no args>
      sql create_users_table;

      // <no args>
      sql create_users_login_index;

      // scoped roles tables cannot be changed, otherwise you have to modify lot of code (fmt: %1%_roles)
      sql declare_scope_template;

      sql get_current_users_version;
      sql create_version_table;
      // migrated note
      sql insert_version;

      sql create_user_properties_table;
      sql get_all_user_properties;
      sql get_user_property;
      sql set_user_property_insert;
      sql set_user_property_update;

      sql add_default_login_path_column;

      sql add_last_pwd_change_ts_column;

      sql update_last_pwd_change_ts;

      dialect()
      {
         user_create =
             "INSERT INTO users (first_name, last_name, email_address, login, is_active, custom_props, "
             "default_login_path) VALUES "
             "(?,?,?,?,?,?,?)";
         user_update =
             "UPDATE users SET first_name = ?, last_name = ?, email_address = ?, login = ?, is_active = ?, "
             "custom_props = ?, default_login_path = ?, record_version = record_version + 1 WHERE id = ? AND "
             "record_version = ?";
         user_delete = "UPDATE users SET is_active = 0, is_deleted = 1 WHERE id = ?";
         user_set_active = "UPDATE users SET is_active = ? WHERE id = ? AND is_deleted = 0";
         user_reset_password = "UPDATE users SET password = ?, last_pwd_change_ts = ? WHERE id = ?";
         user_change_password = "UPDATE users SET password = ?, last_pwd_change_ts = ? WHERE id = ? AND password = ?";
         user_verify_password =
             "SELECT id FROM users WHERE login = ? AND password = ? AND is_active = 1 AND is_deleted = 0";
         user_get_by_id =
             "SELECT id, login, first_name, last_name, email_address, is_active, record_version, custom_props, "
             "default_login_path, last_pwd_change_ts FROM "
             "users WHERE id = ? AND is_deleted = 0";
         user_list =
             "SELECT id, login, first_name, last_name, email_address, is_active, record_version, custom_props, "
             "default_login_path, last_pwd_change_ts FROM "
             "users WHERE is_deleted = 0 ORDER BY login";

         user_list_with_filter =
             "SELECT id, login, first_name, last_name, email_address, is_active, record_version, custom_props, "
             "default_login_path, last_pwd_change_ts FROM "
             "users WHERE is_deleted = 0";

         set_scope_roles = "REPLACE INTO %1%_roles (id, user_id, roles) VALUES (?,?,?)";
         get_scope_roles = "SELECT roles FROM %1%_roles WHERE id = ? AND user_id = ?";
         get_all_scope_roles = "SELECT id, roles FROM %1%_roles WHERE user_id = ?";
         clear_scope_by_user_id = "DELETE FROM %1%_roles WHERE user_id = ?";
         clear_scope_by_scope_and_user_id = "DELETE FROM %1%_roles WHERE id = ? AND user_id = ?";

         get_current_users_version = "SELECT max(version) FROM db_version_users";
         insert_version = "INSERT INTO db_version_users (migrated) VALUES (?)";

         get_all_user_properties = "SELECT key, value FROM user_properties WHERE user_id = ?";
         get_user_property = "SELECT value FROM user_properties WHERE user_id = ? AND key = ?";
         set_user_property_insert = "INSERT INTO user_properties (user_id, key, value) VALUES (?, ?, ?)";
         set_user_property_update = "UPDATE user_properties SET value = ? WHERE user_id = ? AND key = ?";

         update_last_pwd_change_ts = "UPDATE users SET last_pwd_change_ts = ? WHERE password IS NOT NULL";
      }
   };

   struct sqlite_dialect : dialect
   {
      sqlite_dialect()
      {
         dialect_id = "sqlite";

         create_users_table =
             "CREATE TABLE IF NOT EXISTS users (id INTEGER NOT NULL, first_name varchar(100), last_name varchar(100), "
             "email_address varchar(254), login varchar(100), password varchar(100), custom_props varchar(255), "
             "is_active TINYINT, is_deleted TINYINT DEFAULT 0, record_version INTEGER DEFAULT 0, PRIMARY KEY (id))";
         create_users_login_index = "CREATE UNIQUE INDEX IX_users_login ON users (login)";
         declare_scope_template =
             "CREATE TABLE IF NOT EXISTS %1%_roles (id INTEGER NOT NULL, user_id INTEGER, roles INTEGER, PRIMARY KEY "
             "(id, user_id), FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE)";
         create_version_table =
             "CREATE TABLE IF NOT EXISTS db_version_users (version INTEGER NOT NULL, migrated VARCHAR(50), PRIMARY "
             "KEY(version))";

         create_user_properties_table =
             "CREATE TABLE IF NOT EXISTS user_properties (user_id INTEGER NOT NULL, key TEXT NOT NULL, value TEXT NOT "
             "NULL, PRIMARY KEY(user_id, key))";

         add_default_login_path_column = "ALTER TABLE `users` ADD `default_login_path` TEXT DEFAULT NULL";

         add_last_pwd_change_ts_column = "ALTER TABLE `users` ADD `last_pwd_change_ts` INTEGER DEFAULT 0";

      }
   };

   struct mysql_dialect : dialect
   {
      mysql_dialect()
      {
         dialect_id = "mysql";

         create_users_table =
             "CREATE TABLE IF NOT EXISTS users (id INTEGER NOT NULL AUTO_INCREMENT, first_name varchar(100), last_name "
             "varchar(100), email_address varchar(254), login varchar(100), password varchar(100),custom_props "
             "varchar(255), is_active TINYINT, is_deleted TINYINT DEFAULT 0,record_version INTEGER DEFAULT 0, PRIMARY "
             "KEY (id))";
         create_users_login_index = "CREATE UNIQUE INDEX IX_users_login ON users (login)";
         declare_scope_template =
             "CREATE TABLE IF NOT EXISTS %1%_roles (id INTEGER NOT NULL, user_id INTEGER, roles INTEGER, PRIMARY KEY "
             "(id, user_id), FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE)";
         create_version_table =
             "CREATE TABLE IF NOT EXISTS db_version_users (version INTEGER NOT NULL AUTO_INCREMENT, migrated "
             "VARCHAR(50), PRIMARY KEY(version))";

         create_user_properties_table =
             "CREATE TABLE IF NOT EXISTS user_properties (user_id INTEGER NOT NULL, `key` varchar(100) NOT NULL, "
             "`value` TEXT NOT NULL, PRIMARY KEY(`user_id`, `key`))";

         add_default_login_path_column = "ALTER TABLE `users` ADD `default_login_path` varchar(255) DEFAULT NULL";

         add_last_pwd_change_ts_column = "ALTER TABLE `users` ADD `last_pwd_change_ts` BIGINT DEFAULT 0";

      }
   };

   enum dialog_counter
   {
      number_of_dialects = 2
   };
   static const dialect dialects[number_of_dialects] = {sqlite_dialect{}, mysql_dialect{}};
   const dialect *get_dialect(const char *id)
   {
      for (int i = 0; i != number_of_dialects; ++i)
      {
         auto &dialect = dialects[i];
         if (strcmp(id, dialect.dialect_id) == 0)
         {
            return &dialect;
         }
      }

      return nullptr;
   }

} // namespace spider::users