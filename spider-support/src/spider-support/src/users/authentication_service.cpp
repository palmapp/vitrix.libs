#include "users.h"
#include <base64/base64.h>
#include <boost/algorithm/string.hpp>
#include <commons/guid.h>
#include <commons/hex_encode.h>
#include <commons/make_string.h>
#include <commons/rc4.h>
#include <commons/time_utils.h>
#include <easylogging++.h>
#include <memory>

#include "tx_context_factory.h"
#include <spider/auth/principal.h>

using namespace std::chrono_literals;
using namespace spider::users;
using namespace commons;

const char *secret_key = "33931f32-3291-49ea-8e3d-71d0997c2b03";

namespace
{
   struct users_principal : public spider::auth::principal_base
   {
      users_principal(std::string _id, spider::tx_context_factory &factory, user_backend &backend)
         : principal_base{std::move(_id)}, factory_(factory), backend_(backend)
      {
      }

      spider::auth::user_info get_user_info() override
      {
         auto ctx = factory_.create_context();
         auto user = backend_.get_user_by_id(*ctx, id);

         return {user.id, user.login, user.first_name, user.last_name, user.email_address, user.custom_props};
      }

   private:
      spider::tx_context_factory &factory_;
      user_backend &backend_;
   };

} // namespace

authentication_service::authentication_service(tx_context_factory &tx_ctx_factory, user_backend &backend)
   : tx_context_factory_(tx_ctx_factory), backend_(backend)
{
}

std::string authentication_service::get_cas_service_url()
{
   return {};
}

spider::auth::authentication_result authentication_service::authenticate(const string &username, const string &password)
{
   spider::auth::authentication_result result{};
   try
   {
      auto ctx = tx_context_factory_.create_context();
      auto id = backend_.verify_credentials(*ctx, username, password);
      if (!id.empty())
      {
         result.principal = std::make_shared<users_principal>(id, tx_context_factory_, backend_);

         auto token = make_string(guid::generate_unsafe(), ",upwd,", time_utils::now_in_millis(), ",",
                                  base64_encode(username), ",", base64_encode(password), ",", id);

         crypto::rc4 rc4(secret_key);
         rc4.crypt(reinterpret_cast<uint8_t *>(&token[0]), token.size(), false);
         result.data = encoding::hex_encode(token.data(), token.size());
         result.success = true;
      }
   }
   catch (const exception &e)
   {
      result.data = make_string("T:exception.known['", e.what(), "']");
      LOG(ERROR) << "auth exception: " << e.what();
   }
   catch (...)
   {
      result.data = make_string("T:exception.unknown");
      LOG(ERROR) << "auth unknown exception";
   }

   return result;
}

spider::auth::authentication_result authentication_service::verify_token(const string &token)
{
   string decrypted_token = encoding::hex_decode(token);
   crypto::rc4 rc4(secret_key);

   rc4.crypt(reinterpret_cast<uint8_t *>(&decrypted_token[0]), decrypted_token.size(), false);
   vector<string> token_parts;
   boost::split(token_parts, decrypted_token, boost::is_any_of(","));

   spider::auth::authentication_result result;
   if (token_parts.size() == 6 && token_parts[1] == "upwd")
   {
      auto timestamp = time_utils::from_millis(boost::lexical_cast<std::int64_t>(token_parts[2]));
      auto time_diff = std::chrono::system_clock::now() - timestamp;
      if (time_diff > 60s)
      {
         LOG(DEBUG) << "re-authentication";
         result = authenticate(base64_decode(token_parts[3]), base64_decode(token_parts[4]));
      }
      else
      {
         LOG(DEBUG) << "authentication skipped : time_diff = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(time_diff).count() << "ms";

         result.principal = std::make_shared<users_principal>(token_parts[5], tx_context_factory_, backend_);
         result.data = token;
         result.success = true;
      }
   }
   else
   {
      result.data = "invalid token type";
      LOG(WARNING) << "invalid authentication token : " << decrypted_token;
   }

   return result;
}