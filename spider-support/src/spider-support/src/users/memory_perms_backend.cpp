#include "users.h"
#include <cstdint>
#include <cstring>
#include <easylogging++.h>
#include <unordered_set>

using namespace spider::users;

memory_perms_backend::memory_perms_backend(role_configuration cfg) : cfg_(std::move(cfg)) {}

bool memory_perms_backend::has_perm(string const &scope, roles role, string const &perm) const
{
   auto it = cfg_.find(scope);
   if (it != cfg_.end())
   {
      size_t i = 0;
      for (auto const &role_perms : it->second)
      {
         if (role.has_role(i))
         {
            for (auto const &role_perm : role_perms)
            {
               if (role_perm == perm)
               {
                  return true;
               }
            }
         }
         ++i;
      }
   }

   return false;
}

std::vector<std::string> memory_perms_backend::get_perms(string const &scope, roles role) const
{
   unordered_set<std::string> perms;

   auto it = cfg_.find(scope);
   if (it != cfg_.end())
   {
      size_t i = 0;
      for (auto const &role_perms : it->second)
      {
         if (role.has_role(i))
         {
            perms.insert(role_perms.begin(), role_perms.end());
         }
         ++i;
      }
   }

   return {perms.begin(), perms.end()};
}

std::vector<std::string> memory_perms_backend::get_declared_scopes() const
{

   vector<string> result;
   result.reserve(cfg_.size());

   for (auto const &scope : cfg_)
   {
      result.push_back(scope.first);
   }

   return result;
}
