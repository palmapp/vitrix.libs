#include "users.h"

#include <spider/api_app_base.h>
#include <spider/http_date.h>

#include <xdb/xdb.h>

#include <base64/base64.h>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <commons/make_string.h>
#include <commons/md5.h>
#include <commons/time_utils.h>

#include <algorithm>
#include <stdexcept>

using namespace spider::users;
using namespace xdb;

#include "xdb_user_backend.sql.cpp"
#include <cstring>

namespace
{
   const uint8_t g_secret_number[] =
      "RdTsmDrxcB7F8WHMD1ogm6RPy5SS3UuIA5YsiPMpUZdLyUgLEihux0b26P9k4F5PRpq6Y9lKYfhMF7ereKgT5dI7qHlsK0Yk3eR7pZseotUukFE"
      "wMp6WNKW90QJvsDEEaXSejEfKCjVV0kfns1OEGXkN57pGU7OQJOxLC6NzVdVsNTG5TWWA4iuInei08dZ8SqN4YGkg";

   enum secret
   {
      secret_len = sizeof(g_secret_number) - 1
   };

   std::string hash_password(const std::string &pwd)
   {
      commons::hash::md5 hasher;
      hasher.update(g_secret_number, secret_len);
      hasher.update(reinterpret_cast<const uint8_t *>(pwd.data()), static_cast<std::uint32_t>(pwd.size()));

      auto hash_result = hasher.finish();
      return base64_encode(hash_result.begin(), hash_result.size());
   }

   user_info map_row(xdb::query_result::row &row)
   {
      return {std::to_string(row.get_int64(0).value()),
              row.get_string(1).value(),
              row.get_string(2).value_or(""),
              row.get_string(3).value_or(""),
              row.get_string(4).value_or(""),
              row.get_string(7).value_or(""),
              row.get_int64(5).value_or(0) != 0 ? true : false,
              row.get_int64(6).value_or(0),
              {},
              row.get_string(8),
              row.get_int64(9).value_or(0)};
   }

   xdb::connection &get_connection(commons::tx_context &ctx)
   {
      auto &xdb_context = ctx.safe_cast_or_throw<db_context>();
      return xdb_context.get_connection();
   }

   xdb::query prepare_query(commons::tx_context &ctx, std::string const &sql)
   {
      return get_connection(ctx).create_query(sql);
   }
} // namespace

xdb_user_backend::xdb_user_backend(perms_backend &perms)
   : perms_(perms)
{
}

user_id xdb_user_backend::create(commons::tx_context &ctx, const user_info &info)
{
   auto &connection = get_connection(ctx);
   auto query = connection.create_query(dialect_->user_create);

   query.bind(1, info.first_name);
   query.bind(2, info.last_name);
   query.bind(3, info.email_address);
   query.bind(4, info.login);
   query.bind(5, static_cast<int8_t>(info.is_active ? 1 : 0));
   query.bind(6, info.custom_props);
   if (info.default_login_path.has_value())
   {
      query.bind(7, info.default_login_path.value());
   }
   else
   {
      query.bind_null(7);
   }

   query.execute();

   return connection.get_string_last_row_id().value();
}

void xdb_user_backend::update(commons::tx_context &ctx, const user_info &info)
{
   auto query = prepare_query(ctx, dialect_->user_update);
   query.bind(1, info.first_name);
   query.bind(2, info.last_name);
   query.bind(3, info.email_address);
   query.bind(4, info.login);
   query.bind(5, static_cast<int8_t>(info.is_active ? 1 : 0));
   query.bind(6, info.custom_props);
   if (info.default_login_path.has_value())
   {
      query.bind(7, info.default_login_path.value());
   }
   else
   {
      query.bind_null(7);
   }
   query.bind(8, boost::lexical_cast<int64_t>(info.id));
   query.bind(9, info.record_version);

   if (query.execute() == 0)
   {
      auto modification_query = prepare_query(ctx, "select record_version from users where id = ?");
      modification_query.bind(1, boost::lexical_cast<int64_t>(info.id));
      auto record_version = modification_query.execute_for_int64();
      if (!record_version.has_value())
      {
         throw operation_exception(operation_result::make_entity_not_found_result(info.id));
      }
      else if (record_version.value() != info.record_version)
      {
         throw operation_exception(
            operation_result::make_concurrent_modification_result(info.id, fmt::to_string(record_version.value())));
      }

      get_user_by_id(ctx, info.id);
   }
}

void xdb_user_backend::set_active(commons::tx_context &ctx, user_id const &id, bool active)
{
   auto query = prepare_query(ctx, dialect_->user_set_active);
   query.bind(1, static_cast<int8_t>(active ? 1 : 0));
   query.bind(2, boost::lexical_cast<int64_t>(id));

   if (query.execute() == 0)
   {
      get_user_by_id(ctx, id);
   }
}

void xdb_user_backend::delete_user_by_id(commons::tx_context &ctx, const user_id &id)
{
   auto query = prepare_query(ctx, dialect_->user_delete);
   query.bind(1, boost::lexical_cast<int64_t>(id));

   query.execute();
}

user_with_roles xdb_user_backend::get_user_by_id(commons::tx_context &ctx, user_id const &id)
{
   auto query = prepare_query(ctx, dialect_->user_get_by_id);
   query.bind(1, boost::lexical_cast<int64_t>(id));

   vector<user_info> info;
   auto db_rows = query.execute_for_result();
   std::transform(db_rows.begin(), db_rows.end(), back_inserter(info), map_row);

   if (info.empty())
   {
      throw operation_exception(operation_result::make_entity_not_found_result(id));
   }
   else
   {
      user_with_roles result = {info[0]};
      result.user_roles = get_all_roles(ctx, id);

      return result;
   }
}

bool xdb_user_backend::change_password(commons::tx_context &ctx, const user_id &id, const string &old_pwd,
                                       const string &new_pwd)
{
   auto old_pwd_hashed = hash_password(old_pwd);
   auto new_pwd_hashed = hash_password(new_pwd);

   auto query = prepare_query(ctx, dialect_->user_change_password);
   query.bind(1, new_pwd_hashed);
   query.bind(2, commons::time_utils::now_in_millis());
   query.bind(3, boost::lexical_cast<int64_t>(id));
   query.bind(4, old_pwd_hashed);

   return query.execute() != 0;
}

void xdb_user_backend::reset_password(commons::tx_context &ctx, const user_id &id, const string &new_pwd)
{
   auto new_pwd_hashed = hash_password(new_pwd);
   auto query = prepare_query(ctx, dialect_->user_reset_password);

   query.bind(1, new_pwd_hashed);
   query.bind(2, commons::time_utils::now_in_millis());
   query.bind(3, boost::lexical_cast<int64_t>(id));
   query.execute();
}

vector<user_info> xdb_user_backend::list_users(commons::tx_context &ctx, commons::data::data_query const *q)
{
   auto query = ([&]()
   {
      if (q != nullptr && !q->query.empty())
      {
         return prepare_query(ctx, fmt::format("{} AND {}", dialect_->user_list_with_filter, q->query));
      }
      else
      {
         return prepare_query(ctx, dialect_->user_list);
      }

   })();

   vector<user_info> info;
   auto db_rows = query.execute_for_result();
   std::transform(db_rows.begin(), db_rows.end(), back_inserter(info), map_row);

   return info;
}

user_id xdb_user_backend::verify_credentials(commons::tx_context &ctx, std::string const &username,
                                             std::string const &password)
{
   auto query = prepare_query(ctx, dialect_->user_verify_password);
   query.bind(1, username);
   query.bind(2, hash_password(password));

   user_id result;
   for (auto &row : query.execute_for_result())
   {
      result = std::to_string(row.get_int64(0).value());
      break;
   }

   return result;
}

void xdb_user_backend::set_roles(commons::tx_context &ctx, const user_id &id, scoped_roles const &roles)
{
   auto query = prepare_query(ctx, (boost::format(dialect_->set_scope_roles) % roles.scope).str());
   query.bind(1, roles.scope_id);
   query.bind(2, id);
   query.bind(3, static_cast<int64_t>(roles.assigned_roles.assigned_roles));

   query.execute();
}

void xdb_user_backend::clear_roles(commons::tx_context &ctx, user_id const &id)
{
   for (auto &scope : perms_.get_declared_scopes())
   {
      auto query = prepare_query(ctx, (boost::format(dialect_->clear_scope_by_user_id) % scope).str());
      query.bind(1, boost::lexical_cast<std::int64_t>(id));

      query.execute();
   }
}

void xdb_user_backend::clear_roles(commons::tx_context &ctx, user_id const &id, role_scope const &scope)
{
   const char *sql =
      scope.scope_id == -1 ? dialect_->clear_scope_by_user_id : dialect_->clear_scope_by_scope_and_user_id;

   auto query = prepare_query(ctx, (boost::format(sql) % scope.scope).str());
   int i = 1;

   if (scope.scope_id != -1)
   {
      query.bind(i++, scope.scope_id);
   }

   query.bind(i, boost::lexical_cast<int64_t>(id));
   query.execute();
}

roles xdb_user_backend::get_roles(commons::tx_context &ctx, const user_id &id, role_scope const &scope)
{
   auto query = prepare_query(ctx, (boost::format(dialect_->get_scope_roles) % scope.scope).str());
   query.bind(1, scope.scope_id);
   query.bind(2, boost::lexical_cast<int64_t>(id));

   roles result{};
   result.assigned_roles = query.execute_for_int64().value_or(0);

   return result;
}

vector<scoped_roles> xdb_user_backend::get_all_roles(commons::tx_context &ctx, const user_id &id)
{
   vector<scoped_roles> result{};

   auto &connection = get_connection(ctx);
   for (auto &scope : perms_.get_declared_scopes())
   {
      auto query = connection.create_query((boost::format(dialect_->get_all_scope_roles) % scope).str());
      query.bind(1, boost::lexical_cast<int64_t>(id));

      scoped_roles scope_spec{};
      scope_spec.scope = scope;

      for (auto &row : query.execute_for_result())
      {
         scope_spec.scope_id = row.get_int64(0).value_or(0);
         scope_spec.assigned_roles = {row.get_int64(1).value_or(0)};

         result.push_back(scope_spec);
      }
   }

   return result;
}

void xdb_user_backend::set_property(commons::tx_context &ctx, user_id const &id, string_property const &property)
{
   auto &connection = get_connection(ctx);
   auto update_stm = connection.create_query(dialect_->set_user_property_update);
   update_stm.bind(1, property.value);
   update_stm.bind(2, boost::lexical_cast<int64_t>(id));
   update_stm.bind(3, property.key);

   if (update_stm.execute() == 0)
   {
      auto insert_stm = connection.create_query(dialect_->set_user_property_insert);
      insert_stm.bind(1, boost::lexical_cast<int64_t>(id));
      insert_stm.bind(2, property.key);
      insert_stm.bind(3, property.value);

      insert_stm.execute();
   }

   on_property_changed(ctx, id, property);
}

std::string xdb_user_backend::get_property(commons::tx_context &ctx, user_id const &id, std::string const &key)
{
   auto &connection = get_connection(ctx);
   auto get_stm = connection.create_query(dialect_->get_user_property);
   get_stm.bind(1, boost::lexical_cast<int64_t>(id));
   get_stm.bind(2, key);

   return get_stm.execute_for_string().value_or("");
}

std::vector<string_property> xdb_user_backend::get_properties(commons::tx_context &ctx, user_id const &id)
{
   auto &connection = get_connection(ctx);
   auto get_stm = connection.create_query(dialect_->get_all_user_properties);
   get_stm.bind(1, boost::lexical_cast<int64_t>(id));

   std::vector<string_property> properties;
   for (auto &row : get_stm.execute_for_result())
   {
      properties.push_back({row.get_string(0).value(), row.get_string(1).value()});
   }

   return properties;
}

void xdb_user_backend::init_sql_dialect(std::string const &dialect)
{
   dialect_ = get_dialect(dialect.c_str());
   if (dialect_ == nullptr)
      throw std::domain_error(commons::make_string("xdb_user_backend sql dialect not found: ", dialect));
}

void xdb_user_backend::init_database(xdb::db_schema &schema)
{
   schema.add_sql_dialect_initializer([this](const std::string &dialect)
   {
      init_sql_dialect(dialect);
   });

   schema.add_migration("users",
                        [this](xdb::connection &connection, std::int64_t version)
                        {
                           switch (version)
                           {
                           case 0:
                              connection.execute_query(dialect_->create_users_table);
                              connection.execute_query(dialect_->create_users_login_index);
                              return xdb::migration_result{true, "created initial tables"};
                           case 1:
                              connection.execute_query(dialect_->create_user_properties_table);
                              return xdb::migration_result{true, "created properties table"};
                           case 2:
                              connection.execute_query(dialect_->add_default_login_path_column);
                              return xdb::migration_result{true, "added default_login_path column"};
                           case 3:
                           {
                              connection.execute_query(dialect_->add_last_pwd_change_ts_column);
                              auto query = connection.create_query(dialect_->update_last_pwd_change_ts);
                              query.bind(1, commons::time_utils::now_in_millis());
                              query.execute();

                              return xdb::migration_result{true, "added last_pwd_change_ts column"};
                           }
                           default:
                              break;
                           }

                           for (auto &scope : perms_.get_declared_scopes())
                           {
                              connection.execute_query((boost::format(dialect_->declare_scope_template) % scope).str());
                           }

                           return xdb::migration_result{false};
                        });
}
