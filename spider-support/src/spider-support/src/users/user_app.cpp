#include "users.h"

#include <boost/algorithm/string.hpp>
#include <regex>

#include <spider/auth/authorization_scope.h>

using namespace spider;
using namespace spider::users;
using namespace beast::http;

namespace
{
   std::vector<field_validation_error> clean_input_and_validate(user_info &data)
   {
      std::vector<field_validation_error> result{};
      boost::algorithm::trim(data.login);
      boost::algorithm::trim(data.first_name);
      boost::algorithm::trim(data.last_name);
      boost::algorithm::trim(data.email_address);

      if (data.login.empty())
      {
         result.emplace_back("login", "T:userinfo.field.required");
      }

      if (!data.email_address.empty())
      {
         std::regex email_regex{".+@.+\\..+"};
         if (!regex_match(data.email_address, email_regex))
         {
            result.emplace_back("email_address", "T:userinfo.field.invalid_email");
         }
      }

      return result;
   }

   void throw_if_modification_is_on_same_user(spider::request &req, std::string const &id)
   {
      auth::run_principal_scope(req,
                                [&](auth::principal_base &principal)
                                {
                                   if (id == principal.id)
                                   {
                                      throw operation_exception(operation_result::make_forbidden_result());
                                   }
                                });
   }
} // namespace

user_app::user_app(std::string path, tx_context_factory &factory, user_backend &backend,
                   user_properties_backend &properties_backend)
    : api_app_base(std::move(path)), factory_(factory), backend_(backend), user_properties_backend_(properties_backend)
{
   on(verb::get, "/user/",
      [this](request &req, id_request data)
      {
         auto ctx = factory_.create_context(req);
         return backend_.get_user_by_id(*ctx, data.id);
      });

   on(verb::post, "/user/",
      [this](request &req, user_with_roles data)
      {
         throw_if_modification_is_on_same_user(req, data.id);

         auto validation = clean_input_and_validate(data);
         if (!validation.empty())
         {
            return operation_result::make_validation_failed_result(std::move(validation));
         }

         {
            auto ctx = factory_.create_context(req);
            if (!data.id.empty())
            {
               backend_.update(*ctx, data);
            }
            else
            {
               data.id = backend_.create(*ctx, data);
            }

            for (auto &role : data.user_roles)
            {
               backend_.set_roles(*ctx, data.id, role);
            }
         }

         return operation_result::make_success_result(data.id);
      });

   on(verb::get, "/users/",
      [this](request &req)
      {
         auto ctx = factory_.create_context(req);
         return backend_.list_users(*ctx);
      });

   on(verb::delete_, "/users/",
      [this](request &req, ids_request data)
      {
         std::vector<std::string> ids;
         boost::split(ids, data.ids, boost::is_any_of(","));

         for (auto &id : ids)
         {
            throw_if_modification_is_on_same_user(req, id);

            if (id.empty())
            {
               return operation_result::make_failed_result();
            }
         }

         auto ctx = factory_.create_context(req);

         for (auto &id : ids)
         {
            backend_.delete_user_by_id(*ctx, id);
         }

         return operation_result::make_success_result();
      });

   on(verb::post, "/user/set_active",
      [this](request &req, active_request data)
      {
         throw_if_modification_is_on_same_user(req, data.id);

         auto ctx = factory_.create_context(req);
         backend_.set_active(*ctx, data.id, data.is_active);

         return operation_result::make_success_result(data.id);
      });

   on(verb::post, "/user/change_password",
      [this](request &req, change_password_request data)
      {
         auth::run_principal_scope(req,
                                   [&, this](auth::principal_base &principal)
                                   {
                                      auto ctx = factory_.create_context(req);
                                      backend_.reset_password(*ctx, principal.id, data.password);
                                   });

         return operation_result::make_success_result();
      });

   on(verb::post, "/user/reset_password",
      [this](request &req, reset_password_request data)
      {
         auto ctx = factory_.create_context(req);
         backend_.reset_password(*ctx, data.id, data.password);

         return operation_result::make_success_result();
      });

   on(verb::get, "/user/properties/",
      [this](request &req)
      {
         std::vector<string_property> properties;
         auth::run_principal_scope(req,
                                   [&, this](auth::principal_base &principal)
                                   {
                                      auto ctx = factory_.create_context(req);

                                      properties = user_properties_backend_.get_properties(*ctx, principal.id);
                                   });

         return properties;
      });

   on(verb::get, "/user/property/",
      [this](request &req, string_property data)
      {
         auth::run_principal_scope(req,
                                   [&, this](auth::principal_base &principal)
                                   {
                                      auto ctx = factory_.create_context(req);
                                      data.value = user_properties_backend_.get_property(*ctx, principal.id, data.key);
                                   });
         return data;
      });

   on(verb::post, "/user/property/",
      [this](request &req, string_property data)
      {
         auth::run_principal_scope(req,
                                   [&, this](auth::principal_base &principal)
                                   {
                                      auto ctx = factory_.create_context(req);
                                      user_properties_backend_.set_property(*ctx, principal.id, data);
                                   });

         return operation_result::make_success_result();
      });
}