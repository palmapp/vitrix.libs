//
// Created by jhrub on 13.03.2022.
//
#include "stream_data_formatter.h"

#include "./formatters/csv_formatter.h"
#include "./formatters/json_formatter.h"

std::unique_ptr<spider::stream_data_formatter>
spider::stream_data_formatter::create_formatter(const std::string &content_type, fmt::memory_buffer &buffer,
                                                spider::optional<stream_formatter_options> options)
{
   if (content_type == "application/json")
   {
      return std::unique_ptr<spider::stream_data_formatter>{new json_formatter(buffer)};
   }
   else if (content_type == "text/csv")
   {
      return std::unique_ptr<spider::stream_data_formatter>{new csv_formatter(buffer, options)};
   }

   return {};
}
