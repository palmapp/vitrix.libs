//
// Created by jhrub on 20.07.2021.
//
#include "data_stream_body.h"
#include "stream_data_formatter.h"
#include <boost/optional.hpp>
#include <easylogging++.h>

boost::optional<std::pair<spider::data_stream_body::writer::const_buffers_type, bool>>
spider::data_stream_body::writer::get(boost::system::error_code &ec)
{
   ec = {};
   boost::optional<std::pair<const_buffers_type, bool>> result;
   buffer.reserve(8192);

   try
   {
      if (this->context.data_stream == nullptr)
      {
         throw std::logic_error{"this->context.data_stream == nullptr"};
      }

      commons::data::data_stream &stream = *this->context.data_stream;

      if (!this->begin_)
      {
         if (this->context.custom_data_formatter == nullptr)
         {
            this->context.custom_data_formatter = stream_data_formatter::create_formatter(content_type, buffer);
         }

         if (this->context.custom_data_formatter == nullptr)
         {
            throw std::logic_error{fmt::format("unable to create formatter for content type = {}", content_type)};
         }

         this->context.custom_data_formatter->format_begin(stream);

         this->begin_ = true;
      }

      const std::size_t write_rows = 100;
      const bool has_more = this->context.custom_data_formatter->format_rows(stream, write_rows);

      fmt::memory_buffer new_buffer;
      std::swap(new_buffer, buffer);

      result = {std::pair<const_buffers_type, bool>({std::move(new_buffer)}, has_more)};
   }
   catch (const std::exception &e)
   {
      LOG(ERROR) << "streaming failed: " << e.what();
      ec = boost::system::errc::make_error_code(boost::system::errc::operation_canceled);
   }
   catch (...)
   {
      LOG(ERROR) << "unkown failed: unkown";
      ec = boost::system::errc::make_error_code(boost::system::errc::operation_canceled);
   }

   return result;
}
