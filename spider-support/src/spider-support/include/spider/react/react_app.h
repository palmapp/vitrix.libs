#pragma once

#include <functional>
#include <spider/auth/authentication_service.h>
#include <spider/scoped_app_base.h>
#include <spider/static_file_handler.h>

namespace spider::react
{

   using custom_script_generator_callback = std::function<std::string(request &)>;
   class react_app : public scoped_app_base
   {
    public:
      react_app(std::string scope_path, filesystem_path react_index_html_path,
                custom_script_generator_callback script_generator, std::vector<std::string> valid_scopes = {});

      bool is_current_scoped_path(string_view scope) override;

    protected:
      void handle_request_internal(request &request, request_handler_callback callback) override;

    private:
      std::string load_react_app_template() const;
      std::string generate_custom_script(request &request);
      void reload_template_if_changed();

      filesystem_path react_index_html_path_;
      custom_script_generator_callback script_generator_;

      std::string template_;
      std::chrono::system_clock::time_point last_change_;

      std::vector<std::string> valid_scopes_;
   };

} // namespace spider::react
