#pragma once
#include <commons/tx_context.h>
#include <memory>

namespace spider
{
   struct request;
}

namespace spider
{
   class tx_context_factory
   {
    public:
      virtual ~tx_context_factory() = default;
      virtual std::shared_ptr<commons::tx_context> create_context(request &req) = 0;
      virtual std::shared_ptr<commons::tx_context> create_context() = 0;
   };
} // namespace spider