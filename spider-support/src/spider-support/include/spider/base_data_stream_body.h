//
// Created by jhrub on 20.07.2021.
//

#pragma once
#include "stream_data_formatter.h"
#include <commons/data/data_stream.h>
#include <fmt/format.h>
#include <memory>
#include <serialization/json/buffer.h>
#include <spider/spider.h>

namespace spider
{
   class base_data_stream_body
   {
    public:
      struct value_type
      {
         std::unique_ptr<commons::data::data_stream> data_stream;
         std::unique_ptr<stream_data_formatter> custom_data_formatter;
      };

      struct base_writer
      {
       public:
         struct buffer
         {
            std::shared_ptr<fmt::memory_buffer> value;
            boost::asio::const_buffer ptr;

            inline buffer(fmt::memory_buffer &&_value)
                : value(std::make_shared<fmt::memory_buffer>(std::move(_value))), ptr(value->data(), value->size())
            {
            }

            using value_type = boost::asio::const_buffer;
            using const_iterator = value_type const *;

            inline const_iterator begin() const noexcept
            {
               return &ptr;
            }

            /// Get a random-access iterator for one past the last element.
            const_iterator end() const noexcept
            {
               return begin() + 1;
            }
         };
         /// The type of buffer returned by `get`.
         using const_buffers_type = buffer;
         static_assert(boost::asio::is_const_buffer_sequence<const_buffers_type>::value, "buffer is not const buffer");

         /** Construct the writer.

             @param msg The message whose body is to be serialized.
         */
         template <bool isRequest, class Fields>
         base_writer(beast::http::header<isRequest, Fields> &headers, value_type &body)
             : content_type(headers[beast::http::field::content_type]), context(body)
         {
         }

         inline void init(boost::system::error_code &ec)
         {
            // The specification requires this to indicate "no error"
            ec = {};
         }

         virtual boost::optional<std::pair<const_buffers_type, bool>> get(boost::system::error_code &ec) = 0;

       protected:
         string content_type;
         value_type &context;
         fmt::memory_buffer buffer;
         bool begin_ = false;
      };
   };
} // namespace spider