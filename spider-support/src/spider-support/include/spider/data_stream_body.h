//
// Created by jhrub on 20.07.2021.
//

#pragma once
#include "base_data_stream_body.h"

namespace spider
{
   struct data_stream_body : public base_data_stream_body
   {
      struct writer : public base_writer
      {
         template <bool isRequest, class Fields>
         writer(beast::http::header<isRequest, Fields> &fields, value_type &body)
             : base_writer(fields, body), json_state(buffer)
         {
         }

         boost::optional<std::pair<const_buffers_type, bool>> get(boost::system::error_code &ec) final;

       private:
         spider::serialization::json::json_buffer json_state;
      };
   };
} // namespace spider