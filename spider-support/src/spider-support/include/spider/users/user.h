#pragma once
#include <commons/data/types.h>
#include <cstdint>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <visit_struct/visit_struct.hpp>

namespace spider::users
{

   using user_id = std::string;

   struct user_info
   {
      user_id id{};

      std::string login{};
      std::string first_name{};
      std::string last_name{};
      std::string email_address{};
      std::string custom_props{};

      bool is_active = false;
      std::int64_t record_version = 0;

      std::optional<std::unordered_map<std::string, std::string>> extended_properties = {};
      std::optional<std::string> default_login_path{};

      std::int64_t last_pwd_change_ts = 0;
   };

   struct roles
   {
      std::int64_t assigned_roles = 0;

      template <class RoleEnumT>
      static roles make_roles(RoleEnumT value)
      {
         roles result{};
         result.set_role(value);
         return result;
      }

      template <class RoleEnumT>
      void set_role(RoleEnumT value)
      {
         auto bit_set_role = 1 << static_cast<std::uint32_t>(value);

         assigned_roles = static_cast<std::int64_t>(static_cast<std::uint32_t>(assigned_roles) | bit_set_role);
      }

      template <class RoleEnumT>
      bool has_role(RoleEnumT role) const
      {
         auto role_int = 1 << static_cast<std::uint32_t>(role);
         return (static_cast<std::uint32_t>(assigned_roles) & role_int) != 0;
      }

      template <class MappingItT>
      std::vector<std::string> to_string(MappingItT cur, MappingItT end) const
      {
         std::vector<std::string> result;
         for (int i = 0; cur != end; ++i)
         {
            if (has_role(i))
            {
               result.push_back(*cur);
            }

            ++cur;
         }

         return result;
      }

      template <class ContainerT>
      std::string to_string(ContainerT const &mapping) const
      {
         using namespace std;
         return this->to_string(begin(mapping), end(mapping));
      }
   };

   struct role_scope
   {
      std::string scope{};
      std::int64_t scope_id = 0;
   };

   struct scoped_roles : public role_scope
   {
      roles assigned_roles{};
   };

   struct user_with_roles : public user_info
   {
      std::vector<scoped_roles> user_roles{};
   };
} // namespace spider::users

VISITABLE_STRUCT(spider::users::roles, assigned_roles);
VISITABLE_STRUCT(spider::users::role_scope, scope, scope_id);
VISITABLE_STRUCT(spider::users::scoped_roles, scope, scope_id, assigned_roles);
VISITABLE_STRUCT(spider::users::user_info, id, login, first_name, last_name, email_address, custom_props, is_active,
                 record_version, extended_properties, default_login_path, last_pwd_change_ts);

VISITABLE_STRUCT(spider::users::user_with_roles, id, login, first_name, last_name, email_address, custom_props,
                 is_active, record_version, user_roles, default_login_path, last_pwd_change_ts);
