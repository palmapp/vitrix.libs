#pragma once
#include <spider/api_app_base.h>

namespace spider
{
   class tx_context_factory;
}
namespace spider::users
{

   class user_backend;
   class user_properties_backend;
   struct id_request
   {
      std::string id;
   };

   struct ids_request
   {
      std::string ids;
   };

   struct active_request
   {
      std::string id;
      bool is_active;
   };

   struct change_password_request
   {
      std::string password;
   };

   struct reset_password_request
   {
      std::string id;
      std::string password;
   };

   class user_app : public api_app_base
   {
    public:
      user_app(std::string scope_path, tx_context_factory &factory, user_backend &backend,
               user_properties_backend &properties_backend);

    private:
      tx_context_factory &factory_;
      user_backend &backend_;
      user_properties_backend &user_properties_backend_;
   };

} // namespace spider::users

VISITABLE_STRUCT(spider::users::id_request, id);
VISITABLE_STRUCT(spider::users::ids_request, ids);
VISITABLE_STRUCT(spider::users::active_request, id, is_active);
VISITABLE_STRUCT(spider::users::change_password_request, password);
VISITABLE_STRUCT(spider::users::reset_password_request, id, password);
