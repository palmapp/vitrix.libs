#pragma once
#include "user.h"
#include <boost/optional.hpp>
#include <commons/data/data_query.h>
#include <commons/tx_context.h>

namespace spider::users
{
   class user_backend
   {
    public:
      virtual ~user_backend() = default;

      virtual user_id create(commons::tx_context &ctx, user_info const &info) = 0;
      virtual void update(commons::tx_context &ctx, user_info const &info) = 0;
      virtual user_with_roles get_user_by_id(commons::tx_context &ctx, user_id const &id) = 0;

      virtual void set_active(commons::tx_context &ctx, user_id const &id, bool active) = 0;

      virtual void delete_user_by_id(commons::tx_context &ctx, user_id const &id) = 0;
      virtual bool change_password(commons::tx_context &ctx, user_id const &id, std::string const &old_pwd,
                                   std::string const &new_pwd) = 0;
      virtual void reset_password(commons::tx_context &ctx, user_id const &id, std::string const &new_pwd) = 0;

      virtual user_id verify_credentials(commons::tx_context &ctx, std::string const &username,
                                         std::string const &password) = 0;

      virtual std::vector<user_info> list_users(commons::tx_context &ctx,
                                                commons::data::data_query const *query = nullptr) = 0;

      virtual void set_roles(commons::tx_context &ctx, user_id const &id, scoped_roles const &roles) = 0;

      virtual void clear_roles(commons::tx_context &ctx, user_id const &id) = 0;
      virtual void clear_roles(commons::tx_context &ctx, user_id const &id, role_scope const &scope) = 0;

      virtual roles get_roles(commons::tx_context &ctx, user_id const &id, role_scope const &scope) = 0;
      virtual std::vector<scoped_roles> get_all_roles(commons::tx_context &ctx, user_id const &id) = 0;
   };
} // namespace spider::users