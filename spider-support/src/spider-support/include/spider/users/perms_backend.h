#pragma once
#include "user.h"
#include <vector>

namespace spider::users
{
   class perms_backend
   {
    public:
      virtual ~perms_backend() = default;

      virtual bool has_perm(std::string const &scope, roles role, std::string const &perm) const = 0;
      virtual std::vector<std::string> get_perms(std::string const &scope, roles role) const = 0;
      virtual std::vector<std::string> get_declared_scopes() const = 0;
   };
} // namespace spider::users