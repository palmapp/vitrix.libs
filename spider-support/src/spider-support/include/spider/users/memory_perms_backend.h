#pragma once
#include "perms_backend.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace spider::users
{
   using role_perms = std::vector<std::string>;
   using role_configuration = std::unordered_map<std::string, std::vector<role_perms>>;

   class memory_perms_backend : public perms_backend
   {
    public:
      memory_perms_backend(role_configuration cfg);

      bool has_perm(std::string const &scope, roles role, std::string const &perm) const override;
      std::vector<std::string> get_perms(std::string const &scope, roles role) const override;
      std::vector<std::string> get_declared_scopes() const override;

    private:
      const role_configuration cfg_;
   };
} // namespace spider::users