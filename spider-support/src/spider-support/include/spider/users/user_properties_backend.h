#pragma once
#include "user.h"
#include <commons/tx_context.h>
#include <utility>
#include <vector>

namespace spider::users
{
   struct string_property
   {
      std::string key;
      std::string value;
   };

   class user_properties_backend
   {
    public:
      virtual ~user_properties_backend() = default;

      virtual void set_property(commons::tx_context &ctx, user_id const &id, string_property const &property) = 0;
      virtual std::string get_property(commons::tx_context &ctx, user_id const &id, std::string const &key) = 0;
      virtual std::vector<string_property> get_properties(commons::tx_context &ctx, user_id const &id) = 0;
   };
} // namespace spider::users
VISITABLE_STRUCT(spider::users::string_property, key, value);