#pragma once

#include "user_backend.h"
#include "user_properties_backend.h"
#include <boost/signals2.hpp>

namespace xdb
{
   class connection;
   struct db_schema;
} // namespace xdb

namespace spider::users
{
   class xdb_tx_context;
   class perms_backend;
   struct dialect;
   class xdb_user_backend : public user_backend, public user_properties_backend
   {
    public:
      xdb_user_backend(perms_backend &perms);

      user_id create(commons::tx_context &ctx, user_info const &info) override;
      void update(commons::tx_context &ctx, user_info const &info) override;
      void delete_user_by_id(commons::tx_context &ctx, user_id const &id) override;
      void set_active(commons::tx_context &ctx, user_id const &id, bool active) override;

      user_with_roles get_user_by_id(commons::tx_context &ctx, user_id const &id) override;

      bool change_password(commons::tx_context &ctx, user_id const &id, const std::string &old_pwd,
                           const std::string &new_pwd) override;
      void reset_password(commons::tx_context &ctx, user_id const &id, const std::string &new_pwd) override;

      std::vector<user_info> list_users(commons::tx_context &ctx,
                                        commons::data::data_query const *query = nullptr) override;

      user_id verify_credentials(commons::tx_context &ctx, std::string const &username,
                                 std::string const &password) override;

      void set_roles(commons::tx_context &ctx, user_id const &id, scoped_roles const &roles) override;
      void clear_roles(commons::tx_context &ctx, user_id const &id) override;
      void clear_roles(commons::tx_context &ctx, user_id const &id, role_scope const &scope) override;

      roles get_roles(commons::tx_context &ctx, const user_id &id, role_scope const &scope) override;
      std::vector<scoped_roles> get_all_roles(commons::tx_context &ctx, const user_id &id) override;

      // user_properties_backend overrides
      void set_property(commons::tx_context &ctx, user_id const &id, string_property const &property) override;
      std::string get_property(commons::tx_context &ctx, user_id const &id, std::string const &key) override;
      std::vector<string_property> get_properties(commons::tx_context &ctx, user_id const &id) override;

      boost::signals2::signal<void(commons::tx_context &, user_id const &, string_property const &)>
          on_property_changed;

      virtual void init_sql_dialect(std::string const &dialect);

      virtual void init_database(xdb::db_schema &schema);

    private:
      perms_backend &perms_;
      const dialect *dialect_ = nullptr;
   };

} // namespace spider::users