#pragma once
#include "../tx_context_factory.h"
#include "user_backend.h"
#include <spider/auth/authentication_service.h>

namespace spider::users
{
   class authentication_service : public spider::auth::authentication_service
   {
    private:
      tx_context_factory &tx_context_factory_;
      user_backend &backend_;

    public:
      authentication_service(tx_context_factory &tx_ctx_factory, user_backend &backend);

      std::string get_cas_service_url() override;

      auth::authentication_result authenticate(const std::string &username, const std::string &password) override;
      auth::authentication_result verify_token(const std::string &token) override;
   };
} // namespace spider::users