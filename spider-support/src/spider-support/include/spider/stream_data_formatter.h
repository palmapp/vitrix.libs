//
// Created by jhrub on 13.03.2022.
//

#pragma once
#include <commons/data/data_stream.h>
#include <fmt/format.h>
#include <memory>
#include <spider/std_types.h>

namespace spider
{
   struct stream_formatter_options
   {
      char csv_record_delimiter = ',';
      char csv_decimal_point = '.';
   };

   class stream_data_formatter
   {
    public:
      static std::unique_ptr<stream_data_formatter>
      create_formatter(const std::string &content_type, fmt::memory_buffer &buffer,
                       spider::optional<stream_formatter_options> options = {});

      virtual ~stream_data_formatter() = default;

      virtual void format_begin(commons::data::data_stream &rows) = 0;
      virtual bool format_rows(commons::data::data_stream &rows, size_t count) = 0;
   };

} // namespace spider