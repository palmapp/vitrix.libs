#include <catch.hpp>
#include <spider/users.h>

using namespace spider::users;
using namespace std::string_literals;

namespace
{
   enum class api_roles : std::uint32_t
   {
      premium,
      lite
   };

   enum class ui_roles : std::uint32_t
   {
      admin,
      customer
   };
} // namespace

TEST_CASE("test perms memory", "[memory]")
{
   role_perms admin{"create"s, "update"s, "delete"s, "view"s};
   role_perms customer{"view"};
   role_configuration cfg{
       {"ui"s, {admin, customer}},
       {"api"s,
        {{"view"s, "analitcs"s}, /* premium */
         {"view"s}}}             /* lite */
   };

   memory_perms_backend backend{std::move(cfg)};

   SECTION("test declared scopes")
   {
      auto scopes = backend.get_declared_scopes();
      REQUIRE(scopes.size() == 2);

      auto ui = std::find(scopes.begin(), scopes.end(), "ui"s) != scopes.end();
      auto api = std::find(scopes.begin(), scopes.end(), "api"s) != scopes.end();

      REQUIRE(ui == true);
      REQUIRE(api == true);
   }

   SECTION("test get role permissions")
   {
      roles roles{};
      roles.set_role(api_roles::lite);

      auto perms = backend.get_perms("api", roles);
      REQUIRE(perms.size() == 1);
      REQUIRE(perms[0] == "view");

      auto has_perm = backend.has_perm("api", roles, "view");
      REQUIRE(has_perm == true);
      has_perm = backend.has_perm("api", roles, "analitcs");
      REQUIRE(has_perm == false);
   }
}