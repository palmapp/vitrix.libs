#include <catch.hpp>
#include <numeric>
#include <spider/users.h>
#include <xdb/sqlite/sqlite_driver.h>
#include <xdb/xdb.h>

using namespace spider::users;
using namespace std::string_literals;

class testing_xdb_tx_context : public xdb::db_context
{
 public:
   testing_xdb_tx_context(xdb::connection &conn) : connection_(conn) {}

   xdb::connection &get_connection() override
   {
      return connection_;
   }

 private:
   xdb::connection &connection_;
};

class sqlite_driver_registration
{
 public:
   sqlite_driver_registration()
   {
      xdb::connection::register_driver(std::make_unique<xdb::backend::sqlite::driver>());
   }
};

static sqlite_driver_registration driver_reg = sqlite_driver_registration();

namespace
{
   enum class api_roles : std::uint32_t
   {
      premium,
      lite
   };

   enum class ui_roles : std::uint32_t
   {
      admin,
      customer
   };
} // namespace
memory_perms_backend make_perms_backend()
{
   role_perms admin{"create"s, "update"s, "delete"s, "view"s};
   role_perms customer{"view"};
   role_configuration cfg{
       {"ui"s, {admin, customer}},
       {"api"s,
        {{"view"s, "analitcs"s}, /* premium */
         {"view"s}}}             /* lite */
   };

   memory_perms_backend backend{std::move(cfg)};

   return backend;
}

xdb::connection connect_to_db()
{
   // auto connection =
   return xdb::connection::open("sqlite::memory:");
}
using namespace std::string_literals;

TEST_CASE("test xdb_user_backend sqlite", "[user_backend]")
{
   auto connection = connect_to_db();
   REQUIRE(connection.sql_dialect() == "sqlite"s);

   auto ctx = testing_xdb_tx_context(connection);

   auto perms = make_perms_backend();
   auto backend = xdb_user_backend(perms);
   xdb::db_schema schema{};
   backend.init_database(schema);
   schema.execute(connection);

   SECTION("test init_database")
   {
      auto table_count = connection
                             .create_query(
                                 "SELECT count(*) FROM sqlite_master WHERE type='table' AND name IN ('users', "
                                 "'ui_roles', 'api_roles', 'db_version_users') ")
                             .execute_for_int64()
                             .value_or(0);
      REQUIRE(table_count == 4);
   }

   SECTION("test user create/update")
   {
      auto id = backend.create(
          ctx, {{}, "admin", "first_name", "last_name", "email_address", "", true, 0LL, {}, {"default_login_path"s}});

      auto user = backend.get_user_by_id(ctx, id);

      auto assert_user = [](user_with_roles const &user)
      {
         REQUIRE(user.login == "admin");
         REQUIRE(user.first_name == "first_name");
         REQUIRE(user.last_name == "last_name");
         REQUIRE(user.email_address == "email_address");
         REQUIRE(user.is_active == true);
      };

      assert_user(user);
      REQUIRE(user.default_login_path.has_value() == true);
      REQUIRE(user.default_login_path.value() == "default_login_path");

      user.default_login_path = "updated path"s;
      backend.update(ctx, user);

      auto updated_user = backend.get_user_by_id(ctx, id);
      assert_user(updated_user);

      REQUIRE(updated_user.default_login_path.has_value());
      REQUIRE(updated_user.default_login_path.value() == "updated path");

      updated_user.default_login_path.reset();
      backend.update(ctx, updated_user);

      auto reseted_user_login_path = backend.get_user_by_id(ctx, id);
      assert_user(reseted_user_login_path);

      REQUIRE(reseted_user_login_path.default_login_path.has_value() == false);
   }

   SECTION("test user manipulation")
   {
      auto id = backend.create(ctx, {{}, "admin", "first_name", "last_name", "email_address", "", true});
      auto second_id = backend.create(ctx, {{}, "second_admin", "first_name", "last_name", "email_address", "", true});
      backend.reset_password(ctx, second_id, "second_pwd");

      auto users = backend.list_users(ctx);
      REQUIRE(users.size() == 2);
      backend.delete_user_by_id(ctx, second_id);

      users = backend.list_users(ctx);
      REQUIRE(users.size() == 1);

      auto user = users.at(0);
      REQUIRE(user.login == "admin");
      REQUIRE(user.first_name == "first_name");
      REQUIRE(user.last_name == "last_name");
      REQUIRE(user.email_address == "email_address");
      REQUIRE(user.is_active == true);

      // authentication
      auto verified_id = backend.verify_credentials(ctx, "admin", "");
      REQUIRE(verified_id.empty() == true);

      backend.reset_password(ctx, id, "securepassword");
      verified_id = backend.verify_credentials(ctx, "admin", "securepassword");
      REQUIRE(verified_id == id);

      bool changed = backend.change_password(ctx, id, "badpwd", "newpwd");
      REQUIRE(changed == false);

      changed = backend.change_password(ctx, id, "securepassword", "newpwd");
      REQUIRE(changed == true);

      verified_id = backend.verify_credentials(ctx, "admin", "securepassword");
      REQUIRE(verified_id.empty() == true);

      verified_id = backend.verify_credentials(ctx, "admin", "newpwd");
      REQUIRE(verified_id == id);

      user.login = "c:admin";
      user.first_name = "c:fn";
      user.last_name = "c:ln";
      user.email_address = "c:ea";
      user.is_active = false;

      backend.update(ctx, user);

      users = backend.list_users(ctx);
      REQUIRE(users.size() == 1);

      user = users.at(0);
      REQUIRE(user.login == "c:admin");
      REQUIRE(user.first_name == "c:fn");
      REQUIRE(user.last_name == "c:ln");
      REQUIRE(user.email_address == "c:ea");
      REQUIRE(user.is_active == false);

      // we should not be able authenticate deactivated user
      verified_id = backend.verify_credentials(ctx, "admin", "newpwd");
      REQUIRE(verified_id.empty() == true);

      // we should not be able authenticate deleted user
      verified_id = backend.verify_credentials(ctx, "second_admin", "second_pwd");
      REQUIRE(verified_id.empty() == true);
   }

   SECTION("test roles")
   {
      auto id = backend.create(ctx, {{}, "admin", "first_name", "last_name", "email_address", "", true});

      backend.set_roles(ctx, id, {"ui", 0, roles::make_roles(ui_roles::admin)});
      backend.set_roles(ctx, id, {"api", 2, roles::make_roles(api_roles::premium)});
      backend.set_roles(ctx, id, {"api", 1, roles::make_roles(api_roles::lite)});

      auto roles = backend.get_all_roles(ctx, id);
      REQUIRE(roles.size() == 3);

      auto sum_ui = [](int count, scoped_roles const &role) { return role.scope == "ui" ? count + 1 : count; };

      auto sum_api = [](int count, scoped_roles const &role)
      {
         return (role.scope == "api" &&
                 (role.assigned_roles.has_role(api_roles::premium) || role.assigned_roles.has_role(api_roles::lite)))
                    ? count + 1
                    : count;
      };

      auto sum_api_lite = [](int count, scoped_roles const &role)
      { return role.scope == "api" && role.assigned_roles.has_role(api_roles::lite) ? count + 1 : count; };

      auto ui_roles_count = std::accumulate(roles.begin(), roles.end(), 0, sum_ui);

      REQUIRE(ui_roles_count == 1);

      auto api_roles_count = std::accumulate(roles.begin(), roles.end(), 0, sum_api);
      REQUIRE(api_roles_count == 2);

      // clear id "ui" roles
      backend.clear_roles(ctx, id, {"ui"});
      roles = backend.get_all_roles(ctx, id);
      REQUIRE(roles.size() == 2);

      ui_roles_count = std::accumulate(roles.begin(), roles.end(), 0, sum_ui);
      REQUIRE(ui_roles_count == 0);

      api_roles_count = std::accumulate(roles.begin(), roles.end(), 0, sum_api);
      REQUIRE(api_roles_count == 2);

      // clear id "api => 2" roles
      backend.clear_roles(ctx, id, {"api", 2});
      roles = backend.get_all_roles(ctx, id);

      ui_roles_count = std::accumulate(roles.begin(), roles.end(), 0, sum_ui);
      REQUIRE(ui_roles_count == 0);

      api_roles_count = std::accumulate(roles.begin(), roles.end(), 0, sum_api_lite);
      REQUIRE(api_roles_count == 1);

      auto api_role = backend.get_roles(ctx, id, {"api", 1});
      REQUIRE(api_role.has_role(api_roles::lite) == true);
      // clear all the rest
      backend.clear_roles(ctx, id);
      roles = backend.get_all_roles(ctx, id);
      REQUIRE(roles.empty() == true);

      api_role = backend.get_roles(ctx, id, {"api", 1});
      REQUIRE(api_role.has_role(api_roles::lite) == false);
   }

   SECTION("test properties")
   {
      auto first_user_id = backend.create(ctx, {{}, "first", "first_name", "last_name", "email_address", "", true});
      auto second_user_id = backend.create(ctx, {{}, "second", "first_name", "last_name", "email_address", "", true});

      std::vector<std::pair<user_id, string_property>> changed_properties;
      backend.on_property_changed.connect(
          [&](commons::tx_context &ctx, user_id const &id, string_property const &property)
          { changed_properties.push_back(std::make_pair(id, property)); });

      backend.set_property(ctx, first_user_id, {"property1", "property1:first_value"});
      backend.set_property(ctx, second_user_id, {"property1", "property1:second_value"});
      backend.set_property(ctx, first_user_id, {"property2", "property2:first_value"});
      backend.set_property(ctx, second_user_id, {"property2", "property2:second_value"});

      REQUIRE(changed_properties.size() == 4);

      auto first_user_properties = backend.get_properties(ctx, first_user_id);
      REQUIRE(first_user_properties.size() == 2);

      auto second_user_properties = backend.get_properties(ctx, second_user_id);
      REQUIRE(second_user_properties.size() == 2);

      REQUIRE(backend.get_property(ctx, first_user_id, "property1") == "property1:first_value");
      REQUIRE(backend.get_property(ctx, first_user_id, "property2") == "property2:first_value");

      REQUIRE(backend.get_property(ctx, second_user_id, "property1") == "property1:second_value");
      REQUIRE(backend.get_property(ctx, second_user_id, "property2") == "property2:second_value");
   }
}