#pragma once
#include <cstdint>

namespace geo
{
   using floatingpoint_t = float;
   using longitude_t = floatingpoint_t;
   using latitude_t = floatingpoint_t;
   using cartesian_coordinate_t = floatingpoint_t;

   struct position
   {
      latitude_t lat;
      longitude_t lon;
   };

   struct cart_pos_3d
   {
      cartesian_coordinate_t x, y, z;

      inline cart_pos_3d(cartesian_coordinate_t _x, cartesian_coordinate_t _y, cartesian_coordinate_t _z) noexcept
          : x(_x), y(_y), z(_z)
      {
      }

      // garbage constructor
      inline cart_pos_3d() {}
   };

} // namespace geo