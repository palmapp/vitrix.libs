//
// Created by jhrub on 30.03.2023.
//

#pragma once

#include <fmt/format.h>
#include <string_view>
#include "tiles.h"
namespace geo
{
   struct line_style
   {
      int width;
      std::string_view color;
   };

   class kml_buffer{
    public:
      inline kml_buffer(fmt::memory_buffer& buffer): buffer_(buffer)
      {}

      void document(std::string_view name, auto fun)
      {
        fmt::format_to(std::back_inserter(buffer_), "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                        "<kml xmlns=\"http://earth.google.com/kml/2.0\">\n"
                        "<Document>\n"
                        "<name>{}</name>", name);
         fun();

        fmt::format_to(std::back_inserter(buffer_), "</Document></kml>");
      }

      void folder(std::string_view name, auto fun)
      {
        fmt::format_to(std::back_inserter(buffer_), "<Folder>\n"
                        "<name>{}</name>", name);
         fun();

        fmt::format_to(std::back_inserter(buffer_), "</Folder>");
      }

      void placemark(std::string_view name, line_style style, auto fun)
      {
        fmt::format_to(std::back_inserter(buffer_), "<Placemark><name>{}</name>", name);
        fmt::format_to(std::back_inserter(buffer_), "<LineStyle><color>{}</color><width>{}</width></LineStyle>", style.color, style.width);
         fun();
        fmt::format_to(std::back_inserter(buffer_), "</Placemark>", name);
      }

      void line_string(auto fun)
      {
        fmt::format_to(std::back_inserter(buffer_), "<LineString><tessellate>1</tessellate>");
         fun();
        fmt::format_to(std::back_inserter(buffer_), "</LineString>");
      }

      void coordinates(auto fun)
      {
        fmt::format_to(std::back_inserter(buffer_), "<coordinates>");
         fun();
        fmt::format_to(std::back_inserter(buffer_), "</coordinates>");
      }

      inline void point(wgs84_coordinates const& coord)
      {
        fmt::format_to(std::back_inserter(buffer_), "{},{},0\n", coord.lon, coord.lat);
      }

      inline void rect(wgs84_coordinates const& nw, wgs84_coordinates const& se)
      {
         point(nw);
         point({.lat = nw.lat, .lon = se.lon});
         point(se);
         point({.lat = se.lat, .lon = nw.lon});
         point(nw);
      }

    private:
      fmt::memory_buffer& buffer_;
   };
}