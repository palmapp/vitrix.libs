#pragma once

#include "types.h"
#include <algorithm>
#include <cmath>
#include <iterator>
#include <vector>
#ifndef __clang__
#include <execution>
#endif
namespace geo
{
   constexpr floatingpoint_t deg_to_rad(floatingpoint_t deg) noexcept
   {
      return deg / static_cast<floatingpoint_t>(57.29577951);
   }

   // WGS84 earth radius in meters
   constexpr floatingpoint_t equatorial_radius_in_m = 6378137.0;
   constexpr floatingpoint_t flattening = static_cast<floatingpoint_t>(1.0 / 298.257223563);

   inline floatingpoint_t distance_from_start(cart_pos_3d const &start, cart_pos_3d const &dest) noexcept
   {
      return sqrt(pow(dest.x - start.x, 2) + pow(dest.y - start.y, 2) + pow(dest.z - start.z, 2));
   }

   inline floatingpoint_t distance_in_meters(position const &a, position const &b) noexcept
   {
      using namespace std;
      const floatingpoint_t lat1 = deg_to_rad(a.lat);
      const floatingpoint_t lon1 = deg_to_rad(a.lon);

      const floatingpoint_t lat2 = deg_to_rad(b.lat);
      const floatingpoint_t lon2 = deg_to_rad(b.lon);

      return (acos(cos(lat1) * cos(lon1) * cos(lat2) * cos(lon2) + cos(lat1) * sin(lon1) * cos(lat2) * sin(lon2) +
                   sin(lat1) * sin(lat2)) *
              equatorial_radius_in_m);
   }

   inline floatingpoint_t square_segment_distance(cart_pos_3d const &p0, cart_pos_3d const &p1,
                                                  cart_pos_3d const &p2) noexcept
   {
      const auto x2 = p2.x;
      const auto y2 = p2.y;
      const auto z2 = p2.z;
      const auto x0 = p0.x;
      const auto y0 = p0.y;
      const auto z0 = p0.z;

      auto x1 = p1.x;
      auto y1 = p1.y;
      auto z1 = p1.z;

      auto dx = x2 - x1;
      auto dy = y2 - y1;
      auto dz = z2 - z1;

      if (dx != 0.0 || dy != 0.0 || dz != 0.0)
      {
         auto numerator = ((x0 - x1) * dx + (y0 - y1) * dy + (z0 - z1) * dz);
         auto denom = (dx * dx + dy * dy + dz * dz);
         auto t = numerator / denom;

         if (t > 1.0)
         {
            x1 = x2;
            y1 = y2;
            z1 = z2;
         }
         else if (t > 0.0)
         {
            x1 += dx * t;
            y1 += dy * t;
            z1 += dz * t;
         }
      }

      dx = x0 - x1;
      dy = y0 - y1;
      dz = z0 - z1;

      return dx * dx + dy * dy + dz * dz;
   }

   template <class TrackContainerT, class PositionCallbackT>
   std::vector<cart_pos_3d> convert_to_cartesian(TrackContainerT const &cnt, PositionCallbackT get_position_callback)
   {
      using namespace std;
      const floatingpoint_t e2 = 2.0 * flattening - pow(flattening, 2); // mocnina excentricity

      vector<cart_pos_3d> result{};
      result.resize(size(cnt));

      transform(
#ifndef __clang__
          execution::par_unseq,
#endif
          begin(cnt), end(cnt), begin(result), [&](auto const &element) noexcept {
             const position pos = get_position_callback(element);

             const floatingpoint_t lat_radians = deg_to_rad(pos.lat);
             const floatingpoint_t cos_lat_rad = cos(lat_radians);
             const floatingpoint_t sin_lat_rad = sin(lat_radians);

             const floatingpoint_t lon_radians = deg_to_rad(pos.lon);

             const floatingpoint_t n =
                 equatorial_radius_in_m / sqrt(1. - (e2 * pow(sin_lat_rad, 2))); // příčný poloměr křivosti

             const floatingpoint_t n_cos_lat_rad = n * cos_lat_rad;
             return cart_pos_3d(n_cos_lat_rad * cos(lon_radians), n_cos_lat_rad * sin(lon_radians),
                                n * (1. - e2) * sin_lat_rad);
          });

      return result;
   }

   template <class TrackContainerT, class TimeCallbackT, class PositionCallbackT>
   std::vector<TrackContainerT>
   split_track_by_time_and_distance(TrackContainerT const &track, std::int64_t time_treshold, TimeCallbackT get_time,
                                    floatingpoint_t distance_treshold_in_meters, PositionCallbackT get_position)
   {
      std::vector<TrackContainerT> result;
      result.emplace_back();

      std::int64_t last_rec_time = 0;
      position last_position{0, 0};
      for (auto &rec : track)
      {
         const std::int64_t current_rec_time = get_time(rec);

         if (last_rec_time != 0 && current_rec_time - last_rec_time > time_treshold)
         {
            result.emplace_back();
         }

         const position pos = get_position(rec);
         if ((last_position.lat != 0 && last_position.lon != 0) &&
             distance_in_meters(last_position, pos) > distance_treshold_in_meters)
         {
            if (!result.back().empty()) result.emplace_back();
         }

         result.back().push_back(rec);
         last_rec_time = current_rec_time;
         last_position = pos;
      }

      return result;
   }

   inline std::vector<bool> get_douglas_peucker_points(std::vector<cart_pos_3d> const &points,
                                                       floatingpoint_t sq_tolerance)
   {
      using namespace std;

      vector<bool> markers;
      markers.resize(points.size());

      vector<pair<size_t, size_t>> stack_segments;
      stack_segments.reserve(1000);

      stack_segments.emplace_back(0, points.size() - 1);

      while (!stack_segments.empty())
      {
         auto [first, last] = stack_segments.back();
         stack_segments.pop_back();

         size_t index = -1;
         floatingpoint_t max_sq_dist = 0.;

         for (auto i = first + 1; i < last; ++i)
         {
            const auto sq_dist = square_segment_distance(points[i], points[first], points[last]);
            if (sq_dist > max_sq_dist)
            {
               index = i;
               max_sq_dist = sq_dist;
            }
         }

         if (max_sq_dist > sq_tolerance)
         {
            markers[index] = true;

            stack_segments.emplace_back(first, index);
            stack_segments.emplace_back(index, last);
         }
      }

      return markers;
   }

   template <class TrackContainerT, class PositionCallbackT>
   TrackContainerT compress_by_douglas_peucker(TrackContainerT const &track, PositionCallbackT callback,
                                               floatingpoint_t sq_tolerance)
   {
      using namespace std;

      const std::vector<bool> result_points =
          get_douglas_peucker_points(convert_to_cartesian(track, callback), sq_tolerance);

      const size_t result_size = accumulate(begin(result_points), end(result_points), static_cast<size_t>(0),
                                            [](size_t prev, bool bit) -> size_t { return prev + (bit ? 1 : 0); });

      TrackContainerT result;
      result.reserve(result_size);

      for (size_t idx = 0, size = track.size(); idx != size; ++idx)
      {
         if (result_points[idx])
         {
            result.push_back(track[idx]);
         }
      }

      return result;
   }

} // namespace geo
