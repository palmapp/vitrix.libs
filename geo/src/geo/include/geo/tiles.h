//
// Created by jhrub on 27.03.2023.
//

#pragma once
#include <cmath>
#include <cstdint>
#include <iterator>
#include <numbers> // std::numbers
#include <optional>

namespace geo
{

   struct integer_point_2d
   {
      long x;
      long y;
   };

   struct point_2d
   {
      double x;
      double y;

      [[nodiscard]] inline auto to_integer_point() -> integer_point_2d
      {
         return integer_point_2d{.x = static_cast<int>(x), .y = static_cast<int>(y)};
      }
   };

   [[nodiscard]] constexpr auto deg2rad(double deg) -> double
   {
      return (deg / 180.0) * std::numbers::pi_v<double>;
   }

   [[nodiscard]] constexpr auto rad2deg(double rad) -> double
   {
      return (rad * 180.0) / std::numbers::pi_v<double>;
   }

   [[nodiscard]] inline auto sec(double rad) -> double
   {
      return 1.0 / std::cos(rad);
   }

   struct wgs84_coordinates
   {
      double lat;
      double lon;
   };

   struct osm_tile_id
   {
      std::int32_t x;
      std::int32_t y;
      std::int32_t z;

      inline osm_tile_id()
         : x(0), y(0), z(0)
      {
      }

      inline osm_tile_id(std::int32_t x, std::int32_t y, std::int32_t z)
         : x(x), y(y), z(z)
      {
      }

      inline osm_tile_id(wgs84_coordinates const &coords, std::int32_t zoom)
      {
         double lat_rad = deg2rad(coords.lat);
         const auto n = std::pow(2, static_cast<double>(zoom));

         x = static_cast<std::int32_t>((coords.lon + 180.0) / 360.0 * n);
         y = static_cast<std::int32_t>((1.0 - std::asinh(std::tan(lat_rad)) / std::numbers::pi_v<double>) / 2.0 * n);
         z = zoom;
      }

      [[nodiscard]] inline auto get_tile_north_west() const noexcept -> wgs84_coordinates
      {
         auto n = std::pow(2, static_cast<double>(z));

         return wgs84_coordinates{
            .lat = rad2deg(std::atan(std::sinh(std::numbers::pi_v<double> * (1 - 2 * y / n)))),
            .lon = x / n * 360.0 - 180.0,
         };
      }

      [[nodiscard]] inline auto unproject_point(point_2d const &p, int tile_a) const noexcept -> wgs84_coordinates
      {
         auto n = std::pow(2, static_cast<double>(z));
         auto px = static_cast<double>(x) + p.x / static_cast<double>(tile_a);
         auto py = static_cast<double>(y) + p.y / static_cast<double>(tile_a);

         return wgs84_coordinates{
            .lat = rad2deg(std::atan(std::sinh(std::numbers::pi_v<double> * (1 - 2 * py / n)))),
            .lon = px / n * 360.0 - 180.0,
         };
      }

      [[nodiscard]] inline auto get_tile_south_east() const noexcept -> wgs84_coordinates
      {
         return osm_tile_id{x + 1, y + 1, z}.get_tile_north_west();
      }

      [[nodiscard]] inline auto operator==(const osm_tile_id &other) const noexcept -> bool
      {
         return other.x == x && other.y == y && other.z == z;
      }

      [[nodiscard]] inline auto operator!=(const osm_tile_id &other) const noexcept -> bool
      {
         return !this->operator==(other);
      }

      constexpr static int osm_tile_size = 256;
      constexpr static int vector_tile_size = 4096;

      [[nodiscard]] inline auto create_tile_projector(int tile_size = osm_tile_size) const
      {
         return [tile_size, n = std::pow(2, static_cast<double>(z)), tx = tile_size * x,
               ty = tile_size * y](wgs84_coordinates const &coords) -> point_2d
         {
            double lat_rad = deg2rad(coords.lat);

            auto px = ((coords.lon + 180.0) / 360.0 * n) * tile_size;
            auto py = ((1.0 - std::asinh(std::tan(lat_rad)) / std::numbers::pi_v<double>) / 2.0 * n) * tile_size;

            return point_2d{.x = px - tx, .y = py - ty};
         };
      }

      [[nodiscard]] inline auto static project_to_mercator(wgs84_coordinates const &coords, int zoom, int tile_size)
         -> point_2d
      {
         double lat_rad = deg2rad(coords.lat);
         auto n = 1 << zoom;
         auto px = ((coords.lon + 180.0) / 360.0 * n) * tile_size;
         auto py = ((1.0 - std::asinh(std::tan(lat_rad)) / std::numbers::pi_v<double>) / 2.0 * n) * tile_size;

         return point_2d{.x = px, .y = py};
      }

      [[nodiscard]] auto get_key() const -> std::uint64_t
      {
         return static_cast<std::uint64_t>(z) << 56 | static_cast<std::uint64_t>(x) << 32 |
                static_cast<std::uint64_t>(y);
      }
   };

   struct here_tile_id
   {
      std::int32_t x;
      std::int32_t y;
      std::int32_t z;

      inline here_tile_id(std::int32_t x, std::int32_t y, std::int32_t z)
         : x(x), y(y), z(z)
      {
      }

      inline here_tile_id(wgs84_coordinates const &coords, std::int32_t zoom)
      {
         constexpr std::int64_t LON_RANGE = 360, LAT_RANGE = 180, HALF_LON_RANGE = LON_RANGE / 2,
                                HALF_LAT_RANGE = LAT_RANGE / 2;
         auto degrees_per_tile = static_cast<double>(LON_RANGE) / std::pow(2, static_cast<double>(zoom));

         x = static_cast<std::int32_t>((coords.lon + HALF_LON_RANGE) / degrees_per_tile);
         y = static_cast<std::int32_t>((coords.lat + HALF_LAT_RANGE) / degrees_per_tile);
         z = zoom;
      }

      inline here_tile_id()
         : x(0), y(0), z(0)
      {
      }

      [[nodiscard]] inline auto get_quad_key() const noexcept -> std::int32_t
      {
         std::int32_t interleaved = 0;
         for (std::int32_t i = 0; i != z; ++i)
         {
            interleaved |= ((y & (1 << i)) << (i + std::int64_t{1}));
            interleaved |= ((x & (1 << i)) << i);
         }

         return (1 << (2 * z)) | interleaved;
      }

      [[nodiscard]] auto get_tile_north_west() const noexcept -> wgs84_coordinates
      {
         constexpr std::int64_t LON_RANGE = 360, LAT_RANGE = 180, HALF_LON_RANGE = LON_RANGE / 2,
                                HALF_LAT_RANGE = LAT_RANGE / 2;
         auto degrees_per_tile = static_cast<double>(LON_RANGE) / std::pow(2, static_cast<double>(z));

         return wgs84_coordinates{
            .lat = (y * degrees_per_tile) - HALF_LAT_RANGE,
            .lon = (x * degrees_per_tile) - HALF_LON_RANGE,
         };
      }

      [[nodiscard]] auto get_tile_south_east() const noexcept -> wgs84_coordinates
      {
         constexpr std::int64_t LON_RANGE = 360;
         auto degrees_per_tile = static_cast<double>(LON_RANGE) / std::pow(2, static_cast<double>(z));

         auto nw = get_tile_north_west();

         return wgs84_coordinates{
            .lat = nw.lat - degrees_per_tile,
            .lon = nw.lon + degrees_per_tile,
         };
      }

      [[nodiscard]] inline auto operator==(const here_tile_id &other) const noexcept -> bool
      {
         return other.x == x && other.y == y && other.z == z;
      }

      [[nodiscard]] inline auto operator!=(const here_tile_id &other) const noexcept -> bool
      {
         return !this->operator==(other);
      }
   };

   struct here_tile_rectangle
   {
      here_tile_id north_west;
      here_tile_id south_east;
   };

   struct here_tile_iterator_nw_se
   {
      struct iterator
      {
         using iterator_category = std::input_iterator_tag;
         using value_type = here_tile_id;
         using reference = here_tile_id const &;
         using pointer = here_tile_id;

         inline iterator(here_tile_rectangle const &rect, here_tile_id cursor)
            : cursor_(cursor), rect_(&rect)
         {
         }

         inline iterator()
            : rect_(nullptr)
         {
         }

         auto operator++() -> iterator &
         {
            if (cursor_.x < rect_->south_east.x)
            {
               ++cursor_.x;
            }
            else
            {
               cursor_.x = rect_->north_west.x;
               --cursor_.y;
            }

            return *this;
         }

         auto operator++(int) -> iterator
         {
            auto copy = iterator{*rect_, cursor_};
            this->operator++();

            return copy;
         }

         auto operator!=(iterator const &other) -> bool
         {
            if (rect_ == nullptr)
            {
               return !other.is_end();
            }
            return !is_end();
         }

         auto operator->() -> pointer
         {
            return cursor_;
         };

         auto operator*() -> reference
         {
            return cursor_;
         };

      private:
         auto is_end() const -> bool
         {
            return cursor_.y < rect_->south_east.y;
         }

         here_tile_id cursor_;
         here_tile_rectangle const *rect_;
      };

      here_tile_iterator_nw_se(here_tile_rectangle rect)
         : rect_(rect)
      {
      }

      here_tile_rectangle rect_;

      auto begin() -> iterator
      {
         return iterator{rect_, rect_.north_west};
      }

      auto end() -> iterator
      {
         return iterator{};
      }
   };

   struct osm_tile_rectangle
   {
      osm_tile_id north_west;
      osm_tile_id south_east;
   };

   struct osm_tile_iterator_nw_se
   {
      struct iterator
      {
         using iterator_category = std::input_iterator_tag;
         using value_type = osm_tile_id;
         using reference = osm_tile_id const &;
         using pointer = osm_tile_id;

         inline iterator(osm_tile_rectangle const &rect, osm_tile_id cursor)
            : cursor_(cursor), rect_(&rect)
         {
         }

         inline iterator()
            : rect_(nullptr)
         {
         }

         auto operator++() -> iterator &
         {
            if (cursor_.x < rect_->south_east.x)
            {
               ++cursor_.x;
            }
            else
            {
               cursor_.x = rect_->north_west.x;
               ++cursor_.y;
            }

            return *this;
         }

         auto operator++(int) -> iterator
         {
            auto copy = iterator{*rect_, cursor_};
            this->operator++();

            return copy;
         }

         auto operator!=(iterator const &other) -> bool
         {
            if (rect_ == nullptr)
            {
               return !other.is_end();
            }
            return !is_end();
         }

         auto operator->() -> pointer
         {
            return cursor_;
         };

         auto operator*() -> reference
         {
            return cursor_;
         };

      private:
         auto is_end() const -> bool
         {
            return cursor_.y > rect_->south_east.y;
         }

         osm_tile_id cursor_;
         osm_tile_rectangle const *rect_;
      };

      explicit osm_tile_iterator_nw_se(osm_tile_rectangle rect)
         : rect_(rect)
      {
      }

      osm_tile_rectangle rect_;

      auto begin() const -> iterator
      {
         return iterator{rect_, rect_.north_west};
      }

      auto end() const -> iterator
      {
         return iterator{};
      }
   };

   inline auto decode_lat_lon(std::int64_t lat, std::int64_t lon) -> wgs84_coordinates
   {
      double lat_range = 180, lon_range = 360, lat_res = std::int64_t{2} << 30, lon_res = std::int64_t{2} << 31;

      return {.lat = static_cast<double>(lat) * (lat_range / lat_res),
              .lon = static_cast<double>(lon) * (lon_range / lon_res)};
   }

   struct wgs84_rectangle
   {
      wgs84_coordinates north_west;
      wgs84_coordinates south_east;

      inline auto get_osm_tile_rectangle(std::int32_t zoom_level) const noexcept -> osm_tile_rectangle
      {
         return {{north_west, zoom_level}, {south_east, zoom_level}};
      }

      inline auto get_here_tile_rectangle(std::int32_t zoom_level) const noexcept -> here_tile_rectangle
      {
         return {{north_west, zoom_level}, {south_east, zoom_level}};
      }
   };
} // namespace geo

template <>
struct std::hash<geo::osm_tile_id>
{
   std::size_t operator()(geo::osm_tile_id const &id) const noexcept
   {
      return std::hash<std::uint64_t>{}(id.get_key());
   }
};

template <>
struct std::hash<geo::here_tile_id>
{
   std::size_t operator()(geo::here_tile_id const &id) const noexcept
   {
      return std::hash<decltype(id.get_quad_key())>{}(id.get_quad_key());
   }
};