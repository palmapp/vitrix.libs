//
// Created by jhrub on 31.03.2023.
//

#pragma once

#include <vector>
#include "tiles.h"

namespace geo
{
   struct segment_point
   {
      wgs84_coordinates point;
      std::uint64_t sub_segment_index;
   };

   struct vector_segment
   {
      std::int64_t id;
      std::vector<segment_point> points;
   };
}