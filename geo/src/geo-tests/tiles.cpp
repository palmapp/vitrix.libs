//
// Created by jhrub on 27.03.2023.
//
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <geo/kml.h>
#include <geo/tiles.h>
#include <iostream>
#include <unordered_map>
using namespace geo;

TEST_CASE("tile id", "[tileids]")
{
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = 52.5249, .lon = 13.3692}, 12}.get_quad_key() == 23618402);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = -23.551205, .lon = -46.632264}, 12}.get_quad_key() == 18480753);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = -33.86663, .lon = 151.20578}, 12}.get_quad_key() == 22835176);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = 37.774888, .lon = -122.417844}, 12}.get_quad_key() == 19319031);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = 51.470716, .lon = 0.057873}, 12}.get_quad_key() == 23601282);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = 51.470716, .lon = -0.057873}, 12}.get_quad_key() == 20805079);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = -84.491114, .lon = -156.325441}, 12}.get_quad_key() == 16845561);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = 52.52507, .lon = 13.36937}, 12}.get_quad_key() == 23618402);
   REQUIRE(here_tile_id{wgs84_coordinates{.lat = 52.52507, .lon = 13.36937}, 14}.get_quad_key() == 377894440);

   auto palermo = osm_tile_id{{38.1109, 13.3491}, 14};
   REQUIRE(palermo.z == 14);
   REQUIRE(palermo.x == 8799);
   REQUIRE(palermo.y == 6313);
   fmt::print("palermo https://tile.openstreetmap.org/{}/{}/{}.png\n", palermo.z, palermo.x, palermo.y);
}

TEST_CASE("test tile corners", "[tiles]")
{
   auto tile = here_tile_id{wgs84_coordinates{.lat = 52.5249, .lon = 13.3692}, 12};

   auto nw = tile.get_tile_north_west();

   std::cout << nw.lat << "," << nw.lon << std::endl;

   auto se = tile.get_tile_south_east();

   std::cout << se.lat << "," << se.lon << std::endl;
}

TEST_CASE("test tile iterator", "[tiles]")
{
   auto rect = here_tile_rectangle{here_tile_id{wgs84_coordinates{.lat = 52.5249, .lon = 13.3692}, 14},
                                   here_tile_id{wgs84_coordinates{.lat = 52.5249, .lon = 14}, 14}};

   std::cout << rect.north_west.x << "," << rect.north_west.y << std::endl;

   std::cout << rect.south_east.x << "," << rect.south_east.y << std::endl;

   for (auto &tile : here_tile_iterator_nw_se{rect})
   {
      std::cout << tile.x << "," << tile.y << std::endl;
   }
}

TEST_CASE("test point projection", "[point]")
{
   auto palermo_coords = wgs84_coordinates{38.1109, 13.3491};
   auto tile = osm_tile_id{palermo_coords, 14};

   auto project_on_tile = tile.create_tile_projector(osm_tile_id::osm_tile_size);
   auto projected_point = project_on_tile(palermo_coords);

   fmt::print("tile x={},y={}\n", tile.x, tile.y);
   fmt::print("x={},y={}\n", projected_point.x, projected_point.y);
}

TEST_CASE("geo::*_tile_id as hash key", "[tile]")
{
   SECTION("osm_tile_id")
   {
      auto map = std::unordered_map<geo::osm_tile_id, int>{};
      map[osm_tile_id{{38.1109, 13.3491}, 14}] = 1;
      map[osm_tile_id{{38.1109, 13.3491}, 16}] = 2;
      map[osm_tile_id{{38.1109, 13.3491}, 20}] = 3;
      map[osm_tile_id{{40, 50}, 14}] = 4;

      REQUIRE(map[osm_tile_id{{38.1109, 13.3491}, 14}] == 1);
      REQUIRE(map[osm_tile_id{{38.1109, 13.3491}, 16}] == 2);
      REQUIRE(map[osm_tile_id{{38.1109, 13.3491}, 20}] == 3);
      REQUIRE(map[osm_tile_id{{40, 50}, 14}] == 4);
   }

   SECTION("here_tile_id")
   {
      auto map = std::unordered_map<geo::here_tile_id, int>{};
      map[here_tile_id{{38.1109, 13.3491}, 14}] = 1;
      map[here_tile_id{{38.1109, 13.3491}, 16}] = 2;
      map[here_tile_id{{38.1109, 13.3491}, 20}] = 3;
      map[here_tile_id{{40, 50}, 14}] = 4;

      REQUIRE(map[here_tile_id{{38.1109, 13.3491}, 14}] == 1);
      REQUIRE(map[here_tile_id{{38.1109, 13.3491}, 16}] == 2);
      REQUIRE(map[here_tile_id{{38.1109, 13.3491}, 20}] == 3);
      REQUIRE(map[here_tile_id{{40, 50}, 14}] == 4);
   }
}