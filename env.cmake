# This file is obsolete and will be removed in the future
# Do not include this file in new projects
# Use modern_env.cmake instead with vcpkg.json configuration

if (NOT DEFINED VITRIX_ENV)
    MESSAGE(STATUS "SYSTEM:         " ${CMAKE_SYSTEM_NAME})

    if (CMAKE_SYSTEM_NAME STREQUAL "Windows")

        add_definitions(/utf-8)
        add_definitions(-D_WIN32_WINNT=0x0600)
        add_definitions(/DNOMINMAX)
        add_definitions(/D_CRT_SECURE_NO_WARNINGS)

        if (DEFINED ENABLE_PROFILE)
            MESSAGE(STATUS "Profiling is enabled!")
            link_libraries("micro-profiler_x64")
        endif ()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /utf-8 /bigobj /wd4996 /wd4828 /wd4834 /Zc:__cplusplus")

        add_definitions(-DELPP_NO_DEFAULT_LOG_FILE)

        find_package(zeromq)
        find_package(cppzmq CONFIG REQUIRED)
        find_package(unofficial-libmariadb CONFIG REQUIRED)

        set(ZeroMQ_LIBRARIES libzmq cppzmq)

    elseif (CMAKE_SYSTEM_NAME STREQUAL "Android")
        if (NOT TARGET crypto)

            MESSAGE(STATUS "Configuring for android, runtime location: $ENV{ANDROID_RUNTIME}")

            add_library(date_time STATIC IMPORTED)
            add_library(system STATIC IMPORTED)
            add_library(signals STATIC IMPORTED)
            add_library(timer STATIC IMPORTED)
            add_library(filesystem STATIC IMPORTED)
            add_library(thread STATIC IMPORTED)
            add_library(atomic STATIC IMPORTED spider-support/src/spider-support/include/spider/users.h)
            add_library(regex STATIC IMPORTED)
            add_library(random STATIC IMPORTED)
            add_library(iostreams STATIC IMPORTED)

            set(LIB_ROOT $ENV{ANDROID_RUNTIME}/${ANDROID_ABI})
            set(BOOST_ROOT "${LIB_ROOT}")
            set(BOOST_LIBRARYDIR "${BOOST_ROOT}/lib")
            set(Boost_INCLUDE_DIR "${BOOST_ROOT}/include/boost-1_70")

            include_directories(${Boost_INCLUDE_DIR})
            include_directories(${LIB_ROOT}/include)

            set_target_properties(date_time PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_date_time.a")
            set_target_properties(system PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_system.a")
            set_target_properties(timer PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_timer.a")
            set_target_properties(filesystem PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_filesystem.a")
            set_target_properties(thread PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_thread.a")
            set_target_properties(atomic PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_atomic.a")
            set_target_properties(regex PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_regex.a")
            set_target_properties(iostreams PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_iostreams.a")
            set_target_properties(random PROPERTIES IMPORTED_LOCATION "${BOOST_LIBRARYDIR}/libboost_random.a")

            add_library(ssl STATIC IMPORTED)
            add_library(crypto STATIC IMPORTED)

            set_target_properties(ssl PROPERTIES IMPORTED_LOCATION "${LIB_ROOT}/lib/libssl.a")
            set_target_properties(crypto PROPERTIES IMPORTED_LOCATION "${LIB_ROOT}/lib/libcrypto.a")

            add_library(cpprest STATIC IMPORTED)
            set_target_properties(cpprest PROPERTIES IMPORTED_LOCATION "${LIB_ROOT}/lib/libcpprest.a")

            add_library(zmq STATIC IMPORTED)
            set_target_properties(zmq PROPERTIES IMPORTED_LOCATION "${LIB_ROOT}/lib/libzmq.a")

            SET(Boost_LIBRARIES date_time system timer filesystem thread atomic regex random iostreams)
            SET(CPPREST_LIB cpprest)


            SET(ZeroMQ_LIBRARIES zmq cppzmq)
            SET(OPENSSL_LIBRARIES ssl crypto)


        endif ()
    else ()
        find_package(Threads REQUIRED)
        set(EXE_LIBS Threads::Threads -lm -ldl)

        find_package(ZeroMQ CONFIG REQUIRED)
        find_package(cppzmq CONFIG REQUIRED)
        set(ZeroMQ_LIBRARIES libzmq libzmq-static cppzmq)


#        find_package(mimalloc)
#        if (mimalloc_FOUND)
#            message(STATUS "mimalloc found")
#            add_compile_definitions(VITRIX_USE_MIMALLOC=1)
#            set(EXE_LIBS ${EXE_LIBS} mimalloc-static)
#        else ()
#            message(STATUS "mimalloc not found")
#        endif ()
    endif ()

    if (NOT CMAKE_SYSTEM_NAME STREQUAL "Android")
        find_package(OpenSSL REQUIRED)
        MESSAGE(STATUS "OpenSSL VERSION: ${OPENSSL_VERSION} ${OPENSSL_LIBRARIES}")

        if (NOT CPPREST_LIB)
            find_package(cpprestsdk REQUIRED)
            set(CPPREST_LIB cpprestsdk::cpprest)
        endif ()
        find_package(Boost 1.75.0 REQUIRED COMPONENTS date_time system timer filesystem thread atomic regex random iostreams program_options)
        find_package(ZLIB REQUIRED)


    endif ()

    if (CMAKE_BUILD_TYPE MATCHES "^Debug$" OR NOT DEFINED CMAKE_BUILD_TYPE)
        add_definitions(-D_DEBUG)
    endif ()

    add_definitions(-DELPP_THREAD_SAFE -DELPP_FEATURE_CRASH_LOG -DPICOJSON_USE_INT64 -DZMQ_BUILD_DRAFT_API=1)

    SET(VITRIX_ENV 4)

    if (NOT DEFINED CMAKE_CXX_STANDARD)
        MESSAGE(STATUS "CMAKE_CXX_STANDARD NOT SET: DEFAULTING TO C++20")
        set(CMAKE_CXX_STANDARD 20)
    else ()
        MESSAGE(DEBUG "CMAKE_CXX_STANDARD IS SET: C++${CMAKE_CXX_STANDARD}")
    endif ()
endif ()