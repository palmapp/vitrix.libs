#include "multipart_body_writer.h"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/convert/lexical_cast.hpp>

#include <commons/make_string.h>
#include <easylogging++.h>

spider::multipart_body_writer::multipart_body_writer(const std::string &boundary,
                                                     const spider::filesystem_path &temp_path, std::int64_t request_id)
    : temp_path_(temp_path),
      mt_(static_cast<std::uint32_t>(std::chrono::system_clock::now().time_since_epoch().count())), gen_(mt_)
{

   multipart_reader_.setBoundary(boundary);
   multipart_reader_.userData = this;
   multipart_reader_.onPartBegin = &multipart_body_writer::on_part_begin_callback;
   multipart_reader_.onPartData = &multipart_body_writer::on_part_data;
   multipart_reader_.onPartEnd = &multipart_body_writer::on_part_end;
   multipart_reader_.onEnd = &multipart_body_writer::on_end;
}

void spider::multipart_body_writer::init(boost::optional<std::uint64_t> const &length, boost::system::error_code &ec) {}

std::size_t spider::multipart_body_writer::put(request_buffer_type const &b, boost::system::error_code &ec)
{
   using boost::asio::buffer_cast;
   using boost::asio::buffer_size;

   auto buffer_len = buffer_size(b);
   std::size_t result = 0;
   try
   {
      auto buffer = buffer_cast<char const *>(b);

      if (buffer_len > 0 && !multipart_reader_.stopped())
      {
         do
         {
            result += multipart_reader_.feed(buffer + result, buffer_len - result);
         } while (result < buffer_len && !multipart_reader_.stopped());
      }
   }
   catch (const std::exception &ex)
   {
      auto error_msg = strerror(errno);
      CLOG(ERROR, "request") << "unable to process multipart request: " << ex.what() << " errno: " << error_msg;
      ec = beast::http::make_error_code(beast::http::error::bad_chunk);
   }

   if (multipart_reader_.hasError())
   {
      CLOG(ERROR, "request") << "unable to process multipart request:  multipart_reader reported error: "
                             << multipart_reader_.getErrorMessage();

      ec = beast::http::make_error_code(beast::http::error::bad_chunk);
   }

   if (static_cast<int>(state_) < 0)
   {
      CLOG(ERROR, "request") << "unable to process multipart request: " << static_cast<int>(state_);
      ec = beast::http::make_error_code(beast::http::error::bad_chunk);
   }

   if (state_ == state::finished)
   {
      result = buffer_len;
   }

   if (ec || state_ == state::finished)
   {
      output_.finish();
   }

   return result;
}

void spider::multipart_body_writer::finish(boost::system::error_code &ec)
{
   if (state_ != state::finished)
   {
      output_.finish();
      ec = beast::http::make_error_code(beast::http::error::bad_chunk);
      CLOG(WARNING, "request") << "multipart_body_writer::finish state_ != parser_state::eof : state_ = "
                               << static_cast<int>(state_) << " : "
                               << (parts.empty() ? std::string{" no info "}
                                                 : commons::make_string(parts.back().content_disposition.file_name, " ",
                                                                        parts.back().content_disposition.name));
   }
}

void spider::multipart_body_writer::create_part(MultipartHeaders const &headers)
{
   part p;
   for (auto &kv : headers)
   {
      if (boost::iequals(kv.first, "Content-Type"))
      {
         p.content_type = http::parse_content_type(kv.second);
      }
      else if (boost::iequals(kv.first, "Content-Disposition"))
      {
         p.content_disposition = http::parse_content_disposition(kv.second);
      }
   }

   if (p.content_disposition.name.find("..") != std::string::npos ||
       p.content_disposition.file_name.find("..") != std::string::npos)
   {
      state_ = state::error_invalid_filename;
      CLOG(WARNING, "request") << "file upload rejected due restricted character in its name : "
                               << p.content_disposition.name << " " << p.content_disposition.file_name;
   }
   else
   {
      auto guid = boost::lexical_cast<std::string>(gen_());

      auto outputpath = temp_path_ / commons::make_string("part_", parts.size(), "_",
                                                          guid.substr(guid.find_last_of('-') + 1), ".bin");

      output_.open(outputpath.c_str());

      p.temp_file_path = std::move(outputpath);

      CLOG(DEBUG, "request") << " file_name: " << p.content_disposition.file_name
                             << " name: " << p.content_disposition.name << " buffering as "
                             << p.temp_file_path.string();
      parts.push_back(std::move(p));
   }
}

void spider::multipart_body_writer::on_part_begin_callback(const MultipartHeaders &headers, void *user_data)
{
   multipart_body_writer *self = static_cast<multipart_body_writer *>(user_data);
   const auto state = self->state_;
   if (state == state::before_part_begin)
   {
      self->create_part(headers);

      if (static_cast<int>(state) >= 0)
      {
         self->state_ = state::part;
      }
   }
   else if (static_cast<int>(state) >= 0)
   {
      self->state_ = state::error_on_part_begin_callback;
   }
}
void spider::multipart_body_writer::on_part_data(const char *buffer, size_t size, void *user_data)
{
   multipart_body_writer *self = static_cast<multipart_body_writer *>(user_data);
   const auto state = self->state_;
   if (state == state::part)
   {
      self->output_.write({buffer, size});
   }
   else if (static_cast<int>(state) >= 0)
   {
      self->state_ = state::error_on_part_data;
   }
}
void spider::multipart_body_writer::on_part_end(void *user_data)
{
   multipart_body_writer *self = static_cast<multipart_body_writer *>(user_data);
   const auto state = self->state_;
   if (state == state::part)
   {
      self->output_.close();
      self->state_ = state::before_part_begin;
   }
   else if (static_cast<int>(state) >= 0)
   {
      self->state_ = state::error_on_part_end;
   }
}
void spider::multipart_body_writer::on_end(void *user_data)
{
   multipart_body_writer *self = static_cast<multipart_body_writer *>(user_data);
   const auto state = self->state_;
   if (state == state::before_part_begin)
   {
      self->state_ = state::finished;
   }
   else if (static_cast<int>(state) >= 0)
   {
      self->state_ = state::error_on_end;
   }
}

bool spider::multipart_body_writer::wait_for_output() const
{
   return output_.wait_till_finishes();
}
