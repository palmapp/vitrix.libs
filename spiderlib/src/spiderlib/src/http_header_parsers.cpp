#include "http_header_parsers.h"

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/fusion/include/std_tuple.hpp>
#include <boost/spirit/home/x3.hpp>

#include <boost/algorithm/string/trim.hpp>
#include <vector>

namespace x3 = boost::spirit::x3;
using x3::char_;
using x3::lexeme;
using x3::lit;
using x3::phrase_parse;
using x3::standard::space;

BOOST_FUSION_ADAPT_STRUCT(spider::http::content_disposition_header, context, name, file_name)

spider::http::content_disposition_header
spider::http::parse_content_disposition(beast::string_view content_disposition_value)
{

   const auto my_space = char_(" \t");

   const auto context = +(char_ - ';');
   const auto string = lexeme['"' >> +(char_ - '"') >> '"'] | +char_;

   const auto name = lit("name") >> '=' >> string;
   const auto filename = lit("filename") >> '=' >> string;

   const auto cd_parser = context >> -(';' >> name) >> -(';' >> filename);

   content_disposition_header value;
   auto it = content_disposition_value.begin();
   phrase_parse(it, content_disposition_value.end(), cd_parser, my_space, value);

   if (it != content_disposition_value.end())
   {
      return http::content_disposition_header{};
   }
   return value;
}

spider::http::content_type_header spider::http::parse_content_type(beast::string_view content_type_header_value)
{
   spider::http::content_type_header result;
   const auto idx = content_type_header_value.find(';');

   if (idx != beast::string_view::npos)
   {
      const auto quoted_string = lexeme['"' >> +(char_ - '"') >> '"'];
      const auto string = +(char_ - ';');
      const auto key = ';' >> +(char_ - '=');
      const auto key_value = key >> '=' >> (quoted_string | string);

      const auto my_space = char_(" \t");

      result.mime_type = static_cast<std::string>(content_type_header_value.substr(0, idx));
      content_type_header_value = content_type_header_value.substr(idx);
      std::pair<std::string, std::string> kv;
      // std::string val;
      auto it = content_type_header_value.begin();
      while (it != content_type_header_value.end())
      {
         if (phrase_parse(it, content_type_header_value.end(), key_value, my_space, kv))
         {
            if (kv.first == "boundary")
            {
               result.boundary = std::move(kv.second);
            }
            else if (kv.first == "charset")
            {
               result.charset = std::move(kv.second);
            }
         }
         else
         {
            break;
         }

         kv.first.clear();
         kv.second.clear();
      }
   }
   else
   {
      result.mime_type = static_cast<std::string>(content_type_header_value);
   }

   return result;
}

std::pair<std::string, std::string> spider::http::parse_header_line(beast::string_view header_line)
{
   const auto idx = header_line.find(':');
   if (idx != beast::string_view::npos)
   {
      auto value = static_cast<std::string>(header_line.substr(idx + 1));
      boost::trim(value);
      return {static_cast<std::string>(header_line.substr(0, idx)), std::move(value)};
   }

   return {};
}