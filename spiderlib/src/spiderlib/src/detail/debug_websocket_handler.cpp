#include "detail/default_websocket_handler.h"

#include <commons/make_string.h>
#include <easylogging++.h>

void debug_print(spider::websocket_connection &connection, const std::string &method_name,
                 const std::string &message = std::string(""))
{
   LOG(INFO) << method_name << " " << connection.id_ << " " << message;
}

void spider::default_websocket_handler::on_text_message(websocket_connection &connection, std::string message)
{
   debug_print(connection, "on_text_message", message);
   connection.send(commons::make_string("echo text message: ", message));
}

void spider::default_websocket_handler::on_connected(websocket_connection &connection)
{
   debug_print(connection, "on_connected");
   connection.send(commons::make_string("echo connected, connection id: ", connection.id_));
}

void spider::default_websocket_handler::on_disconnected(websocket_connection &connection)
{
   debug_print(connection, "on_disconnected");
}
