#include "file_cache.h"

spider::file_cache::cached_file::cached_file(std::string _etag, std::string _last_modified_formatted,
                                             boost::posix_time::ptime _last_modfied, std::string _content)
    : etag(std::move(_etag)), last_modified_formatted(std::move(_last_modified_formatted)), last_modfied(_last_modfied),
      content(std::move(_content))
{
}