#include "detail/default_response_generator.h"

void spider::detail::default_response_generator::return_redirect(request &req, const std::string &url,
                                                                 request_handler_callback callback)
{
   auto response = req.make_response(beast::http::status::see_other);
   response->set(beast::http::field::location, url);
   req.write_response(response, callback);
}

void spider::detail::default_response_generator::return_error(request &req, const error_message &msg,
                                                              request_handler_callback callback)
{
   auto response = req.make_response(msg.status);
   response->set(beast::http::field::content_type, "text/plain");
   std::string &body = response->body();
   body.reserve(msg.title.size() + msg.explanation.size() + 4);
   body += msg.title;
   body += "\r\n";
   body += msg.explanation;
   body += "\r\n";

   req.write_response(response, callback);
}
