#include "detail/default_websocket_manager.h"
#include <boost/lexical_cast.hpp>
#include <easylogging++.h>

spider::default_websocket_manager::default_websocket_manager(std::size_t max_concurrent_connections)
    : connections_(max_concurrent_connections)
{
}

spider::default_websocket_manager::~default_websocket_manager() {}

void spider::default_websocket_manager::add_ws_handler(websocket_handler &handler)
{
   handlers_.push_back(&handler);
}

void spider::default_websocket_manager::send_all(std::string message)
{
   for_each([&](websocket_connection &connection) { connection.send(message); });
}

void spider::default_websocket_manager::send_by_id(websocket_id id, std::string message)
{
   std::lock_guard<std::mutex> lock{connection_lock_};

   if (auto it = connections_.find(id); it != connections_.end())
   {
      if (it->second != nullptr)
      {
         it->second->send(std::move(message));
      }
   }
}

void spider::default_websocket_manager::send_if(std::string message,
                                                std::function<bool(websocket_connection &)> filter_funct)
{
   for_each(
       [&](websocket_connection &connection)
       {
          if (filter_funct(connection))
          {
             connection.send(message);
          }
       });
}

void spider::default_websocket_manager::close_by_id(websocket_id id)
{
   std::lock_guard<std::mutex> lk{connection_lock_};

   if (auto it = connections_.find(id); it != connections_.end())
   {
      auto connection = it->second;
      if (connection != nullptr)
      {
         connection->close();
         for_each_handler([&](websocket_handler *handler) { handler->on_disconnected(*connection); });

         connections_.erase(it);
      }
   }
}

void spider::default_websocket_manager::close_all()
{
   for_each(
       [&](websocket_connection &connection)
       {
          connection.close();

          for_each_handler([&](websocket_handler *handler) { handler->on_disconnected(connection); });
       });
}

void spider::default_websocket_manager::handle_new_connection(std::shared_ptr<websocket_connection> connection,
                                                              request_handler_callback callback)
{
   std::unique_lock<std::mutex> lock{connection_lock_};

   connections_[connection->id_] = connection;

   lock.unlock();

   on_accept(std::move(connection), callback);
}

void spider::default_websocket_manager::on_accept(std::shared_ptr<websocket_connection> connection_ptr,
                                                  request_handler_callback callback)
{
   connection_ptr->websocket_.async_accept(connection_ptr->original_request_.message(),
                                           [connection_ptr, this, callback](const beast::error_code &ec)
                                           {
                                              if (!ec)
                                              {
                                                 auto &connection = *connection_ptr;
                                                 for_each_handler([&](websocket_handler *handler)
                                                                  { handler->on_connected(connection); });

                                                 on_read(std::move(connection_ptr));
                                                 callback(ec, beast::http::status::upgrade_required);
                                              }
                                              else
                                              {
                                                 connections_.erase(connection_ptr->id_);
                                                 callback(ec, beast::http::status::unknown);
                                              }
                                           });
}

void spider::default_websocket_manager::on_read(std::shared_ptr<websocket_connection> connection_ptr)
{

   auto &connection = *connection_ptr;

   boost::asio::post(connection.websocket_.get_executor(),
                     [this, connection_ptr]()
                     {
                        connection_ptr->websocket_.async_read(
                            connection_ptr->buffer_,
                            [this, connection_ptr](beast::error_code const &ec, size_t)
                            {
                               auto &connection = *connection_ptr;
                               if (ec)
                               {
                                  for_each_handler([&](websocket_handler *handler)
                                                   { handler->on_disconnected(connection); });

                                  std::unique_lock<std::mutex> lock{connection_lock_};
                                  connections_.erase(connection.id_);
                                  lock.unlock();
                               }
                               else
                               {

                                  auto &ws = connection.websocket_;
                                  auto &buffer = connection.buffer_;

                                  if (ws.got_text())
                                  {
                                     auto message = beast::buffers_to_string(buffer.data());

                                     for_each_handler([&](websocket_handler *handler)
                                                      { handler->on_text_message(connection, message); });
                                  }

                                  buffer.consume(buffer.size());
                                  on_read(std::move(connection_ptr));
                               }
                            });
                     });
}

void spider::websocket_connection::close()
{
   auto self = shared_from_this();

   boost::asio::post(websocket_.get_executor(),
                     [self, this]()
                     {
                        boost::system::error_code ec;
                        websocket_.next_layer().shutdown(boost::asio::socket_base::shutdown_both, ec);
                     });
};

void spider::websocket_connection::send(std::string cs)
{
   std::lock_guard<std::mutex> lock{sender_mutex_};
   if (is_sending)
   {
      sender_queue_.push(std::make_shared<std::string>(cs));
   }
   else
   {
      send_internal(std::make_shared<std::string>(cs));
   }
};

void spider::websocket_connection::send_internal(std::shared_ptr<std::string> buffer)
{
   is_sending = true;

   auto self = shared_from_this();
   boost::asio::post(websocket_.get_executor(),
                     [buffer, self, this]
                     {
                        websocket_.text();
                        websocket_.async_write(boost::asio::buffer(*buffer),
                                               [buffer, self, this](const boost::system::error_code &ec, size_t)
                                               {
                                                  std::lock_guard<std::mutex> lock{sender_mutex_};

                                                  is_sending = false;

                                                  if (!ec)
                                                  {
                                                     if (!sender_queue_.empty())
                                                     {
                                                        auto front = std::move(sender_queue_.front());
                                                        sender_queue_.pop();

                                                        send_internal(std::move(front));
                                                     }
                                                  }
                                               });
                     });
}
