#include <boost/beast/http/basic_dynamic_body.hpp>
#include <serialization/serialization.h>
#include <serialization/url.h>

#include "auth/authentication_service.h"
#include "auth/authorization_scope.h"
#include "auth/authorization_service.h"
#include <commons/make_string.h>
#include <msgpack.hpp>

using namespace spider;
using namespace spider::auth;

authorization_scope_base::authorization_scope_base(std::string scope_path,
                                                   std::unique_ptr<request_handler> next_handler,
                                                   authentication_service &authentication,
                                                   authorization_service &authorization)
   : scoped_app_base(std::move(scope_path)), next_handler_(std::move(next_handler)), authentication_(authentication),
     authorization_(authorization)
{
   session_token_cookie_name = default_session_token_cookie_name;
}

void authorization_scope_base::fake_authentication(authentication_result result)
{
   fake_authentication_ = result;
}

void authorization_scope_base::handle_request_internal(request &req, request_handler_callback callback)
{
   auto result = fake_authentication_.has_value()
                    ? fake_authentication_.value()
                    : authenticate_request(authentication_, req, session_token_cookie_name);
   if (result.success)
   {
      req.set_custom_context(result);
      if (authorization_.authorize_request(req))
      {
         req.path = to_our_string_view(req.message().target().substr(scope_path_.size() - 1));
         next_handler_->handle_request(req, callback);
      }
      else
      {
         handle_unauthorized(req, callback);
      }
   }
   else
   {
      handle_unauthenticated(req, callback);
   }
}

struct api_error_message
{
   std::string message;
   MSGPACK_DEFINE_MAP(message)
};

VISITABLE_STRUCT(api_error_message, message);

api_authorization_scope::api_authorization_scope(std::string scope_path, std::unique_ptr<request_handler> next_handler,
                                                 authentication_service &authentication,
                                                 authorization_service &authorization)
   : authorization_scope_base(std::move(scope_path), std::move(next_handler), authentication, authorization)
{
}

void api_authorization_scope::handle_unauthenticated(request &r, request_handler_callback callback)
{
   auto response = r.make_response(beast::http::status::unauthorized);
   response->set(beast::http::field::content_type, "application/json");
   response->body() = spider::serialization::serialize_structure(api_error_message{"T:operation.unauthenticated"},
                                                                 spider::serialization::content_type::json);

   r.write_response(response, callback);
}

void api_authorization_scope::handle_unauthorized(request &r, request_handler_callback callback)
{
   auto response = r.make_response(beast::http::status::forbidden);
   response->result(beast::http::status::forbidden);
   response->set(beast::http::field::content_type, "application/json");
   response->body() = spider::serialization::serialize_structure(api_error_message{"T:operation.forbidden"},
                                                                 spider::serialization::content_type::json);

   r.write_response(response, callback);
}

web_authorization_scope::web_authorization_scope(std::string scope_path, std::unique_ptr<request_handler> next_handler,
                                                 authentication_service &authentication,
                                                 authorization_service &authorization)
   : authorization_scope_base(std::move(scope_path), std::move(next_handler), authentication, authorization)
{
}

void web_authorization_scope::handle_unauthenticated(request &r, request_handler_callback callback)
{
   std::string red;
   if (!do_not_include_path)
   {
      red = commons::make_string(unauthenticated_url, "?path=", url::encode(r.message().target()));
   }
   else
   {
      red = unauthenticated_url;
   }

   r.return_redirect(red, callback);
}

void web_authorization_scope::handle_unauthorized(request &r, request_handler_callback callback)
{
   r.return_redirect(commons::make_string(unauthorized_url, "?path=", url::encode(r.message().target())),
                     callback);
}

namespace
{
   std::string extract_session_token(request &req, const std::string &session_token_cookie_name)
   {
      std::string result;
      const auto it = req.message().find(beast::http::field::cookie);
      if (it != req.message().end())
      {
         auto value = it->value();

         url::parse_query(value.begin(), value.end(),
                          [&session_token_cookie_name, &result](const std::string &key, std::string &value)
                          {
                             if (key == session_token_cookie_name) result = std::move(value);
                          });
      }

      return result;
   }
} // namespace

authentication_result spider::auth::authenticate_request(authentication_service &authentication, request &req,
                                                         const std::string &session_token_cookie_name)
{
   return authentication.verify_token(extract_session_token(req, session_token_cookie_name));
}

std::shared_ptr<principal_base> spider::auth::get_request_principal(request &req)
{
   if (req.has_custom_context<authentication_result>())
   {
      return req.get_custom_context<authentication_result>().principal;
   }

   return {};
}
