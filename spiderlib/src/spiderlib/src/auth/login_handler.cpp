#include "auth/login_handler.h"
#include "auth/authorization_scope.h"
#include <boost/algorithm/string/predicate.hpp>
#include <commons/make_string.h>
#include <serialization/msgpack.h>

namespace spider
{
   namespace auth
   {
      struct login_page
      {
         std::string path;
         std::string ticket;
         MSGPACK_DEFINE_MAP(path, ticket)
      };

      struct login_request
      {
         std::string path;
         std::string username;
         std::string password;

         MSGPACK_DEFINE_MAP(path, username, password)
      };
   } // namespace auth
} // namespace spider
VISITABLE_STRUCT(spider::auth::login_page, path, ticket);
VISITABLE_STRUCT(spider::auth::login_request, path, username, password);

using namespace spider;
using namespace spider::auth;

login_handler::login_handler(std::string scope_path, template_loader &tmpl, authentication_service &auth)
    : api_app_base(std::move(scope_path), tmpl), auth_(auth)
{
   session_token_cookie_name = default_session_token_cookie_name;

   auto cas_url = auth_.get_cas_service_url();
   init_api(cas_url);
   init_login_page(cas_url);
}

login_handler::login_handler(std::string scope_path, authentication_service &auth)
    : api_app_base(std::move(scope_path)), auth_(auth)
{
   session_token_cookie_name = default_session_token_cookie_name;

   auto cas_url = auth_.get_cas_service_url();
   init_api(cas_url);
}

void login_handler::handle_request_internal(request &r, request_handler_callback callback)
{
   if (authenticate_request(auth_, r, session_token_cookie_name).success)
   {
      r.return_redirect(default_redirect_after_login, callback);
   }
   else
   {
      api_app_base::handle_request_internal(r, callback);
   }
}

void login_handler::init_api(std::string const &cas_url)
{
   on(beast::http::verb::post, "/submit/",
      [this](spider::request &req, login_request credentials, request_handler_callback callback)
      {
         auto result = auth_.authenticate(credentials.username, credentials.password);

         std::string redirect_path =
             commons::make_string(scope_path_, "?path=", url::encode(credentials.path), "&failed=1");
         if (result.success)
         {
            req.response_fields.set(
                beast::http::field::set_cookie,
                commons::make_string(session_token_cookie_name, '=', url::encode(result.data), "; Path=/"));
            if (credentials.path.empty() || boost::starts_with(credentials.path, scope_path_))
            {
               redirect_path = default_redirect_after_login;
            }
            else
            {
               redirect_path = credentials.path;
            }

            events.on_logged_in(req, result.principal);
         }
         else
         {
            events.on_login_failed(req, result.data, credentials.username);
         }

         req.return_redirect(redirect_path, callback);
      });

   on(beast::http::verb::get, "/logout/",
      [this](spider::request &req, login_request credentials, request_handler_callback callback)
      {
         auto result = authenticate_request(auth_, req, session_token_cookie_name);
         if (result.success)
         {
            events.on_logged_out(req, result.principal);
         }

         req.response_fields.set(beast::http::field::set_cookie,
                                 commons::make_string(session_token_cookie_name, "=; Path=/"));
         req.return_redirect(scope_path_, callback);
      });

   if (!cas_url.empty())
   {
      on(beast::http::verb::get, "/",
         [this, cas_url](spider::request &req, login_page lr, request_handler_callback callback)
         {
            std::string redirect_path;
            if (lr.ticket.empty())
            {
               redirect_path = cas_url;
            }
            else
            {
               auto result = auth_.authenticate(lr.path, lr.ticket);
               if (result.success)
               {
                  req.response_fields.set(
                      beast::http::field::set_cookie,
                      commons::make_string(session_token_cookie_name, '=', url::encode(result.data), "; Path=/"));

                  if (lr.path.empty() || boost::starts_with(lr.path, scope_path_))
                  {
                     redirect_path = default_redirect_after_login;
                  }
                  else
                  {
                     redirect_path = lr.path;
                  }
               }
               else
               {
                  redirect_path = auth_.get_cas_service_url();
               }
            }

            req.return_redirect(redirect_path, callback);
         });
   }
}

void login_handler::init_login_page(std::string const &cas_url)
{
   if (cas_url.empty())
   {
      on_web(beast::http::verb::get, "/", [](spider::request &req, login_page lr) { return lr; });
   }
}