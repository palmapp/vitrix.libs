#include "api_app_base.h"
#include <easylogging++.h>

namespace spider
{
   std::string get_normalized_path(string_view uri, bool query)
   {
      if (!uri.empty())
      {
         std::stringstream buffer;

         if (uri[0] != '/')
         {
            buffer << '/';
         }

         if (query)
         {
            buffer << uri;
         }
         else
         {
            buffer << uri.substr(0, uri.find('?'));
         }

         return buffer.str();
      }
      return {"/"};
   }
} // namespace spider

spider::api_app_base::api_app_base(std::string scope_path) : scoped_app_base(std::move(scope_path)) {}

spider::api_app_base::api_app_base(std::string scope_path, template_loader &tmpl)
    : scoped_app_base(std::move(scope_path)), tmpl_(&tmpl)
{
}

spider::request_handler *spider::api_app_base::find_request_handler(request &req)
{
   auto &msg = req.message();
   // FIXME this should not be here
   if (is_current_scoped_path(req.path))
   {
      auto it = handlers_.find(msg.method());
      if (it != handlers_.end())
      {
         auto scoped_path = get_current_scoped_path(req.path);
         auto local_path = get_normalized_path(scoped_path, false);
         auto handler_it = it->second.find(local_path);
         if (handler_it != it->second.end())
         {
            CLOG(DEBUG, "spider") << "method=" << msg.method_string() << " handler path " << local_path << " : found";

            return handler_it->second.get();
         }
         else
         {

            CLOG(DEBUG, "spider") << "method=" << msg.method_string() << " handler path " << local_path
                                  << " : not found";
         }
      }
      else
      {
         CLOG(DEBUG, "spider") << "method=" << msg.method_string() << " not registered";
      }
   }
   return nullptr;
}

void spider::api_app_base::handle_request_internal(request &request, request_handler_callback callback)
{
   auto *handler = find_request_handler(request);
   if (handler != nullptr)
   {
      handler->handle_request(request, callback);
   }
   else
   {
      request.return_error({beast::http::status::not_found}, callback);
   }
}
