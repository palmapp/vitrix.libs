#include "io/async_file.h"
#include <commons/make_string.h>

#ifndef BOOST_WINDOWS
#include <sys/sendfile.h>
#endif

#include <easylogging++.h>

#ifdef BOOST_WINDOWS
using namespace boost::asio::windows;

namespace
{

   bool is_handle_valid(HANDLE h)
   {
      return reinterpret_cast<std::size_t>(h) != std::numeric_limits<std::size_t>::max();
   }
   random_access_handle open_file(spider::io::executor service, const spider::filesystem_path &path)
   {
      auto wpath = path.wstring();

      HANDLE h = CreateFileW(wpath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);

      auto native_handle = static_cast<random_access_handle::native_handle_type>(h);
      return random_access_handle{service, native_handle};
   }
} // namespace

bool spider::io::async_file::try_open(async_file &file, const filesystem_path &path, std::size_t read_buffer_size)
{
   auto wpath = path.wstring();

   HANDLE h = CreateFileW(wpath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
   if (is_handle_valid(h))
   {
      file.handle_ = {file.handle_.get_executor(), h};
      file.buffer_size_ = read_buffer_size;

      return true;
   }
   else
   {
      return false;
   }
}
#else
#include <fcntl.h>
#include <sys/stat.h>
namespace
{
   constexpr bool is_handle_valid(int h)
   {
      return h >= 0;
   }

   spider::random_access_stream open_file(spider::io::executor service, const spider::filesystem_path &path)
   {
      auto path_string = path.string();
      auto h = open(path_string.c_str(), O_RDONLY);
      if (is_handle_valid(h))
         return {service, h};
      else
         throw spider::exception(commons::make_string("unable to open file: ", path_string, " => ", strerror(errno)));
   }
} // namespace

bool spider::io::async_file::try_open(async_file &file, const filesystem_path &path, std::size_t read_buffer_size)
{
   auto path_string = path.string();
   auto h = open(path_string.c_str(), O_RDONLY);
   if (is_handle_valid(h))
   {
      file.handle_ = {file.handle_.get_executor(), h};
      file.buffer_size_ = read_buffer_size;

      return true;
   }
   else
      return false;
}
#endif

spider::io::async_file::async_file(executor service) : handle_(service) {}

spider::io::async_file::async_file(executor service, const filesystem_path &path, std::size_t read_buffer_size)
    : handle_(open_file(service, path)), buffer_size_(read_buffer_size)
{
}

spider::io::async_file::async_file(async_file &&other) noexcept
    : handle_(std::move(other.handle_)), buffer_size_(other.buffer_size_)
{
}

spider::io::async_file &spider::io::async_file::operator=(async_file &&other) noexcept
{
   handle_ = std::move(other.handle_);
   buffer_size_ = other.buffer_size_;
   return *this;
}

#ifdef BOOST_WINDOWS

std::size_t spider::io::async_file::size()
{
   LARGE_INTEGER result;
   memset(&result, 0, sizeof(LARGE_INTEGER));

   auto apiResult = GetFileSizeEx(handle_.native_handle(), &result);
   if (apiResult == 0) throw exception("unable to get size of the file");
   return static_cast<std::size_t>(result.QuadPart);
}

#else

std::size_t spider::io::async_file::size()
{
   struct stat buf;
   if (fstat(handle_.native_handle(), &buf) < 0)
   {
      throw exception("unable to get size of the file");
   }

   return static_cast<std::size_t>(buf.st_size);
}
#endif

#ifdef BOOST_WINDOWS

namespace
{
   constexpr std::size_t transmitfile_max = 2147483646;

   template <class Handler>
   class transmit_file_big_state : public std::enable_shared_from_this<transmit_file_big_state<Handler>>
   {
    public:
      transmit_file_big_state(boost::asio::ip::tcp::socket &socket, random_access_handle &file, Handler handler,
                              std::size_t offset, std::size_t len)
          : socket_(socket), file_(file), handler_(handler), offset_(offset), len_(len)
      {
      }

      void transmit_big_file()
      {
         auto self = this->shared_from_this();
         overlapped_ptr overlapped(socket_.get_executor(),
                                   [this, self](boost::system::error_code const &ec, std::size_t bytes) mutable {
                                      total_bytes_ += bytes;
                                      offset_ += bytes;
                                      len_ -= bytes;

                                      if (len_ == 0 || ec)
                                      {
                                         handler_(ec, total_bytes_);
                                      }
                                      else
                                      {
                                         transmit_big_file();
                                      }
                                   });

         LARGE_INTEGER offset;
         offset.QuadPart = static_cast<LONGLONG>(offset_);

         OVERLAPPED *overlapped_data = overlapped.get();
         overlapped_data->Offset = offset.LowPart;
         overlapped_data->OffsetHigh = offset.HighPart;

         auto len = std::min(len_, transmitfile_max);

         BOOL ok = ::TransmitFile(socket_.native_handle(), file_.native_handle(), static_cast<DWORD>(len), 0,
                                  overlapped.get(), 0, 0);
         DWORD last_error = ::GetLastError();

         // Check if the operation completed immediately.
         if (!ok && last_error != ERROR_IO_PENDING)
         {
            // The operation completed immediately, so a completion notification needs
            // to be posted. When complete() is called, ownership of the OVERLAPPED-
            // derived object passes to the io_service.
            boost::system::error_code ec(last_error, boost::asio::error::get_system_category());
            overlapped.complete(ec, 0);
         }
         else
         {
            // The operation was successfully initiated, so ownership of the
            // OVERLAPPED-derived object has passed to the io_service.
            overlapped.release();
         }
      }

    private:
      boost::asio::ip::tcp::socket &socket_;
      random_access_handle &file_;
      Handler handler_;

      std::size_t offset_;
      std::size_t len_;
      std::size_t total_bytes_ = 0;
   };

   template <typename Handler>
   void transmit_file(boost::asio::ip::tcp::socket &socket, random_access_handle &file, std::size_t offset_in,
                      std::size_t len, Handler handler)
   {
      if (len <= transmitfile_max)
      {
         using namespace boost::asio::windows;

         // Construct an OVERLAPPED-derived object to contain the handler.
         overlapped_ptr overlapped(socket.get_executor(), handler);

         LARGE_INTEGER offset;
         offset.QuadPart = static_cast<LONGLONG>(offset_in);

         OVERLAPPED *overlapped_data = overlapped.get();
         overlapped_data->Offset = offset.LowPart;
         overlapped_data->OffsetHigh = offset.HighPart;

         // Initiate the TransmitFile operation.
         BOOL ok = ::TransmitFile(socket.native_handle(), file.native_handle(), static_cast<DWORD>(len), 0,
                                  overlapped.get(), 0, 0);
         DWORD last_error = ::GetLastError();

         // Check if the operation completed immediately.
         if (!ok && last_error != ERROR_IO_PENDING)
         {
            // The operation completed immediately, so a completion notification needs
            // to be posted. When complete() is called, ownership of the OVERLAPPED-
            // derived object passes to the io_service.
            boost::system::error_code ec(last_error, boost::asio::error::get_system_category());
            overlapped.complete(ec, 0);
         }
         else
         {
            // The operation was successfully initiated, so ownership of the
            // OVERLAPPED-derived object has passed to the io_service.
            overlapped.release();
         }
      }
      else
      {
         auto transmit_state =
             std::make_shared<transmit_file_big_state<Handler>>(socket, file, handler, offset_in, len);
         transmit_state->transmit_big_file();
      }
   }

} // namespace

void spider::io::async_file::stream_to_output(boost::asio::ip::tcp::socket &stream, io_callback callback,
                                              std::size_t offset, std::size_t len)
{
   if (len == npos)
   {
      len = size() - offset;
   }

   transmit_file(stream, handle_, offset, len,
                 [callback](const boost::system::error_code &ec, std::size_t) { callback(ec, 0); });
}
#else

namespace
{
   struct stream_op : std::enable_shared_from_this<stream_op>
   {
      stream_op(boost::asio::ip::tcp::socket &socket, int fd, off_t start, std::int64_t len,
                spider::io_callback callback)
          : socket_(socket), fd_(fd), start_(start), count_(len), callback_(callback)
      {
      }

      void start()
      {
         if (count_ == 0)
         {
            callback_({}, 0);
         }
         else
         {
            ssize_t result = ::sendfile(socket_.native_handle(), fd_, &start_, count_);
            if (result == -1)
            {
               auto code = boost::system::errc::make_error_code(static_cast<boost::system::errc::errc_t>(errno));
               CLOG(WARNING, "asyncfile")
                   << "sendfile failed (" << start_ << "," << count_ << ") with message " << code.message();
               callback_(code, count_);
            }
            else if (result == 0)
            {
               CLOG(INFO, "asyncfile") << "finished";
               callback_({}, 0);
            }
            else
            {

               if (result > count_)
               {
                  CLOG(ERROR, "asyncfile") << "wtf? result > count_";
                  count_ = 0;
               }
               else
               {
                  count_ -= result;
               }
               // this will do nothing besides checking if socket
               // is writable again.
               auto self = shared_from_this();
               socket_.async_write_some(boost::asio::null_buffers(), [this, self](const boost::system::error_code &ec,
                                                                                  std::size_t bytes_transferred) {
                  if (ec)
                  {
                     CLOG(WARNING, "asyncfile") << "data write error " << ec.message();
                     callback_(ec, bytes_transferred);
                  }
                  else
                  {
                     start();
                  }
               });
            }
         }
      }

      boost::asio::ip::tcp::socket &socket_;
      int fd_;
      off_t start_;
      std::int64_t count_;
      spider::io_callback callback_;
   };
} // namespace

void spider::io::async_file::stream_to_output(boost::asio::ip::tcp::socket &stream, io_callback callback,
                                              std::size_t offset, std::size_t len)
{
   if (len == npos)
   {
      len = size() - offset;
   }

   auto async_operation =
       std::make_shared<stream_op>(stream, handle_.native_handle(), static_cast<off_t>(offset), len, callback);
   async_operation->start();
}

#endif
