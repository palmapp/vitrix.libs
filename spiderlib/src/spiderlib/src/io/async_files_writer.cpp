#include "io/async_files_writer.h"
#include "io/buffer_pool.h"
#include <easylogging++.h>
#include <gsl/narrow>
using namespace spider::io;

async_files_writer::async_files_writer()
   : active_object(commons::stopping_policy::process_all),
     buffer_(buffer_pool::allocate_buffer()),
     state_(proc_state::in_progress)
{
   buffered_ = 0;
}

async_files_writer::~async_files_writer() = default;

void async_files_writer::open(const posix::path &file)
{
   if (opened_file_)
   {
      close();
   }

   auto path_string = file.string();
   auto buffer = buffer_pool::allocate_buffer();

   std::copy(path_string.c_str(), path_string.c_str() + path_string.size(), buffer.get());

   post_message({async_write_message::msg_type::open, std::move(buffer), path_string.size()});

   opened_file_ = true;
}

void async_files_writer::write(posix::string_view data)
{
   while (!data.empty())
   {
      auto to_copy = std::min(buffer_pool::buffer_capacity() - buffered_, data.size());
      if (to_copy == 0)
      {
         flush_write();
      }
      else
      {
         std::copy(data.begin(), data.begin() + gsl::narrow<posix::string_view::difference_type>(to_copy),
                   buffer_.get() + buffered_);

         buffered_ += to_copy;
         data = data.substr(to_copy);
      }
   }
}

void async_files_writer::close()
{
   if (buffered_ > 0)
   {
      flush_write();
   }

   post_message({async_write_message::msg_type::close, buffer_pool::buffer_ptr{}, 0});
   opened_file_ = false;
}

void async_files_writer::finish()
{
   if (opened_file_)
   {
      close();
   }

   post_message({async_write_message::msg_type::done, buffer_pool::buffer_ptr{}, 0});
}

bool async_files_writer::wait_till_finishes() const
{
   using namespace std::literals;
   std::unique_lock<std::mutex> lk(mutex_);

   while (!wait_cnd_variable_.wait_for(lk, 30s, [this]
   {
      return state_ != proc_state::in_progress;
   }))
   {
      if (get_queue_stats().is_queue_drained())
      {
         return state_ == proc_state::done;
      }
   }

   return state_ == proc_state::done;
}

void async_files_writer::on_message(async_write_message &message)
{

   switch (message.type)
   {
   case async_write_message::msg_type::write:
      output_file_.write_all(message.buffer.get(), gsl::narrow<std::uint32_t>(message.len));
      break;
   case async_write_message::msg_type::open:
   {
      std::string path{message.buffer.get(), message.len};

      output_file_ = posix::file_handle::open({path}, posix::open_mode::write_only_binary_create_or_truncate);

      if (output_file_.get_handle() == posix::invalid_handle)
      {
         throw posix::error("file_handle::open", posix::invalid_handle);
      }
   }
   break;
   case async_write_message::msg_type::close:
      output_file_.close();
      break;
   case async_write_message::msg_type::done:
   {
      {
         std::lock_guard<std::mutex> lk(mutex_);
         state_ = proc_state::done;
      }
      wait_cnd_variable_.notify_one();
   }
   break;
   default:
      break;
   }
}

void async_files_writer::on_exception(const async_write_message &, const std::exception &ex) noexcept
{
   LOG(ERROR) << "async_files_writer::on_exception " << ex.what();

   {
      std::lock_guard<std::mutex> lk(mutex_);
      state_ = proc_state::failed;
   }
   wait_cnd_variable_.notify_one();
}

void async_files_writer::on_unknown_exception(const async_write_message &) noexcept
{
   LOG(ERROR) << "async_files_writer::on_exception : exception";
   {
      std::lock_guard<std::mutex> lk(mutex_);
      state_ = proc_state::failed;
   }
   wait_cnd_variable_.notify_one();
}

void async_files_writer::flush_write()
{
   post_message({async_write_message::msg_type::write, std::move(buffer_), buffered_});
   buffer_ = buffer_pool::allocate_buffer();

   buffered_ = 0;
}