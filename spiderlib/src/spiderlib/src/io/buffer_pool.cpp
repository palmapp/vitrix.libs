#include "io/buffer_pool.h"
#include "commons/threading/semaphore.h"
#include <atomic>
#include <boost/pool/singleton_pool.hpp>
#include <type_traits>

using namespace std::chrono_literals;

namespace spider::io::buffer_pool
{
   struct pool_tag
   {
   };
   using pool_type = boost::singleton_pool<pool_tag, buffer_capacity(), boost::default_user_allocator_new_delete,
                                           boost::details::pool::default_mutex, 1024, 8192>;
   static commons::semaphore g_allocation_limiter(500000);

   buffer_ptr allocate_buffer()
   {
      if (g_allocation_limiter.try_acquire())
      {
         return buffer_ptr{pooled_buffer_ptr{reinterpret_cast<char *>(pool_type::malloc())}};
      }
      else
      {
         return buffer_ptr{system_buffer_ptr{new char[buffer_capacity()]}};
      }
   }
   void free_buffer(char *buffer)
   {
      g_allocation_limiter.release();

      return pool_type::free(reinterpret_cast<void *>(buffer));
   }

   buffer_ptr::buffer_ptr() noexcept {}
   buffer_ptr::buffer_ptr(pooled_buffer_ptr pooled_ptr) noexcept : holder_(std::move(pooled_ptr)) {}
   buffer_ptr::buffer_ptr(system_buffer_ptr system_ptr) noexcept : holder_(std::move(system_ptr)) {}
   bool buffer_ptr::is_empty() const noexcept
   {
      return holder_.index() == 0;
   }
   buffer_ptr::buffer_ptr(buffer_ptr &&other) : holder_(std::move(other.holder_))
   {
      other.holder_ = empty_tag{};
   }
   void buffer_ptr::operator=(buffer_ptr &&other)
   {
      holder_ = std::move(other.holder_);
      other.holder_ = empty_tag{};
   }
   char *buffer_ptr::get() const
   {
      char *ptr = nullptr;
      std::visit(
          [&](auto &holded_ptr) mutable
          {
             if constexpr (!std::is_same_v<std::decay_t<decltype(holded_ptr)>, empty_tag>)
             {
                ptr = holded_ptr.get();
             }
          },
          holder_);
      return ptr;
   }

   void buffer_ptr::reset()
   {
      holder_ = empty_tag{};
   }
   buffer_ptr::allocation_type buffer_ptr::get_allocation_type() const noexcept
   {
      return static_cast<allocation_type>(holder_.index());
   }

} // namespace spider::io::buffer_pool
