#include "app.h"
#include "detail/default_response_generator.h"

#include <chrono>

#include <atomic>
#include <easylogging++.h>
#include <iostream>

using namespace std::chrono_literals;
using namespace std::chrono;
using namespace boost::posix_time;

namespace
{
   using namespace spider;

   class http_connection_counter
   {
    private:
      inline static std::atomic<std::int64_t> &get_connection_counter()
      {
         static std::atomic<std::int64_t> counter;
         return counter;
      }

      inline static std::atomic<std::int64_t> &get_request_counter()
      {
         static std::atomic<std::int64_t> counter;
         return counter;
      }

    public:
      inline static std::int64_t get_next_connection_id()
      {
         auto &counter = http_connection_counter::get_connection_counter();
         return counter++;
      }

      inline static std::int64_t get_next_request_id()
      {
         auto &counter = http_connection_counter::get_request_counter();
         return counter++;
      }
   };

   class http_connection : public std::enable_shared_from_this<http_connection>
   {
    public:
      http_connection(boost::asio::ip::tcp::socket socket, app &handler)
          : socket_(std::move(socket)), handler_(handler), keep_alive_timer_(socket_.get_executor())
      {
         buffer_.reserve(8192 * 4);
         connection_id_ = http_connection_counter::get_next_connection_id();
      }

      virtual ~http_connection()
      {
         close();
      }

      void on_header_read(const beast::error_code &ec)
      {
         // CLOG(DEBUG, "connection") << connection_id_ << " : read_callback";
         if (!ec)
         {
            current_request_->init_from_message();

            if (keep_alive_)
            {
               keep_alive_timer_.cancel_one();
            }

            process_request();
         }
         else if (ec == boost::asio::error::eof || timed_out_ || ec == beast::http::error::end_of_stream)
         {
            // CLOG(DEBUG, "connection") << connection_id_ << " : eof";
            free_request_scope();
            return;
         }
         else
         {
            //  CLOG(ERROR, "connection") << connection_id_ << "async_read_header ec: " << ec.message();
            free_request_scope();
         }
      }

      void begin_read()
      {
         //  CLOG(DEBUG, "connection") << connection_id_ << "begin_read";

         if (socket_.is_open())
         {
            current_request_.emplace(socket_, *handler_.response_generator, buffer_, handler_.get_request_body_limit(),
                                     connection_id_, http_connection_counter::get_next_request_id());

            auto self = shared_from_this();
            beast::http::async_read_header(socket_, buffer_, current_request_->parser,
                                           [this, self](const beast::error_code &ec, std::size_t) mutable
                                           { on_header_read(ec); });
         }
      }

      void on_request_processed(const beast::error_code &ec = {})
      {
         keep_alive_ = current_request_->message().keep_alive();

         free_request_scope();

         if (!socket_.is_open())
         {
            return;
         }

         if (keep_alive_ && !ec)
         {
            // CLOG(INFO, "connection") << "keep_alive_ = true";
            // CLOG(DEBUG, "connection") << connection_id_ << "keep alive begin_read";

            keep_alive_timer_.expires_from_now(30s);

            auto self = shared_from_this();
            keep_alive_timer_.async_wait([self, this](const boost::system::error_code &ec) { on_timed_out(ec); });

            begin_read();
         }
         else
         {
            // CLOG(DEBUG, "connection") << connection_id_ << "connection close";
            close();
         }
      }

      void close()
      {
         boost::system::error_code ec;
         socket_.shutdown(boost::asio::socket_base::shutdown_send, ec);
         socket_.close();
      }

      void on_timed_out(const beast::error_code &ec)
      {
         if (!ec)
         {
            timed_out_ = true;
            boost::system::error_code cancel_error;
            socket_.shutdown(boost::asio::socket_base::shutdown_both, cancel_error);
            socket_.cancel();
         }
      }

      void free_request_scope()
      {
         try
         {
            current_request_.reset();
         }
         catch (const std::exception &ex)
         {
            CLOG(ERROR, "request") << "previous request destruction failed: " << ex.what();
         }
         catch (...)
         {
            CLOG(ERROR, "request") << "previous request destruction failed";
         }
      }

      void process_request()
      {
         auto self = shared_from_this();
         auto request_completion_handler = [this, self](const beast::error_code &ec, beast::http::status status)
         {
            if (ec)
            {
               CLOG(ERROR, "request") << "[" << connection_id_ << "," << current_request_->request_id
                                      << "] : " << current_request_->message().method_string() << " "
                                      << current_request_->full_path << " failed with [" << ec.value() << "] "
                                      << ec.message();
            }
            else
            {
               CLOG(INFO, "request") << "[" << connection_id_ << "," << current_request_->request_id
                                     << "] : " << current_request_->message().method_string() << " "
                                     << current_request_->full_path << " -> " << static_cast<int>(status);

               if (!current_request_->has_been_read)
               {
                  CLOG(DEBUG, "request") << "[" << connection_id_ << "," << current_request_->request_id
                                         << "] : " << current_request_->message().method_string() << " "
                                         << current_request_->full_path << " current_request_->read_message ";

                  current_request_->read_message([self](const beast::error_code &ec, size_t)
                                                 { self->on_request_processed(ec); });
               }
               else
               {
                  self->on_request_processed();
               }
            }
         };

         bool exception = false;
         try
         {
            handler_.execute_extend_callback(*current_request_);
            auto *request_handler = handler_.find_request_handler(*current_request_);
            if (request_handler != nullptr)
            {
               request_handler->handle_request(*current_request_, request_completion_handler);
            }
            else
            {
               handler_.response_generator->return_error(
                   *current_request_, {beast::http::status::not_found, "not found", "this endpoint does not exist"},
                   request_completion_handler);
            }
         }
         catch (const std::exception &ex)
         {
            CLOG(ERROR, "request") << "[" << connection_id_ << "," << current_request_->request_id << "] : "
                                   << " : exception occured in handle_request : " << ex.what();
            exception = true;
         }
         catch (...)
         {
            CLOG(ERROR, "request") << "[" << connection_id_ << "," << current_request_->request_id << "] : "
                                   << " : unknown exception occured in handle_request";
            exception = true;
         }

         if (exception)
         {
            handler_.response_generator->return_error(
                *current_request_,
                {beast::http::status::internal_server_error, "internal server error", "try again later"},
                request_completion_handler);
         }
      }

      boost::asio::ip::tcp::socket socket_;
      beast::flat_buffer buffer_;

      app &handler_;

      boost::asio::basic_waitable_timer<steady_clock> keep_alive_timer_;
      std::optional<request> current_request_;

      bool keep_alive_ = false;
      bool timed_out_ = false;
      std::int64_t connection_id_;
   };
} // namespace

void spider::init_loggers()
{
   el::Loggers::getLogger("request");
   el::Loggers::getLogger("connection");
   el::Loggers::getLogger("asyncfile");
   el::Loggers::getLogger("spider");
}

spider::app::app()
{
   response_generator = std::make_unique<detail::default_response_generator>();
}

void spider::app::add_url_scope_handler(std::unique_ptr<request_handler> handler)
{
   handlers_.push_back(std::move(handler));
}

spider::request_handler *spider::app::find_request_handler(request &req)
{
   request_handler *result = nullptr;

   for (auto &handler : handlers_)
   {
      if ((result = handler->find_request_handler(req)) != nullptr)
      {
         break;
      }
   }

   return result;
}

void spider::app::handle_request(request &req, request_handler_callback callback)
{
   auto handler = find_request_handler(req);
   if (!handler)
   {
      if (response_generator)
      {
         response_generator->return_error(
             req, {beast::http::status::not_found, "not found", "this endpoint don't exist"}, callback);
      }
      else
      {
         CLOG(WARNING, "request") << "app::handle_request unable to generate error response";
      }
   }
   else
   {
      handler->handle_request(req, callback);
   }
}

spider::app_tcp_listener::app_tcp_listener(app &current_app, boost::asio::io_context &io_context,
                                           boost::asio::ip::tcp::acceptor &acceptor)
    : current_app_(current_app), io_context_(io_context), acceptor_(acceptor)
{
}

void spider::app_tcp_listener::handle_connection(boost::asio::ip::tcp::socket socket)
{
   auto connection = std::make_shared<http_connection>(std::move(socket), current_app_);
   connection->begin_read();
}

void spider::app_tcp_listener::begin_async_accept()
{
   if (current_app_.response_generator == nullptr)
   {
      CLOG(FATAL, "connection") << "begin async accept failed must be current_app_.response_generator != nullptr";
   }

   CLOG(DEBUG, "connection") << "begin async accept";
   acceptor_.async_accept(boost::asio::make_strand(io_context_),
                          [this](const boost::system::error_code &ec, boost::asio::ip::tcp::socket socket) mutable
                          {
                             try
                             {
                                if (!ec)
                                {
                                   CLOG(DEBUG, "connection")
                                       << "connection accepted from " << socket.remote_endpoint().address().to_string();

                                   handle_connection(std::move(socket));
                                }
                                else
                                {
                                   LOG(WARNING) << "spider::app_tcp_listener::begin_async_accept failed with ["
                                                << ec.value() << "] message => " << ec.message();
                                }
                             }
                             catch (const std::exception &ex)
                             {

                                CLOG(ERROR, "connection") << "handle_connection failed " << ex.what();
                             }
                             catch (...)
                             {
                                CLOG(ERROR, "connection") << "unkown exception occured";
                             }

                             if (!io_context_.stopped())
                             {
                                begin_async_accept();
                             }
                             else
                             {
                                CLOG(INFO, "connection")
                                    << "spider::app::begin_async_accept finished : io_service is stopped";
                             }
                          });
}