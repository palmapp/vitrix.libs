#include "file_upload_handler.h"
#include "file_upload_processor.h"
#include "multipart_body_writer.h"
#include <boost/lexical_cast.hpp>
#include <boost/scope_exit.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <utility>
#include <commons/run_safe.h>
#include <easylogging++.h>

using namespace std::chrono_literals;

spider::file_upload_handler::file_upload_handler(std::string scope_path, boost::asio::io_context &io,
                                                 file_upload_processor &processor, spider::filesystem_path path,
                                                 shared_thread_pool_ptr tp)
   : scoped_app_base(std::move(scope_path)), io_ctx_(io), temp_path_(std::move(path)), processor_(processor),
     thread_pool_(std::move(tp))
{
   if (!thread_pool_)
   {
      thread_pool_ = std::make_shared<thread_pool>(4);
   }
}

spider::file_upload_handler::~file_upload_handler()
{
   thread_pool_->join();
}

namespace
{
   bool remove_output_folder(const spider::filesystem_path &output_folder)
   {
      boost::system::error_code ec;
      boost::filesystem::remove_all(output_folder, ec);

      if (ec)
      {
         LOG(DEBUG) << "unable to delete temp folder: " << output_folder.string() << " with " << ec.message();

         return false;
      }

      return true;
   }
} // namespace
void spider::file_upload_handler::handle_request_internal(spider::request &request,
                                                          spider::request_handler_callback callback)
{
   bool processing_started = false;

   auto &message = request.message();
   auto it = message.find(beast::http::field::content_type);
   if (it != message.end())
   {
      auto header_value = http::parse_content_type(it->value());
      if (header_value.mime_type == "multipart/form-data" && !header_value.boundary.empty())
      {
         auto output_folder = create_request_temp_folder(request);

         auto body_processor =
            std::make_shared<multipart_body_writer>(header_value.boundary, output_folder, request.request_id);
         request.set_body_processor(body_processor);

         request.read_message([this, output_folder, body_processor, &request, callback](const beast::error_code &ec,
            size_t) mutable
            {
               if (!ec)
               {
                  boost::asio::post(*thread_pool_, [this, &request, body_processor, callback, output_folder]() mutable
                  {

                     int result_code = 0;

                     if (body_processor->wait_for_output())
                     {
                        if (!commons::run_safe(
                           [this, &result_code, &request, body_processor]() mutable
                           {
                              result_code = processor_.handle_uploaded_files(request, body_processor->parts);
                           },
                           "handle_uploaded_files"))
                        {
                           result_code = 400;
                        }
                     }
                     else
                     {
                        result_code = 500;
                     }

                     commons::run_safe([&]()
                     {
                        auto status = static_cast<beast::http::status>(result_code);
                        auto response = request.make_response<>(status);
                        response->set(beast::http::field::content_type, "text/plain");
                        response->body() = result_code == 500 ? "upload failed" : "upload completed";
                        request.write_response(response, callback);
                        schedule_folder_removal(output_folder);
                     }, "generate response");

                  });
               }
               else
               {
                  LOG(ERROR) << "error while reading request: " << request.message().method_string() << " " << request.
                     full_path << " " << ec.message();
                  boost::asio::post(*thread_pool_, [this, body_processor, output_folder]() mutable
                  {
                     commons::run_safe([&]()
                     {
                        // make sure that we switch the state to finished
                        auto ec = beast::error_code{};
                        body_processor->finish(ec);

                        body_processor->wait_for_output();
                        schedule_folder_removal(output_folder);
                     }, "cleanup after error");
                  });

                  callback(ec, beast::http::status::unknown);
               }
            });

         processing_started = true;
      }
   }

   if (!processing_started)
   {
      request.return_error({beast::http::status::expectation_failed, "upload processing not started"}, callback);
   }
}

spider::filesystem_path spider::file_upload_handler::create_request_temp_folder(spider::request &req) const
{
   auto address = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&req));
   std::hash<std::thread::id> hasher;
   auto thread_id = static_cast<uint32_t>(hasher(std::this_thread::get_id()));

   auto time = static_cast<std::uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());

   std::this_thread::yield();
   auto random_time = static_cast<std::uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());

   std::uint32_t seed = address ^ thread_id ^ time ^ random_time;

   boost::mt19937 mt{seed};
   boost::uuids::basic_random_generator<boost::mt19937> gen{mt};

   filesystem_path output_folder;

   do
   {
      auto folder_name = boost::lexical_cast<std::string>(gen());
      output_folder = temp_path_ / folder_name.substr(folder_name.find_last_of('-') + 1);
   } while (!create_directories(output_folder));

   return output_folder;
}

void spider::file_upload_handler::schedule_folder_removal(filesystem_path folder)
{
   if (!remove_output_folder(folder) && !io_ctx_.stopped())
   {
      auto timer = std::make_shared<boost::asio::steady_timer>(io_ctx_);
      timer->expires_from_now(1s);
      timer->async_wait([this, timer, folder](boost::system::error_code const &ec) mutable
      {
         if (!ec)
         {
            LOG(DEBUG) << "scheduled folder removal: " << folder.string();
            schedule_folder_removal(folder);
         }
      });
   }
}
