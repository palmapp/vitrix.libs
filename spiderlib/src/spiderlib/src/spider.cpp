#include "spider.h"
#include <commons/cmd_parser.h>
#include <commons/ignore_sig_pipe.h>
#include <commons/logging.h>
#include <commons/run_safe.h>

#include <boost/dll/runtime_symbol_info.hpp>

#include <atomic>
#include <chrono>
#include <easylogging++.h>
using namespace std::chrono_literals;
using namespace spider;
using namespace boost::asio;
using namespace boost::asio::ip;
using namespace commons;

namespace
{

   class real_running_app : public running_app
   {
   public:
      real_running_app(app_configuration configuration, init_callback callback)
         : config_(std::move(configuration)), io_context_(configuration.thread_count),
           acceptor_(make_strand(io_context_)), init_context_({io_context_, config_}),
           tcp_listener_(current_app_, io_context_, acceptor_)
      {
         tcp_listen(config_.tcp_port);

         commons::run_safe([this, &callback]()
         {
            callback(init_context_, current_app_);
         }, "init main function");

         tcp_listener_.begin_async_accept();

         create_io_threads(config_.thread_count);
      }

      ~real_running_app()
      {
         stop();
         wait();
      }

      void stop() override
      {
         io_context_.stop();
      }

      void wait() override
      {
         for (auto &t : threads_)
         {
            if (t.joinable()) t.join();
         }
      }

   private:
      void create_io_threads(int thread_count)
      {
         threads_.reserve(thread_count);

         for (int thread_num = 0; thread_num != thread_count; ++thread_num)
         {
            threads_.emplace_back(
               [this]()
               {
                  while (!io_context_.stopped())
                  {
                     try
                     {
                        io_context_.run();
                     }
                     catch (const std::exception &ex)
                     {
                        LOG(ERROR) << "spider io thread failure : " << ex.what();
                     }
                     catch (...)
                     {
                        LOG(ERROR) << "spider io thread failure : unknown";
                     }
                  }
               });
         }
      }

      void tcp_listen(int port)
      {
         acceptor_.open(tcp::v4());
         acceptor_.set_option(spider::reuse_port_and_address(true));
         if (config_.bind_loop_back_only)
         {

            acceptor_.bind(tcp::endpoint{ip::address_v4::loopback(), static_cast<unsigned short>(port)});
         }
         else if (!config_.bind_ip_addresses.empty())
         {
            for (auto &ip : config_.bind_ip_addresses)
            {
               acceptor_.bind(tcp::endpoint{ip, static_cast<unsigned short>(port)});
            }
         }
         else
         {
            acceptor_.bind(tcp::endpoint{tcp::v4(), static_cast<unsigned short>(port)});
         }
         acceptor_.listen();
      }

      std::vector<std::thread> threads_;
      app_configuration config_;
      io_context io_context_;
      tcp::acceptor acceptor_;
      spider::app current_app_;
      init_context init_context_;
      app_tcp_listener tcp_listener_;
   };

} // namespace
std::unique_ptr<running_app> spider::run_spider_app(std::string const &app_name, command_line_args const &args,
                                                    init_callback callback)
{
   ignore_sig_pipe();

   string skip_logging;
   cmd::try_get_arg(skip_logging, "-skip-logging-configuration", args.argc, args.argv);

   if (skip_logging != "true")
   {
      el::Configurations disabledLoggers;
      disabledLoggers.setToDefault();
      disabledLoggers.setGlobally(el::ConfigurationType::Enabled, "false");
      el::Loggers::setDefaultConfigurations(disabledLoggers);
   }

   auto application_path = boost::dll::program_location().parent_path();
   auto configuration_path = application_path / "env.properties";
   auto base_path = application_path;

   cmd::try_get_arg(base_path, "-b", args.argc, args.argv);
   cmd::try_get_arg(configuration_path, "-c", args.argc, args.argv);

   spider::init_loggers();

   if (skip_logging != "true")
   {
      init_easylogging(args.argc, args.argv, app_name.c_str(), configuration_path.parent_path(), nullptr, true);
   }

   std::string thread_count_arg;
   cmd::try_get_arg(thread_count_arg, "-t", args.argc, args.argv);
   int thread_count = std::max(std::atoi(thread_count_arg.c_str()), 1);

   std::string port_arg;
   cmd::try_get_arg(port_arg, "-p", args.argc, args.argv);
   int port = std::atoi(port_arg.c_str());
   if (port == 0) port = 10000;

   if (port > 65535)
   {
      LOG(FATAL) << "http port number is out of range " << port << " max allowed port is 65535";
   }

   LOG(INFO) << "starting " << app_name << "\n\tconfig file = " << configuration_path.string()
      << "\n\tbase dir = " << base_path.string() << "\n\tapp dir = " << application_path.string()
      << "\n\tthread count = " << thread_count << "\n\thttp port = " << port;

   return std::make_unique<real_running_app>(
      app_configuration{app_name, configuration_path, application_path, base_path, thread_count, port, false, {}},
      callback);
}

std::unique_ptr<running_app> spider::run_spider_app(std::string const &app_name, filesystem_path path, int thread_count,
                                                    init_callback callback)
{
   ignore_sig_pipe();

   el::Configurations disabledLoggers;
   disabledLoggers.setToDefault();
   disabledLoggers.setGlobally(el::ConfigurationType::Enabled, "false");
   el::Loggers::setDefaultConfigurations(disabledLoggers);

   auto configuration_path = path / "env.properties";

   spider::init_loggers();
   init_easylogging(0, {}, app_name.c_str(), "");

   int port = 10000;

   LOG(INFO) << "starting " << app_name << "\n\tconfig file = " << configuration_path.string()
      << "\n\tbase dir = " << path.string() << "\n\tthread count = " << thread_count
      << "\n\thttp port = " << port;

   return std::make_unique<real_running_app>(
      app_configuration{app_name, configuration_path, path, path, thread_count, port, false, {}}, callback);
}

std::unique_ptr<running_app> spider::run_spider_app_additional(app_configuration cfg, init_callback callback)
{
   return std::make_unique<real_running_app>(std::move(cfg), callback);
}

running_app::~running_app()
{
   // do nothing
}
