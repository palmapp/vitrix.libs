#include "http_byte_ranges.h"
#include <boost/config/warning_disable.hpp>
#include <boost/fusion/include/std_pair.hpp>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/spirit/home/x3.hpp>

#include <vector>

namespace x3 = boost::spirit::x3;
const auto integer_parser = x3::uint_parser<spider::byte_range_int_type, 10, 1, -1>();

using x3::char_;
using x3::phrase_parse;
using x3::standard::space;

BOOST_FUSION_ADAPT_STRUCT(spider::byte_range, offset_begin, offset_end)

bool spider::try_parse_byte_ranges(byte_ranges &out, const string_view &header_value)
{
   const auto my_space = char_(" \t");
   auto it = header_value.begin();
   const auto end = header_value.end();

   const auto result =
       phrase_parse(it, end, "bytes=" >> ((integer_parser >> '-' >> -integer_parser) % ','), my_space, out);

   if (it == end)
   {
      return result;
   }
   return false;
}