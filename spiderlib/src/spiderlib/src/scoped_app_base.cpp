#include "scoped_app_base.h"
#include <boost/algorithm/string.hpp>
#include <boost/scope_exit.hpp>
#include <commons/make_string.h>

spider::scoped_app_base::scoped_app_base(std::string scope_path) : scope_path_(std::move(scope_path)) {}

spider::scoped_app_base::request_handler *spider::scoped_app_base::find_request_handler(request &req)
{
   if (is_current_scoped_path(req.path))
   {
      return this;
   }

   return nullptr;
}

bool spider::scoped_app_base::is_current_scoped_path(string_view scope)
{
   return boost::starts_with(scope, scope_path_);
}

spider::string_view spider::scoped_app_base::get_current_scoped_path(string_view scope)
{
   if (scope.size() < scope_path_.size())
   {
      throw exception(commons::make_string(scope_path_, " is not a base class of scope ", scope));
   }

   if (scope_path_[scope_path_.size() - 1] == '/')
      return scope.substr(scope_path_.size() - 1);
   else
      return scope.substr(scope_path_.size());
}

void spider::scoped_app_base::handle_request(request &req, request_handler_callback callback)
{
   req.path = get_current_scoped_path(req.path);
   handle_request_internal(req, callback);
}
