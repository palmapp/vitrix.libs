#include "string_body_writer.h"
#include <boost/asio/buffer.hpp>
#include <boost/beast/http/error.hpp>

void spider::string_body_writer::init(boost::optional<std::uint64_t> const &length, boost::system::error_code &ec)
{
   if (length)
   {
      if (*length > (std::numeric_limits<std::size_t>::max)())
      {
         ec = beast::http::error::buffer_overflow;
         return;
      }
      try
      {
         body.reserve(static_cast<std::size_t>(*length));
      }
      catch (std::exception const &)
      {
         ec = beast::http::error::buffer_overflow;
         return;
      }
   }
   ec.assign(0, ec.category());
}

std::size_t spider::string_body_writer::put(request_buffer_type const &b, boost::system::error_code &ec)
{
   auto const extra = b.size();
   auto const size = body.size();
   try
   {
      body.resize(size + extra);
   }
   catch (std::exception const &)
   {
      ec = beast::http::error::buffer_overflow;
      return 0;
   }
   ec.assign(0, ec.category());
   char *dest = &body[size];

   using boost::asio::buffer_cast;
   auto const len = boost::asio::buffer_size(b);
   std::char_traits<char>::copy(dest, buffer_cast<char const *>(b), len);

   return extra;
}

void spider::string_body_writer::finish(boost::system::error_code &ec)
{
   ec.assign(0, ec.category());
}
