#include "file_template_loader.h"
#include <atomic>
#include <boost/algorithm/string.hpp>
#include <commons/file_system.h>
#include <commons/run_safe.h>

#include <easylogging++.h>

using namespace std::string_literals;

using namespace spider;

const std::string file_template_loader::fs_separtor = commons::fs::get_separator();

file_template_loader::file_template_loader(filesystem_path base_dir)
   : base_dir_(base_dir)
{
   cache_ = std::make_shared<cache_map>();
}

std::string file_template_loader::load_template(request &req, const std::string &name)
{
   return load_template(get_template_path(req, name));
}

filesystem_path file_template_loader::get_template_path(request &req, const std::string &name) const
{
   const std::string relative_path = boost::replace_all_copy(name, "::"s, fs_separtor);

   return base_dir_ / relative_path;
}

std::string file_template_loader::load_template(filesystem_path path)
{
   const cache_ptr cache = cache_.load();
   if (const auto it = cache->find(path.string()); it != cache->cend())
   {
      boost::system::error_code ec;
      time_t last_write_time = boost::filesystem::last_write_time(path, ec);
      if (ec)
      {
         LOG(WARNING) << "unable to load last write time of template : " << path.string() << " : " << ec.message();
      }
      else if (last_write_time != it->second.last_change)
      {
         return load_and_cache_template(path);
      }

      return it->second.content;
   }
   else
   {
      return load_and_cache_template(path);
   }
}

std::string file_template_loader::load_and_cache_template(filesystem_path const &path)
{
   std::string content;
   commons::run_safe(
      [&]()
      {
         time_t last_write_time = boost::filesystem::last_write_time(path);
         content = commons::read_file(path);

         cache_ptr cache;
         cache_ptr new_cache;
         do
         {
            cache = std::atomic_load(&cache_);
            new_cache = std::make_shared<cache_map>(*cache);
            new_cache->operator[](path.string()) = {last_write_time, content};
         } while (!std::atomic_compare_exchange_strong(&cache_, &cache, new_cache));
      },
      "template loading");

   return content;
}
