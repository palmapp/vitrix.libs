#include "request_handler.h"

void spider::request_body::reader::init(spider::optional<std::uint64_t> const &length, beast::error_code &ec)
{
   if (body_) body_->init(length, ec);
}

std::size_t spider::request_body::reader::put(request_buffer_type const &buffers, beast::error_code &ec)
{
   if (body_)
   {
      return body_->put(buffers, ec);
   }
   return boost::asio::buffer_size(buffers);
}

void spider::request_body::reader::finish(beast::error_code &ec)
{
   if (body_) body_->finish(ec);
}
