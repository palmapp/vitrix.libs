#include "default_redirect_handler.h"

spider::default_redirect_handler::default_redirect_handler(std::string scope_path, std::string redirect_url)
    : scoped_app_base(std::move(scope_path)), redirect_url_(redirect_url)
{
}

void spider::default_redirect_handler::handle_request_internal(request &request, request_handler_callback callback)
{
   request.return_redirect(redirect_url_, callback);
}
