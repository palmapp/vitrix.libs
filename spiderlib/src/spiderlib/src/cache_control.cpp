#include "cache_control.h"

spider::cache_control spider::cache_control::make_immutable(int max_age)
{
   cache_control response;
   response.cache_type = immutable_resource;
   response.max_age = max_age;
   return response;
}

spider::cache_control spider::cache_control::make_cache_public(int max_age)
{
   cache_control response;
   response.cache_type = cache_public;
   response.max_age = max_age;
   return response;
}

spider::cache_control spider::cache_control::make_cache_private(int max_age)
{
   cache_control response;
   response.cache_type = cache_private;
   response.max_age = max_age;
   return response;
}

spider::cache_control spider::cache_control::make_no_cache()
{
   cache_control response;
   response.cache_type = no_cache;

   return response;
}