#include "websocket_handler.h"
#include <easylogging++.h>

namespace
{
   static std::atomic<spider::websocket_id> connection_counter;
}

spider::websocket_app::websocket_app(std::string scope_path, spider::websocket_manager &websocket_manager)
    : scoped_app_base(std::move(scope_path)), websocket_manager_(websocket_manager)
{
}

spider::request_handler *spider::websocket_app::find_request_handler(request &req)
{
   spider::request_handler *result = scoped_app_base::find_request_handler(req);
   if (result && beast::websocket::is_upgrade(req.message()))
   {
      return this;
   }
   return nullptr;
}

void spider::websocket_app::handle_request_internal(request &request, request_handler_callback callback)
{
   auto wc = std::make_shared<websocket_connection>(
       connection_counter++, beast::websocket::stream<socket_stream>(std::move(request.socket)), request);
   websocket_manager_.handle_new_connection(std::move(wc), callback);
}
