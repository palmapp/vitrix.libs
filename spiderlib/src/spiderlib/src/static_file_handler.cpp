#include "static_file_handler.h"

#include "http_date.h"
#include "io/async_file.h"
#include <commons/make_string.h>

#include <base64/base64.h>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <serialization/url.h>

using namespace beast::http;
using namespace boost::asio;

namespace
{
   std::string generate_unsafe_uiid()
   {
      boost::mt19937 mt{static_cast<std::uint32_t>(std::chrono::system_clock::now().time_since_epoch().count())};
      boost::uuids::basic_random_generator<boost::mt19937> gen{mt};
      return boost::lexical_cast<std::string>(gen());
   }

   std::string get_extension(const spider::filesystem_path &path)
   {
      if (path.has_extension())
      {
         auto ext = path.extension().string();
         return ext.substr(1);
      }

      return {};
   }

   bool try_get_if_modified_since(boost::posix_time::ptime &result, spider::request &req)
   {
      auto &msg = req.message();
      auto value = msg.find(field::if_modified_since);
      if (value != msg.end())
      {
         auto value_view = value->value();
         return spider::try_parse_http_date(result, {value_view.data(), value_view.size()});
      }
      return false;
   }

} // namespace

namespace spider
{
   struct stream_funct
   {
      virtual ~stream_funct() = default;
      virtual void operator()(socket_stream &, const byte_range &, io_callback callback) = 0;
   };

   struct file_stream_funct : public stream_funct
   {
      std::shared_ptr<io::async_file> file_;

      file_stream_funct(std::shared_ptr<io::async_file> file)
         : file_(std::move(file))
      {
      }

      void operator()(socket_stream &socket, const byte_range &range, io_callback callback) final
      {
         auto file = file_;
         if (range.offset_end)
         {
            auto size = range.offset_end.value() + 1 - range.offset_begin;

            file_->stream_to_output(
               socket, [file, callback](const boost::system::error_code &ec, size_t len)
               {
                  callback(ec, len);
               },
               range.offset_begin, size);
         }
         else
         {
            file_->stream_to_output(
               socket, [file, callback](const boost::system::error_code &ec, size_t len)
               {
                  callback(ec, len);
               },
               range.offset_begin);
         }
      }
   };

   struct string_stream_funct : public stream_funct
   {
      std::shared_ptr<spider::file_cache::cached_file> content_;

      string_stream_funct(std::shared_ptr<spider::file_cache::cached_file> content)
         : content_(std::move(content))
      {
      }

      void operator()(socket_stream &socket, const byte_range &range, io_callback callback) final
      {
         auto content = content_;
         if (range.offset_end)
         {
            auto size = range.offset_end.value() + 1 - range.offset_begin;
            boost::asio::async_write(socket, boost::asio::buffer(content_->content.data() + range.offset_begin, size),
                                     [callback, content](const boost::system::error_code &error, std::size_t len)
                                     {
                                        callback(error, len);
                                     });
         }
         else
         {
            boost::asio::async_write(socket,
                                     boost::asio::buffer(content_->content.data() + range.offset_begin,
                                                         content_->content.size() - range.offset_begin),
                                     [callback, content](const boost::system::error_code &error, std::size_t len)
                                     {
                                        callback(error, len);
                                     });
         }
      }
   };
} // namespace spider

spider::static_file_handler::static_file_handler(std::string path, filesystem_path base_path,
                                                 cache_control cache_settings, mime_type_map mime_types)
   : scoped_app_base(std::move(path)), base_path_(std::move(base_path)),
     mime_types_(std::move(mime_types)), cache_settings_(cache_settings)
{
}

bool spider::static_file_handler::is_resource_modified(spider::request &request,
                                                       const boost::posix_time::ptime file_modification,
                                                       const std::string &etag)
{
   bool not_modified = false;
   auto value = request.message().find(field::if_none_match);
   if (value != request.message().end())
   {
      not_modified = etag == value->value();
   }
   else
   {
      boost::posix_time::ptime last_modification;
      if (try_get_if_modified_since(last_modification, request))
      {
         not_modified = file_modification <= last_modification;
      }
   }

   return not_modified;
}

spider::optional<std::vector<spider::byte_range>>
spider::static_file_handler::try_get_byte_ranges(spider::request &request) const
{
   auto it = request.message().find(field::range);
   if (it != request.message().end())
   {
      byte_ranges ranges;
      if (try_parse_byte_ranges(ranges, to_string_view(it->value())))
      {
         return {std::move(ranges)};
      }
   }

   return {};
}

bool spider::static_file_handler::validate_byte_ranges(size_t size, const byte_ranges &ranges)
{
   for (auto &range : ranges)
   {
      if (range.offset_begin >= size)
      {
         return false;
      }

      if (range.offset_end)
      {
         if (range.offset_end.value() >= size) return false;

         if (range.offset_end.value() < range.offset_begin) return false;
      }
   }

   return true;
}

namespace
{
   struct range_write_op : std::enable_shared_from_this<range_write_op>
   {
      std::vector<std::string> boundaries_;
      spider::byte_ranges unboxed_byte_ranges_;
      boost::asio::ip::tcp::socket &socket_;
      std::unique_ptr<spider::stream_funct> write_funct_;
      spider::request_handler_callback callback_;
      beast::http::status status_;
      std::size_t i = 0;

      range_write_op(std::vector<std::string> boundaries, spider::byte_ranges unboxed_byte_ranges,
                     boost::asio::ip::tcp::socket &socket, std::unique_ptr<spider::stream_funct> write_funct,
                     spider::request_handler_callback callback, beast::http::status status)
         : boundaries_(std::move(boundaries)), unboxed_byte_ranges_(std::move(unboxed_byte_ranges)), socket_(socket),
           write_funct_(std::move(write_funct)), callback_(callback), status_(status)
      {
      }

      void start()
      {
         auto self = shared_from_this();

         if (i < unboxed_byte_ranges_.size())
         {
            auto write_range = [this, self]()
            {
               auto &range = unboxed_byte_ranges_[i];
               auto &write_funct = *write_funct_;

               write_funct(socket_, range,
                           [this, self](const boost::system::error_code &ec, size_t)
                           {
                              if (!ec)
                              {
                                 ++i;
                                 start();
                              }
                              else
                              {
                                 callback_(ec, status::unknown);
                              }
                           });
            };

            if (!boundaries_.empty())
            {
               auto &separator = boundaries_[i];

               boost::asio::async_write(socket_, buffer(separator.c_str(), separator.size()),
                                        [this, self, write_range](const boost::system::error_code &ec, std::size_t)
                                        {
                                           if (!ec)
                                           {
                                              write_range();
                                           }
                                           else
                                           {
                                              callback_(ec, status::unknown);
                                           }
                                        });
            }
            else
            {
               write_range();
            }
         }
         else
         {
            if (!boundaries_.empty() && i == unboxed_byte_ranges_.size())
            {
               // boundaries.size() == unboxed_byte_ranges.size() + 1
               auto &separator = boundaries_[i];
               boost::asio::async_write(socket_, boost::asio::buffer(separator.c_str(), separator.size()),
                                        [this, self](const boost::system::error_code &ec, std::size_t)
                                        {
                                           if (!ec)
                                           {
                                              callback_(ec, status_);
                                           }
                                           else
                                           {
                                              callback_(ec, status::unknown);
                                           }
                                        });
            }
            else
            {
               boost::system::error_code ec;
               callback_(ec, status_);
            }
         }
      }
   };

} // namespace

void spider::static_file_handler::stream_byte_ranges(
   request &req, const std::string &mime_type, const std::string &etag,
   const boost::posix_time::ptime &file_modification, byte_ranges unboxed_byte_ranges, size_t file_size,
   std::unique_ptr<stream_funct> write_funct, cache_control const &cache_settings, request_handler_callback callback)
{
   if (static_file_handler::validate_byte_ranges(file_size, unboxed_byte_ranges))
   {
      auto status = status::partial_content;
      auto resp = req.make_response<empty_body>(status);

      std::string boundary;
      std::vector<std::string> boundaries;

      if (unboxed_byte_ranges.size() > 1)
      {
         boundary = generate_unsafe_uiid();
         resp->set(field::content_type, commons::make_string("multipart/byteranges; boundary=", boundary));
         boundaries.reserve(unboxed_byte_ranges.size() + 1);

         size_t total_size = 0;
         for (auto &range : unboxed_byte_ranges)
         {
            auto offset_end = range.offset_end.value_or(file_size - 1);
            auto size = offset_end + 1 - range.offset_begin;

            const char *prefix = "--";
            if (boundaries.size() != 0)
            {
               prefix = "\r\n--";
            }

            auto separator =
               commons::make_string(prefix, boundary, "\r\nContent-Type:", mime_type, "\r\nContent-Range: bytes ",
                                    range.offset_begin, "-", offset_end, "/", file_size, "\r\n\r\n");
            total_size += size + separator.size();

            boundaries.push_back(std::move(separator));
         }
         auto separator = commons::make_string("\r\n--", boundary, "--\r\n");
         total_size += separator.size();
         boundaries.push_back(std::move(separator));

         resp->set(field::content_length, commons::make_string(total_size));
      }
      else
      {
         resp->set(field::content_type, mime_type);
         resp->set(field::content_range,
                   commons::make_string("bytes ", unboxed_byte_ranges[0].offset_begin, "-",
                                        unboxed_byte_ranges[0].offset_end.value_or(file_size - 1), "/", file_size));
         resp->set(field::content_length,
                   commons::make_string(unboxed_byte_ranges[0].offset_end.value_or(file_size - 1) + 1 -
                                        unboxed_byte_ranges[0].offset_begin));
      }

      resp->set(field::etag, etag);
      resp->set(field::last_modified, format_http_date(file_modification));
      cache_settings.apply_caching_headers(*resp);

      auto range_write_inst = std::make_shared<range_write_op>(std::move(boundaries), std::move(unboxed_byte_ranges),
                                                               req.socket, std::move(write_funct), callback, status);

      beast::http::async_write(req.socket, *resp,
                               [resp, range_write_inst, callback](const beast::error_code &ec, size_t)
                               {
                                  if (!ec)
                                  {
                                     range_write_inst->start();
                                  }
                                  else
                                  {
                                     callback(ec, beast::http::status::unknown);
                                  }
                               });
   }
   else
   {
      req.generator.return_error(
         req,
         {beast::http::status::range_not_satisfiable, "range_not_satisfiable", "correct your request and continue"},
         callback);
   }
}

void spider::static_file_handler::stream_from_cache(spider::request &req, spider::request_handler_callback callback,
                                                    verb method, const std::string &mime_type,
                                                    spider::optional<std::vector<spider::byte_range>> byte_ranges,
                                                    std::shared_ptr<spider::file_cache::cached_file> cached_file)
{
   if (byte_ranges && !byte_ranges.value().empty())
   {
      auto &unboxed_byte_ranges = byte_ranges.value();

      std::unique_ptr<stream_funct> stream{new string_stream_funct{std::move(cached_file)}};

      stream_byte_ranges(req, mime_type, cached_file->etag, cached_file->last_modfied, std::move(unboxed_byte_ranges),
                         cached_file->content.size(), std::move(stream), cache_settings_, callback);
   }
   else
   {
      bool not_modified = is_resource_modified(req, cached_file->last_modfied, cached_file->etag);
      auto status = not_modified ? status::not_modified : status::ok;
      auto response = req.make_response<empty_body>(status);
      response->set(field::content_type, mime_type);

      auto content_length_str = boost::lexical_cast<std::string>(cached_file->content.size());
      response->set(field::content_length, content_length_str);
      response->set(field::etag, cached_file->etag);
      response->set(field::last_modified, cached_file->last_modified_formatted);
      cache_settings_.apply_caching_headers(*response);

      beast::http::async_write(
         req.socket, *response,
         [&req, response, cached_file, callback, status, method, not_modified](const beast::error_code &ec, size_t)
         {
            if (!ec)
            {
               if (method == verb::get || !not_modified)
               {
                  boost::asio::async_write(
                     req.socket, boost::asio::buffer(cached_file->content.data(), cached_file->content.size()),
                     [callback, status](const boost::system::error_code &ec, std::size_t)
                     {
                        if (!ec)
                        {
                           callback(ec, status);
                        }
                        else
                        {
                           callback(ec, beast::http::status::unknown);
                        }
                     });
               }
               else
               {
                  callback(ec, status);
               }
            }
            else
            {
               callback(ec, beast::http::status::unknown);
            }
         });
   }
}

void spider::static_file_handler::stream_from_filesystem(spider::request &req,
                                                         spider::request_handler_callback callback, verb method,
                                                         const filesystem_path &absolute_path, std::string mime_type,
                                                         spider::optional<std::string> response_file_name,
                                                         cache_control const &cache_settings,
                                                         spider::optional<std::vector<spider::byte_range>> byte_ranges)
{

   auto file_stream = std::make_shared<io::async_file>(req.socket.get_executor());

   if (io::async_file::try_open(*file_stream, absolute_path))
   {
      auto file_modification = boost::posix_time::from_time_t(last_write_time(absolute_path));
      auto etag = format_etag_from_time_point(file_modification);

      bool not_modified = static_file_handler::is_resource_modified(req, file_modification, etag);
      auto status = not_modified ? status::not_modified : status::ok;
      auto file_size = file_stream->size();

      if (byte_ranges && !byte_ranges.value().empty())
      {
         auto &unboxed_byte_ranges = byte_ranges.value();
         std::unique_ptr<stream_funct> stream{new file_stream_funct{std::move(file_stream)}};
         static_file_handler::stream_byte_ranges(req, mime_type, etag, file_modification,
                                                 std::move(unboxed_byte_ranges), file_size, std::move(stream),
                                                 cache_settings, callback);
      }
      else
      {
         auto response = req.make_response<empty_body>(status);
         response->set(field::content_type, mime_type);

         auto file_size_str = boost::lexical_cast<std::string>(file_size);
         response->set(field::content_length, file_size_str);
         response->set(field::etag, etag);
         response->set(field::last_modified, format_http_date(file_modification));

         if (response_file_name.has_value() && !response_file_name->empty())
         {
            response->set(field::content_disposition,
                          commons::make_string("attachment; filename=\"", response_file_name.value(), "\""));
         }

         cache_settings.apply_caching_headers(*response);

         beast::http::async_write(
            req.socket, *response,
            [&req, status, response, file_stream, callback, method, not_modified](const beast::error_code &ec, size_t)
            {
               if (!ec)
               {
                  if (method == verb::get || !not_modified)
                  {
                     file_stream->stream_to_output(
                        req.socket,
                        [file_stream, status, callback](const boost::system::error_code &ec, size_t)
                        {
                           if (!ec)
                           {
                              callback(ec, status);
                           }
                           else
                           {
                              callback(ec, beast::http::status::unknown);
                           }
                        });
                  }
                  else
                  {
                     callback(ec, status);
                  }
               }
               else
               {
                  callback(ec, beast::http::status::unknown);
               }
            });
      }
   }
   else
   {
      req.return_error({beast::http::status::not_found, "not found", "file not found on this server"}, callback);
   }
}

void spider::static_file_handler::handle_request_internal(request &req, request_handler_callback callback)
{
   auto method = req.message().method();

   if (method == verb::get || method == verb::head)
   {
      auto relative_path = decode_safe_path(req);
      auto absolute_path = get_base_path(req) / relative_path;
      auto mime_type = get_mime_type_for_extension(get_extension(absolute_path));
      auto byte_ranges = try_get_byte_ranges(req);

      if (cache)
      {
         auto cached_file = cache->get_file(absolute_path);
         if (cached_file)
         {
            stream_from_cache(req, callback, method, mime_type, std::move(byte_ranges), std::move(cached_file));
            return;
         }
      }

      static_file_handler::stream_from_filesystem(req, callback, method, absolute_path, mime_type, {}, cache_settings_,
                                                  std::move(byte_ranges));
   }
   else
   {
      beast::error_code ec;
      req.generator.return_error(req,
                                 {beast::http::status::method_not_allowed, "method_not_allowed",
                                  "you can only use GET or HEAD to request this resource"},
                                 callback);
   }
}

spider::filesystem_path spider::static_file_handler::get_base_path(request &) const
{
   return base_path_;
}

std::string spider::static_file_handler::decode_safe_path(request &r) const
{
   auto &view = r.path;
   std::string result;
   if (url::try_decode(result, view.begin(), view.end()))
   {
      if (result.find("..") != std::string::npos)
      {
         throw exception("url in invalid format");
      }

      return result;
   }
   throw exception("url in invalid format");
}

const std::string &spider::static_file_handler::get_mime_type_for_extension(const std::string &ext) const
{
   auto it = mime_types_.find(ext);
   if (it == mime_types_.end())
   {
      return mime_types_.find("exe")->second;
   }
   else
   {
      return it->second;
   }
}

spider::mime_type_map spider::get_default_mime_types()
{
   return {{"exe", "application/binary"},
           {"bin", "application/binary"},
           {"msi", "application/binary"},
           {"js", "text/javascript"},
           {"css", "text/css"},
           {"txt", "text/plain"},
           {"png", "image/png"},
           {"jpg", "image/jpeg"},
           {"jpeg", "image/jpeg"},
           {"jpe", "image/jpeg"},
           {"woff", "application/font-woff"},
           {"otf", "font/opentype"},
           {"eot", "application/vnd.ms-fontobject"},
           {"ttf", "font/truetype"},
           {"woff", "font/woff2"},
           {"svg", "image/svg+xml"},
           {"xml", "text/xml"},
           {"html", "text/html"},
           {"htm", "text/html"}};
}
