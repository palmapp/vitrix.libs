#pragma once

#include "scoped_app_base.h"

namespace spider
{
   class default_redirect_handler : public scoped_app_base
   {
    public:
      default_redirect_handler(std::string scope_path, std::string redirect_url);

    protected:
      void handle_request_internal(request &request, request_handler_callback callback) override;
      std::string redirect_url_;
   };

} // namespace spider