#pragma once
#include "http_header_parsers.h"
#include "multipart.h"
#include "request_handler.h"
#include <boost/optional/optional.hpp>
#include <boost/system/error_code.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <string>

#include "io/async_files_writer.h"
#include <multipart/MultipartReader.h>

namespace spider
{
   class multipart_body_writer : public request_body_processor
   {
    public:
      multipart_body_writer(const std::string &boundary, const spider::filesystem_path &temp_path,
                            std::int64_t request_id);

      void init(boost::optional<std::uint64_t> const &length, boost::system::error_code &ec) final;

      std::size_t put(request_buffer_type const &buffers, boost::system::error_code &ec) final;
      void finish(boost::system::error_code &ec) final;
      bool wait_for_output() const;

      std::vector<part> parts;

    private:
      MultipartReader multipart_reader_;

      void create_part(MultipartHeaders const &headers);

      spider::filesystem_path temp_path_;
      boost::mt19937 mt_;
      boost::uuids::basic_random_generator<boost::mt19937> gen_;
      std::size_t content_length_ = 0;

      enum class state : int
      {
         before_part_begin,
         part,
         finished,
         error_on_part_begin_callback = -1,
         error_on_part_data = -2,
         error_on_part_end = -3,
         error_on_end = -4,
         error_invalid_filename = -5
      };

      state state_ = state::before_part_begin;
      io::async_files_writer output_;

      static void on_part_begin_callback(const MultipartHeaders &headers, void *userData);
      static void on_part_data(const char *buffer, size_t size, void *userData);
      static void on_part_end(void *userData);
      static void on_end(void *userData);
   };
} // namespace spider
