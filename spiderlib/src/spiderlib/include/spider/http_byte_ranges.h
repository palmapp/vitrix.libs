#pragma once

#include "std_types.h"

namespace spider
{

   using byte_range_int_type = std::uint64_t;

   struct byte_range
   {
      byte_range_int_type offset_begin;
      optional<byte_range_int_type> offset_end;
   };

   using byte_ranges = std::vector<byte_range>;
   bool try_parse_byte_ranges(byte_ranges &out, const string_view &header_value);


} // namespace spider