#pragma once
#include "buffer_pool.h"
#include <array>
#include <commons/active_object.h>
#include <cstdint>
#include <deque>
#include <posix/posix.h>

namespace spider::io
{
   struct async_write_message
   {
      enum class msg_type
      {
         write,
         open,
         close,
         done
      };
      msg_type type;

      buffer_pool::buffer_ptr buffer;
      std::size_t len;
   };

   class async_files_writer : private commons::active_object<async_write_message>
   {
    public:
      async_files_writer();
      ~async_files_writer() override;

      void open(const posix::path& file);
      void write(posix::string_view data);
      void close();
      void finish();

      bool wait_till_finishes() const;

    protected:
      void on_message(async_write_message &message) final;
      void on_exception(const async_write_message &, const std::exception &) noexcept final;
      void on_unknown_exception(const async_write_message &) noexcept final;

    private:
      buffer_pool::buffer_ptr buffer_;
      std::size_t buffered_ = 0;
      bool opened_file_ = false;

      posix::file_handle output_file_;
      enum class proc_state
      {
         in_progress,
         done,
         failed,
      };

      proc_state state_;
      mutable std::mutex mutex_;
      mutable std::condition_variable wait_cnd_variable_;

      void flush_write();
   };

} // namespace spider::io