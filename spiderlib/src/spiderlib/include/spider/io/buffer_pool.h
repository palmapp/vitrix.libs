#pragma once
#include <boost/pool/singleton_pool.hpp>
#include <memory>
#include <variant>

namespace spider::io::buffer_pool
{
   constexpr std::size_t buffer_capacity()
   {
      return 4096;
   }

   void free_buffer(char *buffer);
   struct pooled_buffer_deleter
   {
      inline void operator()(char *ptr)
      {
         free_buffer(ptr);
      }
   };

   struct system_buffer_deleter
   {
      inline void operator()(char *ptr)
      {
         delete[] ptr;
      }
   };

   using pooled_buffer_ptr = std::unique_ptr<char, pooled_buffer_deleter>;
   using system_buffer_ptr = std::unique_ptr<char, system_buffer_deleter>;

   struct buffer_ptr
   {
    public:
      buffer_ptr() noexcept;
      buffer_ptr(pooled_buffer_ptr pooled_ptr) noexcept;
      buffer_ptr(system_buffer_ptr system_ptr) noexcept;

      bool is_empty() const noexcept;

      enum class allocation_type
      {
         none,
         pooled,
         system
      };
      allocation_type get_allocation_type() const noexcept;

      buffer_ptr(buffer_ptr &&other);
      void operator=(buffer_ptr &&other);

      char *get() const;
      void reset();

    private:
      struct empty_tag
      {
      };

      std::variant<empty_tag, pooled_buffer_ptr, system_buffer_ptr> holder_;
   };

   buffer_ptr allocate_buffer();
} // namespace spider::io::buffer_pool
