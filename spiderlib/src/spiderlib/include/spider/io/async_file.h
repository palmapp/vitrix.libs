#pragma once
#include "../std_types.h"

namespace spider
{
   namespace io
   {
      using executor = boost::asio::any_io_executor;
      class async_file
      {
       public:
         const static std::size_t npos = std::numeric_limits<std::size_t>::max();
         const static std::size_t default_buffer_size = 0x1000;

         static bool try_open(async_file &file, const filesystem_path &path,
                              std::size_t read_buffer_size = default_buffer_size);

         async_file(executor service);

         async_file(executor service, const filesystem_path &path, std::size_t read_buffer_size = default_buffer_size);
         async_file(async_file &&other) noexcept;

         async_file &operator=(async_file &&) noexcept;

         std::size_t size();

         void stream_to_output(boost::asio::ip::tcp::socket &stream, io_callback callback, std::size_t offset = 0,
                               std::size_t len = npos);

       private:
         random_access_stream handle_;
         std::size_t buffer_size_;
      };
   } // namespace io
} // namespace spider
