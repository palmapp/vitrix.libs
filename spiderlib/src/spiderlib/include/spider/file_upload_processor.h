#pragma once
#include "multipart.h"
#include "std_types.h"
#include <string>

namespace spider
{
struct request;
}

namespace spider
{
class file_upload_processor
{
 public:
   virtual ~file_upload_processor() = default;

   // uploaded files are deleted automatically after this function call, so they should be moved to
   // a new location if preservation is necessary
   virtual int handle_uploaded_files(spider::request &request, const std::vector<part> &parts) = 0;
};

} // namespace spider