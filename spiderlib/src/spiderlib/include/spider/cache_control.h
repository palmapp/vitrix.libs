#pragma once
#include <boost/beast/http.hpp>
#include <commons/make_string.h>

namespace beast
{
   using namespace boost::beast;
}
namespace spider
{
   struct cache_control
   {
      enum type
      {
         no_cache,
         immutable_resource = 0x02,

         cache_public = 0x04,
         cache_private = 0x08,

      };

      type cache_type = no_cache;
      int max_age = 0;

      static cache_control make_immutable(int max_age = 3600 * 24 * 365);
      static cache_control make_cache_public(int max_age = 3600);
      static cache_control make_cache_private(int max_age = 3600);
      static cache_control make_no_cache();

      template <class Response>
      void apply_caching_headers(Response &response) const
      {
         if (cache_type == no_cache)
         {
            response.set(beast::http::field::cache_control, "no-cache, no-store, must-revalidate");
         }
         else
         {
            std::string value;
            if ((cache_type & immutable_resource) != 0)
            {
               if ((cache_type & cache_private) != 0)
                  value = commons::make_string("private, max-age=", max_age, ", imutable");
               else
               {
                  value = commons::make_string("max-age=", max_age, ", immutable");
               }
            }
            else if ((cache_type & cache_private) != 0)
            {
               value = commons::make_string("private, max-age=", max_age);
            }
            else
            {
               value = commons::make_string("public, max-age=", max_age);
            }

            response.set(beast::http::field::cache_control, value);
         }
      }
   };

} // namespace spider
