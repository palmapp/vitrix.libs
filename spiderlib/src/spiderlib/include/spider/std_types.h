#pragma once
#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/core/string.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/websocket.hpp>

#include <serialization/std_types.h>

namespace beast
{
   using namespace boost::beast;
}

namespace spider
{
   template <class T>
   using optional = boost::optional<T>;
   using io_callback = std::function<void(const boost::system::error_code &, std::size_t)>;

#ifdef BOOST_WINDOWS_API
   using random_access_stream = boost::asio::windows::random_access_handle;
   using reuse_port_and_address = boost::asio::socket_base::reuse_address;
#else
   using random_access_stream = boost::asio::posix::stream_descriptor;
   using reuse_port_and_address =
       boost::asio::detail::socket_option::boolean<BOOST_ASIO_OS_DEF(SOL_SOCKET),
                                                   BOOST_ASIO_OS_DEF(SO_REUSEADDR | SO_REUSEPORT)>;
#endif

   using thread_pool = boost::asio::thread_pool;
   using shared_thread_pool_ptr = std::shared_ptr<thread_pool>;

} // namespace spider