#pragma once
#include "request_handler.h"
#include <boost/any.hpp>
#include <vector>

namespace spider
{
   struct request;
   class request_handler;
   void init_loggers();

   class app : public request_handler
   {
    public:
      app();

      void add_url_scope_handler(std::unique_ptr<request_handler> handler);

      request_handler *find_request_handler(request &req) final;
      void handle_request(request &req, request_handler_callback callback) final;

      std::unique_ptr<response_generator_base> response_generator;

      inline void set_request_body_limit(std::uint64_t body_limit)
      {
         body_limit_ = body_limit;
      }
      inline std::uint64_t get_request_body_limit() const
      {
         return body_limit_;
      }

      inline void enable_extend_request_callback(std::function<void(request &)> callback)
      {
         extend_request_callback_ = callback;
      }

      inline void execute_extend_callback(request &req)
      {
         if (extend_request_callback_)
         {
            extend_request_callback_(req);
         }
      }

      inline void add_to_app_scope(boost::any object)
      {
         app_scoped_objects_.push_back(std::move(object));
      }

    private:
      std::vector<std::unique_ptr<request_handler>> handlers_;
      std::uint64_t body_limit_ = std::numeric_limits<std::uint64_t>::max();
      std::function<void(request &)> extend_request_callback_;
      std::vector<boost::any> app_scoped_objects_;
   };

   class app_tcp_listener
   {
    public:
      app_tcp_listener(app &current_app, boost::asio::io_context &io_context, boost::asio::ip::tcp::acceptor &acceptor);
      void begin_async_accept();

    private:
      void handle_connection(boost::asio::ip::tcp::socket socket);
      app &current_app_;
      boost::asio::io_context &io_context_;
      boost::asio::ip::tcp::acceptor &acceptor_;
   };

} // namespace spider