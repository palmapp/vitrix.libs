#pragma once

#include "std_types.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <string>

namespace spider
{

   struct request;

   using request_handler_callback = std::function<void(const beast::error_code &ec, beast::http::status)>;

   class request_handler
   {
    public:
      virtual ~request_handler() = default;

      virtual request_handler *find_request_handler(request &req) = 0;
      virtual void handle_request(request &req, request_handler_callback callback) = 0;
   };

   struct error_message
   {
      beast::http::status status;
      std::string title = {};
      std::string explanation = {};
   };

   class response_generator_base
   {
    public:
      virtual ~response_generator_base() = default;

      virtual void return_redirect(request &request, const std::string &url, request_handler_callback callback) = 0;
      virtual void return_error(request &request, const error_message &msg, request_handler_callback callback) = 0;
   };

   using request_buffer_type = boost::asio::const_buffer;

   class request_body_processor
   {
    public:
      virtual ~request_body_processor() noexcept(false) = default;

      virtual void init(boost::optional<std::uint64_t> const &length, beast::error_code &ec) = 0;

      virtual std::size_t put(request_buffer_type const &buffers, beast::error_code &ec) = 0;

      virtual void finish(beast::error_code &ec) = 0;
   };

   class request_body
   {
    public:
      using value_type = std::shared_ptr<request_body_processor>;

      class reader
      {
         value_type &body_;

       public:
         template <bool isRequest, class Fields>
         explicit reader(beast::http::header<isRequest, Fields> &, value_type &body) : body_(body)
         {
         }

         void init(boost::optional<std::uint64_t> const &length, beast::error_code &ec);

         std::size_t put(request_buffer_type const &buffers, beast::error_code &ec);

         void finish(beast::error_code &ec);
      };
   };

   struct request
   {
    private:
      inline static int init_tag()
      {
         static int tag_counter = 0;
         return tag_counter++;
      }

    public:
      using body_parser = beast::http::request_parser<request_body>;

      socket_stream &socket;
      response_generator_base &generator;
      beast::flat_buffer &buffer;

      std::unique_ptr<body_parser> parser_handle_;
      body_parser &parser;

      string_view path;
      string_view query;

      string_view full_path;

      bool has_been_read = false;

      boost::container::flat_map<int, boost::any> context;
      beast::http::fields response_fields;

      std::int64_t connection_id;
      std::int64_t request_id;

      boost::asio::ip::address socket_address;

      inline request(socket_stream &_socket, response_generator_base &_generator, beast::flat_buffer &_buffer,
                     std::uint64_t max_body_size, std::int64_t conn_id, std::int64_t req_id)
          : socket(_socket), generator(_generator), buffer(_buffer), parser_handle_(new body_parser()),
            parser(*parser_handle_), connection_id(conn_id), request_id(req_id)
      {
         parser.body_limit(max_body_size);
         boost::system::error_code ec;
         const auto remote_ep = socket.remote_endpoint(ec);
         if (!ec)
         {
            socket_address = remote_ep.address();
         }
      }

      inline std::string get_client_ip_address()
      {
         auto &msg = message();

         if (auto it = msg.find("X-Forwarded-For"); it != msg.end())
         {
            auto header = it->value();
            if (auto ip_end = header.find(','); ip_end != boost::string_view::npos && ip_end > 0)
            {
               auto result = std::string{header.data(), ip_end};
               boost::algorithm::trim(result);

               return result;
            }
            else
            {
               return static_cast<std::string>(header);
            }
         }

         return socket_address.to_string();
      }

      inline void init_from_message()
      {
         auto target = message().target();

         path = {target.data(), target.size()};
         full_path = path;

         size_t query_idx = path.find('?');
         if (query_idx != string_view::npos)
         {
            query = path.substr(query_idx);
            path = path.substr(0, query_idx);
            full_path = path;
         }
         set_body_processor(nullptr);

         has_been_read = !message().has_content_length();
      }

      template <class CustomContext>
      static int get_context_type_tag()
      {
         static int tag = init_tag();
         return tag;
      }

      template <class CustomContext>
      void set_custom_context(CustomContext custom_context)
      {
         auto tag = get_context_type_tag<CustomContext>();
         context[tag] = std::move(custom_context);
      }

      template <class CustomContext>
      bool has_custom_context()
      {
         return try_get_custom_context<CustomContext>() != nullptr;
      }

      template <class CustomContext>
      CustomContext *try_get_custom_context()
      {
         auto tag = get_context_type_tag<CustomContext>();
         auto it = context.find(tag);
         if (it != context.end())
         {
            return boost::any_cast<CustomContext>(&it->second);
         }

         return nullptr;
      }

      template <class CustomContext>
      CustomContext &get_custom_context()
      {
         auto *ctx = try_get_custom_context<CustomContext>();
         if (ctx)
         {
            return *ctx;
         }

         throw exception("custom context not found");
      }

      inline void return_redirect(const std::string &url, request_handler_callback callback)
      {
         generator.return_redirect(*this, url, callback);
      }

      inline void return_error(const error_message &msg, request_handler_callback callback)
      {
         generator.return_error(*this, msg, callback);
      }

      inline beast::http::request<request_body> &message()
      {
         return this->parser.get();
      }

      inline void set_body_processor(std::shared_ptr<request_body_processor> processor)
      {
         message().body() = std::move(processor);
      }

      inline void release_body_processor()
      {
         message().body().reset();
      }

      inline void read_message(io_callback callback)
      {
         beast::http::async_read(socket, buffer, parser, callback);
      }

      inline bool has_request_body()
      {
         const auto verb = message().method();
         switch (verb)
         {
         case beast::http::verb::post:
         case beast::http::verb::put:
            return true;
         default:
            return false;
         }
      }

      template <class Body = beast::http::string_body>
      std::shared_ptr<beast::http::response<Body>> make_response(beast::http::status status = beast::http::status::ok)
      {
         auto response = std::make_shared<beast::http::response<Body>>();
         response->result(status);

         for (auto &field : response_fields)
         {
            if (field.name() == beast::http::field::unknown)
            {
               response->set(field.name_string(), field.value());
            }
            else
            {
               response->set(field.name(), field.value());
            }
         }

         return response;
      }

      template <class Body>
      void write_response(std::shared_ptr<beast::http::response<Body>> &response, request_handler_callback callback)
      {
         beast::http::async_write(socket, *response,
                                  [response, callback, this](const beast::error_code &ec, std::size_t) mutable
                                  { finish_response(*response, ec, callback); });
      }

      inline void write_response(std::shared_ptr<beast::http::response<beast::http::string_body>> &response,
                                 request_handler_callback callback)
      {
         auto size = response->body().size();
         auto size_str = boost::lexical_cast<std::string>(size);
         response->set(beast::http::field::content_length, size_str);

         beast::http::async_write(socket, *response,
                                  [response, callback, this](const beast::error_code &ec, std::size_t) mutable
                                  { finish_response(*response, ec, callback); });
      }

      template <class Body>
      void finish_response(beast::http::response<Body> &response, const beast::error_code &ec,
                           request_handler_callback callback)
      {
         callback(ec, response.result());
      }
   };
} // namespace spider
