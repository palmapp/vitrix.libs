#pragma once
#include "../request_handler.h"

namespace spider
{
   namespace detail
   {
      class default_response_generator : public response_generator_base
      {
       public:
         void return_redirect(request &request, const std::string &url, request_handler_callback callback) override;
         void return_error(request &request, const error_message &msg, request_handler_callback callback) override;
      };
   } // namespace detail
} // namespace spider
