#pragma once
#include "../websocket_handler.h"
#include <mutex>
#include <thread>

#include <boost/unordered_map.hpp>
namespace spider
{
   class default_websocket_manager : public websocket_manager
   {
    public:
      default_websocket_manager(std::size_t max_concurrent_connections = 1000);
      ~default_websocket_manager() override;
      void send_all(std::string message) override;
      void send_by_id(websocket_id id, std::string message) override;
      void send_if(std::string message, std::function<bool(websocket_connection &)> filter_funct) override;
      void close_by_id(websocket_id id) override;
      void close_all() override;
      void add_ws_handler(websocket_handler &handler) override;
      void handle_new_connection(std::shared_ptr<websocket_connection> connection,
                                 request_handler_callback callback) override;

    private:
      boost::unordered_map<websocket_id, std::shared_ptr<websocket_connection>> connections_;
      std::vector<websocket_handler *> handlers_;
      std::mutex connection_lock_;

      void on_accept(std::shared_ptr<websocket_connection> connection, request_handler_callback callback);
      void on_read(std::shared_ptr<websocket_connection> connection);

      template <class FunctorT>
      void for_each(FunctorT funct)
      {
         std::lock_guard<std::mutex> lock{connection_lock_};
         for (auto &connection : connections_)
         {
            auto connection_ptr = connection.second;
            if (connection_ptr != nullptr)
            {
               funct(*connection_ptr);
            }
         }
      }

      template <class FunctorT>
      void for_each_handler(FunctorT funct)
      {
         for (auto *handler : handlers_)
         {
            if (handler)
            {
               funct(handler);
            }
         }
      }
   };
} // namespace spider
