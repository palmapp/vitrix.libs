#pragma once
#include "../websocket_handler.h"

namespace spider
{

   class default_websocket_handler : public websocket_handler
   {

    public:
      ~default_websocket_handler() = default;

      void on_text_message(websocket_connection &connection, std::string message) override;
      void on_connected(websocket_connection &connection) override;
      void on_disconnected(websocket_connection &connection) override;
   };
} // namespace spider
