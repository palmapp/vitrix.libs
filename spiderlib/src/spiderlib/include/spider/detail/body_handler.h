#pragma once
#include "../request_handler.h"
#include <serialization/serialization.h>
#include <tuple>

namespace spider
{
   inline serialization::content_type get_content_type(request &req)
   {
      serialization::content_type content_type = serialization::content_type::unknown;
      const auto it = req.message().find(beast::http::field::content_type);
      if (it != req.message().end())
      {
         content_type = serialization::parse_content_type(it->value());
      }

      if (content_type == serialization::content_type::unknown) content_type = serialization::content_type::json;

      return content_type;
   }

   template <class Arg>
   struct body_handler
   {
      using tuple_type = Arg;
      static_assert(std::tuple_size_v<tuple_type> == 0 || std::tuple_size_v<tuple_type> > 2,
                    "unable to pass more that one argument into REST API");
   };

   template <>
   struct body_handler<std::tuple<request &>>
   {
      using tuple_type = std::tuple<request &>;
      static tuple_type prepare(request &req, const std::string &, serialization::content_type)
      {
         return {req};
      }
   };

   template <>
   struct body_handler<std::tuple<request &, request_handler_callback>>
   {
      using tuple_type = std::tuple<request &, request_handler_callback>;
      static tuple_type prepare(request &req, const std::string &, serialization::content_type,
                                request_handler_callback callback)
      {
         return {req, callback};
      }
   };

   template <class Arg>
   struct body_handler<std::tuple<request &, Arg>>
   {
      using tuple_type = std::tuple<request &, Arg>;

      static tuple_type prepare(request &req, const std::string &body, serialization::content_type ct)
      {
         tuple_type result{req, Arg{}};
         if (req.has_request_body())
         {
            if (!serialization::parse_structure(std::get<1>(result), body, ct))
            {
               throw exception("invalid input data");
            }
         }
         else
         {
            auto &ref = std::get<1>(result);
            if constexpr (visit_struct::traits::is_visitable<std::decay_t<decltype(ref)>>::value)
            {
               auto url = req.message().target();
               auto query_begins = url.find('?');
               if (query_begins != string_view::npos)
               {
                  auto query = url.substr(query_begins);
                  if (!serialization::parse_structure(std::get<1>(result), static_cast<std::string>(query),
                                                      serialization::content_type::urlencoded))
                  {
                     throw exception("invalid input query");
                  }
               }
            }
         }

         return std::move(result);
      }
   };

   template <class Arg>
   struct body_handler<std::tuple<request &, Arg, request_handler_callback>>
   {
      using tuple_type = std::tuple<request &, Arg, request_handler_callback>;

      static tuple_type prepare(request &req, const std::string &body, serialization::content_type ct,
                                request_handler_callback callback)
      {
         tuple_type result{req, Arg{}, callback};
         if (req.has_request_body())
         {
            if (!serialization::parse_structure(std::get<1>(result), body, ct))
            {
               throw exception("invalid input data");
            }
         }
         else
         {
            auto url = req.message().target();
            auto query_begins = url.find('?');
            if (query_begins != string_view::npos)
            {
               auto query = url.substr(query_begins);
               if (!serialization::parse_structure(std::get<1>(result), static_cast<std::string>(query),
                                                   serialization::content_type::urlencoded))
               {
                  throw exception("invalid input query");
               }
            }
         }

         return std::move(result);
      }
   };

} // namespace spider