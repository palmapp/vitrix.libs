#pragma once

#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>
#include <string>

namespace spider
{
   class file_cache
   {
    public:
      struct cached_file
      {
         std::string etag;
         std::string last_modified_formatted;
         boost::posix_time::ptime last_modfied;

         std::string content;

         cached_file(std::string etag, std::string last_modified_formatted, boost::posix_time::ptime last_modfied,
                     std::string content);
      };

      virtual ~file_cache() = default;
      virtual std::shared_ptr<cached_file> get_file(const boost::filesystem::path &path) const noexcept = 0;
   };

} // namespace spider
