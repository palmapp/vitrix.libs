#pragma once

#include "app.h"
#include "static_file_handler.h"
#include <functional>
#include <thread>

namespace spider
{
   class running_app
   {
    public:
      virtual ~running_app();

      virtual void stop() = 0;
      virtual void wait() = 0;
   };

   struct app_configuration
   {
      std::string app_name;
      filesystem_path configuration_path;
      filesystem_path application_path;
      filesystem_path base_path;

      int thread_count = 1;
      int tcp_port = 0;
      bool bind_loop_back_only = false;

      std::vector<boost::asio::ip::address> bind_ip_addresses;
   };
   struct init_context
   {
      boost::asio::io_context &io_context;
      app_configuration const &config;
   };

   struct command_line_args
   {
      int argc;
      const char **argv;
   };

   using init_callback = std::function<void(init_context &, app &)>;
   // this performs all necessary initialization
   // init_callback - is called for every thread
   std::unique_ptr<running_app> run_spider_app(std::string const &app_name, command_line_args const &args,
                                               init_callback callback);

   std::unique_ptr<running_app> run_spider_app(std::string const &app_name, filesystem_path path, int thread_count,
                                               init_callback callback);

   // this will run an aditional instance of spider app. This will not initialize logging and it will not ignore sig
   // PIPE
   std::unique_ptr<running_app> run_spider_app_additional(app_configuration cfg, init_callback callback);

   template <class HandlerT, class... ArgsT>
   HandlerT &on_url(app &app, ArgsT &&...args)
   {
      auto handler_ptr = std::make_unique<HandlerT>(std::forward<ArgsT>(args)...);
      auto &handler_ref = *handler_ptr;
      app.add_url_scope_handler(std::move(handler_ptr));
      return handler_ref;
   }
} // namespace spider