#pragma once
#include "scoped_app_base.h"
#include <atomic>
#include <boost/beast/websocket.hpp>
#include <mutex>
#include <queue>

namespace spider
{
   using websocket_id = std::size_t;

   class websocket_manager;
   class websocket_handler;

   class websocket_connection : public std::enable_shared_from_this<websocket_connection>
   {
    public:
      inline websocket_connection(websocket_id id, beast::websocket::stream<socket_stream> websocket,
                                  request &original_request)
          : id_(id), websocket_(std::move(websocket)), original_request_(std::move(original_request)),
            buffer_(1024 * 1024)
      {
      }

      websocket_id id_;
      beast::websocket::stream<socket_stream> websocket_;
      request original_request_;
      beast::flat_buffer buffer_;

      std::map<std::string, std::string> context;

      void close();
      void send(std::string cs);

    private:
      std::mutex sender_mutex_;
      bool is_sending = false;
      std::queue<std::shared_ptr<std::string>> sender_queue_;
      void send_internal(std::shared_ptr<std::string> cs);
   };

   class websocket_manager
   {
    public:
      virtual ~websocket_manager() = default;
      virtual void send_all(std::string message) = 0;
      virtual void send_by_id(websocket_id id, std::string message) = 0;
      virtual void send_if(std::string message, std::function<bool(websocket_connection &)> filter_funct) = 0;

      virtual void close_by_id(websocket_id id) = 0;
      virtual void close_all() = 0;

      virtual void add_ws_handler(websocket_handler &handler) = 0;

      virtual void handle_new_connection(std::shared_ptr<websocket_connection> connection,
                                         request_handler_callback callback) = 0;
   };

   class websocket_handler
   {
    public:
      virtual ~websocket_handler() = default;

      virtual void on_text_message(websocket_connection &connection, std::string message) = 0;
      virtual void on_connected(websocket_connection &connection) = 0;
      virtual void on_disconnected(websocket_connection &connection) = 0;
   };

   class websocket_app : public scoped_app_base
   {
    public:
      websocket_app(std::string scope_path, websocket_manager &websocket_manager);
      request_handler *find_request_handler(request &req) override;

    protected:
      void handle_request_internal(request &request, request_handler_callback callback) final;

    private:
      websocket_manager &websocket_manager_;
   };
} // namespace spider
