#pragma once
#include "cache_control.h"
#include "file_cache.h"
#include "http_byte_ranges.h"
#include "scoped_app_base.h"
#include <map>

namespace spider
{

   using mime_type_map = std::map<std::string, std::string>;
   mime_type_map get_default_mime_types();
   struct stream_funct;
   class file_cache;
   class static_file_handler : public scoped_app_base
   {
    public:
      static_file_handler(std::string scope_path, filesystem_path base_path, cache_control cache_settings,
                          mime_type_map mime_types_ = get_default_mime_types());

      file_cache *cache = nullptr;

      static void stream_from_filesystem(spider::request &req, spider::request_handler_callback callback,
                                         beast::http::verb method, const filesystem_path &absolute_path,
                                         std::string mime_type, spider::optional<std::string> response_file_name = {},
                                         cache_control const &cache_settings = cache_control::make_no_cache(),
                                         spider::optional<std::vector<spider::byte_range>> byte_ranges = {});

      static bool is_resource_modified(request &request, const boost::posix_time::ptime file_modification,
                                       const std::string &etag);

    protected:
      static bool validate_byte_ranges(size_t size, const byte_ranges &ranges);

      void handle_request_internal(request &request, request_handler_callback callback) override;

      virtual filesystem_path get_base_path(request &request) const;
      virtual std::string decode_safe_path(request &request) const;

    private:
      filesystem_path base_path_;
      mime_type_map mime_types_;
      cache_control cache_settings_;

      const std::string &get_mime_type_for_extension(const std::string &ext) const;

      optional<byte_ranges> try_get_byte_ranges(request &request) const;

      static void stream_byte_ranges(request &request, const std::string &mime_type, const std::string &etag,
                                     const boost::posix_time::ptime &modification, byte_ranges byte_ranges,
                                     size_t file_size, std::unique_ptr<stream_funct> write_funct,
                                     cache_control const &cache_settings, request_handler_callback callback);

      void stream_from_cache(spider::request &req, spider::request_handler_callback callback, beast::http::verb method,
                             const std::string &mime_type,
                             spider::optional<std::vector<spider::byte_range>> byte_ranges,
                             std::shared_ptr<spider::file_cache::cached_file> cached_file);
   };

} // namespace spider
