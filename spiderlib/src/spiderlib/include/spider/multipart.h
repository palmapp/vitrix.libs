#pragma once
#include "http_header_parsers.h"
#include "std_types.h"

namespace spider
{
   struct part
   {
      http::content_disposition_header content_disposition;
      http::content_type_header content_type;
      filesystem_path temp_file_path;
   };

} // namespace spider
