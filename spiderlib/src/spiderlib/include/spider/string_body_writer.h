#pragma once
#include "request_handler.h"
#include <boost/optional/optional.hpp>
#include <boost/system/error_code.hpp>
#include <string>

namespace spider
{

   class string_body_writer : public request_body_processor
   {
    public:
      std::string body;

      void init(boost::optional<std::uint64_t> const &length, boost::system::error_code &ec) final;

      std::size_t put(request_buffer_type const &buffers, boost::system::error_code &ec) final;
      void finish(boost::system::error_code &ec) final;
   };
} // namespace spider
