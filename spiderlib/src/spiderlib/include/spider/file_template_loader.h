#pragma once
#include "std_types.h"
#include "template_loader.h"
#include <atomic>
#include <chrono>
#include <memory>
#include <string>
#include <unordered_map>

namespace spider
{
   class file_template_loader : public template_loader
   {
   public:
      file_template_loader(filesystem_path base_dir);
      std::string load_template(request &req, const std::string &name) override;

   protected:
      virtual filesystem_path get_template_path(request &req, const std::string &name) const;

   private:
      std::string load_template(filesystem_path path);
      std::string load_and_cache_template(filesystem_path const &path);

      struct template_info
      {
         time_t last_change;
         std::string content;
      };

      using cache_map = std::unordered_map<std::string, template_info>;
      using cache_ptr = std::shared_ptr<cache_map>;

      std::atomic<cache_ptr> cache_;

   protected:
      filesystem_path base_dir_;
      static const std::string fs_separtor;
   };
} // namespace spider