#pragma once
#include "scoped_app_base.h"

namespace spider
{
   class file_upload_processor;

   class file_upload_handler : public scoped_app_base
   {
   public:
      file_upload_handler(std::string scope_path, boost::asio::io_context &io,
                          file_upload_processor &processor, filesystem_path temp_path,
                          shared_thread_pool_ptr thread_pool = {});
      ~file_upload_handler() override;

   protected:
      void handle_request_internal(request &request, request_handler_callback callback) override;
      boost::asio::io_context &io_ctx_;
      filesystem_path temp_path_;
      file_upload_processor &processor_;

      shared_thread_pool_ptr thread_pool_;

   private:
      filesystem_path create_request_temp_folder(request &request) const;

      void schedule_folder_removal(filesystem_path folder);
   };

} // namespace spider