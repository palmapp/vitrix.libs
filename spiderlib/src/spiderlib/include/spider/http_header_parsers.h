#pragma once
#include "std_types.h"
#include <string>

#include <boost/beast/core/error.hpp>
#include <boost/beast/core/string.hpp>

namespace spider
{
   namespace http
   {
      struct content_disposition_header
      {
         std::string context;
         std::string name;
         std::string file_name;
      };
      content_disposition_header parse_content_disposition(beast::string_view header_value);

      struct content_type_header
      {
         std::string mime_type;
         std::string charset;
         std::string boundary;
      };

      content_type_header parse_content_type(beast::string_view content_type_header_value);

      std::pair<std::string, std::string> parse_header_line(beast::string_view header_line);
   } // namespace http
} // namespace spider
