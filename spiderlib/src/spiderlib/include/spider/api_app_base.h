#pragma once

#include "detail/body_handler.h"
#include "function_magic.h"
#include "scoped_app_base.h"

#include "string_body_writer.h"
#include "template_loader.h"
#include <commons/data/types.h>
#include <easylogging++.h>

#include <serialization/mustache.h>
#include <serialization/serialization.h>

namespace spider
{

   template <class Callable>
   class sync_api_wrapper;

   template <class Callable>
   class sync_web_wrapper;

   template <class Callable>
   class simple_api_wrapper;

   class api_app_base : public scoped_app_base
   {
    public:
      api_app_base(std::string scope_path);
      api_app_base(std::string scope_path, template_loader &tmpl);

      template <class Function>
      void on(beast::http::verb verb, std::string &&uri, Function &&fcnt)
      {
         using function_info = function_traits<Function>;
         using last_argument_type = typename function_info::template argument<function_info::arity - 1>::type;

         this->on_internal(verb, std::forward<std::string>(uri), std::forward<Function>(fcnt),
                           typename std::is_same<last_argument_type, request_handler_callback>::type{});
      }

      template <class Function>
      void on_web(beast::http::verb verb, std::string uri, Function &&fcnt)
      {
         if (tmpl_ != nullptr)
         {
            auto wrapper_ptr = std::make_unique<sync_web_wrapper<Function>>(uri, std::forward<Function>(fcnt), *tmpl_);
            handlers_[verb][uri] = std::unique_ptr<request_handler>(wrapper_ptr.release());
         }
         else
         {
            throw exception("template loader not specified");
         }
      }

      request_handler *find_request_handler(request &req) override;

    protected:
      template <class Function>
      void on_internal(beast::http::verb verb, std::string uri, Function &&fcnt, std::false_type)
      {
         auto wrapper_ptr = std::make_unique<sync_api_wrapper<Function>>(uri, std::forward<Function>(fcnt));
         handlers_[verb][uri] = std::unique_ptr<request_handler>(wrapper_ptr.release());
      }

      template <class Function>
      void on_internal(beast::http::verb verb, std::string uri, Function &&fcnt, std::true_type)
      {
         auto wrapper_ptr = std::make_unique<simple_api_wrapper<Function>>(uri, std::forward<Function>(fcnt));
         handlers_[verb][uri] = std::unique_ptr<request_handler>(wrapper_ptr.release());
      }

      void handle_request_internal(request &request, request_handler_callback callback) override;

      std::map<beast::http::verb, std::map<std::string, std::unique_ptr<request_handler>>> handlers_;
      template_loader *tmpl_ = nullptr;
   };

   template <class T>
   struct api_result
   {
      using value_type = T;
      value_type value;
      beast::http::status status;
      std::string content_type = {};

      api_result(value_type &&val, beast::http::status stat = beast::http::status::ok, std::string ct = std::string())
          : value(std::forward<value_type>(val)), status(stat), content_type(std::move(ct))
      {
      }
   };

   struct field_validation_error
   {
      std::string id;
      std::string message;
      std::vector<std::string> tokens;

      inline field_validation_error(std::string _id = {}, std::string _message = {})
          : id(std::move(_id)), message(std::move(_message))
      {
      }
   };

   struct kv_pair
   {
      std::string key;
      std::string value;
   };

   struct operation_result
   {
      commons::data::optional_field_value id;
      std::string message;
      std::vector<field_validation_error> validation_errors = {};
      std::vector<kv_pair> custom_data = {};

      inline static api_result<operation_result> make_success_result(commons::data::optional_field_value id = {},
                                                                     std::string message = "T:operation.success")
      {
         return {{std::move(id), std::move(message), {}, {}}, beast::http::status::ok};
      }

      inline static api_result<operation_result>
      make_validation_failed_result(std::vector<field_validation_error> validation_errors,
                                    std::string message = "T:operation.validation.failed")
      {
         return {{{}, std::move(message), std::move(validation_errors)}, beast::http::status::expectation_failed};
      }

      inline static api_result<operation_result>
      make_entity_not_found_result(commons::data::optional_field_value id, std::string message = "T:entity.not.found")
      {
         return {{std::move(id), std::move(message)}, beast::http::status::not_found};
      }

      inline static api_result<operation_result>
      make_concurrent_modification_result(commons::data::optional_field_value id, std::string record_version,
                                          std::string message = "T:concurrent.modfication")
      {
         return {operation_result{
                     std::move(id), std::move(message), {}, {kv_pair{"expected_version", std::move(record_version)}}},
                 beast::http::status::conflict};
      }

      inline static api_result<operation_result> make_failed_result(std::string message = "T:operation.failed")
      {
         return {{{}, std::move(message)}, beast::http::status::bad_request};
      }

      inline static api_result<operation_result> make_forbidden_result(std::string message = "T:operation.forbidden")
      {
         return {{{}, std::move(message)}, beast::http::status::forbidden};
      }
   };

   struct operation_exception : public exception
   {
      api_result<operation_result> result;

      operation_exception(api_result<operation_result> res) : exception(res.value.message), result(std::move(res)) {}
   };

   template <class T>
   struct web_result : public api_result<T>
   {
      using value_type = typename api_result<T>::value_type;

      std::string content_type;
      std::string template_name;
      std::map<std::string, std::string> partials;

      web_result(value_type &&val, beast::http::status stat = beast::http::status::ok,
                 const std::string &_template_name = {visit_struct::get_name<value_type>()},
                 const std::string &_content_type = "text/html")
          : api_result<T>(std::forward<value_type>(val), stat), content_type(_content_type),
            template_name(_template_name)
      {
      }
   };

   template <class Message>
   serialization::content_type
   get_accept_content_type(Message &msg, serialization::content_type default_ct = serialization::content_type::json)
   {
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
      const auto it = msg.find(beast::http::field::accept);

      if (it != msg.end())
      {
         auto header_val = it->value();
         if (header_val.find(serialization::content_types[static_cast<int>(serialization::content_type::msgpack)]) !=
             string_view::npos)
         {
            return serialization::content_type::msgpack;
         }
      }
#endif
      return default_ct;
   }

   template <class ResponseT>
   void write_response(ResponseT &result, request &req, request_handler_callback callback)
   {
      auto response_content_type = get_accept_content_type(req.message());
      auto status = result.status;
      auto response = req.make_response(status);

      response->set(beast::http::field::content_type,
                    serialization::content_types[static_cast<int>(response_content_type)]);

      if (!(result.status == beast::http::status::not_modified || req.message().method() == beast::http::verb::head))
      {
         if constexpr (std::is_same_v<std::string, decltype(result.value)>)
         {
            response->body() = std::move(result.value);
         }
         else
         {
            response->body() = serialization::serialize_structure(result.value, response_content_type);
         }
      }

      req.write_response(response, callback);
   }

   template <class Callable, class FinishCallback>
   void safe_operation_scope(request &req, FinishCallback callback, Callable op)
   {
      optional<api_result<operation_result>> error_result;
      try
      {
         op();
      }
      catch (operation_exception const &op)
      {
         error_result.reset(op.result);
      }
      catch (std::exception const &ex)
      {
         LOG(ERROR) << "exception on url '" << req.path << "' : " << ex.what();
         error_result.reset(api_result<operation_result>(operation_result::make_failed_result()));
      }
      catch (...)
      {
         LOG(ERROR) << "exception on url '" << req.path << "' : unknown";
         error_result.reset(api_result<operation_result>(operation_result::make_failed_result()));
      }

      if (error_result.has_value())
      {
         write_response(error_result.value(), req, std::move(callback));
      }
   }

   template <class Callable>
   class sync_api_wrapper : public scoped_app_base
   {
      Callable api_call_;

    public:
      sync_api_wrapper(std::string scope, Callable &&api_call)
          : scoped_app_base(std::move(scope)), api_call_(std::forward<Callable>(api_call))
      {
      }

      request_handler *find_request_handler(request &req) final
      {
         return this;
      }

      void handle_request_internal(request &req, request_handler_callback callback) final
      {
         using args_type = typename function_traits<decltype(api_call_)>::arguments;

         auto body_processor = std::make_shared<string_body_writer>();
         req.set_body_processor(body_processor);

         req.read_message(
             [this, body_processor, &req, callback = std::move(callback)](const beast::error_code &ec, size_t)
             {
                if (!ec)
                {
                   auto content_type = get_content_type(req);
                   safe_operation_scope(
                       req, callback,
                       [&]()
                       {
                          auto result = normalize_result(std::apply(
                              api_call_, body_handler<args_type>::prepare(req, body_processor->body, content_type)));
                          write_response(result, req, callback);
                       });
                }
                else
                {
                   callback(ec, beast::http::status::unknown);
                }
             });
      }

    private:
      template <class T>
      api_result<T> normalize_result(api_result<T> &&result)
      {
         return std::move(result);
      }

      template <class T>
      api_result<T> normalize_result(T &&result)
      {
         return {std::forward<T>(result)};
      }
   };

   template <class Callable>
   class simple_api_wrapper : public scoped_app_base
   {
      Callable api_call_;

    public:
      simple_api_wrapper(std::string scope, Callable &&api_call)
          : scoped_app_base(std::move(scope)), api_call_(std::forward<Callable>(api_call))
      {
      }

      template <class Message>
      serialization::content_type
      get_accept_content_type(Message &msg, serialization::content_type default_ct = serialization::content_type::json)
      {
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
         const auto it = msg.find(beast::http::field::accept);
         if (it != msg.end())
         {
            auto header_val = it->value();
            if (header_val.find(serialization::content_types[static_cast<int>(serialization::content_type::msgpack)]) !=
                string_view::npos)
            {
               return serialization::content_type::msgpack;
            }
         }
#endif
         return default_ct;
      }

      request_handler *find_request_handler(request &req) final
      {
         return this;
      }

      void handle_request_internal(request &req, request_handler_callback callback) final
      {

         auto body_processor = std::make_shared<string_body_writer>();
         req.set_body_processor(body_processor);

         req.read_message(
             [this, body_processor, &req, callback](const beast::error_code &ec, size_t)
             {
                using args_type = typename function_traits<decltype(api_call_)>::arguments;

                if (!ec)
                {
                   auto content_type = get_content_type(req);
                   safe_operation_scope(req, callback,
                                        [&]()
                                        {
                                           std::apply(api_call_,
                                                      body_handler<args_type>::prepare(req, body_processor->body,
                                                                                       content_type, callback));
                                        });
                }
                else
                {
                   callback(ec, beast::http::status::unknown);
                }
             });
      }
   };

   template <class Callable>
   class sync_web_wrapper : public scoped_app_base
   {
      Callable api_call_;
      template_loader &template_loader_;

    public:
      sync_web_wrapper(std::string scope, Callable &&api_call, template_loader &template_loader)
          : scoped_app_base(std::move(scope)), api_call_(std::forward<Callable>(api_call)),
            template_loader_(template_loader)
      {
      }

      request_handler *find_request_handler(request &req) final
      {
         return this;
      }

      void handle_request_internal(request &req, request_handler_callback callback) final
      {
         using args_type = typename function_traits<decltype(api_call_)>::arguments;

         auto body_processor = std::make_shared<string_body_writer>();
         req.set_body_processor(body_processor);

         req.read_message(
             [this, body_processor, &req, callback](const beast::error_code &ec, size_t)
             {
                if (!ec)
                {
                   safe_operation_scope(
                       req, callback,
                       [&]()
                       {
                          auto content_type = spider::serialization::content_type::urlencoded;
                          auto result = normalize_result(std::apply(
                              api_call_, body_handler<args_type>::prepare(req, body_processor->body, content_type)));

                          auto content = serialization::mustache::render_template(
                              result.value, template_loader_.load_template(req, result.template_name), result.partials);
                          if (req.message().target().find("_no_layout=1") == spider::string_view::npos)
                          {
                             auto layout = template_loader_.load_template(req, result.template_name + "_layout");
                             if (!layout.empty())
                             {
                                auto layouted_content =
                                    serialization::mustache::render_template(result.value, layout, result.partials);
                                boost::replace_all(layouted_content, "$content$", content);
                                content = std::move(layouted_content);
                             }
                          }

                          auto status = result.status;
                          auto response = req.make_response<>(status);
                          response->set(beast::http::field::content_type, result.content_type);

                          if (!(result.status == beast::http::status::not_modified ||
                                req.message().method() == beast::http::verb::head))
                             response->body() = std::move(content);

                          req.write_response(response, callback);
                       });
                }
                else
                {
                   callback(ec, beast::http::status::unknown);
                }
             });
      }

    private:
      template <class T>
      web_result<T> normalize_result(web_result<T> &&result)
      {
         return std::move(result);
      }

      template <class T>
      web_result<T> normalize_result(T &&result)
      {
         return {std::forward<T>(result)};
      }
   };
} // namespace spider
VISITABLE_STRUCT(spider::field_validation_error, id, message, tokens);
VISITABLE_STRUCT(spider::kv_pair, key, value);
VISITABLE_STRUCT(spider::operation_result, id, message, validation_errors, custom_data);