#pragma once

#include "request_handler.h"

namespace spider
{
   class scoped_app_base : public request_handler
   {
    public:
      scoped_app_base(std::string scope_path);

      request_handler *find_request_handler(request &req) override;

      virtual bool is_current_scoped_path(string_view scope);
      virtual string_view get_current_scoped_path(string_view scope);

      void handle_request(request &req, request_handler_callback callback) override;

    protected:
      virtual void handle_request_internal(request &request, request_handler_callback callback) = 0;

      std::string scope_path_;
   };

} // namespace spider
