#pragma once
#include "principal.h"
#include "user_info.h"
#include <memory>
#include <vector>

namespace spider
{
   namespace auth
   {
      static constexpr const char *default_session_token_cookie_name = "sst";

      struct authentication_result
      {
         std::string data;
         bool success = false;

         std::shared_ptr<principal_base> principal;
      };

      class authentication_service
      {
       public:
         virtual ~authentication_service() = default;

         virtual std::string get_cas_service_url() = 0;

         virtual authentication_result authenticate(const std::string &username, const std::string &password) = 0;
         virtual authentication_result verify_token(const std::string &token) = 0;
      };

   } // namespace auth
} // namespace spider
