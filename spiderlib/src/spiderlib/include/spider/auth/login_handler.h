#pragma once
#include "../api_app_base.h"
#include "authentication_service.h"
#include <boost/signals2.hpp>

namespace spider
{
   namespace auth
   {
      class login_handler : public api_app_base
      {
       public:
         login_handler(std::string scope_path, template_loader &tmpl, authentication_service &auth);
         login_handler(std::string scope_path, authentication_service &auth);

         std::string default_redirect_after_login;
         std::string session_token_cookie_name;

         struct event_handlers
         {
            boost::signals2::signal<void(request &, std::shared_ptr<principal_base>)> on_logged_in{};
            boost::signals2::signal<void(request &, std::string, std::string)> on_login_failed{};
            boost::signals2::signal<void(request &, std::shared_ptr<principal_base>)> on_logged_out{};
         };

         event_handlers events{};

       private:
         void handle_request_internal(request &request, request_handler_callback callback) override;

         void init_api(std::string const &cas_url);
         void init_login_page(std::string const &cas_url);

         authentication_service &auth_;
      };

   } // namespace auth
} // namespace spider
