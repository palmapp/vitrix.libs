#pragma once
#include <map>
#include <string>
#include <visit_struct/visit_struct.hpp>

namespace spider
{
   namespace auth
   {
      struct user_info
      {
         std::string id;
         std::string login;

         std::string first_name;
         std::string last_name;
         std::string email_address;

         std::string custom_data;
      };
   } // namespace auth
} // namespace spider
VISITABLE_STRUCT(spider::auth::user_info, id, login, first_name, last_name, email_address, custom_data);