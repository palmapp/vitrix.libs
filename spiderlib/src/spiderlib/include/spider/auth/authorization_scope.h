#pragma once
#include "../scoped_app_base.h"
#include "authentication_service.h"
#include <easylogging++.h>

namespace spider
{
   namespace auth
   {

      authentication_result authenticate_request(authentication_service &authentication, request &request,
                                                 const std::string &session_token_cookie_name);
      std::shared_ptr<principal_base> get_request_principal(request &req);

      template <typename CallbackT>
      bool run_principal_scope(request &req, CallbackT callback)
      {
         auto principal = get_request_principal(req);

         if (principal)
         {
            callback(*principal);
         }
         else
         {
            LOG(WARNING) << "run_principal_scope is not located under authorized scope";
         }

         return !!principal;
      }

      class authorization_service;

      class authorization_scope_base : public scoped_app_base
      {
       public:
         authorization_scope_base(std::string scope_path, std::unique_ptr<request_handler> next_handler,
                                  authentication_service &authentication, authorization_service &authorization);

         std::string session_token_cookie_name;

         void fake_authentication(authentication_result result);

       protected:
         void handle_request_internal(request &request, request_handler_callback callback) override;

         virtual void handle_unauthenticated(request &request, request_handler_callback callback) = 0;
         virtual void handle_unauthorized(request &request, request_handler_callback callback) = 0;

         std::unique_ptr<request_handler> next_handler_;

         authentication_service &authentication_;
         authorization_service &authorization_;
         optional<authentication_result> fake_authentication_;
      };

      class api_authorization_scope : public authorization_scope_base
      {
       public:
         api_authorization_scope(std::string scope_path, std::unique_ptr<request_handler> next_handler,
                                 authentication_service &authentication, authorization_service &authorization);

       protected:
         void handle_unauthenticated(request &request, request_handler_callback callback) override;
         void handle_unauthorized(request &request, request_handler_callback callback) override;
      };

      class web_authorization_scope : public authorization_scope_base
      {
       public:
         web_authorization_scope(std::string scope_path, std::unique_ptr<request_handler> next_handler,
                                 authentication_service &authentication, authorization_service &authorization);

         std::string unauthenticated_url;
         std::string unauthorized_url;

         bool do_not_include_path = false;

       protected:
         void handle_unauthenticated(request &request, request_handler_callback callback) override;
         void handle_unauthorized(request &request, request_handler_callback callback) override;
      };

   } // namespace auth

} // namespace spider
