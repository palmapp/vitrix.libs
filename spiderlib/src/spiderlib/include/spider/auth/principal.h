#pragma once
#include "user_info.h"
#include <string>
#include <vector>

namespace spider
{
   namespace auth
   {
      struct principal_base
      {
         virtual ~principal_base() = default;
         virtual user_info get_user_info() = 0;

         principal_base(std::string _id = {}) : id(std::move(_id)) {}

         std::string id;
      };
   } // namespace auth
} // namespace spider
