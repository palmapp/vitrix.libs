#pragma once
#include "../request_handler.h"
#include "user_info.h"
#include <memory>
#include <vector>

namespace spider
{
   namespace auth
   {
      class authorization_service
      {
       public:
         virtual ~authorization_service() = default;
         virtual bool authorize_request(spider::request &request) = 0;
      };

      class allow_all_authorization_service final : public authorization_service
      {
       public:
         inline virtual bool authorize_request(spider::request &) final
         {
            return true;
         }
      };
   } // namespace auth
} // namespace spider
