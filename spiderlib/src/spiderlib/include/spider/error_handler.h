#pragma once
#include <exception>

namespace spider
{
   struct request;
   class error_handler
   {
    public:
      virtual ~error_handler() = default;

      virtual void on_std_exception(const std::exception &ex, request &req) = 0;
      virtual void on_unknown_exception(request &req) = 0;
   };

} // namespace spider
