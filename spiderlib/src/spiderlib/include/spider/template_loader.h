#pragma once
#include <string>

namespace spider
{
   struct request;
   class template_loader
   {
    public:
      virtual ~template_loader() = default;
      virtual std::string load_template(request &req, const std::string &name) = 0;
   };
} // namespace spider
