#pragma once
#ifdef SPIDER2_API_USE_VITRIX_COMMONS

#ifndef SPIDER2_SUPPORT_IMPL_HEADER_FILE
#define SPIDER2_SUPPORT_IMPL_HEADER_FILE "spider2/api/support/commons_serialization/support_impl.h"
#endif

#include <spider2/api/support/commons_serialization/support_decl.h>

#ifndef SPIDER2_DEFAULT_FORMATTER
#define SPIDER2_DEFAULT_FORMATTER commons_serialization_formatter_tag
#endif
#endif