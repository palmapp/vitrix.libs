//
// Created by jhrub on 3/10/2024.
//

#pragma once

#ifndef SPIDER2_SUPPORT_IMPL_HEADER_FILE
#define SPIDER2_SUPPORT_IMPL_HEADER_FILE "spider2/api/support/boost_json/support_impl.h"
#endif

#include <spider2/api/support/boost_json/support_decl.h>

#ifndef SPIDER2_DEFAULT_FORMATTER
#define SPIDER2_DEFAULT_FORMATTER boost_json_formatter_tag
#endif