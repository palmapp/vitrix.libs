#include <spider/api_app_base.h>
#include <spider/app.h>
#include <spider/detail/default_websocket_handler.h>
#include <spider/detail/default_websocket_manager.h>
#include <spider/file_upload_handler.h>
#include <spider/file_upload_processor.h>
#include <spider/spider.h>
#include <spider/static_file_handler.h>
#include <spider/websocket_handler.h>

#include <boost/asio.hpp>
#include <thread>

#include "spider/auth/authentication_service.h"
#include "spider/auth/authorization_scope.h"
#include "spider/auth/authorization_service.h"
#include "spider/auth/login_handler.h"
#include <cstdlib>

#include <easylogging++.h>
#include <msgpack.hpp>

INITIALIZE_EASYLOGGINGPP

using namespace boost::asio;
using namespace boost::asio::ip;
using namespace std::chrono_literals;

namespace spider
{
   class sleep_handler : public spider::scoped_app_base
   {
    public:
      sleep_handler(const std::string &scope_path) : scoped_app_base(scope_path) {}

      void handle_request_internal(request &r, request_handler_callback callback) override
      {
         std::this_thread::sleep_for(10s);
         auto response = r.make_response();
         response->set(beast::http::field::content_type, "text/plain");
         response->body() += "slow response";
         r.write_response(response, callback);

         vitrix_random_function();
      }

      void vitrix_random_function()
      {


      }
   };

   struct cool_struct
   {
      int a = 0;
      int b = 0;

      cool_struct() {}

      MSGPACK_DEFINE_MAP(a, b)
   };

   struct echo
   {
      int MyInt;
      MSGPACK_DEFINE_MAP(MyInt)
   };

   class dummy_template_loader : public template_loader
   {
    public:
      std::string templ;

      std::string load_template(request &req, const std::string &name) override
      {
         return templ;
         ;
      }
   };

   class cool_api : public api_app_base
   {
      dummy_template_loader loader_;

    public:
      cool_api(std::string scope) : api_app_base(std::move(scope), loader_)
      {
         loader_.templ = "<!doctype html><html><body><h1>a = {{a}}</h1><h2>b = {{b}}</h2></body></html>";

         on(beast::http::verb::post, "/add/", [](request &req, cool_struct data) { return data; });

         on(beast::http::verb::get, "/add/", [](request &req, cool_struct data) { return data; });

         on(beast::http::verb::post, "/test/", [](request &req, echo data) { return data; });

         on_web(beast::http::verb::get, "/web/", [](request &req, cool_struct data) { return data; });
      }
   };
} // namespace spider

VISITABLE_STRUCT(spider::cool_struct, a, b);
VISITABLE_STRUCT(spider::echo, MyInt);

class print_file_upload_processor : public spider::file_upload_processor
{
 public:
   int handle_uploaded_files(spider::request &request, const std::vector<spider::part> &parts) final
   {
      for (auto &part : parts)
         LOG(INFO) << "uploaded part " << part.content_disposition.name << " " << part.temp_file_path;

      return 200;
   }
};

class my_principal : public spider::auth::principal_base
{
 public:
   spider::auth::user_info user;

   spider::auth::user_info get_user_info() override
   {
      return user;
   }
};

class authentication : public spider::auth::authentication_service, public spider::auth::authorization_service
{
 public:
   std::string get_cas_service_url() override
   {
      return {};
   }

   spider::auth::authentication_result authenticate(const std::string &username, const std::string &password) override
   {
      spider::auth::authentication_result result;
      if (username == "admin" && password == "admin")
      {
         result.success = true;
         result.data = "token";
         result.principal = std::make_shared<my_principal>();
      }
      else
      {
         result.success = false;
      }

      return result;
   }

   spider::auth::authentication_result verify_token(const std::string &token) override
   {
      spider::auth::authentication_result result;
      if (token == "token")
      {
         result.success = true;
         result.data = "token";
         auto principal = std::make_shared<my_principal>();
         ;
         principal->user.login = "admin";
         result.principal = principal;
      }
      else
      {
         result.success = false;
      }

      return result;
   }

   bool authorize_request(spider::request &request) override
   {
      return true;
   }
};

int main(int argc, const char *argv[])
{
   using namespace spider;
   print_file_upload_processor fup{};
   spider::default_websocket_manager manager{};
   spider::default_websocket_handler handler{};
   manager.add_ws_handler(handler);
   auto thread_pool_ptr = std::make_shared<thread_pool>(16);

   auto app_handle = spider::run_spider_app(
       "example app", {argc, argv},
       [&](init_context &ctx, app &my_app)
       {
          auto &base_path = ctx.config.base_path;

          on_url<websocket_app>(my_app, "/", manager);
          on_url<static_file_handler>(my_app, "/immutable/", base_path, spider::cache_control::make_immutable());
          on_url<static_file_handler>(my_app, "/public/", base_path, spider::cache_control::make_cache_public());
          on_url<static_file_handler>(my_app, "/cache/", base_path / "cached", spider::cache_control::make_no_cache());
          on_url<static_file_handler>(my_app, "/nocache/", base_path / "cached",
                                      spider::cache_control::make_no_cache());
          on_url<sleep_handler>(my_app, "/sleep/");
          on_url<cool_api>(my_app, "/api/");

          on_url<file_upload_handler>(my_app, "/uploads/", ctx.io_context, fup, base_path / "uploads", thread_pool_ptr);

          spider::dummy_template_loader login_template_loader;
          login_template_loader.templ =
              "<!DOCTYPE html> <form action='./submit/' method='post'>\
            <input name='path' type='hidden' value='{{path}}'>\
            <input name='username'>\
            <input name='password'>\
            <button type='submit'>Submit</button></form>";

          authentication auth_service{};

          auto login_handler =
              std::make_unique<spider::auth::login_handler>("/login/", login_template_loader, auth_service);
          login_handler->default_redirect_after_login = "/secure/test.txt";
          my_app.add_url_scope_handler(std::move(login_handler));

          auto file_handler =
              std::make_unique<spider::static_file_handler>("/", base_path, spider::cache_control::make_cache_public());
          auto secure_scope_handler = std::make_unique<spider::auth::web_authorization_scope>(
              "/secure/", std::move(file_handler), auth_service, auth_service);
          secure_scope_handler->unauthenticated_url = "/login/";

          my_app.add_url_scope_handler(std::move(secure_scope_handler));
       });

   app_handle->wait();

   return 0;
}