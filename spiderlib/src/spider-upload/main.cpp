#include <easylogging++.h>
#include <spider/api_app_base.h>
#include <spider/app.h>
#include <spider/file_upload_handler.h>
#include <spider/file_upload_processor.h>
#include <spider/spider.h>
INITIALIZE_EASYLOGGINGPP

class print_file_upload_processor : public spider::file_upload_processor
{
 public:
   int handle_uploaded_files(spider::request &request, const std::vector<spider::part> &parts) final
   {
      for (auto &part : parts)
         LOG(INFO) << "uploaded part " << part.content_disposition.name << " "
                   << part.temp_file_path;

      return 200;
   }
};

int main(int argc, const char *argv[])
{
   using namespace spider;
   print_file_upload_processor fup{};
   auto thread_pool_ptr = std::make_shared<thread_pool>(16);

   auto app_handle = run_spider_app("uploads", {argc, argv}, [&](init_context &ctx, app &my_app) {
      on_url<file_upload_handler>(my_app, "/", ctx.io_context, fup, "uploads", thread_pool_ptr);
   });

   app_handle->wait();
}
