
#include <catch.hpp>
#include <spider/http_header_parsers.h>

using namespace spider;

TEST_CASE("test parse content disposition", "[http]")
{
   auto result = http::parse_content_disposition("form-data; name=\"uploadedfile\"; filename=\"hello.o\"");

   REQUIRE(result.context == "form-data");
   REQUIRE(result.name == "uploadedfile");
   REQUIRE(result.file_name == "hello.o");

   result = http::parse_content_disposition("form-data; name=\"uploadedfile\"");
   REQUIRE(result.context == "form-data");
   REQUIRE(result.name == "uploadedfile");
   REQUIRE(result.file_name.empty() == true);

   result = http::parse_content_disposition("form-data");
   REQUIRE(result.context == "form-data");
   REQUIRE(result.name.empty() == true);
   REQUIRE(result.file_name.empty() == true);
}

TEST_CASE("test parse_content_type", "[http]")
{
   auto result = http::parse_content_type("multipart/form-data; charset=utf-8; boundary=\"another cool boundary\"");

   REQUIRE(result.mime_type == "multipart/form-data");
   REQUIRE(result.charset == "utf-8");
   REQUIRE(result.boundary == "another cool boundary");
}

/*
TEST_CASE("test parse content description", "[http]")
{
   beast::error_code ec;
   auto result = http::parse_content_description("Content-Disposition: form-data; name=\"uploadedfile\";
filename=\"hello.o\"\r\nContent-Type: application/x-object\r\n\r\n", ec);

   REQUIRE(result.content_type == "application/x-object");
   REQUIRE(result.content_disposition.context == "form-data");
   REQUIRE(result.content_disposition.name == "uploadedfile");
   REQUIRE(result.content_disposition.file_name == "hello.o");
}
*/