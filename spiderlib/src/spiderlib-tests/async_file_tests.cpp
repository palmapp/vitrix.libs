#include <boost/asio/io_service.hpp>
#include <catch.hpp>
#include <fstream>
#include <iostream>
#include <spider/io/async_file.h>
#include <sstream>
//#include <boost/asio/windows/stream_handle_service.hpp>
#include <boost/filesystem.hpp>
#include <future>

TEST_CASE("test read whole file", "[async_file]")
{
   /*
   const auto len = 10000000;
   {
      std::ofstream a("test1.txt");
      for (int i = 0; i != len; ++i)
         a << 'a';

      std::ofstream f("test2.txt");
      for (int i = 0; i != len; ++i)
         f << 'b';
   }

   boost::asio::io_service service;
   spider::io::async_file a(service, "test1.txt");
   spider::io::async_file b(service, "test2.txt");

   auto first = a.read_all_text();
   auto second = b.read_all_text();
   service.run();

   std::promise<std::string> a_promise;
   std::promise<std::string> b_promise;

   auto a_string = first.();
   auto b_string = second.get();

   REQUIRE(a_string.size() == len);
   REQUIRE(a_string[0] == 'a');
   REQUIRE(a_string[len-1] == 'a');


   REQUIRE(b_string.size() == len);
   REQUIRE(b_string[0] == 'b');
   REQUIRE(b_string[len - 1] == 'b');
   */
}
