#include <catch.hpp>
#include <spider/function_magic.h>

using namespace spider;
void free_funct(int) {}

TEST_CASE("test function magic", "[magic]")
{
   auto funct = [](int) {

   };
   {
      using trait = function_traits<decltype(funct)>;
      static_assert(trait::arity == 1, "");
      static_assert(std::is_same<typename trait::argument<0>::type, int>::value, "");

      trait::arguments args;
      std::get<0>(args) = 0;
   }

   {
      using free_funct_trait = function_traits<decltype(free_funct)>;
      static_assert(free_funct_trait::arity == 1, "");
      static_assert(std::is_same<typename free_funct_trait::argument<0>::type, int>::value, "");
      free_funct_trait::arguments free_arg;
      std::get<0>(free_arg) = 0;
   }
}