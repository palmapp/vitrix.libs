#include <catch.hpp>
#include <easylogging++.h>

#include <spider/io/buffer_pool.h>

using namespace spider::io::buffer_pool;

TEST_CASE("test maximum allocation behavior", "[buffer_pool]")
{
   for (int r = 0; r < 3; ++r)
   {
      std::vector<buffer_ptr> buffers;
      buffers.reserve(510000);

      for (int i = 0; i < 500000; ++i)
      {
         buffers.emplace_back(allocate_buffer());

         REQUIRE(buffers.back().get_allocation_type() == buffer_ptr::allocation_type::pooled);
      }

      for (int i = 0; i < 1000; ++i)
      {
         buffers.emplace_back(allocate_buffer());
         REQUIRE(buffers.back().get_allocation_type() == buffer_ptr::allocation_type::system);
      }

      LOG(INFO) << "allocated buffers: " << buffers.size();
   }
}

TEST_CASE("test speed of system allocator", "[buffer_pool]")
{

   for (int r = 0; r < 3; ++r)
   {
      std::vector<system_buffer_ptr> buffers;
      buffers.reserve(510000);

      for (int i = 0; i < 501000; ++i)
      {
         buffers.emplace_back(new char[buffer_capacity()]);
      }

      LOG(INFO) << "allocated buffers: " << buffers.size();
   }
}