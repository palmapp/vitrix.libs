
#include <catch.hpp>
#include <spider/http_byte_ranges.h>

using namespace spider;

TEST_CASE("test parse byte ranges", "[http]")
{

   byte_ranges ranges;
   bool result = try_parse_byte_ranges(ranges, "bytes=10-100");
   REQUIRE(result == true);
   REQUIRE(ranges.size() == 1);
   ranges.clear();

   result = try_parse_byte_ranges(ranges, "bytes=10-100, 200-400");
   REQUIRE(result == true);
   REQUIRE(ranges.size() == 2);
   ranges.clear();

   result = try_parse_byte_ranges(ranges, "bytes=10-");
   REQUIRE(result == true);
   REQUIRE(ranges.size() == 1);

   result = try_parse_byte_ranges(ranges, "bytes=10");
   REQUIRE(result == false);

   result = try_parse_byte_ranges(ranges, "10-100");
   REQUIRE(result == false);

   result = try_parse_byte_ranges(ranges, reinterpret_cast<const char *>(u8"bytes=10-šešøìèáíý+"));
   REQUIRE(result == false);
}
