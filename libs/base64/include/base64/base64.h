#pragma once

#include <string>
#include <cstdint>

std::string base64_encode(unsigned char const*, std::size_t len);
std::string base64_encode(std::string const&);
std::string base64_decode(std::string const& s);
void base64_direct_decode(const char* bytes, std::size_t input_len, std::uint8_t* out_bytes, std::size_t output_len);