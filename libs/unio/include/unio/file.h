#pragma once

#include "input.h"
#include "fixed_buffer_provider.h"
#include <boost/filesystem.hpp>
namespace unio
{
	class file: public input
	{
	public:
		enum open_mode
		{
         read_only = 0,
         write_only = 0x0001,
         read_write = 0x0002,
         non_block = 0x0004,
		   binary = 0x8000,
			text = 0x4000,
			random_access = 0x0010,
			sequential_access = 0x0020,
		};

		file(buffer_provider & provider = global_fixed_buffer_provider::get_instance());
		file(const boost::filesystem::path& path, int mode = read_only | binary | random_access,  buffer_provider & provider = global_fixed_buffer_provider::get_instance());
		~file();

		file(const file &) = delete;
		file& operator = (const file &) = delete;

		file(file &&);
		
		bool is_open();
		void open(const boost::filesystem::path& path, int mode = read_only | binary | random_access);
      void close();

      int release();

		byte_array_view read_some(std::int64_t limit = -1) override;
		std::size_t read(std::uint8_t * dest, std::size_t len) override;

      void write(const std::uint8_t * buffer, std::size_t len);
      void write(const byte_array_view& buffer);

		std::int64_t size() override;
		std::int64_t seek(std::int64_t location, seek_type t = seek_type::begin) override;
		std::int64_t position() override;
		bool eof() override;
	private:
		int _handle;
		buffer_provider & _provider;
	};
}