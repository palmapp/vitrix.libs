#pragma once
#include "byte_array_view.h"

namespace unio
{
	class buffer_provider
	{
	public:
		virtual ~buffer_provider(){}
		virtual byte_buffer create_buffer() = 0;
	};

}