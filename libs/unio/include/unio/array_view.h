#pragma once
#include <memory>
#include <stdexcept>
#include <iterator>

namespace unio
{
	class buffer_handle
	{
	public:
		virtual ~buffer_handle(){}
	};
	
	using buffer_handle_ptr = std::shared_ptr < buffer_handle > ;
	

	template <class ElementT> class mutable_buffer;
	template <class ElementT> class array_view
	{
	public:
		using value_type = ElementT;
		using const_iterator_type = const value_type *;
		using self_array_view = array_view < ElementT > ;
      using const_reverse_iterator_type = std::reverse_iterator < const_iterator_type > ;

      
		array_view(const ElementT * data, size_t len, buffer_handle_ptr buffer = buffer_handle_ptr(nullptr), bool imutable = true) :
			_data(data),
			_len(len),
			_buffer(std::move(buffer) ),
			_imutable(imutable)
		{
		}	

		array_view(): _data(nullptr), _len(0), _buffer(nullptr)
		{
		}

      void assign(const_iterator_type beg, const_iterator_type end)
      {
         _data = beg;
         _len = std::distance(beg, end);
         _buffer.reset();
      }

		const value_type* data() const
		{
			return _data;
		}

      
      const_reverse_iterator_type rbegin() const
      {
         return const_reverse_iterator_type(end());
      }

      const_reverse_iterator_type rend() const
      {
         return const_reverse_iterator_type(begin());
      }

		const_iterator_type begin() const
		{
			return _data;
		}

		const_iterator_type end() const
		{
			return _data + _len;
		}

		const_iterator_type cbegin() const
		{
			return _data;
		}

		const_iterator_type cend() const
		{
			return _data+_len;
		}

		const value_type& operator [] (size_t idx) const
		{
			return *(_data + idx);
		}

		size_t size() const
		{
			return _len;
		}

		bool empty() const
		{
			return _len == 0;
		}

		self_array_view slice(size_t start, size_t len) const
		{
			if (start + len > _len) throw std::out_of_range("requrested slice is out of range");

			return self_array_view(_data + start, len);
		}

		self_array_view subarray(size_t start, size_t end) const
		{
			return slice(start, end - start);
		}

      self_array_view subarray(const_iterator_type start, const_iterator_type end) const
      {
         return self_array_view(start, std::distance(begin(), end));
      }


		mutable_buffer<ElementT> modify() const
		{
			if (_imutable) throw std::domain_error("array view has been marked as imutable");

			return mutable_buffer<ElementT>(const_cast<ElementT*>(_data), _len, _buffer);
		}

		operator mutable_buffer<ElementT>() const
		{
			return modify();
		}

	private:
		const ElementT * _data;
		size_t _len;
		buffer_handle_ptr _buffer;

		bool _imutable;
	};

	template <class ElementT> class mutable_buffer 
	{
	public:	
		using value_type = ElementT;
		using iterator_type = value_type *;
		using const_iterator_type = const value_type *;
		using self_array_view = mutable_buffer < ElementT >;

		mutable_buffer(ElementT * data, size_t len, buffer_handle_ptr buffer = buffer_handle_ptr(nullptr)) :
			_data(data),
			_len(len),
			_buffer(std::move(buffer))
		{
		}

		mutable_buffer() : _data(nullptr), _len(0), _buffer(nullptr)
		{
		}

		const value_type* data() const
		{
			return _data;
		}

		value_type* data()
		{
			return _data;
		}

		iterator_type begin() 
		{
			return _data;
		}

		iterator_type end()
		{
			return _data + _len;
		}

		const_iterator_type begin() const
		{
			return _data;
		}

		const_iterator_type end() const
		{
			return _data + _len;
		}

		const_iterator_type cbegin() const
		{
			return _data;
		}

		const_iterator_type cend() const
		{
			return _data + _len;
		}

		const value_type& operator [] (size_t idx) const
		{
			return *(_data + idx);
		}

		value_type& operator [] (size_t idx)
		{
			return *(_data + idx);
		}

		size_t size() const
		{
			return _len;
		}

		self_array_view slice(size_t start, size_t len) const
		{
			if (start + len > _len) throw std::out_of_range("requrested slice is out of range");

			return self_array_view(_data + start, len);
		}

		self_array_view subarray(size_t start, size_t end) const
		{
			if (start >= _len) throw std::out_of_range("requrested slice is out of range");
			if (end >= _len) throw std::out_of_range("requrested slice is out of range");
			if (start > end) throw std::out_of_range("requrested slice is out of range");

			return self_array_view(_data + start, end - start);
		}

		array_view<ElementT> readonly() const
		{
			return array_view<ElementT>(_data, _len, _buffer, false);
		}

		operator array_view<ElementT>() const
		{
			return readonly();
		}

	private:
		ElementT * _data;
		size_t _len;
		buffer_handle_ptr _buffer;
	};
}
