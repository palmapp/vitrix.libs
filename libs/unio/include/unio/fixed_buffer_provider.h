#pragma once

#include "fixed_buffer_allocator.h"
#include "buffer_provider.h"

namespace unio
{
	template<size_t size = 8192> class fixed_buffer_provider: public buffer_provider
	{
	public:
		fixed_buffer_provider(size_t len = 40) 
			: _fast_allocator(len) 
		{
		}

		byte_buffer create_buffer() override
		{
			auto buffer = _fast_allocator.allocate();
			return buffer->writable_buffer();
		}
	private:
		fixed_buffer_allocator<uint8_t, size> _fast_allocator;
	};

	class global_fixed_buffer_provider
	{
	public:
		static buffer_provider & get_instance()
		{
			static fixed_buffer_provider<> provider{};
			return provider;
		}
	};
}