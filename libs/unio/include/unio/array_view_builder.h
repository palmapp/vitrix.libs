#pragma once

#include <vector>
#include "array_view.h"

namespace unio
{
   template <class ElementT> class vector_buffer_holder : public buffer_handle
   {
   public:
      std::vector<ElementT> buffer;
      std::weak_ptr<buffer_handle> owner;

      vector_buffer_holder(size_t len)
      {
         buffer.resize(len);
      }

      array_view<ElementT> readonly_view()
      {
         return array_view<ElementT>(buffer.data(), buffer.size(), owner.lock(), false);
      }

      mutable_buffer<ElementT> writable_buffer()
      {
         return mutable_buffer<ElementT>(buffer.data(), buffer.size(), owner.lock());
      }
   };

   template <class ElementT> mutable_buffer<ElementT> make_buffer(size_t capacity)
   {
	   using vector_holder_type = vector_buffer_holder < ElementT >;
	   
	   auto handle = std::make_shared< vector_holder_type >(capacity);
	   handle->owner = std::weak_ptr < vector_holder_type > { handle };

	   return handle->writable_buffer();
   }

   template<class ArrayViewT> class array_view_builder
   {
   public:
      using value_type = typename ArrayViewT::value_type;
      using vector_holder_type = vector_buffer_holder < value_type >;
      using vector_holder_shared_ptr = std::shared_ptr < vector_holder_type >;

      array_view_builder()
      {}

      void add(const ArrayViewT & buffer)
      {
         _arrays.push_back(buffer);
      }

      ArrayViewT build()
      {
         if (_arrays.size() == 1)
         {
            return _arrays[0];
         }

         size_t total = 0;
         for (const auto & view : _arrays)
         { 
            total += view.size();
         }

         vector_holder_shared_ptr h = std::make_shared< vector_holder_type >(total);
         h->owner = std::weak_ptr < vector_holder_type > { h };
         
         auto & buffer = h->buffer;
         size_t idx = 0;
         for (const auto & view : _arrays)
         {
            std::copy(view.begin(), view.end(), buffer.begin() + idx);
            idx += view.size();
         }
         
         return  h->readonly_view();
      }

      void clear()
      {
         _arrays.clear();
      }

      bool empty() const
      {
         return _arrays.empty();
      }

   private:
      std::vector<ArrayViewT> _arrays;
   };

}