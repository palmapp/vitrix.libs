#pragma once
#include "byte_array_view.h"
#include <cstdint>
#include <iterator>
namespace unio
{
	
	class input
	{
	public:
		virtual ~input() {}
		
		virtual byte_array_view read_some(std::int64_t len = -1) = 0;
		virtual std::size_t read(std::uint8_t * dest, std::size_t len) = 0;

		enum class seek_type
		{
			begin = 0,
			relative = 1,
			end = 2
		};

		virtual std::int64_t size() = 0;
		virtual std::int64_t seek(std::int64_t location, seek_type t = seek_type::begin) = 0;
		virtual std::int64_t position() = 0;
		virtual bool eof() = 0;
	};




   class input_iterator
   {


   public:
     using iterator_category  = std::forward_iterator_tag;
     /// The type "pointed to" by the iterator.
     using   value_type = std::uint8_t;
     /// Distance between iterators is represented as this type.
     using  difference_type = std::int64_t;
     /// This type represents a pointer-to-value_type.
     using    pointer = const std::uint8_t*;
     /// This type represents a reference-to-value_type.
     using  reference = std::uint8_t;

      input_iterator();
      input_iterator(input& input, std::int64_t limit = -1);
      input_iterator(const input_iterator& it);
      const input_iterator& operator= (const input_iterator&);
      
      input_iterator& operator ++();
      input_iterator operator ++(int);
      
      bool operator == (const input_iterator&);
      bool operator != (const input_iterator&);
      
      const std::uint8_t& operator*();
      std::int64_t position() const;
      
   private:
      byte_array_view::const_iterator_type _buffer_position = nullptr;
      std::int64_t _consumed = 0;
      std::int64_t _limit = 0;
      std::int64_t _initial_position = 0;
      input* _input = nullptr;
      byte_array_view _loaded_buffer;


      void increment();
      void buffer();
   };

   inline input_iterator begin(input& input)
   {
      return input_iterator(input);
   }

   inline input_iterator end(input& )
   {
      return {};
   }
}