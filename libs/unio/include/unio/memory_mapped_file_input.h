#pragma once

#include <boost/iostreams/device/mapped_file.hpp>
#include "input.h"
#include "byte_array_view.h"
#include <boost/filesystem.hpp>

namespace unio
{
	class memory_mapped_file_input: public input
	{
	public:
		memory_mapped_file_input(const boost::filesystem::path& filename);

		memory_mapped_file_input(const memory_mapped_file_input &) = delete;
		memory_mapped_file_input& operator = (const memory_mapped_file_input &) = delete;

		memory_mapped_file_input(memory_mapped_file_input &&);

		byte_array_view read_some(std::int64_t limit = -1) override;
		std::size_t read(std::uint8_t * dest, std::size_t len) override;

		std::int64_t size() override;
		std::int64_t seek(std::int64_t location, seek_type t = seek_type::begin) override;
		std::int64_t position() override;
		bool eof() override;

	private:
		int64_t _cursor;
		boost::iostreams::mapped_file_source _file;
	};

}