#pragma once

#include "array_view.h"
#include <array>
#include <boost/pool/object_pool.hpp>

namespace unio
{
	template <class ElementT, size_t size> class fixed_buffer_allocator
	{
	public:
		class array_holder : public buffer_handle
		{
		public:
			std::array<ElementT, size> buffer;
			std::weak_ptr<buffer_handle> owner;

			array_view<ElementT> readonly_view()
			{
				return array_view<ElementT>(buffer.data(), buffer.size(), owner.lock(), false);
			}

			mutable_buffer<ElementT> writable_buffer()
			{
				return mutable_buffer<ElementT>(buffer.data(), buffer.size(), owner.lock());
			}
		};
		using array_ptr = std::shared_ptr < array_holder > ;

		fixed_buffer_allocator(size_t poolSize) : _pool(poolSize)
		{
		}

		array_ptr allocate()
		{
			auto self = this;
			array_holder * ptr =  _pool.construct();
			array_ptr holder(ptr, [=](array_holder * ptr){
				self->_pool.destroy(ptr);
			});

			ptr->owner = std::weak_ptr < buffer_handle > { holder };

			return holder;
		}
	
	private:
		boost::object_pool <array_holder> _pool;

	};
}