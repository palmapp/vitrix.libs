#pragma once
#include "array_view.h"
#include <cstdint>

namespace unio
{
	using byte_array_view = array_view < std::uint8_t > ;
	using byte_buffer = mutable_buffer < std::uint8_t >;

   template <size_t N> 
   byte_array_view make_const_string_array_view(const char (&str)[N])
   {
      auto * ptr = reinterpret_cast<const std::uint8_t*>(str);
      return byte_array_view{ ptr, N - 1  };
   }
}