#include "file.h"

#include <io.h>
#include <fcntl.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <algorithm>
namespace unio
{
	file::file(buffer_provider & provider) : 
		_handle(0),
		_provider(provider)
	{
	}

	file::file(const boost::filesystem::path& path, int mode, buffer_provider & provider) :
		_handle(0), 
		_provider(provider)
	{
		open(path, mode);
	}

	file::file(file && f) : _handle(0), 
		_provider(f._provider)
	{
		std::swap(_handle, f._handle);
	}

	file::~file()
	{
      close();
	}

   void file::close()
   {
      if (_handle != 0)
      {
         ::_close(_handle);
         _handle = 0;
      }
	}

	bool file::is_open()
	{
		return _handle != 0;
	}

	void file::open(const boost::filesystem::path& path, int mode)
	{
		errno_t err = _wsopen_s(&_handle, path.c_str(), mode, _SH_DENYWR, _S_IREAD) != 0;
		if (err != 0)
		{
			throw std::system_error(std::error_code(err, std::system_category()),  "unable to open file");
		}
	}

   int file::release()
   {
      auto handle = _handle;
      _handle = 0;
      return handle;
   }

	byte_array_view file::read_some(std::int64_t limit)
	{
		auto buffer = _provider.create_buffer();
		if (limit == -1) limit = buffer.size();


		auto res = read(buffer.data(), std::min(static_cast<std::int64_t>(buffer.size()), limit));
		if (res == buffer.size())
		{
			return buffer;
		}
		else
		{
			return buffer.slice(0, res);
		}
	}

	std::size_t file::read(std::uint8_t * dest, size_t len)
	{
		auto result = _read(_handle, dest, static_cast<unsigned int>(len));
		if (result == -1)
			throw std::system_error(std::error_code(errno, std::system_category()), "unable to read");

		return result;
	}

   void file::write(const std::uint8_t * buffer, std::size_t len)
	{
      auto result = _write(_handle, buffer, static_cast<unsigned int>(len));
      if (result == -1)
         throw std::system_error(std::error_code(errno, std::system_category()), "unable to write");
	}
   
   void file::write(const byte_array_view& buffer)
	{
      write(buffer.data(), buffer.size());
	}

	std::int64_t file::size()
	{
		struct _stat64 info;
		auto res = _fstati64(_handle, &info);
		if (res != 0)
			throw std::system_error(std::error_code(errno, std::system_category()), "unable to obtain file size");

		return info.st_size;
	}
	
	std::int64_t file::seek(std::int64_t location, seek_type t)
	{
		auto res = _lseeki64(_handle, location, static_cast<int>(t));
		if (res == -1)
			throw std::system_error(std::error_code(errno, std::system_category()), "unable to seek");

		return res;
	}

	int64_t file::position()
	{
		int64_t r = _telli64(_handle);
		if (r == -1)
			throw std::system_error(std::error_code(errno, std::system_category()), "unable to get file cursor position");

		return r;
	}
	
	bool file::eof()
	{
		auto r = _eof(_handle) ;
		if (r == -1)
			throw std::system_error(std::error_code(errno, std::system_category()), "unable to get eof");
		
		return r == 1;
	}

	


}