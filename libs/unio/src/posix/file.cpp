#include "file.h"

#ifdef ANDROID
#include <sys/uio.h>
#else
#include <sys/io.h>
#endif

#include <fcntl.h>
#include <sys/signalfd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <algorithm>
#include <csignal>
#include <unistd.h>

namespace unio
{

   file::file(buffer_provider &provider) : _handle(0), _provider(provider) {}

   file::file(const boost::filesystem::path &path, int mode, buffer_provider &provider)
       : _handle(0), _provider(provider)
   {
      open(path, mode);
   }

   file::file(file &&f) : _handle(0), _provider(f._provider)
   {
      std::swap(_handle, f._handle);
   }

   file::~file()
   {
      close();
   }

   void file::close()
   {
      if (_handle != 0)
      {

         ::close(_handle);
         _handle = 0;
      }
   }

   bool file::is_open()
   {
      return _handle != 0;
   }

   void file::open(const boost::filesystem::path &path, int mode)
   {
      _handle = ::open(path.c_str(), mode);
      if (_handle < 0)
      {
         throw std::system_error(std::error_code(errno, std::system_category()), "unable to open file");
      }
   }

   int file::release()
   {
      auto handle = _handle;
      _handle = 0;
      return handle;
   }

   byte_array_view file::read_some(std::int64_t limit)
   {
      auto buffer = _provider.create_buffer();
      if (limit == -1) limit = buffer.size();

      auto res = read(buffer.data(), std::min(static_cast<std::int64_t>(buffer.size()), limit));
      if (res == buffer.size())
      {
         return buffer;
      }
      else
      {
         return buffer.slice(0, res);
      }
   }

   std::size_t file::read(std::uint8_t *dest, size_t len)
   {
      auto result = ::read(_handle, dest, static_cast<unsigned int>(len));
      if (result == -1) throw std::system_error(std::error_code(errno, std::system_category()), "unable to read");

      return result;
   }

   void file::write(const std::uint8_t *buffer, std::size_t len)
   {
      auto result = ::write(_handle, buffer, static_cast<unsigned int>(len));
      if (result == -1) throw std::system_error(std::error_code(errno, std::system_category()), "unable to write");
   }

   void file::write(const byte_array_view &buffer)
   {
      write(buffer.data(), buffer.size());
   }

   std::int64_t file::size()
   {
      struct stat info;
      auto res = fstat(_handle, &info);
      if (res != 0)
         throw std::system_error(std::error_code(errno, std::system_category()), "unable to obtain file size");

      return info.st_size;
   }

   std::int64_t file::seek(std::int64_t location, seek_type t)
   {
      auto res = lseek(_handle, location, static_cast<int>(t));
      if (res == -1) throw std::system_error(std::error_code(errno, std::system_category()), "unable to seek");

      return res;
   }

   int64_t file::position()
   {
#ifdef ANDROID
      int64_t r = lseek64(_handle, (off64_t)0, SEEK_CUR);
#else
      int64_t r = lseek(_handle, (off_t)0, SEEK_CUR);
#endif
      if (r == -1)
         throw std::system_error(std::error_code(errno, std::system_category()), "unable to get file cursor position");

      return r;
   }

   bool file::eof()
   {
      return size() == position();
   }

} // namespace unio
