#include "memory_mapped_file_input.h"
#include <boost/filesystem/path.hpp>

namespace unio
{
   memory_mapped_file_input::memory_mapped_file_input(const boost::filesystem::path &filename)
      : _cursor(0),
        _file(filename)
   {
   }

   memory_mapped_file_input::memory_mapped_file_input(memory_mapped_file_input &&old)
      : _cursor(old._cursor),
        _file(std::move(old._file))
   {
   }

   byte_array_view memory_mapped_file_input::read_some(std::int64_t limit)
   {
      if (limit == -1) limit = size() - _cursor;
      std::int64_t endcursor = std::max(std::min(_cursor + limit, size()), std::int64_t(0));
      limit = endcursor - _cursor;

      byte_array_view res(reinterpret_cast<const uint8_t *>(_file.data() + _cursor), limit);

      _cursor = endcursor;
      return res;
   }

   std::size_t memory_mapped_file_input::read(std::uint8_t *dest, std::size_t _len)
   {
      std::int64_t len = static_cast<std::int64_t>(_len);
      std::int64_t endcursor = std::max(std::min(_cursor + len, size()), std::int64_t(0));

      len = endcursor - _cursor;
      memcpy(dest, _file.data() + _cursor, len);

      _cursor = endcursor;

      return len;
   }

   std::int64_t memory_mapped_file_input::size()
   {
      return static_cast<int64_t>(_file.size());
   }

   std::int64_t memory_mapped_file_input::seek(std::int64_t location, seek_type t)
   {
      std::int64_t begin = 0;
      switch (t)
      {
      case unio::input::seek_type::begin:
         break;
      case unio::input::seek_type::relative:
         begin = _cursor;
         break;
      case unio::input::seek_type::end:
         begin = size();
         break;
      default:
         break;
      }

      _cursor = std::max(std::min(begin + location, size()), std::int64_t(0));
      return _cursor;
   }

   std::int64_t memory_mapped_file_input::position()
   {
      return _cursor;
   }

   bool memory_mapped_file_input::eof()
   {
      return _cursor == size();
   }


}