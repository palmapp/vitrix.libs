#include "input.h"
#include <cassert>

unio::input_iterator::input_iterator()
{
}

unio::input_iterator::input_iterator(input &input, std::int64_t limit)
   : _limit(limit),
     _initial_position(input.position()),
     _input(&input)
{
   buffer();
}

unio::input_iterator::input_iterator(const input_iterator &it)
{
   _buffer_position = it._buffer_position;
   _consumed = it._consumed;
   _limit = it._limit;
   _initial_position = it._initial_position;
   _input = it._input;
   _loaded_buffer = it._loaded_buffer;
}

const unio::input_iterator &unio::input_iterator::operator=(const input_iterator &it)
{
   _initial_position = it._initial_position;
   _buffer_position = it._buffer_position;
   _limit = it._limit;
   _consumed = it._consumed;
   _input = it._input;
   _loaded_buffer = it._loaded_buffer;
   return *this;
}

unio::input_iterator &unio::input_iterator::operator++()
{
   increment();
   return *this;
}

unio::input_iterator unio::input_iterator::operator++(int)
{
   auto it = *this;
   increment();
   return it;
}

void unio::input_iterator::increment()
{
   if (_consumed == _limit)
   {
      throw new std::out_of_range("cannot increment iterator");
   }

   ++_consumed;
   ++_buffer_position;

   if (_consumed == _limit)
   {
      _buffer_position = nullptr;
   }
   else if (_buffer_position == _loaded_buffer.end())
   {
      _input->seek(_initial_position + _consumed);
      buffer();
   }

}

void unio::input_iterator::buffer()
{
   if (_input->eof())
   {
      _buffer_position = nullptr;
   }
   else
   {
      _loaded_buffer = _input->read_some();
      _buffer_position = _loaded_buffer.begin();
   }
}

bool unio::input_iterator::operator ==(const input_iterator &a)
{
   return (_buffer_position == nullptr && a._buffer_position == nullptr)
          || (a._input == _input && a._initial_position == _initial_position && a._buffer_position == _buffer_position);
}

bool unio::input_iterator::operator !=(const input_iterator &it)
{
   return !this->operator==(it);
}

const std::uint8_t &unio::input_iterator::operator*()
{
   return *_buffer_position;
}

std::int64_t unio::input_iterator::position() const
{
   return _initial_position + _consumed;
}


