



if (NOT TARGET logging)
    set(LOGGING_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/easylogging/include)
    message(STATUS "Logging library: " ${LOGGING_INCLUDE_DIR})
    add_library(logging easylogging/src/easylogging++.cc)
    target_include_directories(logging PRIVATE ${LOGGING_INCLUDE_DIR})
    target_include_directories(logging PUBLIC ${Boost_INCLUDE_DIR})
    target_include_directories(logging INTERFACE ${LOGGING_INCLUDE_DIR})

    if (NOT CMAKE_SYSTEM_NAME STREQUAL "Windows")
        target_compile_options(logging PUBLIC -fPIC)
    endif ()
endif ()

if (NOT TARGET visit_struct)
    #visit structs
    set(VS_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/visit_struct/include)
    message(STATUS "visit_struct library: " ${VS_INCLUDE_DIR})
    add_library(visit_struct INTERFACE)
    target_compile_definitions(visit_struct INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(visit_struct INTERFACE ${VS_INCLUDE_DIR})
endif ()

if (NOT TARGET formats)
    set(FORMATS_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/formats/include)


    message(STATUS "formats library: " ${FORMATS_INCLUDE_DIR})
    add_library(formats formats/src/pugixml.cpp)
    target_include_directories(formats PRIVATE ${FORMATS_INCLUDE_DIR}/formats)
    target_include_directories(formats INTERFACE ${FORMATS_INCLUDE_DIR})
    target_include_directories(formats INTERFACE ${FORMATS_INCLUDE_DIR}/formats)
    target_link_libraries(formats PUBLIC msgpack-cxx)

    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/mstch-1.0.2)
    message(STATUS "mstch lib: " ${mstch_INCLUDE_DIR})
    include_directories(${mstch_INCLUDE_DIR})
    target_include_directories(formats INTERFACE ${mstch_INCLUDE_DIR})


endif ()

if (NOT TARGET base64)
    set(BASE64_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/base64/include)
    message(STATUS "base64 library: " ${BASE64_INCLUDE_DIR})
    add_library(base64 base64/src/base64.cpp)
    target_include_directories(base64 PRIVATE ${BASE64_INCLUDE_DIR}/base64)
    target_include_directories(base64 INTERFACE ${BASE64_INCLUDE_DIR})
endif ()

#fakeit lib
if (NOT TARGET fakeit)
    set(FAKEIT_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/fakeit/include)
    message(STATUS "fakeit library: " ${FAKEIT_INCLUDE_DIR})
    add_library(fakeit INTERFACE)
    target_compile_definitions(fakeit INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(fakeit INTERFACE ${FAKEIT_INCLUDE_DIR})
endif ()

if (NOT TARGET unio)
    add_subdirectory(unio)
endif ()


if (NOT TARGET boost_application)
    #boost Application library
    set(APPLICATION_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/boost-application/include)
    message(STATUS "Boost Application library: " ${APPLICATION_INCLUDE_DIR})
    add_library(boost_application INTERFACE)
    target_compile_definitions(boost_application INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(boost_application INTERFACE ${APPLICATION_INCLUDE_DIR})
endif ()

if (NOT TARGET signals2)
    #boost Application library
    set(SIGNALS2_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/signals2/include)
    message(STATUS "Boost signals2 library: " ${SIGNALS2_INCLUDE_DIR})
    add_library(signals2 INTERFACE)
    target_compile_definitions(signals2 INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(signals2 INTERFACE ${SIGNALS2_INCLUDE_DIR})
endif ()

if (NOT TARGET mailio)
    add_subdirectory(mailio)
endif ()

if (NOT TARGET gsl)
    #https://github.com/microsoft/GSL 
    set(GSL_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/gsl/include)
    message(STATUS "GSL library : " ${GSL_INCLUDE_DIR})
    add_library(gsl INTERFACE)
    target_compile_definitions(gsl INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(gsl INTERFACE ${GSL_INCLUDE_DIR})
endif ()


if (NOT TARGET date)
    set(DATE_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/date/include)
    message(STATUS "date library: " ${DATE_INCLUDE_DIR})
    add_library(date INTERFACE)
    target_compile_definitions(date INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(date INTERFACE ${DATE_INCLUDE_DIR})
endif ()


if (NOT TARGET multipart)
    set(MULTIPART_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/multipart/include)
    message(STATUS "multipart library: " ${MULTIPART_INCLUDE_DIR})
    add_library(multipart INTERFACE)
    target_compile_definitions(multipart INTERFACE LIBRARY_HEADER_ONLY)
    target_include_directories(multipart INTERFACE ${MULTIPART_INCLUDE_DIR})
endif ()
