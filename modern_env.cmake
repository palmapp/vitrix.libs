option(USE_ASAN "Use AddressSanitizer" OFF)
option(USE_TSAN "Use ThreadSanitizer" OFF)
option(USE_UBSAN "Use UndefinedBehaviorSanitizer" OFF)
if (NOT DEFINED VITRIX_ENV)
    MESSAGE(STATUS "SYSTEM:         " ${CMAKE_SYSTEM_NAME})
    set(Boost_NO_WARN_NEW_VERSIONS 1)

    if (WIN32)
        add_compile_definitions(/DUNICODE /D_UNICODE)
        add_definitions(/utf-8)
        add_definitions(-D_WIN32_WINNT=0x0600)
        add_definitions(/DNOMINMAX)
        add_definitions(/D_CRT_SECURE_NO_WARNINGS)

        if (DEFINED ENABLE_PROFILE)
            MESSAGE(STATUS "Profiling is enabled!")
            link_libraries("micro-profiler_x64")
        endif ()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /utf-8 /bigobj /wd4996 /wd4828 /wd4834 /Zc:__cplusplus")

        add_definitions(-DELPP_NO_DEFAULT_LOG_FILE)
        set(CPPREST_LIB cpprestsdk::cpprest)

    else ()
        find_package(Threads REQUIRED)
        set(EXE_LIBS -pthread -lm -ldl)

#        find_package(mimalloc)
#        if (mimalloc_FOUND)
#            message(STATUS "mimalloc found")
#            add_compile_definitions(VITRIX_USE_MIMALLOC=1)
#            set(EXE_LIBS ${EXE_LIBS} mimalloc-static)
#        else ()
#            message(STATUS "mimalloc not found")
#        endif ()
    endif ()

    if (CMAKE_BUILD_TYPE MATCHES "^Debug$" OR NOT DEFINED CMAKE_BUILD_TYPE)
        add_definitions(-D_DEBUG)
    endif ()

    add_definitions(-DELPP_THREAD_SAFE -DPICOJSON_USE_INT64 -DZMQ_BUILD_DRAFT_API=1)

    SET(VITRIX_ENV 5)

    if (NOT DEFINED CMAKE_CXX_STANDARD)
        MESSAGE(STATUS "CMAKE_CXX_STANDARD NOT SET: DEFAULTING TO C++20")
        set(CMAKE_CXX_STANDARD 20)
    else ()
        MESSAGE(DEBUG "CMAKE_CXX_STANDARD IS SET: C++${CMAKE_CXX_STANDARD}")
    endif ()
endif ()

if (USE_ASAN)
    if (UNIX)
        add_compile_options("-fsanitize=address")
        add_link_options("-fsanitize=address")
    else ()
        add_compile_options("/fsanitize=address")
        add_link_options("/fsanitize=address")
    endif ()
endif ()

if (USE_TSAN)
    if (UNIX)
        add_compile_options("-fsanitize=thread")
        add_link_options("-fsanitize=thread")
    else ()
        message(STATUS "ThreadSanitizer is not supported on Windows")
    endif ()
endif ()

if (USE_UBSAN)
    if (UNIX)
        add_compile_options("-fsanitize=undefined")
        add_link_options("-fsanitize=undefined")
    else ()
        message(STATUS "UndefinedBehaviorSanitizer is not supported on Windows")
    endif ()
endif ()

function(target_separate_debug_symbols target)
    if (UNIX)
        target_compile_options(${target} PRIVATE -fno-omit-frame-pointer)
        add_custom_command(TARGET ${target} POST_BUILD
                COMMAND ${CMAKE_OBJCOPY} --only-keep-debug $<TARGET_FILE:${target}> $<TARGET_FILE:${target}>.debug
                COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${target}>
                COMMAND ${CMAKE_OBJCOPY} --add-gnu-debuglink=$<TARGET_FILE:${target}>.debug $<TARGET_FILE:${target}>
                COMMENT "Stripping debug symbols from ${target} to ${target}.debug"
        )
    endif ()
endfunction()

function(global_enable_all_warnings)
    if (MSVC)
        # warning level 4 and execpt
        add_compile_options(/W4 /wd4100 /wd4702 /wd4458 /wd4459 /wd4706 /wd4251 /wd4068 /wd4267 /wd4244)
    else ()
        add_compile_options(-Wall -Wextra -Wpedantic -Wno-unused-parameter -Wno-unknown-pragmas)
    endif ()
endfunction()

function(global_warnings_as_errors)
    if (MSVC)
        add_compile_options(/WX)
    else ()
        add_compile_options(-Werror)
    endif ()
endfunction()