#pragma once

#include "pimpl.h"
#include "query_result.h"
#include <cstdint>
#include <string>

namespace xdb
{
   class connection;
   namespace backend
   {
      class iquery;
      class iconnection;
   } // namespace backend

   class query : public pimpl<backend::iquery>
   {
    public:
      using pimpl<backend::iquery>::pimpl;

      inline query(query &&other) : pimpl<backend::iquery>(std::move(other)) {}

      inline query &operator=(query &&other)
      {
         pimpl<backend::iquery>::operator=(std::move(other));
         return *this;
      }

      void swap(query &);

      string get_query_string() const;
      query_result execute_for_result();
      int64_t execute();

      optional<string> execute_for_string();
      optional<int64_t> execute_for_int64();
      optional<double> execute_for_double();

      void bind(std::size_t i, field_value value);
      void bind_optional(std::size_t i, optional_field_value value);
      void bind_null(std::size_t i);

      void bind(string_view key, field_value value);
      void bind_optional(string_view key, optional_field_value value);
      void bind_null(string_view key);
   };

} // namespace xdb