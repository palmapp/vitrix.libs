#pragma once

#include "query.h"
#include "query_result.h"
#include "stack_unwinding_detector.h"
#include "transaction_guard.h"
#include "types.h"
#include <string>
#include "exception.h"
#include "iconnection.h"
#include "idriver.h"
#include "iquery.h"
#include "iquery_result.h"
#include "itransaction.h"
#include "stm_builder.h"

namespace xdb
{
   class connection_pool_releaser
   {
   public:
      virtual ~connection_pool_releaser() = default;
      virtual void return_to_pool(ptr<backend::iconnection> connection) = 0;
   };

   class connection : public pimpl<backend::iconnection>
   {
   public:
      static void register_driver(std::unique_ptr<backend::ixdb_driver> driver);

      static connection open(const std::string& connection_string);
      static ptr<backend::iconnection> open_naked(const std::string& connection_string);

      using pimpl<backend::iconnection>::pimpl;

      connection(ptr<backend::iconnection> connection, connection_pool_releaser* pool = nullptr);

      ~connection();

      inline connection(connection&& other) : pimpl<backend::iconnection>(std::move(other))
      {
         this->pool_ = other.pool_;
      }

      inline connection& operator=(connection&& other)
      {
         pimpl<backend::iconnection>::operator=(std::move(other));
         this->pool_ = other.pool_;

         return *this;
      }

      void swap(connection&);
      bool is_connected() const;

      connection(const connection&) = delete;
      connection& operator=(const connection&) = delete;

      transaction_guard begin_transaction(isolation_level level = isolation_level::serializable);

      query create_query(const std::string& query);

      std::int64_t execute_query_no_except(const std::string& query) noexcept;
      std::int64_t execute_query(const std::string& query);
      query_result execute_query_for_result(const std::string& query);

      optional<std::int64_t> get_int64_last_row_id();
      optional<std::string> get_string_last_row_id();

      stm_builder get_builder(stm_type type, std::string table_name);

      void close();

      const char* sql_dialect() const;

      inline query create_query_sv(std::string_view view)
      {
         return create_query(static_cast<std::string>(view));
      }

      inline std::int64_t execute_query_no_except_sv(std::string_view view)
      {
         return execute_query_no_except(static_cast<std::string>(view));
      }

      inline std::int64_t execute_query_sv(std::string_view view)
      {
         return execute_query(static_cast<std::string>(view));
      }

      inline query_result execute_query_for_result_sv(std::string_view view)
      {
         return execute_query_for_result(static_cast<std::string>(view));
      }

      template <class... Ts>
      std::int64_t execute_query_sv(std::string_view query, Ts&&... bind_values);

      template <class... Ts>
      query create_query_sv(std::string_view query, Ts&&... bind_values);

      template <class... Ts>
      std::int64_t execute_query(std::string const& query, Ts&&... bind_values);

      template <class... Ts>
      query create_query(std::string const& query, Ts&&... bind_values);

      connection_pool_releaser* pool_ = nullptr;
   };

   struct connection_provider
   {
      virtual ~connection_provider() = default;
      virtual connection open() = 0;
   };

   struct default_connection_provider : public connection_provider
   {
   public:
      default_connection_provider(std::string connection_string);

      connection open() override;

   private:
      std::string connection_string_;
   };
} // namespace xdb

#include "connection.impl.h"
