#pragma once

namespace xdb
{
   enum class isolation_level
   {
      serializable,
      repeatable_read,
      read_commited,
      read_uncommited
   };
}