//
// Created by jhrub on 13.07.2021.
//

#pragma once

#include "connection.h"
#include <functional>
#include <map>

namespace xdb
{
   struct migration_result
   {
      bool migrated = false;
      optional<string> reason = {};
   };

   using migration_step_funct_t = std::function<migration_result(xdb::connection &, std::int64_t)>;
   using dialect_initializer_funct_t = std::function<void(const std::string &)>;

   struct db_schema
   {
   public:
      void add_sql_dialect_initializer(dialect_initializer_funct_t dialect_funct);
      void add_migration(std::string const &scope, migration_step_funct_t funct);
      void execute(xdb::connection &db);

   private:
      std::set<std::string> scopes_;
      std::vector<std::pair<std::string, migration_step_funct_t>> migrations_;
      std::vector<dialect_initializer_funct_t> dialects_;
   };
} // namespace xdb