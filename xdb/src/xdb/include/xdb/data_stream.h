//
// Created by jhrub on 03.06.2021.
//

#pragma once
#include "connection.h"
#include "query_result.h"
#include <commons/data/data_stream.h>
#include <memory>

namespace xdb
{
   class data_stream : public commons::data::data_stream
   {
    public:
      /**
       * This creates a new data_stream that is designed to be returned from a function
       *
       * @param connection - connection holder is required param - it must remain active until query finishes,
       * connection might be reused again after query finishes, by calling release connection
       * @param q - created bound query to be executed
       */
      data_stream(connection conn, query q);
      data_stream(connection conn, query q, query_result result);

      optional<array_view<string_view>> get_column_names() final;
      optional<commons::data::data_row> next_row() final;

      /**
       * This method moves cursor to the end and releases the connection,
       * caller must make sure that the returned connection will have longer life time than
       * this object.
       *
       * @return released connection
       */
      connection release_connection();

    private:
      std::vector<optional_field_value> row_;
      std::vector<std::string> column_name_string_;
      std::vector<string_view> column_name_views_;

      bool has_first_ = false;
      query_result::row_iterator it_;
      query_result::row_iterator end_;

      connection connection_holder_;
      query query_holder_;
      query_result result_;
   };

} // namespace xdb