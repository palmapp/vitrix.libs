#pragma once
#include "exception.h"
#include <commons/basic_types.h>
#include <commons/data/data_table.h>

#include <cstdint>
#include <memory>
#include <optional>
#include <type_traits>
#include <vector>

namespace xdb
{
   using namespace commons::basic_types;

   template <class T>
   using ptr = std::unique_ptr<T>;

   template <class T, class... ArgT>
   ptr<T> make_ptr(ArgT &&...arg)
   {
      return std::make_unique<T>(std::forward<ArgT>(arg)...);
   }

} // namespace xdb