#pragma once

#include "types.h"
#include <cstdint>
#include <string>

#include <functional>

namespace xdb
{
   namespace backend
   {
      class iquery_result
      {
       public:
         virtual ~iquery_result() = default;
         iquery_result() = default;

         iquery_result(const iquery_result &) = delete;
         iquery_result(iquery_result &&) = delete;
         iquery_result &operator=(iquery_result &&) = delete;
         iquery_result &operator=(const iquery_result &) = delete;

         virtual std::size_t get_column_count() = 0;
         virtual string_view get_column_name(std::size_t idx) = 0;
         virtual std::vector<string> get_column_names() = 0;
         virtual void fill_column_names(mutable_array_view<string_view> column_names) = 0;

         virtual optional<bool> get_bool(std::size_t col) = 0;
         virtual optional<i8> get_int8(std::size_t col) = 0;
         virtual optional<i16> get_int16(std::size_t col) = 0;
         virtual optional<i32> get_int32(std::size_t col) = 0;
         virtual optional<i64> get_int64(std::size_t col) = 0;

         virtual optional<u8> get_uint8(std::size_t col) = 0;
         virtual optional<u16> get_uint16(std::size_t col) = 0;
         virtual optional<u32> get_uint32(std::size_t col) = 0;
         virtual optional<u64> get_uint64(std::size_t col) = 0;

         virtual optional<string> get_string(std::size_t col) = 0;
         virtual optional<string_view> get_string_view(std::size_t col) = 0;

         virtual optional<fl64> get_double(std::size_t col) = 0;
         virtual optional<fl32> get_float(std::size_t col) = 0;
         virtual optional_field_value get_value(std::size_t col) = 0;
         virtual void fill_row_values(mutable_array_view<optional_field_value> fields) = 0;
         virtual void fill_row_values(mutable_array_view<field_value> fields) = 0;

         virtual bool next_row() = 0;
         virtual bool first_row() = 0;
      };

   } // namespace backend
} // namespace xdb
