#pragma once
#include "isolation_level.h"
#include "pimpl.h"
namespace xdb
{
   namespace backend
   {
      class itransaction;
   }

   class transaction_guard : public pimpl<backend::itransaction>
   {
    public:
      ~transaction_guard() noexcept(false);

      using pimpl<backend::itransaction>::pimpl;

      inline transaction_guard(transaction_guard &&other) : pimpl<backend::itransaction>(std::move(other)) {}

      inline transaction_guard &operator=(transaction_guard &&other)
      {
         pimpl<backend::itransaction>::operator=(std::move(other));
         return *this;
      }

      void commit();
      void rollback();
   };
} // namespace xdb