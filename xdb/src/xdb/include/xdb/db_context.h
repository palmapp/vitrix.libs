#pragma once
#include "connection.h"
#include <commons/tx_context.h>

namespace xdb
{
   class db_context : public commons::tx_context
   {
    public:
      static const std::int64_t required_type_id;
      virtual ~db_context() noexcept(false) = default;

      virtual xdb::connection &get_connection() = 0;

      std::int64_t get_context_type_id() const noexcept override;
   };
} // namespace xdb