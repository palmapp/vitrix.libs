//
// Created by jhrub on 31.01.2022.
//

#pragma once
#include "connection.h"
#include <utility>

namespace xdb
{
   namespace impl
   {

      template <class T, class... Ts>
      void bind(query &q, std::size_t &idx, T &&value, Ts &&... rest)
      {
         q.bind(idx++, std::forward<T>(value));

         if constexpr (sizeof...(rest) > 0)
         {
            bind(q, idx, std::forward<Ts>(rest)...);
         }
      }
   } // namespace impl

   template <class... Ts>
   std::int64_t connection::execute_query_sv(std::string_view query, Ts &&... bind_values)
   {
      auto stm = create_query_sv(query);

      std::size_t i = 1;
      impl::bind(stm, i, std::forward<Ts...>(bind_values...));

      return stm.execute();
   }

   template <class... Ts>
   query connection::create_query_sv(std::string_view query, Ts &&... bind_values)
   {
      auto stm = create_query_sv(query);

      std::size_t i = 1;
      impl::bind(stm, i, std::forward<Ts>(bind_values)...);

      return stm;
   }

   template <class... Ts>
   std::int64_t connection::execute_query(std::string const &query, Ts &&... bind_values)
   {
      auto stm = create_query(query);

      std::size_t i = 1;
      impl::bind(stm, i, std::forward<Ts>(bind_values)...);

      return stm.execute();
   }

   template <class... Ts>
   query connection::create_query(std::string const &query, Ts &&... bind_values)
   {
      auto stm = create_query(query);

      std::size_t i = 1;
      impl::bind(stm, i, std::forward<Ts>(bind_values)...);

      return stm;
   }

} // namespace xdb