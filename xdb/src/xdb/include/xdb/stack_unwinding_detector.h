#pragma once

#include <memory>

namespace xdb
{
   class stack_unwinding_detector
   {
    public:
      inline stack_unwinding_detector()
      {
         _uncaught_exceptions = std::uncaught_exceptions();
      }

      template <class FunctionT>
      inline void catch_exceptions_if_unwinding_in_process(FunctionT destroy_funct)
      {
         if (is_stack_unwinding_in_process())
         {
            try
            {
               destroy_funct();
            }
            catch (...)
            {
               // I tried to clean up properly but it failed
            }
         }
         else
         {
            // it is safe to throw now
            destroy_funct();
         }
      }

      inline bool is_stack_unwinding_in_process()
      {
         return _uncaught_exceptions != std::uncaught_exceptions();
      }

    private:
      int _uncaught_exceptions;
   };
} // namespace xdb