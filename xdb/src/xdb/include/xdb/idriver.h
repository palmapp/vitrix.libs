#pragma once

#include "pimpl.h"
namespace xdb
{
   namespace backend
   {
      class iconnection;
      class ixdb_driver
      {
       public:
         virtual ~ixdb_driver() = default;
         ixdb_driver() = default;

         ixdb_driver(const ixdb_driver &) = delete;
         ixdb_driver(ixdb_driver &&) = delete;
         ixdb_driver &operator=(ixdb_driver &&) = delete;
         ixdb_driver &operator=(const ixdb_driver &) = delete;

         virtual const char *uri_schema() const = 0;

         virtual ptr<backend::iconnection> open(const std::string &connection_string) = 0;
      };
   } // namespace backend

} // namespace xdb
