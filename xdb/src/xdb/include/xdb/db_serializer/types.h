//
// Created by jhrub on 30.05.2021.
//

#pragma once

#include "../connection.h"
#include "../query.h"
#include "../query_result.h"
#include "../stm_builder.h"

#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <chrono>
#include <commons/data/data_query.h>
#include <commons/make_string.h>
#include <gsl/narrow>
#include <map>
#include <serialization/binary_types.h>
#include <type_traits>
#include <variant>
#include <vector>
#include <visit_struct/visit_struct.hpp>
namespace xdb::serialization
{
   using stm_builder = stm_builder;
   using column = column;
   using stm_type = stm_type;

   using namespace spider::serialization::binary::types;

   template <class T>
   using unordered_set = boost::unordered_set<T>;

   template <class K, class T>
   using unordered_map = boost::unordered_map<K, T>;

} // namespace xdb::serialization