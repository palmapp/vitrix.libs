//
// Created by jhrub on 30.05.2021.
//

#pragma once
#include "types.h"

namespace xdb::serialization
{
   struct /*[[deprecated("Use query_value_binder instead")]]*/ query_binder
   {

      xdb::query &query;
      int idx;
   };

   template <class Data>
   class /*[[deprecated("Use query_value_binder instead")]]*/ query_binder_visitor
   {
      query_binder &binder_;

    public:
      query_binder_visitor(query_binder &binder) : binder_(binder) {}

      void operator()(const std::string &, bool &val)
      {
         int64_t i = val ? 1 : 0;
         binder_.query.bind(binder_.idx++, i);
      }

      void operator()(const std::string &, int32_t &val)
      {
         binder_.query.bind(binder_.idx++, int64_t{val});
      }

      void operator()(const std::string &, int8_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, int16_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, int64_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, uint8_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, uint16_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, uint32_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, uint64_t &val)
      {
         auto tmp = val;
         binder_.query.bind(binder_.idx++, tmp);
      }

      void operator()(const std::string &, double &val)
      {
         binder_.query.bind(binder_.idx++, val);
      }

      void operator()(const std::string &, std::chrono::system_clock::time_point &val)
      {
         binder_.query.bind(binder_.idx++, val.time_since_epoch().count());
      }

      void operator()(const std::string &, std::string &val)
      {
         binder_.query.bind(binder_.idx++, val);
      }

      void operator()(const std::string &name, s16_le &val)
      {
         binder_.query.bind(binder_.idx++, val.value);
      }

      void operator()(const std::string &name, s32_le &val)
      {
         binder_.query.bind(binder_.idx++, val.value);
      }

      void operator()(const std::string &name, s64_le &val)
      {
         binder_.query.bind(binder_.idx++, val.value);
      }

      void operator()(const std::string &name, u16_le &val)
      {
         binder_.query.bind(binder_.idx++, val.value);
      }

      void operator()(const std::string &name, u32_le &val)
      {
         binder_.query.bind(binder_.idx++, val.value);
      }

      void operator()(const std::string &name, u64_le &val)
      {
         binder_.query.bind(binder_.idx++, val.value);
      }

      template <typename BinaryType, std::size_t Size>
      void operator()(const std::string &name, fixed_array<BinaryType, Size> &arr)
      {
         std::stringstream ss{};

         for (int i = 0; i < Size; ++i)
         {
            ss.str(std::string{});
            ss << name << "_" << i;
            this->operator()(ss.str(), arr.values.at(i));
         }
      }

      template <class ComplexStruct>
      void operator()(const std::string &name, ComplexStruct &data)
      {
         query_binder_visitor<ComplexStruct> visitor{binder_};
         visit_struct::for_each(data, visitor);
      }
   };
} // namespace xdb::serialization