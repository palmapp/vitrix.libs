//
// Created by jhrub on 30.05.2021.
//

#pragma once
#include "object_mapping_visitor.h"
#include "query_binder.h"
#include "query_value_binder.h"
#include "select_query_visitor.h"
#include "stm_builder_visitor.h"
#include "table_info.h"
#include <algorithm>

namespace xdb::serialization
{

   template <class EntityT>
   inline void fill_entity(EntityT &val, query_result::row &row, std::size_t start_index = 0,
                           unordered_set<std::string> *skipped_fields = nullptr)
   {
      object_mapping_visitor visitor{row, start_index};
      visitor.skipped_fields = skipped_fields;
      visit_struct::for_each(val, visitor);
   }

   template <class EntityT>
   inline std::string get_table_name()
   {
      return table_info<EntityT>::get_table_name();
   }

   const auto is_suspicious_char = [](char c)
   { return std::isspace(c) || c == ']' || c == '[' || c == '(' || c == ')'; };

   inline string create_select_query(string_view table_name, const commons::data::data_query &query = {},
                                     bool count_with_filter = false)
   {
      std::stringstream ss;
      ss << "SELECT ";
      if (count_with_filter)
      {
         ss << " count(*) ";
      }
      else
      {
         if (query.columns.has_value())
         {
            for (auto const &col : query.columns.value())
            {
               if (std::any_of(col.begin(), col.end(), is_suspicious_char))
               {
                  throw std::range_error(commons::make_string("suspicious input sql column detected : ", col));
               }
            }

            bool has_col = false;
            for (auto const &col : query.columns.value())
            {
               if (!has_col)
               {
                  has_col = true;
               }
               else
               {
                  ss << ",";
               }
               ss << col;
            }
         }
         else
         {
            ss << "*";
         }
      }
      ss << " FROM " << table_name;

      if (!query.query.empty())
      {
         ss << " WHERE " << query.query;
      }

      if (!count_with_filter)
      {
         if (query.order_by && !query.order_by->empty())
         {
            ss << " ORDER BY ";
            bool first = true;
            for (const auto &column : *query.order_by)
            {
               if (std::any_of(column.name.begin(), column.name.end(), is_suspicious_char))
               {
                  throw std::range_error(
                      commons::make_string("suspicious input sql order by column detected : ", column.name));
               }

               if (first)
               {
                  ss << column.name;
                  first = false;
               }
               else
               {
                  ss << ", " << column.name;
               }

               ss << " " << (column.is_asc ? "ASC" : "DESC");
            }
         }
         if (query.limit > 0)
         {
            ss << " LIMIT " << query.limit;
         }

         if (query.offset > 0)
         {
            ss << " OFFSET " << query.offset;
         }
      }
      ss << ";";
      return ss.str();
   }

   template <class EntityT>
   std::vector<string> get_column_names()
   {
      EntityT val{};
      column_names_visitor visitor{};
      visit_struct::for_each(val, visitor);

      return std::move(visitor.fields);
   }

   template <class EntityT>
   std::string create_select_query(const commons::data::data_query &query = {},
                                   std::optional<std::string> table_name = {})
   {
      if (!query.columns.has_value())
      {
         commons::data::data_query query_copy = query;
         query_copy.columns = get_column_names<EntityT>();

         return create_select_query(table_name.value_or(get_table_name<EntityT>()), query_copy);
      }
      else
      {
         return create_select_query(table_name.value_or(get_table_name<EntityT>()), query);
      }
   }

   template <class RecordT>
   std::string make_insert_statement(connection &conn, std::optional<std::string> table_name = {})
   {
      static auto ignored_field = boost::unordered_set<std::string>{table_info<RecordT>::get_id_field_name()};
      auto builder = conn.get_builder(stm_type::insert, table_name.value_or(get_table_name<RecordT>()));
      stm_builder_visitor<RecordT> visitor{builder};

      visitor.skipped_fields = &ignored_field;

      RecordT tmp;
      visit_struct::for_each(tmp, visitor);

      return builder.get_sql();
   }

   template <class RecordT>
   std::string make_insert_statement(connection &conn, boost::unordered_set<std::string> const &ignored_fields,
                                     std::optional<std::string> table_name = {})
   {
      auto builder = conn.get_builder(stm_type::insert, table_name.value_or(get_table_name<RecordT>()));
      stm_builder_visitor<RecordT> visitor{builder};

      visitor.skipped_fields = &ignored_fields;

      RecordT tmp;
      visit_struct::for_each(tmp, visitor);

      return builder.get_sql();
   }

   template <class RecordT>
   const std::string &get_insert_statement(connection &conn)
   {
      static auto cached_stm = make_insert_statement<RecordT>(conn);

      return cached_stm;
   }

   template <class RecordT>
   std::string make_update_statement(connection &conn, std::string const &id_field,
                                     std::optional<std::string> table_name = {})
   {
      auto builder = conn.get_builder(stm_type::update, table_name.value_or(get_table_name<RecordT>()));
      builder.set_id_field(id_field);

      stm_builder_visitor<RecordT> visitor{builder};

      RecordT tmp;
      visit_struct::for_each(tmp, visitor);

      return builder.get_sql();
   }

   template <class RecordT>
   const std::string &get_update_statement(connection &conn, std::string const &id_field)
   {
      static auto update_stm = make_update_statement<RecordT>(conn, id_field);

      return update_stm;
   }

   template <class RecordT>
   std::string make_replace_statement(connection &conn, boost::unordered_set<std::string> const &skipped_fields,
                                      std::optional<std::string> table_name = {})
   {
      auto builder = conn.get_builder(stm_type::replace, table_name.value_or(get_table_name<RecordT>()));

      stm_builder_visitor<RecordT> visitor{builder};
      visitor.skipped_fields = &skipped_fields;

      RecordT tmp;
      visit_struct::for_each(tmp, visitor);

      return builder.get_sql();
   }

   template <class RecordT>
   std::string make_replace_statement(connection &conn, std::optional<std::string> table_name = {})
   {
      auto builder = conn.get_builder(stm_type::replace, table_name.value_or(get_table_name<RecordT>()));

      stm_builder_visitor<RecordT> visitor{builder};

      RecordT tmp;
      visit_struct::for_each(tmp, visitor);

      return builder.get_sql();
   }

   template <class RecordT>
   const std::string &get_replace_statement(connection &conn)
   {
      static auto cached_stm = make_replace_statement<RecordT>(conn);

      return cached_stm;
   }

   template <class RecordT>
   void visit_default_value(stm_builder &builder, boost::unordered_set<std::string> const &skipped_fields)
   {
      stm_builder_visitor<RecordT> visitor{builder};
      visitor.skipped_fields = &skipped_fields;
      RecordT tmp;

      visit_struct::for_each(tmp, visitor);
   }

   template <class RecordT>
   void visit_default_value(stm_builder &builder)
   {
      stm_builder_visitor<RecordT> visitor{builder};

      RecordT tmp;
      visit_struct::for_each(tmp, visitor);
   }

   template <class Data, int beginBindFrom = 1>
   std::size_t bind_query(query &query, Data const &data)
   {
      query_value_binder binder{query, beginBindFrom};

      binder.bind(data);
      return binder.next_index();
   }

   template <class Data, int beginBindFrom = 1>
   std::size_t bind_query(query &query, Data const &data, boost::unordered_set<std::string> const &skipped_fields)
   {
      query_value_binder binder{query, beginBindFrom};
      binder.skipped_fields = &skipped_fields;

      binder.bind(data);
      return binder.next_index();
   }

   template <class EntityT>
   void create_table(connection &connection, std::optional<std::string> table_name = {})
   {
      auto table_builder =
          connection.get_builder(stm_type::create_table, table_name.value_or(get_table_name<EntityT>()));
      visit_default_value<EntityT>(table_builder);
      connection.execute_query(table_builder.get_sql());
   }

   template <class EntityT>
   inline stm_builder prepare_create_stmt(connection &conn, boost::unordered_set<std::string> const &skipped_fields,
                                          std::optional<std::string> table_name = {})
   {
      auto table_builder = conn.get_builder(stm_type::create_table, table_name.value_or(get_table_name<EntityT>()));
      visit_default_value<EntityT>(table_builder, skipped_fields);
      return table_builder;
   }

   template <class EntityT>
   inline stm_builder prepare_create_stmt(connection &conn, std::optional<std::string> table_name = {})
   {
      auto table_builder = conn.get_builder(stm_type::create_table, table_name.value_or(get_table_name<EntityT>()));
      visit_default_value<EntityT>(table_builder);
      return table_builder;
   }

   inline void add_primary_constraint(stm_builder &builder, const char *column_name = "id")
   {
      builder.add_constraint(column_name, "PRIMARY KEY");
   }

   inline void add_unique_constraint(stm_builder &builder, const std::string &columns,
                                     const std::string &conflict_res = "REPLACE")
   {
      std::stringstream ss{};
      ss << "UNIQUE(" << columns << ") ON CONFLICT " << conflict_res;
      builder.add_constraint(ss.str());
   }

   template <class ForeignEntityT>
   inline void add_foreign_key(stm_builder &builder, const char *column_name, const std::string &foreign_column_name,
                               const std::string &on_delete = {}, const std::string &on_update = {})
   {
      std::stringstream ss{};

      ss << "FOREIGN KEY (" << column_name << ") REFERENCES " << get_table_name<ForeignEntityT>() << "("
         << foreign_column_name << ")";

      if (!on_delete.empty())
      {
         ss << " ON DELETE " << on_delete;
      }

      if (!on_update.empty())
      {
         ss << " ON UPDATE " << on_update;
      }

      builder.add_constraint(ss.str());
   }

} // namespace xdb::serialization