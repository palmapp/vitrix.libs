//
// Created by jhrub on 30.05.2021.
//

#pragma once

namespace xdb::serialization
{
   class query_value_binder;

   template <class T>
   struct is_optional : std::false_type
   {
   };

   template <class T>
   struct is_optional<std::optional<T>> : std::true_type
   {
   };

   template <class T>
   constexpr bool is_optional_v = is_optional<T>::value;

   template <class T>
   void conversion_bind(query_value_binder &binder, const T &value);

   struct default_conversion_binder
   {
      template <class T>
      void operator()(query_value_binder &binder, const T &value)
      {
         conversion_bind(binder, value);
      }
   };

   class query_value_binder
   {
      xdb::query &query_;
      std::size_t idx_;

    public:
      inline std::size_t next_index()
      {
         return idx_++;
      }
      unordered_set<std::string> const *skipped_fields = nullptr;

      inline query_value_binder(xdb::query &q, std::size_t begin_index = 1) : query_(q), idx_(begin_index) {}

      template <class T, class ConvertorT = default_conversion_binder>
      void bind(const T &value, ConvertorT &&convertor = ConvertorT{})
      {
         static_assert(!is_optional_v<T>, "Value cannot be optional. Use bind(std::optional<T>) instead.");

         using namespace std;
         if constexpr (is_same_v<T, field_value>)
         {
            query_.bind(next_index(), value);
         }
         else if constexpr (is_constructible_v<field_value_variant, T>)
         {
            query_.bind(next_index(), field_value{value});
         }
         else
         {
            convertor(*this, value);
         }
      }

      template <class T, class ConvertorT = default_conversion_binder>
      void bind(const std::optional<T> &value, ConvertorT &&convertor = ConvertorT{})
      {
         static_assert(is_optional_v<std::decay_t<decltype(value)>>, "Value must be optional.");

         if (value.has_value())
         {
            this->bind(value.value(), std::forward<ConvertorT>(convertor));
         }
         else
         {
            query_.bind_null(next_index());
         }
      }

      template <class T>
      void operator()(string_view name, T const &value)
      {
         if (skipped_fields != nullptr && skipped_fields->find(static_cast<string>(name)) != skipped_fields->end())
         {
            return;
         }

         this->bind(value);
      }
   };

   template <class T>
   void conversion_bind(query_value_binder &binder, const T &value)
   {
      using namespace std;
      using namespace std::chrono;

      if constexpr (is_same_v<T, system_clock::time_point>)
      {
         binder.bind(chrono::duration_cast<chrono::hours>(value.time_since_epoch()).count());
      }
      else if constexpr (visit_struct::traits::is_visitable<T>::value)
      {
         visit_struct::for_each(value, binder);
      }
      else
      {
         static_assert(is_same_v<T, system_clock::time_point> || visit_struct::traits::is_visitable<T>::value,
                       "Supply your own conversion with a callable signature  void operator(query_binder& , const T&)");
      }
   }
} // namespace xdb::serialization