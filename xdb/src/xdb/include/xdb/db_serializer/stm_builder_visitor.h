//
// Created by jhrub on 30.05.2021.
//

#pragma once
#include "types.h"
#include <optional>
#include <unordered_set>

namespace xdb::serialization
{
   struct stm_declarator
   {
      static void declare(stm_builder &builder, std::string_view name, i8 const &)
      {
         builder.declare_int8_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, i16 const &)
      {
         builder.declare_int16_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, i32 const &)
      {
         builder.declare_int32_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, i64 const &)
      {
         builder.declare_int64_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, u8 const &)
      {
         builder.declare_uint8_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, u16 const &)
      {
         builder.declare_uint16_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, u32 const &)
      {
         builder.declare_uint32_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, u64 const &)
      {
         builder.declare_uint64_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, fl32 const &)
      {
         builder.declare_double_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, fl64 const &)
      {
         builder.declare_double_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, bool const &)
      {
         builder.declare_bool_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, std::chrono::system_clock::time_point const &)
      {
         builder.declare_int64_field(name);
      }

      static void declare(stm_builder &builder, std::string_view name, string_view view)
      {
         builder.declare_text_field(name);
      }
   };

   template <class T>
   struct support_builder
   {
      static void declare(stm_builder &builder, std::string_view name, T const &field)
      {
         stm_declarator::declare(builder, name, field);
      }
   };

   template <class T>
   struct support_builder<xdb::optional<T>>
   {
      static void declare(stm_builder &builder, std::string_view name, xdb::optional<T> const &field)
      {
         typename xdb::optional<T>::value_type type_value{};
         if constexpr (std::is_integral_v<decltype(type_value)> || std::is_floating_point_v<decltype(type_value)>)
         {
            type_value = 0;
         }

         stm_declarator::declare(builder, name, type_value);
         builder.add_constraint(name, "NULL");
      };
   };

   template <class T, class Endianess>
   struct support_builder<multibyte_integer<T, Endianess>>
   {
      static void declare(stm_builder &builder, std::string_view name, multibyte_integer<T, Endianess> const &field)
      {
         stm_declarator::declare(builder, name, field.value);
      }
   };

   template <typename BinaryType, std::size_t Size>
   struct support_builder<fixed_array<BinaryType, Size>>
   {
      static void declare(stm_builder &builder, std::string_view name, fixed_array<BinaryType, Size> const &field)
      {
         std::stringstream ss{};

         for (int i = 0; i < Size; ++i)
         {
            ss.str(std::string{});
            ss << name << "_" << i;

            BinaryType type{0};

            auto field_name = ss.str();
            stm_declarator::declare(builder, field_name, type);
         }
      }
   };

   template <class Data>
   class stm_builder_visitor
   {

    public:
      boost::unordered_set<std::string> const *skipped_fields = nullptr;
      stm_builder_visitor(stm_builder &builder) : builder_(builder) {}

      bool should_process(const std::string &name)
      {
         if (skipped_fields != nullptr)
         {
            auto &dict = *skipped_fields;
            return dict.find(name) == dict.end();
         }

         return true;
      }

      template <class T>
      void operator()(const std::string &name, T const &data)
      {
         if (should_process(name))
         {
            if constexpr (visit_struct::traits::is_visitable<T>::value)
            {
               using namespace std;
               builder_.push_prefix(name);
               stm_builder_visitor<T> visitor{builder_};
               visit_struct::for_each(data, visitor);
               builder_.pop_prefix();
            }
            else if constexpr (std::is_enum_v<T>)
            {
               if constexpr (sizeof(T) == 1)
               {
                  builder_.declare_int8_field(name);
               }
               else if constexpr (sizeof(T) == 2)
               {
                  builder_.declare_int16_field(name);
               }
               else if constexpr (sizeof(T) == 4)
               {
                  builder_.declare_int32_field(name);
               }
               else if constexpr (sizeof(T) == 8)
               {
                  builder_.declare_int64_field(name);
               }
               else
               {
                  // not supported
               }
            }
            else
            {
               support_builder<T>::declare(builder_, name, data);
            }
         }
      }

    protected:
      stm_builder &builder_;
      boost::unordered_set<std::string> skipped_fields_;
   };
} // namespace xdb::serialization