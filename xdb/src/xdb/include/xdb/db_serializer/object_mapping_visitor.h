//
// Created by jhrub on 30.05.2021.
//

#pragma once
#include "types.h"

namespace xdb::serialization
{

   class object_mapping_visitor
   {
      xdb::query_result::row &row_;
      std::size_t idx_;

    public:
      unordered_set<std::string> *skipped_fields = nullptr;

      explicit object_mapping_visitor(xdb::query_result::row &row, std::size_t start_index = 0)
          : row_(row), idx_(start_index)
      {
      }

      bool is_processed(const std::string &field) const
      {
         if (skipped_fields != nullptr)
         {
            if (auto it = skipped_fields->find(field); it != skipped_fields->end())
            {
               return false;
            }
         }

         return true;
      }

      void operator()(const std::string &f, bool &val)
      {
         if (is_processed(f))
         {
            val = row_.get_int64(idx_).value_or(0) == 1;
            ++idx_;
         }
      }

      void operator()(const std::string &f, int32_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<int32_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, int8_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<int8_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, int16_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<int16_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, int64_t &val)
      {
         if (is_processed(f))
         {
            val = row_.get_int64(idx_).value_or(0);
            ++idx_;
         }
      }

      void operator()(const std::string &f, uint8_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<uint8_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, uint16_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<uint16_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, uint32_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<uint32_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, uint64_t &val)
      {
         if (is_processed(f))
         {
            val = gsl::narrow<uint64_t>(row_.get_int64(idx_).value_or(0));
            ++idx_;
         }
      }

      void operator()(const std::string &f, double &val)
      {
         if (is_processed(f))
         {
            val = row_.get_double(idx_).value_or(0);
            ++idx_;
         }
      }

      void operator()(const std::string &f, float &val)
      {
         if (is_processed(f))
         {
            val = row_.get_float(idx_).value_or(0);
            ++idx_;
         }
      }

      void operator()(const std::string &f, std::chrono::system_clock::time_point &val)
      {
         if (is_processed(f))
         {
            val = std::chrono::time_point<std::chrono::system_clock>(
                std::chrono::milliseconds(row_.get_int64(idx_).value_or(0)));
            ++idx_;
         }
      }

      void operator()(const std::string &f, std::string &val)
      {
         if (is_processed(f))
         {
            val = row_.get_string(idx_).value_or("");
            ++idx_;
         }
      }

      void operator()(const std::string &name, s16_le &val)
      {
         int16_t rval = 0;
         this->operator()(name, rval);
         val = rval;
      }

      void operator()(const std::string &name, s32_le &val)
      {
         int32_t rval = 0;
         this->operator()(name, rval);
         val = rval;
      }

      void operator()(const std::string &name, s64_le &val)
      {
         int64_t rval = 0;
         this->operator()(name, rval);
         val = rval;
      }

      void operator()(const std::string &name, u16_le &val)
      {
         uint16_t rval = 0;
         this->operator()(name, rval);
         val = rval;
      }

      void operator()(const std::string &name, u32_le &val)
      {
         uint32_t rval = 0;
         this->operator()(name, rval);
         val = rval;
      }

      void operator()(const std::string &name, u64_le &val)
      {
         uint64_t rval = 0;
         this->operator()(name, rval);
         val = rval;
      }

      template <typename BinaryType, std::size_t Size>
      void operator()(const std::string &name, fixed_array<BinaryType, Size> &arr)
      {
         if (is_processed(name))
         {
            std::stringstream ss{};

            for (int i = 0; i < Size; ++i)
            {
               ss.str(std::string{});
               ss << name << "_" << i;
               this->operator()(ss.str(), arr.values.at(i));
            }
         }
      }

      template <class T>
      void operator()(const std::string &name, std::optional<T> &data)
      {
         if (is_processed(name))
         {
            if (row_.get_value(idx_).has_value())
            {
               auto &val = data.emplace();
               this->operator()(name, val);
            }
            else
            {
               data.reset();
               ++idx_;
            }
         }
      }

      template <class ComplexStruct>
      void operator()(const std::string &name, ComplexStruct &data)
      {
         object_mapping_visitor visitor{row_, idx_};
         visitor.skipped_fields = skipped_fields;
         visit_struct::for_each(data, visitor);
         idx_ = visitor.idx_;
      }
   };
} // namespace xdb::serialization