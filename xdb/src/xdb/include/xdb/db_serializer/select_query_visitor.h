//
// Created by jhrub on 30.05.2021.
//

#pragma once

#include "types.h"
namespace xdb::serialization
{
   class select_query_visitor
   {
      std::stringstream query_;
      int idx_ = 0;

    public:
      explicit select_query_visitor()
      {
         query_ << "SELECT ";
      }

      template <typename BinaryType, std::size_t Size>
      void operator()(const std::string &name, fixed_array<BinaryType, Size> &arr)
      {
         std::stringstream ss{};

         for (int i = 0; i < Size; ++i)
         {
            ss.str(std::string{});
            ss << name << "_" << i;
            this->operator()(ss.str(), arr.values.at(i));
         }
      }

      template <typename T>
      void operator()(const std::string &name, T &val)
      {
         if (idx_ != 0)
         {
            query_ << ", " << name;
         }
         else
         {
            query_ << name;
         }

         ++idx_;
      }

      std::string get()
      {
         return query_.str();
      }
   };

   struct column_names_visitor
   {
    public:
      vector<string> fields;
      column_names_visitor()
      {
         fields.reserve(20);
      }

      template <typename BinaryType, std::size_t Size>
      void operator()(const std::string &name, fixed_array<BinaryType, Size> &arr)
      {
         std::stringstream ss{};

         for (int i = 0; i < Size; ++i)
         {
            ss.str(std::string{});
            ss << name << "_" << i;
            this->operator()(ss.str(), arr.values.at(i));
         }
      }

      template <typename T>
      void operator()(const std::string &name, T &val)
      {
         fields.push_back(name);
      }
   };
} // namespace xdb::serialization