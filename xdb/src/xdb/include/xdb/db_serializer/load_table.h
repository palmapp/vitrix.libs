//
// Created by jhrub on 30.05.2021.
//

#pragma once

#include "query_utils.h"
#include "query_value_binder.h"

namespace xdb::serialization
{
   inline void bind_position_arguments(query &q, const commons::data::data_query &params)
   {
      query_value_binder binder{q};
      for (auto &param : params.position_arguments.value())
      {
         binder.bind(param);
      }
   }

   inline void bind_named_arguments(query &q, const commons::data::data_query &params)
   {
      for (auto &[k, v] : params.named_arguments.value())
      {
         if (v.has_value())
         {
            q.bind(k, v.value());
         }
         else
         {
            q.bind_null(k);
         }
      }
   }

   inline void bind_arguments(query &q, const commons::data::data_query &params)
   {
      if (params.position_arguments.has_value())
      {
         bind_position_arguments(q, params);
      }
      else if (params.named_arguments.has_value())
      {
         bind_named_arguments(q, params);
      }
   }

   template <class EntityT>
   xdb::query prepare_select_query(connection &conn, const commons::data::data_query &params = {},
                                   std::optional<std::string> table_name = {})
   {
      query q = conn.create_query(create_select_query<EntityT>(params, table_name));
      bind_arguments(q, params);

      return q;
   }

   template <class EntityT, class OutIt>
   inline OutIt load_table_into_output_iterator(OutIt out, query_result result)
   {
      for (auto &row : result)
      {
         if constexpr (std::is_integral_v<EntityT>)
         {
            if constexpr (std::is_signed_v<EntityT>)
            {
               *out++ = gsl::narrow<EntityT>(row.get_int64().value_or(0));
            }
            else
            {
               *out++ = gsl::narrow<EntityT>(row.get_uint64().value_or(0));
            }
         }
         else
         {
            auto val = EntityT{};
            fill_entity(val, row);
            *out++ = std::move(val);
         }
      }

      return out;
   }

   template <class ContainerT>
   inline void load_table_into_container(ContainerT &container, query_result result)
   {
      using entity_type = typename ContainerT::value_type;
      load_table_into_output_iterator<entity_type>(std::back_inserter(container), std::move(result));
   }

   template <class EntityT>
   inline std::vector<EntityT> load_table(query_result result)
   {
      std::vector<EntityT> ret;
      ret.reserve(100);

      load_table_into_container(ret, std::move(result));

      return std::move(ret);
   }

   template <class EntityT>
   inline std::vector<EntityT> load_table(connection &conn, const commons::data::data_query &params = {},
                                          std::optional<std::string> table_name = {})
   {
      auto q = prepare_select_query<EntityT>(conn, params, table_name);
      bind_arguments(q, params);

      return load_table<EntityT>(q.execute_for_result());
   }

   template <class EntityT>
   inline std::optional<EntityT> load_single(query_result result)
   {
      std::vector<EntityT> ret;
      ret.reserve(1);

      load_table_into_container(ret, std::move(result));

      if (!ret.empty())
      {
         return {std::move(ret.front())};
      }
      return {};
   }

} // namespace xdb::serialization