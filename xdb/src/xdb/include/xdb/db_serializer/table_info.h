//
// Created by jhrub on 14.07.2021.
//

#pragma once

#include "types.h"

namespace xdb::serialization
{
   template <class EntityT>
   struct table_info
   {
      static std::string get_table_name()
      {
         std::string res = visit_struct::get_name<EntityT>();
         auto pos = res.find_last_of("::");
         return pos == std::string::npos ? res : res.substr(pos + 1);
      }

      static std::string get_id_field_name()
      {
         return "id";
      }
   }; // namespace table_info
} // namespace xdb::serialization