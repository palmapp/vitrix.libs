#pragma once

#include "types.h"

namespace xdb::backend
{
   class istm_builder
   {
    public:
      virtual ~istm_builder() = default;

      istm_builder() = default;

      virtual void set_id_field(string_view id_field) = 0;
      virtual void push_prefix(string_view prefix) = 0;

      virtual void pop_prefix() = 0;

      virtual void declare_text_field(string_view name) = 0;

      virtual void declare_int8_field(string_view name) = 0;

      virtual void declare_int16_field(string_view name) = 0;

      virtual void declare_int32_field(string_view name) = 0;

      virtual void declare_int64_field(string_view name) = 0;

      virtual void declare_uint8_field(string_view name) = 0;

      virtual void declare_uint16_field(string_view name) = 0;

      virtual void declare_uint32_field(string_view name) = 0;

      virtual void declare_uint64_field(string_view name) = 0;

      virtual void declare_double_field(string_view name) = 0;

      virtual void declare_bool_field(string_view name) = 0;

      virtual void add_constraint(string_view column_name, string_view constraint) = 0;
      virtual void set_exact_data_type(string_view column_name, string_view type) = 0;

      virtual void add_constraint(string_view constraint) = 0;

      virtual string get_sql() const = 0;
      virtual string_view get_table_name() const = 0;
   };

} // namespace xdb::backend