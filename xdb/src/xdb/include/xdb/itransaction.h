#pragma once

namespace xdb
{
   namespace backend
   {
      class itransaction
      {
       public:
         virtual ~itransaction() = default;
         itransaction() = default;

         itransaction(const itransaction &) = delete;
         itransaction(itransaction &&) = delete;
         itransaction &operator=(itransaction &&) = delete;
         itransaction &operator=(const itransaction &) = delete;

         virtual void commit() = 0;
         virtual void rollback() = 0;
      };
   } // namespace backend
} // namespace xdb