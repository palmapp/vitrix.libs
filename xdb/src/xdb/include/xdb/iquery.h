#pragma once

#include "types.h"
#include <cstdint>
#include <string>
#include <vector>

namespace xdb
{
   namespace backend
   {
      class iquery_result;
      class iquery
      {
       public:
         virtual ~iquery() = default;
         iquery() = default;

         iquery(const iquery &) = delete;
         iquery(iquery &&) = delete;
         iquery &operator=(iquery &&) = delete;
         iquery &operator=(const iquery &) = delete;

         virtual string get_query_string() const = 0;

         virtual void bind(std::size_t i, field_value value) = 0;
         virtual void bind_optional(std::size_t i, optional_field_value value) = 0;
         virtual void bind_null(std::size_t i) = 0;

         virtual void bind(string_view key, field_value value) = 0;
         virtual void bind_optional(string_view key, optional_field_value value) = 0;
         virtual void bind_null(string_view key) = 0;

         virtual ptr<iquery_result> execute_for_result() = 0;
         virtual i64 execute() = 0;
         virtual i64 execute_no_except() noexcept = 0;
         virtual optional<string> execute_for_string() = 0;
         virtual optional<i64> execute_for_int64() = 0;
         virtual optional<fl64> execute_for_double() = 0;
      };
   } // namespace backend
} // namespace xdb