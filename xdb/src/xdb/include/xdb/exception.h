#pragma once

#include <stdexcept>
#include <string>
namespace xdb
{

   class exception : public std::exception
   {
    public:
      enum class error_code
      {
         INVALID_CONNECTION_STRING,
         DRIVER_NOT_FOUND,
         DRIVER_EXCEPTION,
         CALLER_EXCEPTION
      };

      exception(error_code code, const std::string &message = "");

      error_code get_code() const;
      const char *what() const noexcept override;

    private:
      error_code _code;
      std::string _message;
   };

} // namespace xdb