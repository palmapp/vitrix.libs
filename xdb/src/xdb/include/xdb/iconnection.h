#pragma once

#include "isolation_level.h"
#include "istm_builder.h"
#include "query.h"
#include "query_result.h"
#include "stm_builder.h"
#include "types.h"

namespace xdb
{
   namespace backend
   {
      class itransaction;
      class iquery_result;
      class iconnection
      {
       public:
         virtual ~iconnection() = default;
         iconnection() = default;

         iconnection(const iconnection &) = delete;
         iconnection(iconnection &&) = delete;
         iconnection &operator=(iconnection &&) = delete;
         iconnection &operator=(const iconnection &) = delete;

         virtual bool is_connected() const = 0;

         virtual ptr<itransaction> begin_transaction(isolation_level level = isolation_level::serializable) = 0;
         virtual ptr<iquery> create_query(const std::string &query) = 0;

         virtual ptr<iquery_result> execute_query_for_result(const std::string &query) = 0;
         virtual std::int64_t execute_query(const std::string &query) = 0;
         virtual std::int64_t execute_query_no_except(const std::string &query) noexcept = 0;

         virtual std::int64_t get_int64_last_row_id() = 0;
         virtual std::string get_string_last_row_id() = 0;

         virtual ptr<istm_builder> get_builder(stm_type type, std::string table_name) = 0;
         virtual void close() = 0;

         virtual const char *sql_dialect() const = 0;

         virtual bool ping() noexcept = 0;
      };
   } // namespace backend
} // namespace xdb