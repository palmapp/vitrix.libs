#pragma once
#include "stack_unwinding_detector.h"
#include "types.h"
namespace xdb
{
   template <class InterfaceT>
   class pimpl : protected stack_unwinding_detector
   {
    public:
      pimpl() = default;
      pimpl(ptr<InterfaceT> impl) : _impl(std::move(impl)) {}

      pimpl(pimpl &&other) : stack_unwinding_detector(std::move(other))
      {
         _impl = std::move(other._impl);
      }

      virtual ~pimpl() noexcept(false)
      {
         ptr<InterfaceT> &impl = _impl;
         catch_exceptions_if_unwinding_in_process([&impl]() { impl.reset(nullptr); });
      }

      pimpl &operator=(pimpl &&other)
      {
         stack_unwinding_detector::operator=(std::move(other));
         _impl = std::move(other._impl);

         return *this;
      }

      pimpl<InterfaceT> &operator=(const pimpl<InterfaceT> &other) = delete;
      pimpl(const pimpl<InterfaceT> &other) = delete;

      inline bool is_initialized() const
      {
         return _impl != nullptr;
      }

    protected:
      ptr<InterfaceT> _impl;
   };

} // namespace xdb
