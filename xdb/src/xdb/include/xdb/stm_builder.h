#pragma once

#include "istm_builder.h"
#include "pimpl.h"

namespace xdb
{
   struct column
   {
    public:
      std::string type;
      std::string constraint{"NOT NULL"};

      inline explicit column(string _type) : type(std::move(_type)) {}
   };

   enum class stm_type
   {
      create_table,
      insert,
      replace,
      update,
      alter_table_add_column
   };

   class stm_builder : public pimpl<xdb::backend::istm_builder>
   {
    public:
      ~stm_builder() = default;

      static stm_builder wrap(ptr<backend::istm_builder> stm_builder);

      using pimpl<backend::istm_builder>::pimpl;

      inline stm_builder(stm_builder &&other) noexcept : pimpl<xdb::backend::istm_builder>(std::move(other)) {}

      inline stm_builder &operator=(stm_builder &&other)
      {
         pimpl<backend::istm_builder>::operator=(std::move(other));
         return *this;
      }

      void swap(stm_builder &);

      stm_builder(const stm_builder &) = delete;

      stm_builder &operator=(const stm_builder &) = delete;
      void set_id_field(string_view id_field);
      void push_prefix(string_view prefix);

      void pop_prefix();

      void declare_text_field(string_view name);

      void declare_int8_field(string_view name);

      void declare_int16_field(string_view name);

      void declare_int32_field(string_view name);

      void declare_int64_field(string_view name);

      void declare_uint8_field(string_view name);

      void declare_uint16_field(string_view name);

      void declare_uint32_field(string_view name);

      void declare_uint64_field(string_view name);

      void declare_double_field(string_view name);

      void declare_bool_field(string_view name);

      void add_constraint(string_view column_name, string_view constraint);
      void set_exact_data_type(string_view column_name, string_view type);

      void add_constraint(string_view constraint);

      string get_sql() const;
      string_view get_table_name() const;
   };

} // namespace xdb