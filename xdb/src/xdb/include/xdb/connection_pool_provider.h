//
// Created by jhrub on 09.11.2022.
//

#pragma once

#include "connection.h"
#include <condition_variable>
#include <mutex>
#include <thread>

namespace xdb
{
   struct pool_settings
   {
      std::size_t initial_connections = 5;
      std::size_t maximum_connections = 50;
      std::size_t maximum_allowed_connections = 500;
      std::size_t maximum_allowed_connection_timeout_seconds = 5000;
      std::size_t connection_revalidation_interval_seconds = 60;
   };

   class connection_pool_provider : public connection_provider, public connection_pool_releaser
   {
   public:
      explicit connection_pool_provider(std::string connection_string, pool_settings settings = {});
      void return_to_pool(ptr<backend::iconnection> connection) final;
      connection open() final;

      inline std::size_t size() const
      {
         std::lock_guard lk{lock_};
         return pool_.size();
      }

      inline std::size_t active_connections() const
      {
         return active_connections_;
      }

   private:
      void check(std::stop_token const &token);

      ptr<backend::iconnection> try_get_connection_from_pool();

      mutable std::mutex lock_;
      pool_settings settings_;

      std::vector<ptr<backend::iconnection>> pool_;
      string connection_string_;

      std::atomic<std::size_t> active_connections_ = 0;
      std::condition_variable cv_;

      std::jthread check_thread_;
   };
} // namespace xdb