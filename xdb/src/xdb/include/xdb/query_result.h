#pragma once

#include "pimpl.h"
#include <iterator>
#include <string>
#include <vector>

namespace xdb
{

   namespace backend
   {
      class iquery_result;
   }

   class query_result : public pimpl<backend::iquery_result>
   {
    public:
      using pimpl<backend::iquery_result>::pimpl;

      inline query_result(query_result &&other) : pimpl<backend::iquery_result>(std::move(other)) {}

      inline query_result &operator=(query_result &&other)
      {
         pimpl<backend::iquery_result>::operator=(std::move(other));
         return *this;
      }

      class row_iterator;
      class row
      {
       public:
         row(backend::iquery_result *result = nullptr);
         row(const row &) = delete;
         row &operator=(const row &) = delete;

         optional<bool> get_bool(std::size_t col = 0);
         optional<i8> get_int8(std::size_t col = 0);
         optional<i16> get_int16(std::size_t col = 0);
         optional<i32> get_int32(std::size_t col = 0);
         optional<i64> get_int64(std::size_t col = 0);

         optional<u8> get_uint8(std::size_t col = 0);
         optional<u16> get_uint16(std::size_t col = 0);
         optional<u32> get_uint32(std::size_t col = 0);
         optional<u64> get_uint64(std::size_t col = 0);

         optional<string> get_string(std::size_t col = 0);
         optional<string_view> get_string_view(std::size_t col = 0);
         optional<fl64> get_double(std::size_t col = 0);
         optional<fl32> get_float(std::size_t col = 0);

         optional_field_value get_value(std::size_t col = 0);

         void fill_row_values(mutable_array_view<optional_field_value> fields);
         void fill_row_values(mutable_array_view<field_value> fields);

       private:
         backend::iquery_result *_result;

         friend class row_iterator;
      };

      class row_iterator
      {
       public:
         using value_type = row;
         using reference = row &;
         using iterator_category = std::input_iterator_tag;

         row_iterator(backend::iquery_result *result = nullptr);
         row_iterator(const row_iterator &);
         row_iterator &operator=(const row_iterator &);

         row_iterator &operator++();
         reference operator*();

         bool operator!=(const row_iterator &);
         bool operator==(const row_iterator &);

       private:
         backend::iquery_result *_result = nullptr;
         value_type _row;
      };

      using value_type = row;
      using iterator = row_iterator;

      iterator begin();
      iterator end();

      std::size_t get_column_count() const;
      string_view get_column_name(std::size_t idx) const;
      std::vector<string> get_column_names() const;

      void fill_column_names(mutable_array_view<string_view> column_names);
   };
} // namespace xdb

template <>
class std::iterator_traits<xdb::query_result::row_iterator>
{
 public:
   using value_type = xdb::query_result::row;
   using reference = xdb::query_result::row &;
   using iterator_category = std::input_iterator_tag;
};