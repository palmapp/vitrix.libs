#include "db_context.h"

const std::int64_t xdb::db_context::required_type_id =
    commons::tx_type_id_generator::generate_type_id<xdb::db_context>();

std::int64_t xdb::db_context::get_context_type_id() const noexcept
{
   return db_context::required_type_id;
}
