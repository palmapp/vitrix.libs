// xdb.cpp : Defines the exported functions for the static library.
//

#include "connection.h"

#include <map>
#include <string>

namespace xdb
{
   std::map<std::string, std::unique_ptr<backend::ixdb_driver>> &drivers()
   {
      static std::map<std::string, std::unique_ptr<backend::ixdb_driver>> g_drivers;
      return g_drivers;
   }

   void connection::register_driver(std::unique_ptr<backend::ixdb_driver> driver)
   {
      drivers().insert_or_assign(driver->uri_schema(), std::move(driver));
   }

   std::string get_uri_schema(const std::string &connection_string)
   {
      auto pos = connection_string.find(':');
      if (pos == std::string::npos)
         throw exception(exception::error_code::INVALID_CONNECTION_STRING,
                         "the connection string must contain the schema part");
      else
         return {connection_string, 0, pos};
   }

   std::string get_uri_specific_part(const std::string &connection_string)
   {
      auto pos = connection_string.find(':');
      if (pos == std::string::npos)
         throw exception(exception::error_code::INVALID_CONNECTION_STRING,
                         "the connection string must contain the schema part");
      else
         return {connection_string, pos + 1};
   }

   connection connection::open(const std::string &connection_string)
   {
      return {open_naked(connection_string)};
   };

   ptr<backend::iconnection> connection::open_naked(const string &connection_string)
   {
      auto &driv = drivers();
      auto schema = get_uri_schema(connection_string);
      auto it = driv.find(schema);
      if (it == driv.end())
         throw exception(exception::error_code::DRIVER_NOT_FOUND, "driver has not been found");
      else
      {
         return it->second->open(get_uri_specific_part(connection_string));
      }
   }
   connection::connection(ptr<backend::iconnection> connection, connection_pool_releaser *pool)
       : pimpl<backend::iconnection>(std::move(connection)), pool_(pool)
   {
   }

   connection::~connection()
   {
      if (_impl != nullptr)
      {
         catch_exceptions_if_unwinding_in_process(
             [this]()
             {
                if (pool_ != nullptr)
                {
                   pool_->return_to_pool(std::move(_impl));
                }
                else
                {
                   close();
                }
             });
      }
   }

   void connection::swap(connection &other)
   {
      connection copy = std::move(other);
      other = std::move(*this);
      *this = std::move(copy);
   }

   bool connection::is_connected() const
   {
      return _impl->is_connected();
   }

   transaction_guard connection::begin_transaction(isolation_level level)
   {
      return {_impl->begin_transaction(level)};
   }

   query connection::create_query(const std::string &query)
   {
      return {_impl->create_query(query)};
   }

   query_result connection::execute_query_for_result(const std::string &query)
   {
      return {_impl->execute_query_for_result(query)};
   }

   std::int64_t connection::execute_query_no_except(const std::string &query) noexcept
   {
      return _impl->execute_query_no_except(query);
   }

   std::int64_t connection::execute_query(const std::string &query)
   {
      return _impl->execute_query(query);
   }

   optional<std::int64_t> connection::get_int64_last_row_id()
   {
      return _impl->get_int64_last_row_id();
   }

   optional<std::string> connection::get_string_last_row_id()
   {
      return _impl->get_string_last_row_id();
   }

   void connection::close()
   {
      return _impl->close();
   }

   const char *connection::sql_dialect() const
   {
      return _impl->sql_dialect();
   }

   xdb::stm_builder connection::get_builder(stm_type type, std::string table_name)
   {
      return {_impl->get_builder(type, std::move(table_name))};
   }

   default_connection_provider::default_connection_provider(std::string str) : connection_string_(std::move(str)) {}
   connection default_connection_provider::open()
   {
      return connection::open(connection_string_);
   }
} // namespace xdb