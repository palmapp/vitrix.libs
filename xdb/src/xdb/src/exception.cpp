
#include "exception.h"

namespace xdb
{
   exception::exception(exception::error_code code, const std::string &message) : _code(code), _message(message) {}

   exception::error_code exception::get_code() const
   {
      return _code;
   }

   const char *exception::what() const noexcept
   {
      return _message.c_str();
   }
} // namespace xdb