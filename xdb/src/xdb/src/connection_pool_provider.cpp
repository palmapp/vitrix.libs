//
// Created by jhrub on 09.11.2022.
//
#include "connection_pool_provider.h"
#include <easylogging++.h>
#include <unordered_set>

xdb::connection_pool_provider::connection_pool_provider(std::string connection_string, xdb::pool_settings settings)
   : settings_(settings), connection_string_(connection_string)
{
   for (std::size_t i = 0; i < settings_.initial_connections; ++i)
   {
      pool_.push_back(connection::open_naked(connection_string));
   }

   check_thread_ = std::jthread{[&](std::stop_token stop_token)
   {
      while (!stop_token.stop_requested())
      {
         try
         {
            auto continue_at =
               std::chrono::steady_clock::now() +
               std::chrono::seconds{settings_.connection_revalidation_interval_seconds};

            while (continue_at > std::chrono::steady_clock::now())
            {
               std::this_thread::sleep_for(std::chrono::milliseconds{500});
               if (stop_token.stop_requested()) return;
            }

            check(stop_token);
         }
         catch (std::exception const &ex)
         {
            LOG(ERROR) << ex.what();
         }
         catch (...)
         {
            LOG(ERROR) << "unknown exception";
         }
      }
   }};
}

void xdb::connection_pool_provider::return_to_pool(xdb::ptr<xdb::backend::iconnection> connection)
{
   std::unique_lock lk{lock_};
   if (pool_.size() > settings_.maximum_connections)
   {
      connection.reset(nullptr);
   }
   else
   {
      pool_.push_back(std::move(connection));
      --active_connections_;

      lk.unlock();
      cv_.notify_one();
   }
}

xdb::connection xdb::connection_pool_provider::open()
{
   auto connection_ptr = try_get_connection_from_pool();
   if (connection_ptr == nullptr && active_connections_ < settings_.maximum_allowed_connections)
   {
      connection_ptr = connection::open_naked(connection_string_);
      ++active_connections_;
   }

   if (connection_ptr == nullptr)
   {
      throw xdb::exception(xdb::exception::error_code::DRIVER_EXCEPTION, "maximum allowed connections reached");
   }

   return {std::move(connection_ptr), this};
}

void xdb::connection_pool_provider::check(std::stop_token const &token)
{
   std::unordered_set<backend::iconnection *> processed;
   std::unique_lock lk{lock_};

   auto ping_and_return = [&](size_t i)
   {
      if (i >= pool_.size()) return;

      auto it = pool_.begin() + i;
      if (processed.contains(it->get()))
      {
         return;
      }

      ++active_connections_;
      auto connection_ptr = std::move(*it);
      pool_.erase(it);
      lk.unlock();

      bool ping_result = connection_ptr->ping();

      lk.lock();

      if (ping_result)
      {
         processed.insert(connection_ptr.get());
         pool_.push_back(std::move(connection_ptr));
         --active_connections_;

         lk.unlock();
         cv_.notify_one();

         std::this_thread::yield();
         lk.lock();
      }
      else
      {
         --active_connections_;
      }
   };

   auto pool_size = pool_.size();
   if (pool_size == 0) return;

   for (auto i = pool_size - 1; i != 0; --i)
   {
      if (token.stop_requested()) return;

      ping_and_return(i);
   }
}

xdb::ptr<xdb::backend::iconnection> xdb::connection_pool_provider::try_get_connection_from_pool()
{
   std::unique_lock lk{lock_};

   auto get_next_in_pool = [&]
   {
      auto result = std::move(pool_.back());
      pool_.pop_back();

      ++active_connections_;

      return result;
   };

   if (pool_.empty())
   {
      if (active_connections_ < settings_.maximum_allowed_connections)
      {
         return {};
      }
      else if (cv_.wait_for(
         lk, std::chrono::seconds{std::max(settings_.maximum_allowed_connection_timeout_seconds, std::size_t{1})},
         [&]
         {
            return !pool_.empty();
         }))
      {
         return get_next_in_pool();
      }
      else
      {
         return {};
      }
   }
   else
   {
      return get_next_in_pool();
   }
}
