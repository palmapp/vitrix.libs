#include "transaction_guard.h"
#include "itransaction.h"

namespace xdb
{
   transaction_guard::~transaction_guard() noexcept(false)
   {
      if (_impl != nullptr)
      {
         auto self = this;
         catch_exceptions_if_unwinding_in_process([self]() {
            if (self->is_stack_unwinding_in_process())
            {
               self->rollback();
            }
            else
            {
               self->commit();
            }
         });
      }
   }
   void transaction_guard::commit()
   {
      _impl->commit();
   }

   void transaction_guard::rollback()
   {
      _impl->rollback();
   }

} // namespace xdb