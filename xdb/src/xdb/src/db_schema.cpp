//
// Created by jhrub on 13.07.2021.
//
#include "db_schema.h"
#include <commons/time_utils.h>
#include <fmt/format.h>
#include <gsl/narrow>

void xdb::db_schema::add_migration(const std::string &scope, migration_step_funct_t funct)
{
   if (scopes_.insert(scope).second)
   {
      migrations_.emplace_back(scope, std::move(funct));
   }
   else
   {
      throw std::logic_error{"unable to register migration"};
   }
}

void xdb::db_schema::execute(xdb::connection &db)
{
   std::string dialect = db.sql_dialect();
   for (auto &dialect_initializer : dialects_)
   {
      if (dialect_initializer)
      {
         dialect_initializer(dialect);
      }
   }

   for (auto &scope : migrations_)
   {
      if (dialect == "mysql")
      {
         db.execute_query(
             fmt::format("CREATE TABLE IF NOT EXISTS db_version_{} (version BIGINT NOT NULL PRIMARY KEY, "
                         "last_change BIGINT NOT NULL, reason VARCHAR(500))",
                         scope.first));
      }
      else
      {
         db.execute_query(
             fmt::format("CREATE TABLE IF NOT EXISTS db_version_{} (version INTEGER NOT NULL PRIMARY KEY, "
                         "last_change INTEGER NOT NULL, reason VARCHAR(500))",
                         scope.first));
      }
      std::int64_t version = [&]()
      {
         auto version_query = db.create_query(fmt::format("SELECT max(version) FROM db_version_{}", scope.first));
         return version_query.execute_for_int64().value_or(0);
      }();

      auto migration_sql_query =
          fmt::format("INSERT INTO db_version_{} (version, last_change, reason) VALUES(?,?,?)", scope.first);
      migration_result result{true};
      while (result.migrated)
      {

         result = scope.second(db, version++);
         if (result.migrated)
         {
            auto update_query = db.create_query(migration_sql_query);

            update_query.bind(1, version);
            update_query.bind(2, commons::time_utils::now_in_millis());
            update_query.bind_optional(3, result.reason);

            update_query.execute();
         }
      }
   }
}

void xdb::db_schema::add_sql_dialect_initializer(xdb::dialect_initializer_funct_t dialect_funct)
{
   dialects_.push_back(std::move(dialect_funct));
}
