#include "stm_builder.h"

using namespace xdb;

stm_builder stm_builder::wrap(ptr<backend::istm_builder> connection)
{
   return {std::move(connection)};
}

void stm_builder::swap(stm_builder &other)
{
   stm_builder copy = std::move(other);
   other = std::move(*this);
   *this = std::move(copy);
}

void stm_builder::push_prefix(string_view prefix)
{
   return _impl->push_prefix(prefix);
}

void stm_builder::pop_prefix()
{
   return _impl->pop_prefix();
}

void stm_builder::declare_text_field(string_view name)
{
   return _impl->declare_text_field(name);
}

void stm_builder::declare_int8_field(string_view name)
{
   return _impl->declare_int8_field(name);
}

void stm_builder::declare_int16_field(string_view name)
{
   return _impl->declare_int16_field(name);
}

void stm_builder::declare_int32_field(string_view name)
{
   return _impl->declare_int32_field(name);
}

void stm_builder::declare_int64_field(string_view name)
{
   return _impl->declare_int64_field(name);
}

void stm_builder::declare_uint8_field(string_view name)
{
   return _impl->declare_uint8_field(name);
}

void stm_builder::declare_uint16_field(string_view name)
{
   return _impl->declare_uint16_field(name);
}

void stm_builder::declare_uint32_field(string_view name)
{
   return _impl->declare_uint32_field(name);
}

void stm_builder::declare_uint64_field(string_view name)
{
   return _impl->declare_uint64_field(name);
}

void stm_builder::declare_double_field(string_view name)
{
   return _impl->declare_double_field(name);
}

void stm_builder::declare_bool_field(string_view name)
{
   return _impl->declare_bool_field(name);
}

void stm_builder::add_constraint(string_view column_name, string_view constraint)
{
   return _impl->add_constraint(column_name, constraint);
}

void stm_builder::add_constraint(string_view constraint)
{
   return _impl->add_constraint(constraint);
}

std::string stm_builder::get_sql() const
{
   return _impl->get_sql();
}
string_view stm_builder::get_table_name() const
{
   return _impl->get_table_name();
}

void stm_builder::set_id_field(string_view id_field)
{
   _impl->set_id_field(id_field);
}

void stm_builder::set_exact_data_type(string_view column_name, string_view type)
{
   _impl->set_exact_data_type(column_name, type);
}
