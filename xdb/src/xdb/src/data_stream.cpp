//
// Created by jhrub on 04.06.2021.
//
#include "data_stream.h"
#include <algorithm>

using namespace xdb;

data_stream::data_stream(connection conn, query query, query_result result)
    : connection_holder_(std::move(conn)), query_holder_(std::move(query)), result_(std::move(result))
{
   column_name_string_ = result_.get_column_names();
   column_name_views_.reserve(column_name_string_.size());
   row_.resize(column_name_string_.size());

   std::transform(column_name_string_.begin(), column_name_string_.end(), std::back_inserter(column_name_views_),
                  [](string const &col) -> string_view { return {col}; });

   it_ = result_.begin();
   end_ = result_.end();
}

data_stream::data_stream(connection conn, query q)
{
   auto result = q.execute_for_result();
   new (this) data_stream(std::move(conn), std::move(q), std::move(result));
}

optional<array_view<string_view>> data_stream::get_column_names()
{
   return {array_view<string_view>{column_name_views_}};
}

optional<commons::data::data_row> data_stream::next_row()
{
   if (has_first_)
   {
      ++it_;
   }
   else
   {
      has_first_ = true;
   }

   if (it_ != end_)
   {
      query_result::row &row = *it_;
      row.fill_row_values(mutable_array_view<optional_field_value>{row_});

      return {commons::data::data_row{row_}};
   }
   else
   {
      return {};
   }
}
connection data_stream::release_connection()
{
   if (it_ != end_)
   {
      it_ = end_;
   }

   return std::move(connection_holder_);
}
