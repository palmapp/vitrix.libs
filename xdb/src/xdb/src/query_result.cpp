#include "query_result.h"
#include "iquery_result.h"
#include <exception.h>

using namespace xdb;

query_result::row::row(backend::iquery_result *result) : _result(result) {}

optional<i64> query_result::row::get_int64(std::size_t col)
{
   return _result->get_int64(col);
}

optional<string> query_result::row::get_string(std::size_t col)
{
   return _result->get_string(col);
}

optional<string_view> query_result::row::get_string_view(std::size_t col)
{
   return _result->get_string_view(col);
}

optional<fl64> query_result::row::get_double(std::size_t col)
{
   return _result->get_double(col);
}

optional<bool> query_result::row::get_bool(std::size_t col)
{
   return _result->get_bool(col);
}

optional<i8> query_result::row::get_int8(std::size_t col)
{
   return _result->get_int8(col);
}

optional<i16> query_result::row::get_int16(std::size_t col)
{
   return _result->get_int16(col);
}

optional<i32> query_result::row::get_int32(std::size_t col)
{
   return _result->get_int32(col);
}

optional<u8> query_result::row::get_uint8(std::size_t col)
{
   return _result->get_uint8(col);
}

optional<u16> query_result::row::get_uint16(std::size_t col)
{
   return _result->get_uint16(col);
}

optional<u32> query_result::row::get_uint32(std::size_t col)
{
   return _result->get_uint32(col);
}

optional<u64> query_result::row::get_uint64(std::size_t col)
{
   return _result->get_uint64(col);
}

optional<fl32> query_result::row::get_float(std::size_t col)
{
   return _result->get_float(col);
}

optional_field_value query_result::row::get_value(std::size_t col)
{
   return _result->get_value(col);
}

void query_result::row::fill_row_values(mutable_array_view<optional_field_value> fields)
{
   _result->fill_row_values(fields);
}

void query_result::row::fill_row_values(mutable_array_view<field_value> fields)
{
   _result->fill_row_values(fields);
}

query_result::row_iterator::row_iterator(backend::iquery_result *result) : _result(result), _row(result) {}

query_result::row_iterator::row_iterator(const row_iterator &other)
{
   _result = other._result;
   _row._result = other._result;
}

query_result::row_iterator &query_result::row_iterator::operator=(const row_iterator &other)
{
   _result = other._result;
   _row._result = other._result;

   return *this;
}

query_result::row_iterator &query_result::row_iterator::operator++()
{
   if (_result == nullptr) throw exception(exception::error_code::CALLER_EXCEPTION, "_result ==  nullptr");

   if (!_result->next_row())
   {
      _result = nullptr;
   }
   return *this;
}

query_result::row_iterator::reference query_result::row_iterator::operator*()
{
   if (_result == nullptr) throw exception(exception::error_code::CALLER_EXCEPTION, "_result ==  nullptr");

   return _row;
}

bool query_result::row_iterator::operator!=(const row_iterator &other)
{
   return _result != other._result;
}

bool query_result::row_iterator::operator==(const row_iterator &other)
{
   return _result == other._result;
}

query_result::iterator query_result::begin()
{

   if (!_impl->first_row()) return iterator();
   return iterator(_impl.get());
}

query_result::iterator query_result::end()
{
   return iterator();
}

std::size_t query_result::get_column_count() const
{
   return _impl->get_column_count();
}

string_view query_result::get_column_name(std::size_t idx) const
{
   return _impl->get_column_name(idx);
}

std::vector<string> query_result::get_column_names() const
{
   return _impl->get_column_names();
}

void query_result::fill_column_names(mutable_array_view<string_view> column_names)
{
   _impl->fill_column_names(column_names);
}
