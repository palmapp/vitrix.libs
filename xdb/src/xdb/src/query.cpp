#include "query.h"
#include "iquery.h"
#include "iquery_result.h"

namespace xdb
{

   void query::swap(query &other)
   {
      auto copy = std::move(other);
      other = std::move(*this);
      *this = std::move(copy);
   }

   string query::get_query_string() const
   {
      return _impl->get_query_string();
   }

   query_result query::execute_for_result()
   {
      return {_impl->execute_for_result()};
   }

   i64 query::execute()
   {
      return _impl->execute();
   }

   optional<string> query::execute_for_string()
   {
      return _impl->execute_for_string();
   }

   optional<i64> query::execute_for_int64()
   {
      return _impl->execute_for_int64();
   }

   optional<double> query::execute_for_double()
   {
      return _impl->execute_for_double();
   }

   void query::bind(std::size_t i, field_value value)
   {
      _impl->bind(i, value);
   }

   void query::bind_optional(std::size_t i, optional_field_value value)
   {
      _impl->bind_optional(i, value);
   }

   void query::bind_null(std::size_t i)
   {
      _impl->bind_null(i);
   }

   void query::bind(string_view key, field_value value)
   {
      _impl->bind(key, value);
   }

   void query::bind_optional(string_view key, optional_field_value value)
   {
      _impl->bind_optional(key, value);
   }

   void query::bind_null(string_view key)
   {
      _impl->bind_null(key);
   }
} // namespace xdb