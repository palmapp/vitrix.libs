#pragma once
#include "query.h"
#include "query_result.h"

namespace xdb
{
   namespace backend
   {
      namespace mysql
      {
         class self_contained_query_result : private xdb::backend::mysql::query,
                                             public xdb::backend::mysql::query_result
         {
          public:
            self_contained_query_result(connection &connection, const std::string &sql);
         };
      } // namespace mysql
   }    // namespace backend
} // namespace xdb