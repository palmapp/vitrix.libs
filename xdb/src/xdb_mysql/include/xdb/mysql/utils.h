#pragma once
#include <string_view>
struct st_mysql;

namespace xdb
{
   namespace backend
   {
      namespace mysql
      {

         void throw_if_error(st_mysql *connection, std::string_view sql = {});

      }
   } // namespace backend
} // namespace xdb