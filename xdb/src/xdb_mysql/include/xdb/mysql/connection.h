#pragma once

#include "config.h"
#include "utils.h"
#include <boost/algorithm/string.hpp>
#include <stm_builder.h>
#include <xdb/backend.h>
namespace xdb
{
   namespace backend
   {
      namespace mysql
      {

         class connection : public iconnection
         {
          public:
            connection(const std::string &path);
            ~connection();
            // Inherited via iconnection
            virtual bool is_connected() const override;
            virtual ptr<itransaction> begin_transaction(isolation_level level = isolation_level::serializable) override;
            virtual ptr<iquery> create_query(const std::string &query) override;
            virtual ptr<iquery_result> execute_query_for_result(const std::string &query) override;
            virtual std::int64_t execute_query(const std::string &query) override;
            virtual std::int64_t execute_query_no_except(const std::string &) noexcept override;
            virtual std::int64_t get_int64_last_row_id() override;
            virtual std::string get_string_last_row_id() override;
            virtual void close() override;
            virtual const char *sql_dialect() const override;
            virtual bool ping() noexcept override;

            void reset_connection();

            std::string get_last_error_message();
            void execute_sql(const char *sql);

            template <class FunctT>
            void call_mysql(FunctT funct)
            {
               funct(_connection);

               throw_if_error(_connection);
            }

            template <class FunctT>
            void call_mysql(FunctT funct, string_view sql)
            {
               funct(_connection);

               throw_if_error(_connection, sql);
            }

            ptr<backend::istm_builder> get_builder(stm_type type, std::string table_name) override;

          private:
            class connection_string_parser
            {
             public:
               std::string server;
               std::string user;
               std::string password;
               std::string database;
               int port = 0;

               inline void operator()(const std::string &key, const std::string &value)
               {
                  if (key == "server")
                  {

                     if (auto pos = value.find(':'); pos != std::string::npos)
                     {
                        server = std::string(value.begin(), value.begin() + pos);

                        std::string port_string(value.begin() + pos + 1, value.end());
                        port = std::stoi(port_string);
                     }
                     else
                     {
                        server = value;
                     }
                  }
                  else if (key == "uid")
                     user = value;
                  else if (key == "pwd")
                     password = value;
                  else if (key == "database")
                     database = value;
                  else if (key == "port")
                     port = std::stoi(value);
               }
            };

            MYSQL *_connection;
            connection_string_parser parser_;
         };
      } // namespace mysql
   }    // namespace backend
} // namespace xdb