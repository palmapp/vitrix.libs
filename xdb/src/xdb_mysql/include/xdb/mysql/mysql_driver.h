#pragma once
#include <memory>
#include <xdb/backend.h>


namespace xdb
{
   namespace backend
   {
      namespace mysql
      {
         class driver : public ixdb_driver
         {
         public:
            constexpr static const char* driver_uri_schema = "mysql";

            driver() = default;
            virtual const char* uri_schema() const override;

            virtual ptr<backend::iconnection> open(const std::string& connection_string) override;
         };
      } // namespace mysql
   } // namespace backend
} // namespace xdb
