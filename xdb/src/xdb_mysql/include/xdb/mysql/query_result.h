#pragma once
#include "connection.h"
#include <xdb/backend.h>

namespace xdb
{
   namespace backend
   {
      namespace mysql
      {
         using data_bool = std::remove_pointer_t<decltype(MYSQL_BIND::is_null)>;

         class query;
         class query_result : public iquery_result
         {
          public:
            query_result(query &query);
            ~query_result();

            // Inherited via iquery_result
            std::size_t get_column_count() final;
            string_view get_column_name(std::size_t idx) final;
            std::vector<string> get_column_names() final;

            optional<i64> get_int64(std::size_t col) final;
            optional<string> get_string(std::size_t col) final;
            optional<string_view> get_string_view(std::size_t col) final;

            optional<fl64> get_double(std::size_t col) final;
            optional<bool> get_bool(std::size_t col) final;

            optional<i8> get_int8(std::size_t col) final;
            optional<i16> get_int16(std::size_t col) final;
            optional<i32> get_int32(std::size_t col) final;
            optional<u8> get_uint8(std::size_t col) final;
            optional<u16> get_uint16(std::size_t col) final;
            optional<u32> get_uint32(std::size_t col) final;
            optional<u64> get_uint64(std::size_t col) final;
            optional<fl32> get_float(std::size_t col) final;

            bool next_row() final;
            bool first_row() final;

            void fill_column_names(mutable_array_view<string_view> column_names) final;
            optional_field_value get_value(std::size_t col) final;

            void fill_row_values(mutable_array_view<optional_field_value> fields) final;
            void fill_row_values(mutable_array_view<field_value> fields) final;
            // internal driver api
            std::int64_t get_non_optional_int64(std::size_t col);
            double get_non_optional_double(std::size_t col);
            string_view get_non_optional_string(std::size_t col);

          private:
            query &_query;
            MYSQL_STMT *_stm = nullptr;
            MYSQL_RES *_result = nullptr;

            struct data_info
            {
               unsigned long length;
               data_bool error;
               data_bool is_null;
            };

            std::vector<data_info> _info;
            std::vector<MYSQL_FIELD> _list_of_fields;
            std::vector<uint8_t> _buffer;
            std::vector<MYSQL_BIND> _bound_data;

            std::size_t _num_fields;

            field_value get_empty_field_value(std::size_t col);
            std::size_t _calculate_buffer_size();
            void _bind_to_buffer();
            connection &_get_connection();
            size_t increment_by_type(size_t position, const MYSQL_FIELD &f) const;

            enum_field_types get_field_type(std::size_t col);
            MYSQL_BIND &get_field_value(std::size_t col);
         };
      } // namespace mysql
   }    // namespace backend
} // namespace xdb