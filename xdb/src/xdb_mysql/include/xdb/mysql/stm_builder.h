#pragma once

#include "xdb/istm_builder.h"
#include "xdb/stm_builder.h"

namespace xdb::backend::mysql
{
   class stm_builder : public istm_builder
   {
    public:
      stm_builder(stm_type type, std::string table_name);

      ~stm_builder() final = default;

      virtual void set_id_field(string_view id_field) override;
      virtual void push_prefix(string_view prefix) override;

      virtual void pop_prefix() override;

      virtual void declare_text_field(string_view name) override;

      virtual void declare_int8_field(string_view name) override;

      virtual void declare_int16_field(string_view name) override;

      virtual void declare_int32_field(string_view name) override;

      virtual void declare_int64_field(string_view name) override;

      virtual void declare_uint8_field(string_view name) override;

      virtual void declare_uint16_field(string_view name) override;

      virtual void declare_uint32_field(string_view name) override;

      virtual void declare_uint64_field(string_view name) override;

      virtual void declare_double_field(string_view name) override;

      virtual void declare_bool_field(string_view name) override;

      virtual void add_constraint(string_view column_name, string_view constraint) override;

      virtual void set_exact_data_type(string_view column_name, string_view type) override;

      virtual void add_constraint(string_view constraint) override;

      virtual string get_sql() const override;
      string_view get_table_name() const override;

    protected:
      std::string add_prefix(string_view name);

      std::string _table_name;
      using column_def = std::pair<std::string, column>;
      std::vector<std::string> _constraints;
      stm_type _type;
      std::vector<std::string> _prefix_stack;
      std::vector<column_def> _columns;
      std::string _id_field;
   };

} // namespace xdb::backend::mysql