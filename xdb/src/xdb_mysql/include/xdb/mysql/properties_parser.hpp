#pragma once
#include <cctype>
#include <iostream>
#include <iterator>
#include <string>

namespace parse
{
   enum properties_parser_state
   {
      pps_wait_for_key,
      pps_key,
      pps_key_end,
      pps_comment,
      pps_wait_for_value,
      pps_string_value,
      pps_string_escape,
      pps_value
   };

   template <class ItT, class PropertiesHandlerT>
   void parse_properties(ItT cursor, ItT end, PropertiesHandlerT &handler)
   {
      properties_parser_state state = pps_wait_for_key;
      std::string key_buffer;
      std::string value_buffer;

      for (; cursor != end; ++cursor)
      {
         char ch = *cursor;
         switch (state)
         {
         case pps_wait_for_key:
            if (ch == '#')
            {
               state = pps_comment;
               break;
            }
            else if (std::isalpha(ch))
            {
               state = pps_key;
               // fallthrough to pps_key
            }
            else
            {
               break;
            }
            [[fallthrough]];
         case pps_key:
            if (std::isspace(ch) || ch == '=')
            {
               state = pps_key_end;
               // fallthroug pps_key_end
            }
            else
            {
               key_buffer += ch;
               break;
            }
         case pps_key_end:
            if (ch == '=')
            {
               state = pps_wait_for_value;
            }
            break;
         case pps_wait_for_value:
            if (std::isprint(ch) && !std::isspace(ch))
            {
               if (ch == '"')
               {
                  state = pps_string_value;
               }
               else
               {
                  state = pps_value;
                  value_buffer += ch;
               }
            }
            break;
         case pps_string_value:
            if (ch == '\\')
            {
               state = pps_string_escape;
            }
            else if (ch == '"')
            {
               handler(key_buffer, value_buffer);
               key_buffer.clear();
               value_buffer.clear();
               state = pps_wait_for_key;
            }
            else
            {
               value_buffer += ch;
            }
            break;
         case pps_value:
            if (std::isprint(ch) && ch != ';')
               value_buffer += ch;
            else
            {
               handler(key_buffer, value_buffer);
               key_buffer.clear();
               value_buffer.clear();
               state = pps_wait_for_key;
            }
            break;
         case pps_string_escape:
            value_buffer += ch;
            state = pps_string_value;
            break;

         case pps_comment:
            if (ch == '\n') state = pps_wait_for_key;
            break;
         }
      }

      if (state >= pps_wait_for_value)
      {
         handler(key_buffer, value_buffer);
      }
   }

   template <class PropertiesHandlerT>
   void parse_properties(std::istream &is, PropertiesHandlerT &handler)
   {
      std::istreambuf_iterator<char> end; // end-of-range iterator
      std::istreambuf_iterator<char> begin(is.rdbuf());
      parse_properties(begin, end, handler);
   }

} // namespace parse