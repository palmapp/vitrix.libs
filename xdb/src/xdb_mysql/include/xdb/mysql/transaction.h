#pragma once
#include "connection.h"
#include <xdb/backend.h>
namespace xdb
{
   namespace backend
   {
      namespace mysql
      {
         class transaction : public itransaction
         {
          public:
            transaction(connection &connection);
            ~transaction();

            virtual void commit() override;
            virtual void rollback() override;

          private:
            bool is_rolled_back;
            connection &_connection;
         };
      } // namespace mysql
   }    // namespace backend
} // namespace xdb