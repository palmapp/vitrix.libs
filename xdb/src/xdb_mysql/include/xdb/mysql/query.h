#pragma once
#include "connection.h"
#include <deque>
#include <vector>
#include <xdb/backend.h>

namespace xdb
{
   namespace backend
   {
      namespace mysql
      {
         class query_result;
         class query : public iquery
         {
          public:
            query(connection &_connection, const string &sql);
            // Inherited via iquery
            virtual string get_query_string() const final;

            virtual void bind(std::size_t i, field_value value) final;
            virtual void bind_optional(std::size_t i, optional_field_value value) final;
            virtual void bind_null(std::size_t i) final;

            virtual void bind(string_view key, field_value value) final;
            virtual void bind_optional(string_view key, optional_field_value value) final;
            virtual void bind_null(string_view key) final;

            virtual ptr<iquery_result> execute_for_result() final;
            virtual i64 execute() final;
            virtual i64 execute_no_except() noexcept final;
            virtual optional<string> execute_for_string() final; // returns first object at column 1, row 1
            virtual optional<i64> execute_for_int64() final;
            virtual optional<double> execute_for_double() final;

            query_result &make_query_result_object();

            inline MYSQL_BIND get_bound_data()
            {
               return _bound_data.front();
            }

          private:
            MYSQL_STMT *prepare_statement();
            string _sql;
            connection &_connection;
            std::vector<MYSQL_BIND> _bound_data;
            std::deque<std::string> _param_strings;
            std::deque<field_value> _param_primitive;

            void adjust_size(std::size_t index);

            friend class query_result;
         };
      } // namespace mysql
   }    // namespace backend
} // namespace xdb