#pragma once
#include <xutility>
namespace xdb
{
   namespace utils
   {
      template <class ContainerT>
      class limited_insert_iterator
      { // wrap pushes to back of container as output iterator
       public:
         typedef typename ContainerT::value_type _Valty;
         explicit limited_insert_iterator(ContainerT &_Cont, size_t max_size) : _container(&_Cont), _size(max_size)
         { // construct with container
         }

         limited_insert_iterator &operator=(const _Valty &_Val)
         { // push value into container
            if (_size > 0)
            {
               _container->push_back(_Val);
               _size--;
            }
            return (*this);
         }

         limited_insert_iterator &operator=(_Valty &&_Val)
         { // push value into container
            if (_size > 0)
            {
               _container->push_back(_STD forward<_Valty>(_Val));
               _size--;
            }
            return (*this);
         }

         limited_insert_iterator &operator*()
         { // pretend to return designated value
            return (*this);
         }

         limited_insert_iterator &operator++()
         { // pretend to preincrement
            return (*this);
         }

         limited_insert_iterator &operator++(int)
         { // pretend to postincrement
            return (*this);
         }

       protected:
         ContainerT *_container; // pointer to container
         size_t _size;
      };

      template <class ContainerT>
      inline limited_insert_iterator<ContainerT> limited_inserter(ContainerT &_Cont, size_t size)
      { // return a back_insert_iterator
         return (limited_insert_iterator<ContainerT>(_Cont, size));
      }
   } // namespace utils
} // namespace xdb
