#include "connection.h"
#include "mysql_driver.h"
#include "properties_parser.hpp"
#include "query.h"
#include "self_contained_query_result.h"
#include "stm_builder.h"
#include "transaction.h"
#include "utils.h"
#include <string>
#include <gsl/narrow>
#include <xdb/backend.h>

xdb::backend::mysql::connection::connection(const std::string &conn_str)
   : _connection(nullptr)
{
   parse::parse_properties(conn_str.begin(), conn_str.end(), parser_);
   if (parser_.server.empty())
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "'server' name is missing from connection string.");
   }
   if (parser_.user.empty())
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "'user' name is missing from connection string.");
   }
   if (parser_.password.empty())
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "'password' is missing from connection string.");
   }
   if (parser_.database.empty())
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "'database' is missing from connection string.");
   }
   if (parser_.port == 0)
   {

      throw exception(exception::error_code::DRIVER_EXCEPTION, "'port' is missing from connection string.");
   }

   _connection = mysql_init(nullptr);

   MYSQL *result_connection =
      mysql_real_connect(_connection, parser_.server.c_str(), parser_.user.c_str(), parser_.password.c_str(),
                         parser_.database.c_str(), parser_.port, "", 0);

   if (result_connection == nullptr)
   {
      throw_if_error(_connection); // able to print null connection
   }

   my_bool reconnect = 1;
   mysql_options(_connection, MYSQL_OPT_RECONNECT, &reconnect);

   mysql_set_character_set(_connection, "utf8mb4");
}

xdb::backend::mysql::connection::~connection()
{
   close();
}

bool xdb::backend::mysql::connection::is_connected() const
{
   return _connection != nullptr;
}

xdb::ptr<xdb::backend::itransaction> xdb::backend::mysql::connection::begin_transaction(xdb::isolation_level)
{
   return make_ptr<transaction>(*this);
}

xdb::ptr<xdb::backend::iquery> xdb::backend::mysql::connection::create_query(const std::string &sql_query)
{
   return make_ptr<query>(*this, sql_query);
}

xdb::ptr<xdb::backend::iquery_result>
xdb::backend::mysql::connection::execute_query_for_result(const std::string &sql_query)
{
   return make_ptr<self_contained_query_result>(*this, sql_query);
}

std::int64_t xdb::backend::mysql::connection::execute_query(const std::string &sql_query)
{
   my_ulonglong num_changes = 0;
   call_mysql([&](MYSQL *connection)
   {
      mysql_query(connection, sql_query.c_str());
   }, sql_query);

   call_mysql([&](MYSQL *connection)
   {
      num_changes = mysql_affected_rows(connection);
   }, sql_query);

   if (num_changes == static_cast<my_ulonglong>(-1)) num_changes = 0;

   if (num_changes > static_cast<my_ulonglong>((std::numeric_limits<int64_t>::max)()))
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION,
                      "Number of changes too large to be supported by this driver.");
   }
   return gsl::narrow<std::int64_t>(num_changes);
}

std::int64_t xdb::backend::mysql::connection::execute_query_no_except(const std::string &sql_query) noexcept
{
   mysql_query(_connection, sql_query.c_str());
   auto num_changes = mysql_affected_rows(_connection);
   if (num_changes > static_cast<my_ulonglong>((std::numeric_limits<int64_t>::max)()))
   {
      return -1;
   }
   if (mysql_errno(_connection) != 0)
   {
      return -1;
   }
   else
   {
      return num_changes;
   }
}

std::int64_t xdb::backend::mysql::connection::get_int64_last_row_id()
{
   int64_t id;
   call_mysql([&](MYSQL *connection)
   {
      id = mysql_insert_id(connection);
   });
   return id;
}

std::string xdb::backend::mysql::connection::get_string_last_row_id()
{
   int64_t id;
   call_mysql([&](MYSQL *connection)
   {
      id = mysql_insert_id(connection);
   });
   return std::to_string(id);
}

void xdb::backend::mysql::connection::close()
{
   if (is_connected())
   {
      mysql_close(_connection);
      _connection = nullptr;
   }
}

xdb::ptr<xdb::backend::istm_builder> xdb::backend::mysql::connection::get_builder(xdb::stm_type type,
   std::string table_name)
{
   return make_ptr<xdb::backend::mysql::stm_builder>(type, std::move(table_name));
}

const char *xdb::backend::mysql::connection::sql_dialect() const
{
   return driver::driver_uri_schema;
}

void xdb::backend::mysql::connection::reset_connection()
{
   if (mysql_errno(_connection) != 0)
   {
      mysql_reset_connection(_connection);
   }
}

bool xdb::backend::mysql::connection::ping() noexcept
{
   if (_connection != nullptr)
   {
      return mysql_ping(_connection) == 0;
   }
   return false;
}
