#include "stm_builder.h"
#include <algorithm>

xdb::backend::mysql::stm_builder::stm_builder(stm_type type, std::string table_name)
{
   _type = type;
   _table_name = std::move(table_name);
}

void xdb::backend::mysql::stm_builder::push_prefix(string_view prefix)
{
   _prefix_stack.push_back(static_cast<string>(prefix));
}

void xdb::backend::mysql::stm_builder::pop_prefix()
{
   _prefix_stack.pop_back();
}

void xdb::backend::mysql::stm_builder::declare_int64_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"BIGINT"});
}

void xdb::backend::mysql::stm_builder::declare_int32_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"INT"});
}

void xdb::backend::mysql::stm_builder::declare_int16_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"SMALLINT"});
}

void xdb::backend::mysql::stm_builder::declare_int8_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"TINYINT"});
}

void xdb::backend::mysql::stm_builder::declare_uint64_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"BIGINT UNSIGNED"});
}

void xdb::backend::mysql::stm_builder::declare_uint32_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"INT UNSIGNED"});
}

void xdb::backend::mysql::stm_builder::declare_uint16_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"SMALLINT UNSIGNED"});
}

void xdb::backend::mysql::stm_builder::declare_uint8_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"TINYINT UNSIGNED"});
}

void xdb::backend::mysql::stm_builder::declare_double_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"FLOAT"});
}

void xdb::backend::mysql::stm_builder::declare_bool_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"TINYINT"});
}

void xdb::backend::mysql::stm_builder::declare_text_field(string_view name)
{
   _columns.emplace_back(add_prefix(name), xdb::column{"TEXT"});
}

void xdb::backend::mysql::stm_builder::add_constraint(string_view column_name, string_view constraint)
{
   auto it =
      std::find_if(_columns.begin(), _columns.end(), [=](const column_def &def)
      {
         return def.first == column_name;
      });
   if (it != _columns.end())
   {
      auto &def = *it;
      def.second.constraint = constraint;
   }
   else
   {
      throw std::domain_error("column must be declared first");
   }
}

void xdb::backend::mysql::stm_builder::set_exact_data_type(string_view column_name, string_view type)
{
   auto it =
      std::find_if(_columns.begin(), _columns.end(), [=](const column_def &def)
      {
         return def.first == column_name;
      });
   if (it != _columns.end())
   {
      auto &def = *it;
      def.second.type = static_cast<string>(type);
   }
   else
   {
      throw std::domain_error("column must be declared first");
   }
}

void xdb::backend::mysql::stm_builder::add_constraint(string_view constraint)
{
   if (_type != stm_type::create_table)
   {
      throw std::domain_error("constaint not allowed in other mode than create_table!");
   }

   _constraints.emplace_back(constraint);
}

std::string xdb::backend::mysql::stm_builder::get_sql() const
{
   const int field_count = static_cast<int>(_columns.size());
   std::string result;
   result.reserve(1024);

   if (_type == stm_type::update)
   {
      result += "UPDATE ";
      result += get_table_name();

      result += " SET ";
      bool first = true;
      for (auto const &column : _columns)
      {
         if (column.first == _id_field)
         {
            continue;
         }

         if (first)
         {
            first = false;
         }
         else
         {
            result += ',';
         }

         result += '`';
         result += column.first;
         result += '`';

         result += " = ?";
      }

      result += " WHERE ";
      result += _id_field;
      result += " = ? ";
   }
   else if (_type == stm_type::alter_table_add_column)
   {
      for (auto &column : _columns)
      {
         result += "ALTER TABLE ";
         result += _table_name;
         result += " ADD ";
         result += '`';

         result += column.first;
         result += '`';

         result += " ";
         result += column.second.type;
         result += " ";
         result += column.second.constraint;
         result += ";";
      }
   }
   else
   {

      auto type = _type;
      switch (type)
      {
      case stm_type::create_table:
         result += "CREATE TABLE ";
         break;
      case stm_type::insert:
         result += "INSERT INTO ";
         break;
      case stm_type::replace:
         result += "REPLACE INTO ";
         type = stm_type::insert;
         break;
      case stm_type::update:
         break;
      case stm_type::alter_table_add_column:
         break;
      }
      result += _table_name;
      result += "(";
      bool first = true;
      for (auto const &column : _columns)
      {
         if (first)
         {
            first = false;
         }
         else
         {
            result += ",";
         }

         result += '`';

         result += column.first;
         result += '`';

         if (type == stm_type::create_table)
         {
            result += " ";
            result += column.second.type;
            result += " ";
            result += column.second.constraint;
         }
      }

      for (auto const &constraint : _constraints)
      {
         result += ",";
         result += constraint;
      }

      result.reserve(type == stm_type::insert ? result.size() + 13 + field_count * 2 : result.size() + 2);
      result += ')';

      if (type == stm_type::insert)
      {
         result += " VALUES (";
         for (int i = 0; i != field_count; ++i)
         {
            if (i != 0)
            {
               result += ',';
            }
            result += '?';
         }
         result += ')';
      }
   }
   return result;
}

std::string xdb::backend::mysql::stm_builder::add_prefix(string_view name)
{
   std::string name_with_prefix = "";
   for (auto &prefix : _prefix_stack)
   {
      name_with_prefix += prefix;
      name_with_prefix += "_";
   }
   return name_with_prefix += name;
}

commons::basic_types::string_view xdb::backend::mysql::stm_builder::get_table_name() const
{
   return _table_name;
}

void xdb::backend::mysql::stm_builder::set_id_field(commons::basic_types::string_view id_field)
{
   _id_field = static_cast<std::string>(id_field);
}
