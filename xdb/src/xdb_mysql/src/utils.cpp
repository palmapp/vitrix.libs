#include "utils.h"
#include "config.h"
#include <fmt/format.h>
#include <string>
#include <xdb/exception.h>

void xdb::backend::mysql::throw_if_error(st_mysql *connection, std::string_view sql)
{
   int err = mysql_errno(connection);

   if (err != 0)
   {

      std::string err_string = mysql_error(connection);

      if (sql.empty())
      {
         throw exception(exception::error_code::DRIVER_EXCEPTION, err_string);
      }
      else
      {
         throw exception(exception::error_code::DRIVER_EXCEPTION, fmt::format("{}\n\tSQL:{}", err_string, sql));
      }
   }
}