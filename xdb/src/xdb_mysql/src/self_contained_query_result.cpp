#include "self_contained_query_result.h"
#include "connection.h"

xdb::backend::mysql::self_contained_query_result::self_contained_query_result(connection &connection,
                                                                              const std::string &sql)
    : query(connection, sql), query_result(*static_cast<query *>(this))
{
}