#include "query_result.h"
#include "connection.h"
#include "query.h"
#include <algorithm>
#include <bitset>
#include <fmt/format.h>
#include <gsl/narrow>
using namespace xdb;

backend::mysql::query_result::query_result(query &q)
   : _query(q), _stm(q.prepare_statement())
{
   const unsigned long num_of_rows_to_prefetch = 100;
   mysql_stmt_attr_set(_stm, STMT_ATTR_PREFETCH_ROWS, &num_of_rows_to_prefetch);

   _get_connection().call_mysql([&](MYSQL *connection)
   {
      mysql_stmt_execute(_stm);
   });
   _get_connection().call_mysql(
      [&](MYSQL *connection)
      {
         _result = mysql_stmt_result_metadata(_stm);

         _num_fields = mysql_num_fields(_result);
         _list_of_fields.reserve(_num_fields);
         MYSQL_FIELD *field = nullptr;
         while ((field = mysql_fetch_field(_result)) != nullptr)
         {
            _list_of_fields.push_back(*field);
         }

         _buffer.resize(_calculate_buffer_size());
         _bound_data.resize(_list_of_fields.size());
         _info.resize(_list_of_fields.size());

         _bind_to_buffer();
      });
}

template <class T>
size_t increment_position(size_t pos)
{
   size_t align_modulo;
   align_modulo = pos % alignof(T);
   if (align_modulo > 0)
   {
      pos += (alignof(T) - align_modulo);
   }
   pos += sizeof(T);

   return pos;
}

backend::mysql::query_result::~query_result()
{
   mysql_free_result(_result);
   mysql_stmt_close(_stm);
}

std::size_t backend::mysql::query_result::_calculate_buffer_size()
{
   size_t position = 0;
   for (auto const &f : _list_of_fields)
   {
      // first iteration is for the size of the buffer
      position = increment_by_type(position, f);
   }
   return position;
}

size_t backend::mysql::query_result::increment_by_type(size_t position, const MYSQL_FIELD &f) const
{
   enum_field_types type = f.type;

   switch (type)
   {
   case MYSQL_TYPE_TINY: // one byte
      position = increment_position<u8>(position);
      break;
   case MYSQL_TYPE_SHORT: // two bytes
      position = increment_position<u16>(position);
      break;
   case MYSQL_TYPE_LONG: // four bytes
      position = increment_position<u32>(position);
      break;
   case MYSQL_TYPE_LONGLONG: // eight bytes
      position = increment_position<u64>(position);
      break;
   case MYSQL_TYPE_INT24: // three bytes
      position += 4;
      break;
   case MYSQL_TYPE_FLOAT:
      position = increment_position<fl32>(position);
      break;
   case MYSQL_TYPE_DOUBLE:
      position = increment_position<fl64>(position);
      break;
   case MYSQL_TYPE_VARCHAR:
      [[fallthrough]];
   case MYSQL_TYPE_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_VAR_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_TINY_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_MEDIUM_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_LONG_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_BLOB:
      if (f.max_length > 0)
         position += f.max_length;
      else
         position += f.length;
      break;
   default:
      throw exception(
         exception::error_code::DRIVER_EXCEPTION,
         fmt::format("Data type not supported by this driver. MYSQL_FIELD.type={}", static_cast<uint8_t>(type)));
   }
   return position;
}

void backend::mysql::query_result::_bind_to_buffer()
{
   size_t position = 0;
   int bind_result = 0;
   int counter = 0; // For index of fields.
   for (MYSQL_FIELD &f : _list_of_fields)
   {
      enum_field_types type = f.type;

      auto &bind = _bound_data[counter];
      auto &info = _info[counter++];

      bind.buffer = &_buffer[position];
      auto new_position = increment_by_type(position, f);
      bind.buffer_length = gsl::narrow<decltype(bind.buffer_length)>(new_position - position);
      position = new_position;

      bind.buffer_type = type;
      bind.is_null = &info.is_null;
      bind.is_unsigned = (UNSIGNED_FLAG & f.flags) != 0;
      bind.error = &info.error;
      bind.length = &info.length;
   }

   _get_connection().call_mysql([&](MYSQL *connection)
   {
      bind_result = mysql_stmt_bind_result(_stm, _bound_data.data());
   });
}

std::size_t backend::mysql::query_result::get_column_count()
{
   return _num_fields;
}

string_view backend::mysql::query_result::get_column_name(std::size_t idx)
{
   if (_list_of_fields.empty())
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "Insufficient amount of fields in database.");
   }

   auto &field = _list_of_fields.at(idx);
   return {field.name, field.name_length};
}

std::vector<string> backend::mysql::query_result::get_column_names()
{
   if (_list_of_fields.empty())
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "Insufficient amount of fields in database.");
   }

   std::vector<string> names_list;
   names_list.reserve(_list_of_fields.size());

   std::transform(_list_of_fields.begin(), _list_of_fields.end(), std::back_inserter(names_list),
                  [](MYSQL_FIELD const &field)
                  {
                     return string{field.name, field.name_length};
                  });

   return names_list;
}

namespace
{
   namespace column_ext
   {
      uint32_t get_int24(st_mysql_bind &bind_struct)
      {
         uint8_t *data_address = static_cast<uint8_t *>(bind_struct.buffer);

         uint8_t byte1 = *data_address++;
         uint8_t byte2 = *data_address++;
         uint8_t byte3 = *data_address;

         return (byte1) + (byte2 << 8) + (byte3 << 16);
      }

      template <class T>
      optional<T> get_floating_value(enum_field_types type, st_mysql_bind &bind_struct)
      {
         static_assert(std::is_floating_point_v<T>, "T is not a floating point number");
         if ((bind_struct.is_null != nullptr && *bind_struct.is_null) || bind_struct.is_null_value)
         {
            return {};
         }

         switch (type)
         {
         case MYSQL_TYPE_FLOAT:
            return {static_cast<T>(*reinterpret_cast<fl32 *>(bind_struct.buffer))};
         case MYSQL_TYPE_DOUBLE:
            return {static_cast<T>(*reinterpret_cast<fl64 *>(bind_struct.buffer))};
         default:
            throw exception(exception::error_code::CALLER_EXCEPTION, "unsupported type conversion");
         }
      }

      template <class T>
      optional<T> get_integral_value(enum_field_types type, st_mysql_bind &bind_struct)
      {
         static_assert(std::is_integral_v<T>, "T is not an integer");
         if ((bind_struct.is_null != nullptr && *bind_struct.is_null) || bind_struct.is_null_value)
         {
            return {};
         }

         if (bind_struct.is_unsigned)
         {
            switch (type)
            {
            case MYSQL_TYPE_TINY:
               return gsl::narrow<T>(*static_cast<u8 *>(bind_struct.buffer));
            case MYSQL_TYPE_SHORT:
               return gsl::narrow<T>(*static_cast<u16 *>(bind_struct.buffer));
            case MYSQL_TYPE_LONG:
               return gsl::narrow<T>(*static_cast<u32 *>(bind_struct.buffer));
            case MYSQL_TYPE_LONGLONG:
               return gsl::narrow<T>(*static_cast<u64 *>(bind_struct.buffer));
            case MYSQL_TYPE_INT24:
               return gsl::narrow<T>(get_int24(bind_struct));
            default:
               throw exception(exception::error_code::CALLER_EXCEPTION, "unsupported type conversion");
            }
         }
         else
         {
            switch (type)
            {
            case MYSQL_TYPE_TINY:
               return gsl::narrow<T>(*static_cast<i8 *>(bind_struct.buffer));
            case MYSQL_TYPE_SHORT:
               return gsl::narrow<T>(*static_cast<i16 *>(bind_struct.buffer));
            case MYSQL_TYPE_LONG:
               return gsl::narrow<T>(*static_cast<i32 *>(bind_struct.buffer));
            case MYSQL_TYPE_LONGLONG:
               return gsl::narrow<T>(*static_cast<i64 *>(bind_struct.buffer));
            case MYSQL_TYPE_INT24:
               return gsl::narrow<T>(get_int24(bind_struct));
            default:
               throw exception(exception::error_code::CALLER_EXCEPTION, "unsupported type conversion");
            }
         }
      }
   } // namespace column_ext

} // namespace

optional<i64> backend::mysql::query_result::get_int64(std::size_t col)
{
   return column_ext::get_integral_value<i64>(get_field_type(col), get_field_value(col));
}

optional_field_value backend::mysql::query_result::get_value(std::size_t col)
{
   auto &bind_struct = get_field_value(col);
   if ((bind_struct.is_null != nullptr && *bind_struct.is_null) || bind_struct.is_null_value)
   {
      return {};
   }

   auto type = _list_of_fields.at(col).type;
   bool is_unsigned = bind_struct.is_unsigned != 0;

   switch (type)
   {
   case MYSQL_TYPE_TINY:
      if (is_unsigned)
      {
         return get_uint8(col).value();
      }
      else
      {
         return get_int8(col).value();
      }
   case MYSQL_TYPE_SHORT:
      if (is_unsigned)
      {
         return get_uint16(col).value();
      }
      else
      {
         return get_int16(col).value();
      }
   case MYSQL_TYPE_LONG:
      if (is_unsigned)
      {
         return get_uint32(col).value();
      }
      else
      {
         return get_int32(col).value();
      }
   case MYSQL_TYPE_LONGLONG:
      if (is_unsigned)
      {
         return get_uint64(col).value();
      }
      else
      {
         return get_int64(col).value();
      }
   case MYSQL_TYPE_INT24:
      if (is_unsigned)
      {
         return get_uint32(col).value();
      }
      else
      {
         return get_int32(col).value();
      }
   case MYSQL_TYPE_DOUBLE:
      return get_double(col).value();
   case MYSQL_TYPE_FLOAT:
      return get_float(col).value();
   case MYSQL_TYPE_VARCHAR:
      [[fallthrough]];
   case MYSQL_TYPE_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_VAR_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_TINY_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_MEDIUM_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_LONG_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_BLOB:
      return get_string_view(col).value();
   default:
      throw exception(exception::error_code::DRIVER_EXCEPTION,
                      fmt::format("Data type conversion not supported by this driver. MYSQL_FIELD.type={}",
                                  static_cast<uint8_t>(type)));
   }
}

optional<fl64> backend::mysql::query_result::get_double(std::size_t col)
{
   return column_ext::get_floating_value<fl64>(get_field_type(col), get_field_value(col));
}

optional<bool> backend::mysql::query_result::get_bool(std::size_t col)
{
   auto result = column_ext::get_integral_value<u8>(get_field_type(col), get_field_value(col));
   return result.has_value() ? optional<bool>{result.value() != 0} : optional<bool>{};
}

optional<i8> backend::mysql::query_result::get_int8(std::size_t col)
{
   return column_ext::get_integral_value<i8>(get_field_type(col), get_field_value(col));
}

optional<i16> backend::mysql::query_result::get_int16(std::size_t col)
{
   return column_ext::get_integral_value<i16>(get_field_type(col), get_field_value(col));
}

optional<i32> backend::mysql::query_result::get_int32(std::size_t col)
{
   return column_ext::get_integral_value<i32>(get_field_type(col), get_field_value(col));
}

optional<u8> backend::mysql::query_result::get_uint8(std::size_t col)
{
   return column_ext::get_integral_value<u8>(get_field_type(col), get_field_value(col));
}

optional<u16> backend::mysql::query_result::get_uint16(std::size_t col)
{
   return column_ext::get_integral_value<u16>(get_field_type(col), get_field_value(col));
}

optional<u32> backend::mysql::query_result::get_uint32(std::size_t col)
{
   return column_ext::get_integral_value<u32>(get_field_type(col), get_field_value(col));
}

optional<u64> backend::mysql::query_result::get_uint64(std::size_t col)
{
   return column_ext::get_integral_value<u64>(get_field_type(col), get_field_value(col));
}

optional<fl32> backend::mysql::query_result::get_float(std::size_t col)
{
   return column_ext::get_floating_value<fl32>(get_field_type(col), get_field_value(col));
}

bool backend::mysql::query_result::next_row()
{
   int fetched_data = -1;
   _get_connection().call_mysql([&](MYSQL *connection)
   {
      fetched_data = mysql_stmt_fetch(_stm);
   });
   if (fetched_data == 0) // 0x64 == 100 == MYSQL_NO_DATA, 0x65 == 101 == MYSQL_DATA_TRUNCATED
   {
      return true;
   }
   else
   {
      return false;
   }
}

bool backend::mysql::query_result::first_row()
{
   return next_row();
}

i64 backend::mysql::query_result::get_non_optional_int64(std::size_t col)
{
   return get_int64(col).value_or(0);
}

string_view backend::mysql::query_result::get_non_optional_string(std::size_t col)
{
   return get_string_view(col).value_or(string_view{});
}

fl64 backend::mysql::query_result::get_non_optional_double(std::size_t col)
{
   return get_double(col).value_or(0.0);
}

backend::mysql::connection &backend::mysql::query_result::_get_connection()
{
   return _query._connection;
}

void backend::mysql::query_result::fill_column_names(mutable_array_view<string_view> column_names)
{
   if (column_names.size() < _list_of_fields.size())
   {
      throw exception(exception::error_code::CALLER_EXCEPTION,
                      "column_names.size() is smaller than get_column_count() preallocate buffer first! ");
   }

   std::transform(_list_of_fields.begin(), _list_of_fields.end(), column_names.begin(),
                  [](MYSQL_FIELD const &field)
                  {
                     return string_view{field.name, field.name_length};
                  });
}

void backend::mysql::query_result::fill_row_values(
   commons::basic_types::mutable_array_view<optional_field_value> fields)
{
   if (fields.size() < _list_of_fields.size())
   {
      throw exception(exception::error_code::CALLER_EXCEPTION,
                      "fields.size() is smaller than get_column_count() preallocate buffer first! ");
   }

   for (std::size_t i = 0; i < _list_of_fields.size(); ++i)
   {
      fields[i] = get_value(i);
   }
}

void backend::mysql::query_result::fill_row_values(mutable_array_view<field_value> fields)
{
   for (std::size_t i = 0; i < _list_of_fields.size(); ++i)
   {
      fields[i] = get_value(i).value_or(get_empty_field_value(i));
   }
}

optional<string_view> backend::mysql::query_result::get_string_view(std::size_t col)
{
   auto &bind_struct = get_field_value(col);
   if ((bind_struct.is_null != nullptr && *bind_struct.is_null) || bind_struct.is_null_value)
   {
      return {};
   };

   MYSQL_FIELD &field = _list_of_fields.at(col);
   switch (field.type)
   {
   case MYSQL_TYPE_VARCHAR:
      [[fallthrough]];
   case MYSQL_TYPE_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_VAR_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_TINY_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_MEDIUM_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_LONG_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_BLOB:
      return {string_view{static_cast<const char *>(bind_struct.buffer), _info[col].length}};
   default:
      throw exception(exception::error_code::DRIVER_EXCEPTION,
                      fmt::format("Data type conversion not supported by this driver. MYSQL_FIELD.type={}",
                                  static_cast<uint8_t>(field.type)));
   }
}

optional<string> backend::mysql::query_result::get_string(std::size_t col)
{
   auto result = get_string_view(col);
   if (result.has_value())
   {
      return {static_cast<string>(result.value())};
   }
   else
   {
      return {};
   }
}

field_value backend::mysql::query_result::get_empty_field_value(std::size_t col)
{
   auto type = get_field_type(col);
   auto &bind_struct = get_field_value(col);
   bool is_unsigned = bind_struct.is_unsigned != 0;

   switch (type)
   {
   case MYSQL_TYPE_NULL:

      return i64{0};

   case MYSQL_TYPE_TINY:
      if (is_unsigned)
      {
         return u8{0};
      }
      else
      {
         return i8{0};
      }
   case MYSQL_TYPE_SHORT:
      if (is_unsigned)
      {
         return u16{0};
      }
      else
      {
         return i16{0};
      }
   case MYSQL_TYPE_LONG:
      if (is_unsigned)
      {
         return u32{0};
      }
      else
      {
         return i32{0};
      }
   case MYSQL_TYPE_LONGLONG:
      if (is_unsigned)
      {
         return u64{0};
      }
      else
      {
         return i64{0};
      }
   case MYSQL_TYPE_INT24:
      if (is_unsigned)
      {
         return u32{0};
      }
      else
      {
         return i32{0};
      }
   case MYSQL_TYPE_DOUBLE:
      return fl64{0.0};
   case MYSQL_TYPE_FLOAT:
      return fl32{0.0f};
   case MYSQL_TYPE_VARCHAR:
      [[fallthrough]];
   case MYSQL_TYPE_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_VAR_STRING:
      [[fallthrough]];
   case MYSQL_TYPE_TINY_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_MEDIUM_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_LONG_BLOB:
      [[fallthrough]];
   case MYSQL_TYPE_BLOB:
      return string_view{};
   default:
      throw exception(exception::error_code::DRIVER_EXCEPTION,
                      fmt::format("Data type conversion not supported by this driver. MYSQL_FIELD.type={}",
                                  static_cast<uint8_t>(type)));
   }
}

enum_field_types backend::mysql::query_result::get_field_type(std::size_t col)
{
   if (col < _list_of_fields.size())
   {
      return _list_of_fields[col].type;
   }
   return MYSQL_TYPE_NULL;
}

namespace
{
   MYSQL_BIND make_out_of_bounds_bind()
   {
      MYSQL_BIND bind;
      memset(&bind, 0, sizeof(bind));
      bind.is_null_value = true;

      return bind;
   }
} // namespace

MYSQL_BIND &backend::mysql::query_result::get_field_value(std::size_t col)
{
   static MYSQL_BIND out_of_bounds_bind = make_out_of_bounds_bind();

   if (col < _bound_data.size())
   {
      return _bound_data[col];
   }

   return out_of_bounds_bind;
}
