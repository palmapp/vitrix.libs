// mysql_driver.cpp : Defines the entry point for the console application.

#include "mysql_driver.h"
#include "connection.h"
#include "properties_parser.hpp"
#include "utils.h"
#include <algorithm>
#include <iterator>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>
#include <xdb/backend.h>

const char *xdb::backend::mysql::driver::uri_schema() const
{
   return driver::driver_uri_schema;
}

xdb::ptr<xdb::backend::iconnection> xdb::backend::mysql::driver::open(const std::string &connection_string)
{
   return make_ptr<connection>(connection_string);
}