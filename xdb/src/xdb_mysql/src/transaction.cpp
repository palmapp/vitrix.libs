#include "transaction.h"

xdb::backend::mysql::transaction::transaction(connection &connection) : _connection(connection)
{
   connection.call_mysql([&](MYSQL *connection) { mysql_query(connection, "START TRANSACTION"); });
   is_rolled_back = false;
}

xdb::backend::mysql::transaction::~transaction()
{
   rollback();
}

void xdb::backend::mysql::transaction::commit()
{
   _connection.call_mysql([&](MYSQL *connection) { mysql_commit(connection); });
}

void xdb::backend::mysql::transaction::rollback()
{
   if (!is_rolled_back)
   {
      _connection.call_mysql([&](MYSQL *connection) { mysql_rollback(connection); });
      is_rolled_back = true;
   }
}