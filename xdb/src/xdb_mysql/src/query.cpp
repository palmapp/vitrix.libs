#include "query.h"
#include "query_result.h"
#include <algorithm>
#include <fmt/format.h>
#include <gsl/narrow>
#include <memory.h>
#include <query.h>
#include <sstream>

xdb::backend::mysql::query::query(xdb::backend::mysql::connection &connection, const std::string &sql)
   : _sql(sql), _connection(connection)
{
   _bound_data.reserve(1000);
}

std::int64_t xdb::backend::mysql::query::execute()
{
   std::unique_ptr<MYSQL_STMT, decltype(&mysql_stmt_close)> own_ptr = {prepare_statement(), &mysql_stmt_close};

   MYSQL_STMT *stm = own_ptr.get();

   _connection.call_mysql(
      [&](MYSQL *conn)
      {
         auto res = mysql_stmt_execute(stm);
         if (res != 0)
         {
            throw exception(exception::error_code::DRIVER_EXCEPTION,
                            fmt::format("{}\n\tSQL:{}", mysql_stmt_error(stm), _sql));
         }
      });

   my_ulonglong changes = 0;
   _connection.call_mysql(
      [&](MYSQL *connection)
      {
         changes = mysql_affected_rows(connection);

         if (changes > static_cast<my_ulonglong>((std::numeric_limits<int64_t>::max)()))
         {
            throw exception(exception::error_code::DRIVER_EXCEPTION,
                            "Number of changes too large to be supported by this driver.");
         }
      });

   return changes;
}

MYSQL_STMT *xdb::backend::mysql::query::prepare_statement()
{
   // stm = nullptr;
   MYSQL_STMT *stm = nullptr;
   _connection.reset_connection();
   _connection.call_mysql([&](MYSQL *connection)
   {
      stm = mysql_stmt_init(connection);
   });
   std::unique_ptr<MYSQL_STMT, decltype(&mysql_stmt_close)> own_ptr = {stm, &mysql_stmt_close};

   unsigned long length = static_cast<unsigned long>(_sql.size());
   _connection.call_mysql([&](MYSQL *connection)
   {
      mysql_stmt_prepare(stm, _sql.c_str(), length);
   });

   _connection.call_mysql(
      [&](MYSQL *connection)
      {
         auto err = mysql_stmt_bind_param(stm, _bound_data.data());
         if (err)
         {
            throw exception(exception::error_code::DRIVER_EXCEPTION,
                            fmt::format("{}\n\tSQL:{}", mysql_stmt_error(stm), _sql));
         }
      });

   return own_ptr.release();
}

std::int64_t xdb::backend::mysql::query::execute_no_except() noexcept
{
   try
   {
      return execute();
   }
   catch (...)
   {
      return -1;
   }
}

xdb::optional<std::int64_t> xdb::backend::mysql::query::execute_for_int64()
{
   query_result res(*this);
   if (res.first_row())
   {
      return res.get_int64(0);
   }
   else
   {
      return {};
   }
   // return first row, first column; if not there return optional type
}

xdb::optional<double> xdb::backend::mysql::query::execute_for_double()
{
   query_result res(*this);
   if (res.first_row())
   {
      return res.get_double(0);
   }
   else
   {
      return {};
   }
}

xdb::optional<std::string> xdb::backend::mysql::query::execute_for_string()
{
   query_result res(*this);
   if (res.first_row())
   {
      return res.get_string(0);
   }
   else
   {
      return {};
   }
}

void xdb::backend::mysql::query::adjust_size(std::size_t index)
{
   const auto size = _bound_data.size();
   if (index < size)
   {
      return;
   }

   if (index > 0xffff)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "Index was given that is too large.");
   }

   _bound_data.insert(_bound_data.end(), index + 1 - size, MYSQL_BIND{});
}

void xdb::backend::mysql::query::bind(std::size_t i, xdb::field_value value)
{
   if (i == 0)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "MYSQL: query bind index is zero.");
   }

   // bind arguments are indexed from one
   --i;

   const auto type = static_cast<field_value_type>(value.index());

   // ensure the size plus one
   adjust_size(i);

   MYSQL_BIND &bind = _bound_data.at(i);

   static my_bool false_val = 0;
   bind.is_null = &false_val;
   bind.is_null_value = false_val;

   switch (type)
   {
   case field_value_type::type_string:
   {
      string *value_ptr = value.try_cast<string>();
      _param_strings.emplace_back(value_ptr->data(), value_ptr->size());
      bind.buffer = reinterpret_cast<u8 *>(_param_strings.back().data());
      bind.buffer_length = gsl::narrow<decltype(bind.buffer_length)>(value_ptr->size());
      bind.length_value = gsl::narrow<decltype(bind.length_value)>(value_ptr->size());
      bind.buffer_type = MYSQL_TYPE_STRING;
   }
   break;
   case field_value_type::type_string_view:
   {
      string_view *value_ptr = value.try_cast<string_view>();
      _param_strings.emplace_back(value_ptr->data(), value_ptr->size());
      bind.buffer = reinterpret_cast<u8 *>(_param_strings.back().data());
      bind.buffer_length = gsl::narrow<decltype(bind.buffer_length)>(value_ptr->size());
      bind.length_value = gsl::narrow<decltype(bind.length_value)>(value_ptr->size());
      bind.buffer_type = MYSQL_TYPE_STRING;
   }
   break;
   case field_value_type::type_bool:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<bool>());
      bind.buffer_type = MYSQL_TYPE_TINY;
      bind.is_unsigned = true;
      break;
   case field_value_type::type_i8:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<i8>());
      bind.buffer_type = MYSQL_TYPE_TINY;
      break;
   case field_value_type::type_i16:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<i16>());
      bind.buffer_type = MYSQL_TYPE_SHORT;
      break;
   case field_value_type::type_i32:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<i32>());
      bind.buffer_type = MYSQL_TYPE_LONG;
      break;
   case field_value_type::type_i64:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<i64>());
      bind.buffer_type = MYSQL_TYPE_LONGLONG;
      break;
   case field_value_type::type_u8:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<u8>());
      bind.buffer_type = MYSQL_TYPE_TINY;
      bind.is_unsigned = true;

      break;
   case field_value_type::type_u16:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<u16>());
      bind.buffer_type = MYSQL_TYPE_SHORT;
      bind.is_unsigned = true;
      break;
   case field_value_type::type_u32:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<u32>());
      bind.buffer_type = MYSQL_TYPE_LONG;
      bind.is_unsigned = true;
      break;
   case field_value_type::type_u64:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<u64>());
      bind.buffer_type = MYSQL_TYPE_LONGLONG;
      bind.is_unsigned = true;
      break;
   case field_value_type::type_fl32:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<fl32>());
      bind.buffer_type = MYSQL_TYPE_FLOAT;
      break;
   case field_value_type::type_fl64:
      _param_primitive.push_back(value);
      bind.buffer = reinterpret_cast<u8 *>(_param_primitive.back().try_cast<fl64>());
      bind.buffer_type = MYSQL_TYPE_DOUBLE;
      break;
   default:
      throw exception(exception::error_code::CALLER_EXCEPTION, "The type is unknown. Update the MYSQL driver.");
   }
}

void xdb::backend::mysql::query::bind_optional(std::size_t i, xdb::optional_field_value value)
{
   if (i == 0)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "MYSQL: query bind index is zero.");
   }

   if (value.has_value())
   {
      bind(i, value.value());
   }
   else
   {
      bind_null(i);
   }
}

void xdb::backend::mysql::query::bind_null(std::size_t i)
{
   if (i == 0)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "MYSQL: query bind index is zero.");
   }

   static my_bool true_val = 1;

   --i;
   adjust_size(i);

   MYSQL_BIND &bind = _bound_data[i];
   bind.is_null = &true_val;
   bind.is_null_value = true_val;
   bind.buffer_type = MYSQL_TYPE_NULL;
}

void xdb::backend::mysql::query::bind(commons::basic_types::string_view key, xdb::field_value value)
{
   throw exception(exception::error_code::DRIVER_EXCEPTION, "Lookup by string not supported in MySQL.");
}

void xdb::backend::mysql::query::bind_optional(commons::basic_types::string_view key, xdb::optional_field_value value)
{

   throw exception(exception::error_code::DRIVER_EXCEPTION, "Lookup by string not supported in MySQL.");
}

void xdb::backend::mysql::query::bind_null(commons::basic_types::string_view key)
{
   throw exception(exception::error_code::DRIVER_EXCEPTION, "Lookup by string not supported in MySQL.");
}

std::string xdb::backend::mysql::query::get_query_string() const
{
   return _sql;
}

xdb::ptr<xdb::backend::iquery_result> xdb::backend::mysql::query::execute_for_result()
{
   return make_ptr<query_result>(*this);
}
