#pragma once
#include "sqlite3.h"
namespace xdb
{
   namespace backend
   {
      namespace sqlite
      {

         void throw_if_error(int code, sqlite3 *connection);

      }
   } // namespace backend
} // namespace xdb