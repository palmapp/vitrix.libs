#pragma once
#include "sqlite3.h"
#include "utils.h"
#include <memory>
#include <xdb/backend.h>


namespace xdb
{
    namespace backend
    {
        namespace sqlite
        {
            class driver : public ixdb_driver
            {
            public:
                driver() = default;

                constexpr static const char* driver_uri_schema = "sqlite";

                virtual const char* uri_schema() const override;
                virtual ptr<backend::iconnection> open(const string& connection_string) override;
            };

            class connection : public iconnection
            {
            public:
                connection(const string& path);
                ~connection();
                // Inherited via iconnection
                virtual bool is_connected() const override;
                virtual ptr<itransaction>
                begin_transaction(isolation_level level = isolation_level::serializable) override;
                virtual ptr<iquery> create_query(const string& query) override;
                virtual ptr<iquery_result> execute_query_for_result(const string& query) override;
                virtual i64 execute_query(const string& query) override;
                virtual i64 execute_query_no_except(const string&) noexcept override;
                virtual i64 get_int64_last_row_id() override;
                virtual string get_string_last_row_id() override;
                virtual void close() override;
                virtual const char* sql_dialect() const override;

                virtual bool ping() noexcept override;

                void execute_sql(const char* sql);

                ptr<backend::istm_builder> get_builder(stm_type type, string table_name) override;

                inline sqlite3* handle()
                {
                    return _connection;
                }

                string get_last_error_message();

                template <class FunctT>
                void call_sqlite(FunctT funct)
                {
                    throw_if_error(funct(_connection), _connection);
                }

                i64 transaction_counter = 0;

            private:
                sqlite3* _connection;
            };

            class transaction : public itransaction
            {
            public:
                transaction(connection& connection);
                ~transaction();

                virtual void commit() override;
                virtual void rollback() override;

            private:
                connection* _connection;
            };

            class query_result;

            class query : public iquery
            {
            public:
                query(connection& _connection, const string& sql);
                ~query();
                // Inherited via iquery
                string get_query_string() const final;

                void bind(std::size_t i, field_value value) final;
                void bind_optional(std::size_t i, optional_field_value value) final;
                void bind_null(std::size_t i) final;

                void bind(string_view key, field_value value) final;
                void bind_optional(string_view key, optional_field_value value) final;
                void bind_null(string_view key) final;

                ptr<iquery_result> execute_for_result() final;
                i64 execute() final;
                i64 execute_no_except() noexcept final;
                optional<string> execute_for_string() final;
                optional<i64> execute_for_int64() final;
                optional<fl64> execute_for_double() final;

            private:
                connection& _connection;
                sqlite3_stmt* _stm;
                string _sql;

                friend class query_result;
            };

            class query_result : public iquery_result
            {
            public:
                query_result(query& query);
                ~query_result();

                // Inherited via iquery_result
                std::size_t get_column_count() final;

                string_view get_column_name(std::size_t idx) final;

                std::vector<string> get_column_names() final;
                void fill_column_names(mutable_array_view<string_view> column_names) final;

                optional<i64> get_int64(std::size_t col) final;
                optional<string> get_string(std::size_t col) final;
                optional<fl64> get_double(std::size_t col) final;
                optional<i8> get_int8(std::size_t col) final;
                optional<i16> get_int16(std::size_t col) final;
                optional<i32> get_int32(std::size_t col) final;
                optional<u8> get_uint8(std::size_t col) final;
                optional<u16> get_uint16(std::size_t col) final;
                optional<u32> get_uint32(std::size_t col) final;
                optional<u64> get_uint64(std::size_t col) final;
                optional<fl32> get_float(std::size_t col) final;
                optional<bool> get_bool(std::size_t col) final;
                optional<string_view> get_string_view(std::size_t col) final;
                optional_field_value get_value(std::size_t col) final;
                void fill_row_values(mutable_array_view<optional_field_value> fields) final;
                void fill_row_values(mutable_array_view<field_value> fields) final;

                virtual bool next_row() final;
                virtual bool first_row() final;

                // internal driver api
                i64 get_non_optional_int64(std::size_t col);
                fl64 get_non_optional_double(std::size_t col);
                string_view get_non_optional_string(std::size_t col);

            private:
                query& _query;
            };

            class self_contained_query_result : private query, public query_result
            {
            public:
                self_contained_query_result(connection& connection, const string& sql);
            };
        } // namespace sqlite
    } // namespace backend
} // namespace xdb
