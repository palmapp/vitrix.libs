#include "sqlite_driver.h"
#include "stm_builder.h"
#include "utils.h"
#include <algorithm>
#include <cstring>
#include <gsl/narrow>
#include <iostream>
#include <iterator>
#include <sstream>
#include <xdb/exception.h>

using namespace xdb;

backend::sqlite::connection::connection(const std::string &path)
   : _connection(nullptr)
{
   sqlite3 *connection;
   throw_if_error(sqlite3_open(path.c_str(), &connection), nullptr);
   _connection = connection;
}

backend::sqlite::connection::~connection()
{
   close();
}

bool backend::sqlite::connection::is_connected() const
{
   return _connection != nullptr;
}

std::string backend::sqlite::connection::get_last_error_message()
{
   return {sqlite3_errmsg(_connection)};
}

ptr<backend::itransaction> backend::sqlite::connection::begin_transaction(isolation_level level)
{
   return make_ptr<transaction>(*this);
}

ptr<backend::iquery> backend::sqlite::connection::create_query(const std::string &sql_query)
{
   return make_ptr<query>(*this, sql_query);
}

ptr<backend::iquery_result> backend::sqlite::connection::execute_query_for_result(const std::string &sql_query)
{
   return make_ptr<self_contained_query_result>(*this, sql_query);
}

i64 backend::sqlite::connection::execute_query(const std::string &sql_query)
{
   query stm{*this, sql_query};
   return stm.execute();
}

i64 backend::sqlite::connection::execute_query_no_except(const std::string &sql_query) noexcept
{
   query stm{*this, sql_query};
   return stm.execute_no_except();
}

i64 backend::sqlite::connection::get_int64_last_row_id()
{
   return sqlite3_last_insert_rowid(_connection);
}

std::string backend::sqlite::connection::get_string_last_row_id()
{
   std::stringstream buff;
   buff << get_int64_last_row_id();

   return buff.str();
}

void backend::sqlite::connection::close()
{
   if (_connection != nullptr)
   {
      throw_if_error(sqlite3_close(_connection), _connection);
      _connection = nullptr;
   }
}

const char *backend::sqlite::connection::sql_dialect() const
{
   return driver::driver_uri_schema;
}

bool backend::sqlite::connection::ping() noexcept
{
   return true;
}

void backend::sqlite::connection::execute_sql(const char *sql)
{
   throw_if_error(sqlite3_exec(_connection, sql, nullptr, nullptr, nullptr), _connection);
}

namespace
{
   std::string begin_transaction_sql(i64 current)
   {
      std::stringstream ss;
      ss << "SAVEPOINT "
         << "sp_" << current;
      return ss.str();
   }

   std::string commit_transaction_sql(i64 current)
   {
      std::stringstream ss;
      ss << "RELEASE "
         << "sp_" << current;
      return ss.str();
   }

   std::string rollback_transaction_sql(i64 current)
   {
      std::stringstream ss;
      ss << "ROLLBACK TO "
         << "sp_" << current;
      return ss.str();
   }
} // namespace

backend::sqlite::transaction::transaction(connection &connection)
   : _connection(&connection)
{
   const auto current_transaction = connection.transaction_counter + 1;
   auto stm = begin_transaction_sql(current_transaction);
   connection.execute_sql(stm.c_str());
   connection.transaction_counter = current_transaction;
}

backend::sqlite::transaction::~transaction()
{
   rollback();
}

void backend::sqlite::transaction::commit()
{
   if (_connection != nullptr)
   {
      auto stm = commit_transaction_sql(_connection->transaction_counter);
      _connection->execute_sql(stm.c_str());
      _connection->transaction_counter--;
      _connection = nullptr;
   }
}

void backend::sqlite::transaction::rollback()
{
   if (_connection != nullptr)
   {
      auto stm = rollback_transaction_sql(_connection->transaction_counter);
      _connection->execute_sql(stm.c_str());
      _connection->transaction_counter--;
      _connection = nullptr;
   }
}

backend::sqlite::query::query(connection &connection, const std::string &sql)
   : _connection(connection), _stm(nullptr), _sql(sql)
{
   sqlite3_stmt *stm = nullptr;
   connection.call_sqlite(
      [&stm, &sql](sqlite3 *_db)
      {
         const char *tail;
         return sqlite3_prepare(_db, sql.c_str(), static_cast<int>(sql.size()), &stm, &tail);
      });

   _stm = stm;
}

backend::sqlite::query::~query()
{
   if (_stm != nullptr)
   {
      sqlite3_finalize(_stm);
      _stm = nullptr;
   }
}

std::string backend::sqlite::query::get_query_string() const
{
   return _sql;
}

namespace
{
   int _bind_(sqlite3_stmt *stm, std::size_t idx, string_view val)
   {
      return sqlite3_bind_text(stm, gsl::narrow<int>(idx), val.data(), gsl::narrow<int>(val.size()), SQLITE_TRANSIENT);
   }

   int _bind_(sqlite3_stmt *stm, std::size_t idx, i64 val)
   {
      return sqlite3_bind_int64(stm, gsl::narrow<int>(idx), val);
   }

   int _bind_(sqlite3_stmt *stm, std::size_t idx, i32 val)
   {
      return sqlite3_bind_int(stm, gsl::narrow<int>(idx), val);
   }

   int _bind_(sqlite3_stmt *stm, std::size_t idx, fl64 val)
   {
      return sqlite3_bind_double(stm, gsl::narrow<int>(idx), val);
   }

   int _bind_(sqlite3_stmt *stm, std::size_t idx, std::nullptr_t)
   {
      return sqlite3_bind_null(stm, gsl::narrow<int>(idx));
   }

   template <class T>
   void _bind_stm(backend::sqlite::connection &connection, sqlite3_stmt *stm, string_view key, const T &t)
   {
      auto z_key = static_cast<string>(key);
      connection.call_sqlite([&](sqlite3 *_db)
      {
         return _bind_(stm, sqlite3_bind_parameter_index(stm, z_key.c_str()), t);
      });
   }

   template <class T>
   void _bind_stm(backend::sqlite::connection &connection, sqlite3_stmt *stm, std::size_t idx, const T &t)
   {
      connection.call_sqlite([&](sqlite3 *_db)
      {
         return _bind_(stm, idx, t);
      });
   }

   template <class KeyType>
   struct unbox_value
   {
      xdb::backend::sqlite::connection &connection;
      sqlite3_stmt *stm;
      KeyType i;

      template <class T>
      void operator()(T &value)
      {
         using value_type = std::decay_t<decltype(value)>;
         if constexpr (std::is_integral_v<value_type>)
         {
            if constexpr (std::is_unsigned_v<value_type>)
            {
               if constexpr (sizeof(i32) > sizeof(value_type))
               {
                  ::_bind_stm(connection, stm, i, static_cast<i32>(value));
               }
               else
               {
                  ::_bind_stm(connection, stm, i, gsl::narrow<i64>(value));
               }
            }
            else
            {
               if constexpr (sizeof(i32) >= sizeof(value_type))
               {
                  ::_bind_stm(connection, stm, i, static_cast<i32>(value));
               }
               else
               {
                  ::_bind_stm(connection, stm, i, static_cast<i64>(value));
               }
            }
         }
         else if constexpr (std::is_same_v<fl32, value_type>)
         {
            ::_bind_stm(connection, stm, i, static_cast<fl64>(value));
         }
         else
         {
            ::_bind_stm(connection, stm, i, value);
         }
      }
   };
} // namespace

void backend::sqlite::query::bind_null(std::size_t i)
{
   if (i == 0)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "SQLITE: query bind index is zero.");
   }

   ::_bind_stm(_connection, _stm, i, nullptr);
}

void backend::sqlite::query::bind(std::size_t i, field_value boxed_value)
{
   if (i == 0)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "SQLITE: query bind index is zero.");
   }

   boxed_value.visit(unbox_value<std::size_t>{_connection, _stm, i});
}

void backend::sqlite::query::bind_optional(std::size_t i, optional_field_value value)
{
   if (i == 0)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, "SQLITE: query bind index is zero.");
   }

   if (value.has_value())
   {
      bind(i, value.value());
   }
   else
   {
      bind_null(i);
   }
}

void backend::sqlite::query::bind(string_view key, field_value boxed_value)
{
   boxed_value.visit(unbox_value<string_view>{_connection, _stm, key});
}

void backend::sqlite::query::bind_optional(string_view key, optional_field_value value)
{
   if (value.has_value())
   {
      bind(key, value.value());
   }
   else
   {
      bind_null(key);
   }
}

void backend::sqlite::query::bind_null(string_view key)
{
   ::_bind_stm(_connection, _stm, key, nullptr);
}

ptr<backend::iquery_result> backend::sqlite::query::execute_for_result()
{
   return make_ptr<query_result>(*this);
}

i64 backend::sqlite::query::execute()
{
   i64 counter = 0;
   int result = sqlite3_step(_stm); // mysql equivalent is mysql_stmt_execute()

   if (result == SQLITE_DONE)
   {
      counter = sqlite3_changes(_connection.handle()); //(mysql_affected_rows
   }
   else if (result != SQLITE_ROW)
   {
      throw exception(exception::error_code::DRIVER_EXCEPTION, _connection.get_last_error_message());
   }

   sqlite3_reset(_stm);
   return counter;
}

i64 backend::sqlite::query::execute_no_except() noexcept
{
   i64 counter = 0;
   int result = sqlite3_step(_stm);
   if (result == SQLITE_DONE)
   {
      counter = sqlite3_changes(_connection.handle());
   }

   sqlite3_reset(_stm);
   return counter;
}

namespace
{

   namespace sqlite_value_mapping
   {
      void val(sqlite3_stmt *stm, std::size_t scol, string_view &result)
      {
         int col = gsl::narrow<int>(scol);

         int type = sqlite3_column_type(stm, col);
         switch (type)
         {
         case SQLITE_BLOB:
         {
            const char *str = reinterpret_cast<const char *>(sqlite3_column_blob(stm, col));
            int len = sqlite3_column_bytes(stm, col);
            if (str != nullptr && len > 0)
            {
               result = {str, static_cast<std::size_t>(len)};
            }
         }
         break;
         case SQLITE_TEXT:
         {
            const char *str = reinterpret_cast<const char *>(sqlite3_column_text(stm, col));
            int len = sqlite3_column_bytes(stm, col);
            if (str != nullptr && len > 0)
            {
               result = {str, static_cast<std::size_t>(len)};
            }
         }
         break;
         default:
         {
            const char *str = reinterpret_cast<const char *>(sqlite3_column_text(stm, col));
            if (str != nullptr) result = {str};
         }
         break;
         }
      }

      void val(sqlite3_stmt *stm, std::size_t scol, i64 &result)
      {
         int col = gsl::narrow<int>(scol);
         result = sqlite3_column_int64(stm, col);
      }

      void val(sqlite3_stmt *stm, std::size_t scol, double &result)
      {
         int col = gsl::narrow<int>(scol);
         result = sqlite3_column_double(stm, col);
      }

      void val(sqlite3_stmt *stm, std::size_t scol, float &result)
      {
         int col = gsl::narrow<int>(scol);
         result = static_cast<float>(sqlite3_column_double(stm, col));
      }

      void val(sqlite3_stmt *stm, std::size_t scol, i32 &result)
      {
         int col = gsl::narrow<int>(scol);

         result = static_cast<i32>(sqlite3_column_int(stm, col));
      }

      template <class T>
      void val(sqlite3_stmt *stm, std::size_t scol, T &result)
      {
         static_assert(std::is_integral_v<T>, "T must be integral type");
         int col = gsl::narrow<int>(scol);
         i64 temp = sqlite3_column_int64(stm, col);

         result = gsl::narrow<T>(temp);
      }

   } // namespace sqlite_value_mapping

   template <class T>
   void get_value(sqlite3_stmt *stm, std::size_t col, optional<T> &result)
   {
      T val;
      sqlite_value_mapping::val(stm, col, val);

      result = std::move(val);
   }

   void get_value(sqlite3_stmt *stm, std::size_t col, optional<string> &result)
   {
      string_view val;
      sqlite_value_mapping::val(stm, col, val);

      result = static_cast<string>(val);
   }

   template <class T>
   optional<T> get_optional_value(sqlite3_stmt *stm, std::size_t col)
   {
      optional<T> result;
      std::size_t col_type = sqlite3_column_type(stm, gsl::narrow_cast<int>(col));

      if (col_type != SQLITE_NULL)
      {
         ::get_value(stm, col, result);
      }

      return std::move(result);
   }

   template <class T>
   void _execute_for_value(backend::sqlite::connection &con, sqlite3_stmt *stm, optional<T> &val)
   {
      int code = sqlite3_step(stm);
      if (code == SQLITE_ROW)
      {
         val = get_optional_value<T>(stm, 0);
         sqlite3_reset(stm);
      }
      else if (code == SQLITE_DONE)
      {
         sqlite3_reset(stm);
      }
      else
      {
         sqlite3_reset(stm);
         throw exception(exception::error_code::DRIVER_EXCEPTION, con.get_last_error_message());
      }
   }
} // namespace

optional<std::string> backend::sqlite::query::execute_for_string()
{
   optional<std::string> val;
   ::_execute_for_value(_connection, _stm, val);
   return val;
}

optional<i64> backend::sqlite::query::execute_for_int64()
{
   optional<i64> val;
   ::_execute_for_value(_connection, _stm, val);
   return val;
}

optional<fl64> backend::sqlite::query::execute_for_double()
{
   optional<double> val;
   ::_execute_for_value(_connection, _stm, val);
   return val;
}

backend::sqlite::query_result::query_result(query &query)
   : _query(query)
{
}

backend::sqlite::query_result::~query_result()
{
   sqlite3_reset(_query._stm);
}

std::size_t backend::sqlite::query_result::get_column_count()
{
   return static_cast<std::size_t>(sqlite3_column_count(_query._stm));
}

string_view backend::sqlite::query_result::get_column_name(std::size_t idx)
{
   return {sqlite3_column_name(_query._stm, static_cast<int>(idx))};
}

std::vector<string> backend::sqlite::query_result::get_column_names()
{
   std::vector<string> result;
   std::size_t col_count = get_column_count();
   if (col_count > 0)
   {
      result.reserve(col_count);
      for (std::size_t i = 0; i != col_count; ++i)
      {
         result.emplace_back(static_cast<string>(get_column_name(i)));
      }
   }

   return result;
}

void backend::sqlite::query_result::fill_column_names(mutable_array_view<string_view> column_names)
{
   std::size_t col_count = get_column_count();
   if (column_names.size() >= col_count)
   {
      for (std::size_t i = 0; i != col_count; ++i)
      {
         column_names[i] = get_column_name(i);
      }
   }
   else
   {
      throw exception(
         exception::error_code::CALLER_EXCEPTION,
         "column_names.size() must be larger or equal to get_column_count(). Allocate properly before use.");
   }
}

optional<i64> backend::sqlite::query_result::get_int64(std::size_t col)
{
   return ::get_optional_value<i64>(_query._stm, col);
}

optional<std::string> backend::sqlite::query_result::get_string(std::size_t col)
{
   return ::get_optional_value<std::string>(_query._stm, col);
}

optional<double> backend::sqlite::query_result::get_double(std::size_t col)
{
   return ::get_optional_value<double>(_query._stm, col);
}

optional<i8> backend::sqlite::query_result::get_int8(std::size_t col)
{
   return ::get_optional_value<i8>(_query._stm, col);
}

optional<i16> backend::sqlite::query_result::get_int16(std::size_t col)
{
   return ::get_optional_value<i16>(_query._stm, col);
}

optional<i32> backend::sqlite::query_result::get_int32(std::size_t col)
{
   return ::get_optional_value<i32>(_query._stm, col);
}

optional<u8> backend::sqlite::query_result::get_uint8(std::size_t col)
{
   return ::get_optional_value<u8>(_query._stm, col);
}

optional<u16> backend::sqlite::query_result::get_uint16(std::size_t col)
{
   return ::get_optional_value<u16>(_query._stm, col);
}

optional<u32> backend::sqlite::query_result::get_uint32(std::size_t col)
{
   return ::get_optional_value<u32>(_query._stm, col);
}

optional<u64> backend::sqlite::query_result::get_uint64(std::size_t col)
{
   return ::get_optional_value<u64>(_query._stm, col);
}

optional<fl32> backend::sqlite::query_result::get_float(std::size_t col)
{
   return ::get_optional_value<fl32>(_query._stm, col);
}

bool backend::sqlite::query_result::next_row()
{
   int result = sqlite3_step(_query._stm);
   switch (result)
   {
   case SQLITE_ROW:
      return true;
   case SQLITE_DONE:
      return false;
   default:
      throw exception(exception::error_code::DRIVER_EXCEPTION, _query._connection.get_last_error_message());
   }
}

bool backend::sqlite::query_result::first_row()
{
   return next_row();
}

i64 backend::sqlite::query_result::get_non_optional_int64(std::size_t col)
{
   return sqlite3_column_int64(_query._stm, gsl::narrow_cast<int>(col));
}

double backend::sqlite::query_result::get_non_optional_double(std::size_t col)
{
   return sqlite3_column_double(_query._stm, gsl::narrow_cast<int>(col));;
}

string_view backend::sqlite::query_result::get_non_optional_string(std::size_t col)
{
   const char *text = reinterpret_cast<const char *>(sqlite3_column_text(_query._stm, gsl::narrow_cast<int>(col)));
   std::size_t len = static_cast<std::size_t>(sqlite3_column_bytes(_query._stm, gsl::narrow_cast<int>(col)));

   return {text, len};
}

optional<bool> backend::sqlite::query_result::get_bool(std::size_t col)
{
   auto result = get_int32(col);
   if (result.has_value())
   {
      return {result.value() != 0};
   }
   else
   {
      return {};
   }
}

optional<string_view> backend::sqlite::query_result::get_string_view(std::size_t col)
{
   return ::get_optional_value<string_view>(_query._stm, gsl::narrow_cast<int>(col));
}

optional_field_value backend::sqlite::query_result::get_value(std::size_t col)
{
   std::size_t col_type = sqlite3_column_type(_query._stm, gsl::narrow_cast<int>(col));

   switch (col_type)
   {
   case SQLITE_NULL:
      return {};
   case SQLITE_INTEGER:
      return get_int64(col).value();
   case SQLITE_FLOAT:
      return get_double(col).value();
   case SQLITE_TEXT:
      return get_string_view(col).value();
   case SQLITE_BLOB:
      return get_string_view(col).value();
   default:
      throw exception(exception::error_code::DRIVER_EXCEPTION, "unsupported conversion type");
   }
}

void backend::sqlite::query_result::fill_row_values(mutable_array_view<optional_field_value> fields)
{
   const auto column_count = get_column_count();
   if (fields.size() < column_count)
   {
      throw exception(exception::error_code::CALLER_EXCEPTION,
                      "fields.size() is smaller than get_column_count() preallocate buffer first! ");
   }

   for (std::size_t i = 0; i < column_count; ++i)
   {
      fields[i] = get_value(i);
   }
}

void backend::sqlite::query_result::fill_row_values(mutable_array_view<field_value> fields)
{
   const auto column_count = get_column_count();

   if (fields.size() < column_count)
   {
      throw exception(exception::error_code::CALLER_EXCEPTION,
                      "fields.size() is smaller than get_column_count() preallocate buffer first! ");
   }

   for (std::size_t i = 0; i < column_count; ++i)
   {
      fields[i] = get_value(i).value_or(field_value{i64{0}});
   }
}

backend::sqlite::self_contained_query_result::self_contained_query_result(connection &connection,
                                                                          const std::string &sql)
   : query(connection, sql), query_result(*static_cast<query *>(this))
{
}

const char *backend::sqlite::driver::uri_schema() const
{
   return driver::driver_uri_schema;
}

ptr<backend::iconnection> backend::sqlite::driver::open(const std::string &connection_string)
{
   return make_ptr<connection>(connection_string);
}

ptr<backend::istm_builder> backend::sqlite::connection::get_builder(stm_type type, std::string table_name)
{
   return make_ptr<sqlite::stm_builder>(type, std::move(table_name));
}