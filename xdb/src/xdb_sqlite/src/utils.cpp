#include "utils.h"
#include "sqlite3.h"
#include <xdb/exception.h>

void xdb::backend::sqlite::throw_if_error(int code, sqlite3 *connection)
{
   if (code != SQLITE_OK)
   {
      if (connection == nullptr)
         throw exception(exception::error_code::DRIVER_EXCEPTION, sqlite3_errstr(code));
      else
         throw exception(exception::error_code::DRIVER_EXCEPTION, sqlite3_errmsg(connection));
   }
}
