#include "xdb_mysql_test.h"
#include <catch.hpp> //doc on github

using namespace xdb;
using namespace std::string_literals;

TEST_CASE("Can data be entered and bound? exp")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");

   REQUIRE(db.execute_query("CREATE TABLE experiment ( id INT NOT NULL AUTO_INCREMENT, value TEXT "
                            "NOT NULL, PRIMARY KEY(id))") == 0);

   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES ('Joe'), ('Rogers')") == 2);
   query_result result = db.execute_query_for_result("SELECT * FROM experiment");

   std::vector<std::pair<std::int64_t, std::string>> content;
   for (auto &row : result)
   {
      content.emplace_back(row.get_int64(0).value(), row.get_string(1).value());
   }

   REQUIRE(content.size() == 2);
}

TEST_CASE("Can text be read from multiple types? exp")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");
   REQUIRE(db.execute_query("CREATE TABLE experiment (id INT NOT NULL AUTO_INCREMENT, value BLOB "
                            "NOT NULL, PRIMARY KEY(id))") == 0);
   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES ('J0E')") == 1);
   query_result result = db.execute_query_for_result("SELECT * FROM experiment");

   std::vector<std::pair<std::int64_t, std::string>> content;
   for (auto &row : result)
   {
      content.emplace_back(row.get_int64(0).value(), row.get_string(1).value());
   }
   REQUIRE(content.size() == 1);
   REQUIRE(content[0] == std::make_pair(std::int64_t(1), "J0E"s));

   db.execute_query("DROP TABLE IF EXISTS experiment");
   REQUIRE(db.execute_query("CREATE TABLE experiment (id INT NOT NULL AUTO_INCREMENT, value "
                            "CHAR(8) NOT NULL, PRIMARY KEY(id))") == 0);
   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES ('ROGERS')") == 1);
   query_result char_result = db.execute_query_for_result("SELECT * FROM experiment");
   std::vector<std::pair<std::int64_t, std::string>> char_content;
   for (auto &row : char_result)
   {
      char_content.emplace_back(row.get_int64(0).value(), row.get_string(1).value());
   }
   REQUIRE(char_content.size() == 1);
   REQUIRE(char_content[0] == std::make_pair(std::int64_t(1), "ROGERS"s));
}
