#include "xdb_mysql_test.h"
#include <catch.hpp>
#include <gsl/narrow>

using namespace std;
using namespace xdb;
using namespace std::string_literals;

TEST_CASE("Is database connection opened?")
{
   connection db = test_db::open_mysql_connection();
   REQUIRE(db.is_connected() == true);
}

TEST_CASE("Is database connection closed?")
{
   connection db = test_db::open_mysql_connection();
   db.close();
   REQUIRE(db.is_connected() == false);
}

TEST_CASE("Can create table and execute statement?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS test");
   REQUIRE(db.execute_query("CREATE TABLE test( id INT NOT NULL AUTO_INCREMENT, value TEXT, PRIMARY KEY(id))") == 0);
   REQUIRE(db.execute_query("INSERT INTO test (value) VALUES ('test')") == 1);
}

TEST_CASE("Can query column names?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS test");
   REQUIRE(db.execute_query("CREATE TABLE test ( id INT NOT NULL AUTO_INCREMENT, value TEXT, PRIMARY KEY(id))") == 0);

   query_result result = db.execute_query_for_result("SELECT * FROM test");
   REQUIRE(result.get_column_count() == 2);

   REQUIRE(result.get_column_name(0) == "id");
   REQUIRE(result.get_column_name(1) == "value");

   auto columns = result.get_column_names();
   REQUIRE(columns.size() == 2);
   REQUIRE(columns[0] == "id");
   REQUIRE(columns[1] == "value");
}

TEST_CASE("Can query result be empty?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS test");

   REQUIRE(db.execute_query("CREATE TABLE test ( id INT NOT NULL AUTO_INCREMENT, value TEXT, PRIMARY KEY(id))") == 0);

   query_result result = db.execute_query_for_result("SELECT * FROM test");

   int count = 0;
   for (auto &row : result)
   {
      static_cast<void>(row);
      ++count;
   }

   REQUIRE(count == 0);
}

TEST_CASE("Does query result contain inserted values?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS test");

   REQUIRE(db.execute_query(
         "CREATE TABLE test ( id INT NOT NULL AUTO_INCREMENT NOT NULL, value TEXT NOT NULL, PRIMARY KEY(id))") ==
      0);
   REQUIRE(db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')") == 3);

   query_result result = db.execute_query_for_result("SELECT * FROM test");

   std::vector<std::pair<std::int64_t, std::string>> content;
   for (auto &row : result)
   {
      content.emplace_back(row.get_int64(0).value(), row.get_string(1).value());
   }

   REQUIRE(content.size() == 3);
   REQUIRE(content[0] == std::make_pair(std::int64_t(1), "test1"s));
   REQUIRE(content[1] == std::make_pair(std::int64_t(2), "test2"s));
   REQUIRE(content[2] == std::make_pair(std::int64_t(3), "test3"s));
}

TEST_CASE("Are transactions working correctly?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS test");
   REQUIRE(db.execute_query(
      "CREATE TABLE IF NOT EXISTS test ( id INT NOT NULL AUTO_INCREMENT, value TEXT, PRIMARY KEY(id))") == 0);

   SECTION("Is transaction commited at the end of scope?")
   {
      {
         transaction_guard transaction = db.begin_transaction();
         auto inserted = db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         REQUIRE(inserted == 3);
      }

      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 3ll);
   }

   SECTION("Is transaction rolled back when exception is thrown?")
   {
      try
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");

         throw 0;
      }
      catch (...)
      {
      }

      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 0ll);
   }

   SECTION("Is transaction rolled-back manually?")
   {
      connection test_db = test_db::open_mysql_connection();
      test_db.execute_query("DROP TABLE IF EXISTS test");

      REQUIRE(test_db.execute_query("CREATE TABLE IF NOT EXISTS test ( id INT NOT NULL AUTO_INCREMENT, value TEXT NOT NULL, "
         "PRIMARY KEY(id))") == 0);
      {
         transaction_guard transaction = test_db.begin_transaction();
         test_db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         transaction.rollback();
      }

      auto query = test_db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(1) == 0ll);
   }

   SECTION("Is transaction committed manually?")
   {
      try
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         transaction.commit();
         throw 0;
      }
      catch (...)
      {
      }
      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 3ll);
   }

   SECTION("Is transaction committed in catch handler?")
   {
      try
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         throw 0;
      }
      catch (...)
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
      }

      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 3ll);
   }
}

// Additional testing.

TEST_CASE("Can data be entered and bound?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");

   REQUIRE(db.execute_query(
      "CREATE TABLE experiment ( id INT NOT NULL AUTO_INCREMENT, value TEXT NOT NULL, PRIMARY KEY(id))") == 0);

   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES ('Joe'), ('Rogers')") == 2);
   query_result result = db.execute_query_for_result("SELECT * FROM experiment");

   std::vector<std::pair<std::int64_t, std::string>> content;
   for (auto &row : result)
   {
      content.emplace_back(row.get_int64(0).value(), row.get_string(1).value());
   }

   REQUIRE(content.size() == 2);
}

TEST_CASE("Can text be read from multiple types?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");
   REQUIRE(db.execute_query("CREATE TABLE experiment (id INT NOT NULL AUTO_INCREMENT, "
      "value_blob BLOB NOT NULL,"
      "value_varchar VARCHAR(255) NOT NULL,"
      "value_text TEXT NOT NULL,"
      "PRIMARY KEY(id))") == 0);
   REQUIRE(db.execute_query(
      "INSERT INTO experiment (value_blob, value_varchar,value_text ) VALUES ('J0E', 'J0E','J0E')") == 1);
   query_result result = db.execute_query_for_result("SELECT value_blob, value_varchar,value_text FROM experiment");

   bool has_rows = false;
   for (auto &row : result)
   {
      REQUIRE(row.get_string(0) == "J0E");
      REQUIRE(row.get_string(1) == "J0E");
      REQUIRE(row.get_string(2) == "J0E");
      REQUIRE(row.get_string_view(0) == "J0E");
      REQUIRE(row.get_string_view(1) == "J0E");
      REQUIRE(row.get_string_view(2) == "J0E");
      has_rows = true;
   }
   REQUIRE(has_rows == true);
}

TEST_CASE("Can a three byte integer be stored and retrieved?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");
   REQUIRE(db.execute_query(
         "CREATE TABLE experiment (id INT NOT NULL AUTO_INCREMENT, value MEDIUMINT NOT NULL, PRIMARY KEY(id))") ==
      0);
   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES (1500000)") == 1);
   query_result result = db.execute_query_for_result("SELECT value FROM experiment");

   bool has_result = false;
   for (auto &row : result)
   {
      REQUIRE(row.get_int64(0).value() == 1500000);
      has_result = true;
   }

   REQUIRE(has_result == true);
}

TEST_CASE("Can a null value can be retrieved?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");
   REQUIRE(db.execute_query(
      "CREATE TABLE experiment (id INT NOT NULL AUTO_INCREMENT, value TINYINT NULL, PRIMARY KEY(id))") == 0);
   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES (NULL)") == 1);
   query_result result = db.execute_query_for_result("SELECT value FROM experiment");

   bool has_rows = false;
   for (auto &row : result)
   {
      REQUIRE(row.get_int8(0).has_value() == false);
      REQUIRE(row.get_int16(0).has_value() == false);
      REQUIRE(row.get_int32(0).has_value() == false);
      REQUIRE(row.get_int64(0).has_value() == false);

      has_rows = true;
   }

   REQUIRE(has_rows == true);
}

TEST_CASE("Is narrowing error thrown on overflow?")
{
   connection db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS experiment");
   REQUIRE(db.execute_query(
         "CREATE TABLE experiment (id INT NOT NULL AUTO_INCREMENT, value SMALLINT UNSIGNED, PRIMARY KEY(id))") ==
      0);
   REQUIRE(db.execute_query("INSERT INTO experiment (value) VALUES (65535)") == 1);
   query_result result = db.execute_query_for_result("SELECT value FROM experiment");

   bool narrow_failed = false;
   for (auto &row : result)
   {
      try
      {
         row.get_int16(0);
      }
      catch (gsl::narrowing_error const &)
      {
         narrow_failed = true;
      }

      REQUIRE(row.get_uint16(0).value_or(0) == 0xffff);
   }

   REQUIRE(narrow_failed == true);
}
