#include "xdb_mysql_test.h"
#include <catch.hpp>
#include <iostream>
#include <thread>
#include <vector>

using namespace xdb;
using namespace std::string_literals;
using namespace std::string_view_literals;
using namespace std::chrono_literals;

TEST_CASE("all operations will finish on limited connection pool")
{
   auto pool = test_db::create_mysql_connection_pool({
       .initial_connections = 1,
       .maximum_connections = 1,
       .maximum_allowed_connections = 1,
       .maximum_allowed_connection_timeout_seconds = 10,
       .connection_revalidation_interval_seconds = 1,
   });

   std::atomic<int> fails = 0;

   std::atomic<int> value = 0;
   auto thread_fun = [&](std::stop_token const &token)
   {
      for (int i = 0; i < 10; i++)
      {
         try
         {
            std::this_thread::sleep_for(250ms);
            auto connection = pool.open();

            xdb::query q = connection.create_query("SELECT 1;");
            value += static_cast<int>(q.execute_for_int64().value_or(0));
         }
         catch (std::exception const &ex)
         {
            std::cout << ex.what() << std::endl;
            ++fails;
         }
      }
   };

   int expected = 0;
   {
      auto threads = std::vector<std::jthread>{};
      for (int i = 0; i != 10; ++i)
         threads.emplace_back(thread_fun);

      expected = static_cast<int>(threads.size()) * 10;
   }

   REQUIRE(fails == 0);
   REQUIRE(value == expected);

   REQUIRE(pool.size() == 1);
   REQUIRE(pool.active_connections() == 0);
}