#pragma once
#include <cstring>
#include <xdb/connection.h>
#include <xdb/connection_pool_provider.h>
#include <xdb/mysql/mysql_driver.h>
#include <xdb/sqlite/sqlite_driver.h>

namespace test_db
{

   inline xdb::connection open_mysql_connection()
   {
      xdb::connection::register_driver(std::make_unique<xdb::backend::mysql::driver>());
      xdb::connection db =
          xdb::connection::open("mysql:server=127.0.0.1;port=3306;uid=root;pwd=root;database=xdb_test");
      return db;
   }

   inline xdb::connection open_sqlite_memory_connection()
   {
      xdb::connection::register_driver(std::make_unique<xdb::backend::sqlite::driver>());
      xdb::connection db = xdb::connection::open("sqlite::memory:");
      return db;
   }

   inline xdb::connection_pool_provider create_mysql_connection_pool(xdb::pool_settings pool_settings)
   {
      xdb::connection::register_driver(std::make_unique<xdb::backend::mysql::driver>());

      return xdb::connection_pool_provider{"mysql:server=127.0.0.1;port=3306;uid=root;pwd=root;database=xdb_test",
                                           pool_settings};
   }

} // namespace test_db