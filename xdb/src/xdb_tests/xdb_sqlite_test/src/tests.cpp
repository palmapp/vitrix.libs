#include "xdb_mysql_test.h"
#include <catch.hpp>
#include <gsl/narrow>
#include <xdb/connection.h>

using namespace xdb;
using namespace std::string_literals;

TEST_CASE("Is database connection opened? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.is_connected() == true);
}

TEST_CASE("Is database connection closed? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   db.close();
   REQUIRE(db.is_connected() == false);
}

TEST_CASE("Can create table and execute statement? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)") == 0);
   REQUIRE(db.execute_query("INSERT INTO test (value) VALUES ('test')") == 1);
}

TEST_CASE("Can query column names? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)") == 0);

   query_result result = db.execute_query_for_result("SELECT * FROM test");
   REQUIRE(result.get_column_count() == 2);

   REQUIRE(result.get_column_name(0) == "key");
   REQUIRE(result.get_column_name(1) == "value");

   auto columns = result.get_column_names();
   REQUIRE(columns.size() == 2);
   REQUIRE(columns[0] == "key");
   REQUIRE(columns[1] == "value");
}

TEST_CASE("Can query result be empty? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)") == 0);

   query_result result = db.execute_query_for_result("SELECT * FROM test");

   int count = 0;
   for (auto &row : result)
   {
      static_cast<void>(row);

      ++count;
   }

   REQUIRE(count == 0);
}

TEST_CASE("Does query result contain inserted values? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)") == 0);
   REQUIRE(db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')") == 3);

   query_result result = db.execute_query_for_result("SELECT * FROM test");

   std::vector<std::pair<std::int64_t, std::string>> content;
   for (auto &row : result)
   {
      content.emplace_back(row.get_int64(0).value(), row.get_string(1).value());
   }

   REQUIRE(content.size() == 3);
   REQUIRE(content[0] == std::make_pair(std::int64_t(1), "test1"s));
   REQUIRE(content[1] == std::make_pair(std::int64_t(2), "test2"s));
   REQUIRE(content[2] == std::make_pair(std::int64_t(3), "test3"s));
}

TEST_CASE("Are transactions working correctly? sqlite", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)") == 0);

   SECTION("Is transaction commited at the end of scope?")
   {
      {
         transaction_guard transaction = db.begin_transaction();
         REQUIRE(db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')") == 3);
      }

      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 3ll);
   }

   SECTION("Is transaction rolled back when exception is thrown?")
   {
      try
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");

         throw 0;
      }
      catch (...) // variadic function; unlimited errors
      {
      }

      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(1) == 0ll);
   }

   SECTION("Is transaction rolled-back manually?")
   {
      connection test_db = test_db::open_sqlite_memory_connection();
      REQUIRE(test_db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)") == 0);

      {
         transaction_guard transaction = test_db.begin_transaction();
         test_db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         transaction.rollback();
      }

      auto query = test_db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(1) == 0ll);
   }

   SECTION("Is transaction committed manually?")
   {
      try
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         transaction.commit();
         throw 0;
      }
      catch (...)
      {
      }
      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 3ll);
   }

   SECTION("Is transaction committed in catch handler?")
   {
      try
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
         throw 0;
      }
      catch (...)
      {
         transaction_guard transaction = db.begin_transaction();
         db.execute_query("INSERT INTO test (value) VALUES ('test1'), ('test2'), ('test3')");
      }

      auto query = db.create_query("SELECT count(*) FROM test");
      REQUIRE(query.execute_for_int64().value_or(0) == 3ll);
   }
}

TEST_CASE("Is parameters working correctly? sqlite", "[sqlite]")
{
   struct TRow
   {
      int64_t int_value;
      double double_value;
      std::string text_value;
   };
   connection db = test_db::open_sqlite_memory_connection();
   REQUIRE(db.execute_query("CREATE TABLE test ( key INTEGER PRIMARY KEY AUTOINCREMENT,int_value INTEGER, double_value "
      "DOUBLE, text_value TEXT)") == 0);
   auto query2 = db.create_query(
      "INSERT INTO test (int_value,double_value, text_value ) VALUES (@firstParam, 1.0, 'test1'), (2, ?, 'test2'), "
      "(3, 3.0, ?)");

   query2.bind("@firstParam", {static_cast<int64_t>(1)});
   query2.bind(3, string_view{"test3"});
   query2.bind(2, 2.0);
   REQUIRE(query2.execute() == 3);
   std::vector<TRow> content;

   query_result result = db.execute_query_for_result("SELECT * FROM test");
   for (auto &row : result)
   {
      content.emplace_back(TRow{row.get_int64(1).value(), row.get_double(2).value(), row.get_string(3).value()});
   }
   REQUIRE(content.size() == 3);
   REQUIRE(content[0].int_value == 1);
   REQUIRE(content[1].double_value == 2);
   REQUIRE(content[2].text_value == "test3");
}

TEST_CASE("SQLITE: All supported types test store and retrieve limit values", "[sqlite]")
{
   connection db = test_db::open_sqlite_memory_connection();

   db.execute_query("DROP TABLE IF EXISTS test");
   db.execute_query(
      "CREATE TABLE test ("
      "value_text LONGTEXT NOT NULL,"                 // 1
      "value_varchar VARCHAR(255) NOT NULL,"          // 2
      "value_blob LONGBLOB NOT NULL,"                 // 3
      "value_tiny_int TINYINT NOT NULL,"              // 4 i8
      "value_small_int SMALLINT NOT NULL,"            // 5 i16
      "value_int INT NOT NULL,"                       // 6 i32
      "value_big_int BIGINT NOT NULL,"                // 7 i64
      "u_value_tiny_int TINYINT UNSIGNED NOT NULL,"   // 8 u8
      "u_value_small_int SMALLINT UNSIGNED NOT NULL," // 9 u16
      "u_value_int INT UNSIGNED NOT NULL,"            // 10 u32
      "u_value_big_int BIGINT UNSIGNED NOT NULL,"     // 11 u64
      "u_value_float FLOAT  NOT NULL,"                // 12 fl32
      "u_value_double DOUBLE  NOT NULL"               // 13 fl64
      ")");

   std::string big_value(1000000, '2');
   std::string big_value_zeros(1000000, '0');

   std::string small_value(0xff, '2');

   auto stmt = db.create_query("INSERT INTO test VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);");

   stmt.bind(1, big_value);
   stmt.bind(2, small_value);
   stmt.bind(3, big_value_zeros);

   stmt.bind(4, std::numeric_limits<i8>::max());
   stmt.bind(5, std::numeric_limits<i16>::max());
   stmt.bind(6, std::numeric_limits<i32>::max());
   stmt.bind(7, std::numeric_limits<i64>::max());

   stmt.bind(8, std::numeric_limits<u8>::max());
   stmt.bind(9, std::numeric_limits<u16>::max());
   stmt.bind(10, std::numeric_limits<u32>::max());

   bool u64_bind_failed = false;
   try
   {
      stmt.bind(11, std::numeric_limits<u64>::max());
   }
   catch (gsl::narrowing_error const &)
   {
      u64_bind_failed = true;
   }
   REQUIRE(u64_bind_failed == true);

   stmt.bind(11, u64{std::numeric_limits<i64>::max()});
   stmt.bind(12, 1000.33f);
   stmt.bind(13, 10000.0);

   REQUIRE(stmt.execute() == 1);

   stmt.bind(1, "");
   stmt.bind(2, "");
   stmt.bind(3, "\0");

   stmt.bind(4, std::numeric_limits<i8>::min());
   stmt.bind(5, std::numeric_limits<i16>::min());
   stmt.bind(6, std::numeric_limits<i32>::min());
   stmt.bind(7, std::numeric_limits<i64>::min());

   stmt.bind(8, std::numeric_limits<u8>::min());
   stmt.bind(9, std::numeric_limits<u16>::min());
   stmt.bind(10, std::numeric_limits<u32>::min());
   stmt.bind(11, std::numeric_limits<u64>::min());

   stmt.bind(12, 0.f);
   stmt.bind(13, 0.0);

   REQUIRE(stmt.execute() == 1);

   auto retrieve_stm = db.create_query("SELECT * FROM test ORDER BY value_tiny_int DESC");

   auto result = retrieve_stm.execute_for_result();
   int i = 0;

   for (auto &row : result)
   {
      auto text = row.get_string_view(0);
      auto varchar = row.get_string_view(1);
      auto blob = row.get_string_view(2);

      auto text_str = row.get_string(0);
      auto varchar_str = row.get_string(1);
      auto blob_str = row.get_string(2);

      auto int8 = row.get_int8(3);
      auto int16 = row.get_int16(4);
      auto int32 = row.get_int32(5);
      auto int64 = row.get_int64(6);

      auto uint8 = row.get_uint8(7);
      auto uint16 = row.get_uint16(8);
      auto uint32 = row.get_uint32(9);
      auto uint64 = row.get_uint64(10);

      auto floating_point_val = row.get_float(11);
      auto double_point_val = row.get_double(12);

      if (i == 0)
      {
         REQUIRE(text.value() == big_value);
         REQUIRE(varchar.value() == small_value);
         REQUIRE(blob.value() == big_value_zeros);
         REQUIRE(text_str.value() == big_value);
         REQUIRE(varchar_str.value() == small_value);
         REQUIRE(blob_str.value() == big_value_zeros);

         REQUIRE(int8.value() == std::numeric_limits<i8>::max());
         REQUIRE(int16.value() == std::numeric_limits<i16>::max());
         REQUIRE(int32.value() == std::numeric_limits<i32>::max());
         REQUIRE(int64.value() == std::numeric_limits<i64>::max());

         REQUIRE(uint8.value() == std::numeric_limits<u8>::max());
         REQUIRE(uint16.value() == std::numeric_limits<u16>::max());
         REQUIRE(uint32.value() == std::numeric_limits<u32>::max());
         // we can only get i64 max value in sqlite
         REQUIRE(static_cast<i64>(uint64.value()) == std::numeric_limits<i64>::max());

         REQUIRE(floating_point_val.value() == 1000.33f);
         REQUIRE(double_point_val.value() == 10000.0);
      }
      else
      {
         REQUIRE(text->empty() == true);
         REQUIRE(varchar->empty() == true);
         REQUIRE(blob->empty() == true);
         REQUIRE(text_str->empty() == true);
         REQUIRE(varchar_str->empty() == true);
         REQUIRE(blob_str->empty() == true);

         REQUIRE(int8.value() == std::numeric_limits<i8>::min());
         REQUIRE(int16.value() == std::numeric_limits<i16>::min());
         REQUIRE(int32.value() == std::numeric_limits<i32>::min());
         REQUIRE(int64.value() == std::numeric_limits<i64>::min());

         REQUIRE(uint8.value() == std::numeric_limits<u8>::min());
         REQUIRE(uint16.value() == std::numeric_limits<u16>::min());
         REQUIRE(uint32.value() == std::numeric_limits<u32>::min());
         REQUIRE(uint64.value() == std::numeric_limits<u64>::min());

         REQUIRE(floating_point_val.value() == .0f);
         REQUIRE(double_point_val.value() == 0.0);
      }

      ++i;
   }

   REQUIRE(i == 2);
}

TEST_CASE("SQLITE: All supported types are extracted as proper types values", "[sqlite]")
{
   /// this will work only in mysql / sqllite does not have proper type information available
   connection db = test_db::open_sqlite_memory_connection();

   db.execute_query("DROP TABLE IF EXISTS test");
   db.execute_query(
      "CREATE TABLE test ("
      "value_text LONGTEXT NOT NULL,"                 // 1
      "value_varchar VARCHAR(255) NOT NULL,"          // 2
      "value_blob LONGBLOB NOT NULL,"                 // 3
      "value_tiny_int TINYINT NOT NULL,"              // 4 i8
      "value_small_int SMALLINT NOT NULL,"            // 5 i16
      "value_int INT NOT NULL,"                       // 6 i32
      "value_big_int BIGINT NOT NULL,"                // 7 i64
      "u_value_tiny_int TINYINT UNSIGNED NOT NULL,"   // 8 u8
      "u_value_small_int SMALLINT UNSIGNED NOT NULL," // 9 u16
      "u_value_int INT UNSIGNED NOT NULL,"            // 10 u32
      "u_value_big_int BIGINT UNSIGNED NOT NULL,"     // 11 u64
      "u_value_float FLOAT  NOT NULL,"                // 12 fl32
      "u_value_double DOUBLE  NOT NULL"               // 13 fl64
      ")");

   std::string big_value(1000000, '2');
   std::string big_value_zeros(1000000, '0');

   std::string small_value(0xff, '2');

   auto stmt = db.create_query("INSERT INTO test VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);");

   stmt.bind(1, big_value);
   stmt.bind(2, small_value);
   stmt.bind(3, big_value_zeros);

   stmt.bind(4, std::numeric_limits<i8>::max());
   stmt.bind(5, std::numeric_limits<i16>::max());
   stmt.bind(6, std::numeric_limits<i32>::max());
   stmt.bind(7, std::numeric_limits<i64>::max());

   stmt.bind(8, std::numeric_limits<u8>::max());
   stmt.bind(9, std::numeric_limits<u16>::max());
   stmt.bind(10, std::numeric_limits<u32>::max());
   stmt.bind(11, u64{std::numeric_limits<i64>::max()});

   stmt.bind(12, 1000.33f);
   stmt.bind(13, 10000.0);

   REQUIRE(stmt.execute() == 1);

   auto retrieve_stm = db.create_query("SELECT * FROM test ORDER BY value_tiny_int DESC");

   auto result = retrieve_stm.execute_for_result();

   for (auto &row : result)
   {
      std::array<optional_field_value, 13> row_data;
      row.fill_row_values(row_data);

      REQUIRE(row_data[0].value().try_cast<string_view>() != nullptr);
      REQUIRE(row_data[1].value().try_cast<string_view>() != nullptr);
      REQUIRE(row_data[2].value().try_cast<string_view>() != nullptr);

      REQUIRE(row_data[3].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[4].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[5].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[6].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[7].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[8].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[9].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[10].value().try_cast<i64>() != nullptr);
      REQUIRE(row_data[11].value().try_cast<fl64>() != nullptr);
      REQUIRE(row_data[12].value().try_cast<fl64>() != nullptr);

      std::array<field_value, 13> row_data_no_nulls;
      row.fill_row_values(row_data_no_nulls);

      REQUIRE(row_data_no_nulls[0].try_cast<string_view>() != nullptr);
      REQUIRE(row_data_no_nulls[1].try_cast<string_view>() != nullptr);
      REQUIRE(row_data_no_nulls[2].try_cast<string_view>() != nullptr);
      REQUIRE(row_data_no_nulls[3].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[4].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[5].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[6].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[7].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[8].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[9].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[10].try_cast<i64>() != nullptr);
      REQUIRE(row_data_no_nulls[11].try_cast<fl64>() != nullptr);
      REQUIRE(row_data_no_nulls[12].try_cast<fl64>() != nullptr);
   }
}
