//
// Created by jhrub on 21.06.2021.
//
#include "xdb_mysql_test.h"
#include <catch.hpp> //doc on github
#include <iostream>
#include <xdb/data_stream.h>
#include <xdb/db_serializer.h>
using namespace xdb;
using namespace xdb::serialization;

struct test
{
   i64 id;
   string name;
   string value;
};

VISITABLE_STRUCT(test, id, name, value);

TEST_CASE("load table variants")
{
   connection db = test_db::open_sqlite_memory_connection();
   db.execute_query("CREATE TABLE test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, value TEXT)");

   {
      auto insert = db.create_query("INSERT INTO test (name, value) VALUES (?, ?)");

      insert.bind(1, "john");
      insert.bind(2, "some value");
      insert.execute();

      insert.bind(1, "tomas");
      insert.bind_null(2);
      insert.execute();
   }

   commons::data::data_query params;
   params.query = "name = ?";
   params.position_arguments = {{"john"}};

   SECTION("using load_table<test>")
   {
      auto table_vector = load_table<test>(db, params);
      REQUIRE(table_vector.size() == 1);
      REQUIRE(table_vector[0].name == "john");
      REQUIRE(table_vector[0].value == "some value");
   }

   SECTION("using create_select_query and untyped data_stream")
   {
      auto query = db.create_query(create_select_query("test", params));
      bind_position_arguments(query, params);

      data_stream stream(std::move(db), std::move(query));

      auto opt_columns = stream.get_column_names();
      REQUIRE(opt_columns.has_value() == true);
      auto columns = opt_columns.value();

      REQUIRE(columns.size() == 3);
      REQUIRE(columns[0] == "id");
      REQUIRE(columns[1] == "name");
      REQUIRE(columns[2] == "value");

      auto row = stream.next_row();
      REQUIRE(row.has_value() == true);

      auto row_values = row.value();
      REQUIRE(row_values[0].value() == field_value{i64{1}});
      REQUIRE(row_values[1].value() == field_value{"john"});
      REQUIRE(row_values[2].value() == field_value{"some value"});

      row = stream.next_row();
      REQUIRE(row.has_value() == false);
   }
}