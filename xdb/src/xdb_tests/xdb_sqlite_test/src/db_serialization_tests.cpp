#include <catch.hpp>

#include "xdb_mysql_test.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <xdb/db_serializer.h>

struct simple
{
   int i;
   bool b;
   std::string s;
};

VISITABLE_STRUCT(simple, i, b, s);

template <class EntityT>
void insert_row(xdb::connection &conn, EntityT val)
{
   auto stmt = xdb::serialization::get_insert_statement<EntityT>(conn);

   auto query = conn.create_query(stmt);
   xdb::serialization::bind_query<EntityT>(query, val);
   query.execute();
}

TEST_CASE("test db ops sqlite", "[db ser tests]")
{
   auto db = test_db::open_sqlite_memory_connection();

   SECTION("test insert stmt")
   {
      auto stmt = xdb::serialization::make_insert_statement<simple>(db);

      REQUIRE(stmt == "INSERT INTO simple(i,b,s) VALUES (?,?,?)");
   }

   SECTION("test replace stmt")
   {
      auto stmt = xdb::serialization::make_replace_statement<simple>(db);

      REQUIRE(stmt == "INSERT OR REPLACE INTO simple(i,b,s) VALUES (?,?,?)");
   }

   SECTION("test create stmt")
   {
      auto builder = db.get_builder(xdb::serialization::stm_type::create_table, "simple");

      builder.declare_int64_field("id");
      builder.add_constraint("id", "PRIMARY KEY");
      xdb::serialization::visit_default_value<simple>(builder);

      REQUIRE(builder.get_sql() ==
              "CREATE TABLE simple(id INTEGER PRIMARY KEY,i INTEGER NOT NULL,b INTEGER NOT NULL,s "
              "TEXT NOT NULL)");
   }

   SECTION("test bind query")
   {
      simple test{1, true, "a"};
      xdb::serialization::create_table<simple>(db);

      insert_row(db, test);

      auto count_query = db.create_query("SELECT COUNT(*) FROM simple;");
      auto count = count_query.execute_for_int64().value_or(0);
      REQUIRE(count == 1);
   }

   SECTION("test select bind query")
   {
      simple test{1, true, "a"};

      xdb::serialization::create_table<simple>(db);

      insert_row(db, test);

      auto query = db.create_query("SELECT i, b, s FROM simple WHERE i = ?");
      query.bind(1, (int64_t)test.i);

      auto res = query.execute_for_result();
      auto row = res.begin();

      simple to_verify = {static_cast<int>((*row).get_int64(0).value()), (*row).get_int64(1).value() != 0,
                          (*row).get_string(2).value()};

      REQUIRE(to_verify.i == test.i);
      REQUIRE(to_verify.b == test.b);
      REQUIRE(to_verify.s == test.s);
   }

   SECTION("test object serialization")
   {
      simple test{1, true, "a"};

      xdb::serialization::create_table<simple>(db);

      insert_row(db, test);

      auto query = db.create_query("SELECT i, b, s FROM simple WHERE i = ?");
      query.bind(1, (int64_t)test.i);

      auto res = query.execute_for_result();
      auto row = res.begin();

      simple to_verify;

      xdb::serialization::fill_entity<simple>(to_verify, *row);

      REQUIRE(to_verify.i == test.i);
      REQUIRE(to_verify.b == test.b);
      REQUIRE(to_verify.s == test.s);
   }

   SECTION("test simple select query")
   {
      auto res = xdb::serialization::create_select_query<simple>();

      REQUIRE(res == "SELECT i,b,s FROM simple;");
   }

   SECTION("test select query")
   {
      commons::data::data_query query{};
      query.limit = 10;

      query.query = "i > '10'";
      std::vector<commons::data::sort_column> sort{};
      sort.emplace_back(commons::data::sort_column{"i", true});
      sort.emplace_back(commons::data::sort_column{"s", false});

      query.order_by = sort;

      auto res = xdb::serialization::create_select_query<simple>(query);

      REQUIRE(res == "SELECT i,b,s FROM simple WHERE i > '10' ORDER BY i ASC, s DESC LIMIT 10;");
   }

   SECTION("test load table")
   {
      simple test{1, true, "a"};
      simple test_1{2, false, "b"};

      xdb::serialization::create_table<simple>(db);

      insert_row(db, test);
      insert_row(db, test_1);

      auto res = xdb::serialization::load_table<simple>(db);

      REQUIRE(res.size() == 2);

      REQUIRE(res[0].i == test.i);
      REQUIRE(res[0].b == test.b);
      REQUIRE(res[0].s == test.s);

      REQUIRE(res[1].i == test_1.i);
      REQUIRE(res[1].b == test_1.b);
      REQUIRE(res[1].s == test_1.s);
   }

   SECTION("test multiple constraints")
   {
      auto builder = db.get_builder(xdb::serialization::stm_type::create_table, "simple");

      builder.declare_int64_field("id");
      builder.add_constraint("id", "PRIMARY KEY");
      xdb::serialization::visit_default_value<simple>(builder);
      builder.add_constraint("i", "UNIQUE NOT NULL");

      builder.add_constraint("FOREIGN KEY (i) REFERENCES other1 (id) ON DELETE SET NULL");
      builder.add_constraint("FOREIGN KEY (i) REFERENCES other2 (id) ON DELETE CASCADE");

      REQUIRE(builder.get_sql() ==
              "CREATE TABLE simple(id INTEGER PRIMARY KEY,i INTEGER UNIQUE NOT NULL,b INTEGER NOT "
              "NULL,s TEXT NOT NULL,FOREIGN KEY (i) REFERENCES other1 (id) ON DELETE SET "
              "NULL,FOREIGN KEY (i) REFERENCES other2 (id) ON DELETE CASCADE)");
   }
}

TEST_CASE("test db ops mysql", "[db ser tests]")
{
   auto db = test_db::open_mysql_connection();
   db.execute_query("DROP TABLE IF EXISTS simple;");

   SECTION("test insert stmt")
   {
      auto stmt = xdb::serialization::make_insert_statement<simple>(db);

      REQUIRE(stmt == "INSERT INTO simple(`i`,`b`,`s`) VALUES (?,?,?)");
   }

   SECTION("test replace stmt")
   {
      auto stmt = xdb::serialization::make_replace_statement<simple>(db);

      REQUIRE(stmt == "REPLACE INTO simple(`i`,`b`,`s`) VALUES (?,?,?)");
   }

   SECTION("test create stmt")
   {
      auto builder = db.get_builder(xdb::serialization::stm_type::create_table, "simple");

      builder.declare_int64_field("id");
      builder.add_constraint("id", "PRIMARY KEY");
      xdb::serialization::visit_default_value<simple>(builder);

      REQUIRE(builder.get_sql() ==
              "CREATE TABLE simple(`id` BIGINT PRIMARY KEY,`i` INT NOT NULL,`b` TINYINT NOT NULL,`s` "
              "TEXT NOT NULL)");
   }

   SECTION("test bind query")
   {
      simple test{1, true, "a"};
      xdb::serialization::create_table<simple>(db);

      insert_row(db, test);

      auto count_query = db.create_query("SELECT COUNT(*) FROM simple;");
      auto count = count_query.execute_for_int64().value_or(0);
      REQUIRE(count == 1);
   }

   SECTION("test select bind query")
   {
      simple test{1, true, "a"};

      xdb::serialization::create_table<simple>(db);

      insert_row(db, test);

      auto query = db.create_query("SELECT i, b, s FROM simple WHERE i = ?");
      query.bind(1, (int64_t)test.i);

      auto res = query.execute_for_result();
      auto row = res.begin();

      simple to_verify = {static_cast<int>((*row).get_int64(0).value()), (*row).get_int64(1).value() != 0,
                          (*row).get_string(2).value()};

      REQUIRE(to_verify.i == test.i);
      REQUIRE(to_verify.b == test.b);
      REQUIRE(to_verify.s == test.s);
   }

   SECTION("test multiple constraints")
   {
      auto builder = db.get_builder(xdb::serialization::stm_type::create_table, "simple");

      builder.declare_int64_field("id");
      builder.add_constraint("id", "PRIMARY KEY");
      xdb::serialization::visit_default_value<simple>(builder);
      builder.add_constraint("i", "UNIQUE NOT NULL");

      builder.add_constraint("FOREIGN KEY (i) REFERENCES other1 (id) ON DELETE SET NULL");
      builder.add_constraint("FOREIGN KEY (i) REFERENCES other2 (id) ON DELETE CASCADE");

      REQUIRE(builder.get_sql() ==
              "CREATE TABLE simple(`id` BIGINT PRIMARY KEY,`i` INT UNIQUE NOT NULL,`b` TINYINT NOT "
              "NULL,`s` TEXT NOT NULL,FOREIGN KEY (i) REFERENCES other1 (id) ON DELETE SET "
              "NULL,FOREIGN KEY (i) REFERENCES other2 (id) ON DELETE CASCADE)");
   }
}