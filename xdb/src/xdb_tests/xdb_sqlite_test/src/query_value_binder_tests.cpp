//
// Created by jhrub on 30.05.2021.
//
#include "xdb_mysql_test.h"
#include <catch.hpp>
#include <xdb/db_serializer.h>

using namespace xdb;

struct my_xdb_ready_struct
{
   xdb::i8 a;
   xdb::i16 b;
   xdb::i32 c;
   xdb::i64 d;
};
VISITABLE_STRUCT(my_xdb_ready_struct, a, b, c, d);

TEST_CASE("test query_binder", "[sqlite][query_binder]")
{
   auto db = test_db::open_sqlite_memory_connection();

   SECTION("test insert stmt")
   {
      auto stmt = serialization::get_insert_statement<my_xdb_ready_struct>(db);

      REQUIRE(stmt == "INSERT INTO my_xdb_ready_struct(a,b,c,d) VALUES (?,?,?,?)");
   }
}