set(target xdb_tests)

add_executable(${target}
        xdb_mysql_test/src/experiment_test.cpp
        xdb_mysql_test/src/xdb_mysql_test.cpp
        xdb_mysql_test/src/xdb_mysql_bind_test.cpp
        xdb_sqlite_test/src/tests.cpp
        xdb_sqlite_test/src/db_serialization_tests.cpp
        src/utils_test.cpp
        xdb_sqlite_test/src/query_value_binder_tests.cpp xdb_sqlite_test/src/data_stream_tests.cpp xdb_mysql_test/src/xdb_mysql_connection_pool_test.cpp)

target_include_directories(${target} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/xdb_mysql_test/include ${VCPKG_INCLUDE_DIR})
target_link_libraries(${target} PUBLIC xdb xdb_sqlite xdb_mysql Catch2::Catch2WithMain logging)
