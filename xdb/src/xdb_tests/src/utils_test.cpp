#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "xdb_mysql_test.h"
#include <xdb/db_serializer.h>
#include <xdb/mysql/mysql_driver.h>

#include <easylogging++.h>
INITIALIZE_EASYLOGGINGPP
struct simple
{
   int i;
   bool b;
   std::string s;
};

struct other
{
   int j;
};

VISITABLE_STRUCT(simple, i, b, s);
VISITABLE_STRUCT(other, j);

TEST_CASE("[utils test]", "[db utils]")
{
   xdb::connection::register_driver(std::make_unique<xdb::backend::mysql::driver>());
   auto conn = test_db::open_sqlite_memory_connection();

   auto res = xdb::serialization::prepare_create_stmt<simple>(conn);

   SECTION("prepare create stmt")
   {
      REQUIRE(res.get_sql() == "CREATE TABLE simple(i INTEGER NOT NULL,b INTEGER NOT NULL,s TEXT NOT NULL)");
   }

   SECTION("primary constraint")
   {
      xdb::serialization::add_primary_constraint(res, "i");

      REQUIRE(res.get_sql() == "CREATE TABLE simple(i INTEGER PRIMARY KEY,b INTEGER NOT NULL,s TEXT NOT NULL)");
   }

   SECTION("unique constraint")
   {
      xdb::serialization::add_unique_constraint(res, "i, b");

      REQUIRE(res.get_sql() ==
              "CREATE TABLE simple(i INTEGER NOT NULL,b INTEGER NOT NULL,s TEXT NOT NULL,UNIQUE(i, "
              "b) ON CONFLICT REPLACE)");
   }

   SECTION("unique constraint")
   {
      xdb::serialization::add_foreign_key<other>(res, "i", "j", "RESTRICT", "RESTRICT");

      REQUIRE(res.get_sql() ==
              "CREATE TABLE simple(i INTEGER NOT NULL,b INTEGER NOT NULL,s TEXT NOT NULL,FOREIGN "
              "KEY (i) REFERENCES other(j) ON DELETE RESTRICT ON UPDATE RESTRICT)");
   }
}