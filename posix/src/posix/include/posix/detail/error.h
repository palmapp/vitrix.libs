#pragma once

#include "types.h"
#include <stdexcept>
#include <string>

namespace posix
{
   std::string get_errno_string();

   class error : public std::exception
   {
    public:
      error(string_view operation, handle_t handle = invalid_handle);
      error(std::string what);

      char const *what() const noexcept final;

    private:
      std::string what_;
   };
} // namespace posix