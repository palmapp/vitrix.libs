#pragma once

#include "file_handle.h"
#include <vector>

namespace posix
{
   class buffered_file_writer
   {
    public:
      buffered_file_writer(std::uint32_t capacity = 0xffff);
      ~buffered_file_writer() noexcept;
      void open(path file);
      void close();

      void write(string_view data);

    private:
      void flush();
      std::uint32_t buffered_ = 0;
      std::vector<char> buffer_;
      file_handle output_file_;
   };
} // namespace posix