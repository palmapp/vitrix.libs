#pragma once
#include "types.h"
namespace posix
{
   handle_t open(path const &path, open_mode mode) noexcept;
   int close(handle_t handle) noexcept;
   std::int64_t write(handle_t handle, char const *data, std::uint32_t len) noexcept;
} // namespace posix