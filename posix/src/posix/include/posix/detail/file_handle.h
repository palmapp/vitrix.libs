#pragma once

#include "types.h"

namespace posix
{
   class file_handle
   {
    public:
      static file_handle open(path const &file_path, open_mode mode);
      file_handle(posix::handle_t handle = invalid_handle);

      file_handle(file_handle &&);
      file_handle &operator=(file_handle &&);

      ~file_handle() noexcept;

      file_handle(file_handle const &) = delete;
      file_handle &operator=(file_handle const &) = delete;

      bool is_valid() const noexcept;
      void close() noexcept;
      void write_all(char const *data, std::uint32_t len);
      void write_all(string_view data);

      posix::handle_t get_handle() const noexcept;
      void swap(file_handle &other) noexcept;
      posix::handle_t release_handle() noexcept;

    private:
      posix::handle_t handle_;
   };
} // namespace posix