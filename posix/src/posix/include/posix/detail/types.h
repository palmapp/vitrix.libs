#pragma once

#include <cstdint>
#include <filesystem>
#include <string_view>

namespace posix
{
   using handle_t = int;
   constexpr handle_t invalid_handle = -1;

   enum class open_mode : std::uint32_t
   {
      read_only,
      write_only,
      read_write,
      append = 0x0008,
      create = 0x0010,
      truncate = 0x0020,
      exclude = 0x0040,

      text = 0x4000,
      binary = 0x8000,

      write_only_binary_create_or_truncate = write_only | binary | create | truncate
   };

   inline open_mode operator|(open_mode lhs, open_mode rhs)
   {
      return static_cast<open_mode>(static_cast<std::uint32_t>(lhs) | static_cast<std::uint32_t>(rhs));
   }

   using path = std::filesystem::path;
   using string_view = std::string_view;
} // namespace posix
