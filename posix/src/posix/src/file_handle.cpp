#include "posix.h"
#include <easylogging++.h>
#include <gsl/narrow>

posix::file_handle posix::file_handle::open(path const &file_path, open_mode mode)
{
   return {posix::open(file_path, mode)};
}

posix::file_handle::file_handle(posix::handle_t handle) : handle_(handle) {}

posix::file_handle::file_handle(file_handle &&other) : handle_(other.handle_)
{
   other.handle_ = invalid_handle;
}
posix::file_handle &posix::file_handle::operator=(posix::file_handle &&other)
{
   handle_ = other.handle_;
   other.handle_ = -1;
   return *this;
}

posix::file_handle::~file_handle() noexcept
{
   close();
}

bool posix::file_handle::is_valid() const noexcept
{
   return handle_ != invalid_handle;
}

void posix::file_handle::close() noexcept
{
   if (handle_ != invalid_handle)
   {
      const int result = posix::close(handle_);
      if (result < 0)
      {
         LOG(ERROR) << "unable to close file handle [error ignored] " << get_errno_string();
      }

      handle_ = invalid_handle;
   }
}

void posix::file_handle::write_all(char const *data, std::uint32_t len)
{
   while (len > 0)
   {
      auto result = posix::write(handle_, data, len);
      if (result < 0)
      {
         throw error("posix::write", handle_);
      }

      data += result;
      len -= gsl::narrow_cast<std::uint32_t>(result);
   }
}

void posix::file_handle::write_all(string_view data)
{
   write_all(data.data(), gsl::narrow<std::uint32_t>(data.size()));
}

posix::handle_t posix::file_handle::get_handle() const noexcept
{
   return handle_;
}

void posix::file_handle::swap(file_handle &other) noexcept
{
   const posix::handle_t mine = handle_;
   handle_ = other.handle_;
   other.handle_ = mine;
}

posix::handle_t posix::file_handle::release_handle() noexcept
{
   const posix::handle_t mine = handle_;
   handle_ = invalid_handle;

   return mine;
}
