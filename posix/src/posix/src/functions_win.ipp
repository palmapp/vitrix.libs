#include <direct.h> /* for _getcwd() and _chdir() */
#include <fcntl.h>
#include <io.h>
#include <process.h> /* for getpid() and the exec..() family */
#include <stdlib.h>

#include "detail/functions.h"

posix::handle_t posix::open(std::filesystem::path const &path, open_mode mode) noexcept
{
   return ::_wopen(path.c_str(), static_cast<int>(mode), _S_IREAD | _S_IWRITE);
}
int posix::close(posix::handle_t handle) noexcept
{
   return ::_close(handle);
}
std::int64_t posix::write(posix::handle_t handle, char const *data, std::uint32_t len) noexcept
{
   return ::_write(handle, reinterpret_cast<void const *>(data), len);
}