#include "detail/error.h"
#include <commons/make_string.h>
#include <string.h>

std::string posix::get_errno_string()
{
   return {strerror(errno)};
}
using namespace posix;

error::error(string_view operation, handle_t)
{
   what_ = commons::make_string("operation : ", operation, " failed due : ", get_errno_string());
}

error::error(std::string what) : what_(std::move(what)) {}

char const *error::what() const noexcept
{
   return what_.c_str();
}