#include "detail/functions.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

namespace
{
   inline void to_os_mode(int &output, posix::open_mode mode, posix::open_mode test, int ouput_value)
   {
      auto value = static_cast<int>(mode);
      auto test_value = static_cast<int>(test);
      if ((value & test_value) != 0)
      {
         output |= ouput_value;
      }
   }
} // namespace

posix::handle_t posix::open(path const &path, open_mode mode) noexcept
{
   int os_mode = 0;
   to_os_mode(os_mode, mode, open_mode::write_only, O_WRONLY);
   to_os_mode(os_mode, mode, open_mode::read_write, O_RDWR);

   to_os_mode(os_mode, mode, open_mode::truncate, O_TRUNC);
   to_os_mode(os_mode, mode, open_mode::create, O_CREAT);
   to_os_mode(os_mode, mode, open_mode::exclude, O_EXCL);

   return ::open(path.c_str(), os_mode, S_IRUSR | S_IWUSR);
}
int posix::close(handle_t handle) noexcept
{
   return ::close(handle);
}
std::int64_t posix::write(handle_t handle, char const *data, std::uint32_t len) noexcept
{
   return ::write(handle, reinterpret_cast<void const *>(data), len);
}