#include "detail/buffered_file_writer.h"
#include <algorithm>
#include <commons/run_safe.h>
#include <gsl/narrow>

using namespace posix;

buffered_file_writer::buffered_file_writer(std::uint32_t capacity)
{
   buffer_.resize(capacity);
}

buffered_file_writer::~buffered_file_writer() noexcept
{
   commons::run_safe([this]() { close(); }, "~buffered_file_writer");
}

void buffered_file_writer::open(path file)
{
   close();
   output_file_ = file_handle::open(file, open_mode::write_only_binary_create_or_truncate);
}
void buffered_file_writer::close()
{
   flush();
   output_file_.close();
}

void buffered_file_writer::write(string_view data)
{
   while (data.size() > 0)
   {
      auto to_copy = std::min(buffer_.size() - buffered_, data.size());
      if (to_copy == 0)
      {
         flush();
      }
      else
      {
         std::copy(data.begin(), data.begin() + to_copy, buffer_.begin() + buffered_);

         buffered_ += gsl::narrow_cast<std::uint32_t>(to_copy);
         data = data.substr(to_copy);
      }
   }
}

void buffered_file_writer::flush()
{
   output_file_.write_all(buffer_.data(), buffered_);
   buffered_ = 0;
}