#include <commons/file_system.h>
#include <posix/posix.h>

#include <easylogging++.h>
INITIALIZE_EASYLOGGINGPP

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

using namespace posix;

TEST_CASE("test write different size of buffers", "[binary_output_file]")
{
   buffered_file_writer out(5);
   out.open("test.txt");
   out.write("vitrixisthebest");
   out.close();

   out.open("test1.txt");
   out.write("itisreal");
   out.close();

   REQUIRE(commons::read_file("test.txt") == "vitrixisthebest");
   REQUIRE(commons::read_file("test1.txt") == "itisreal");
}