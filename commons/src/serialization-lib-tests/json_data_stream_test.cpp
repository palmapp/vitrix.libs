#include <catch.hpp>
#include <commons/data/data_stream.h>
// clang-format off
#include <serialization/json/support_data_stream.h>
#include <serialization/json.h>
// clang-format on
using namespace commons::basic_types;
namespace ds = spider::serialization::json_data_stream;
namespace json = spider::serialization::json;

struct test_data_stream : public commons::data::data_stream
{
   test_data_stream(vector<string_view> _columns, vector<optional_field_value> _rows)
       : columns(std::move(_columns)), rows(std::move(_rows))
   {
   }

   optional<array_view<string_view>> get_column_names() override
   {
      return array_view<string_view>{columns.data(), columns.size()};
   }

   optional<commons::data::data_row> next_row() override
   {
      auto row_begin = (row++) * columns.size();
      if (row_begin < rows.size())
      {
         return commons::data::data_row{rows.data() + row_begin, columns.size()};
      }

      return optional<commons::data::data_row>();
   }

   void reset()
   {
      row = 0;
   }
   vector<string_view> columns;
   vector<optional_field_value> rows;
   int row = 0;
};

template <class Num>
using numeric_limits = std::numeric_limits<Num>;

TEST_CASE("is data stream serialized correctly")
{
   test_data_stream stream{
       vector<string_view>{"bool", "i8", "i16", "i32", "i64", "u8", "u16", "u32", "u64", "fl32", "fl64", "string_view"},
       vector<optional_field_value>{
           field_value{true},
           field_value{numeric_limits<i8>::max()},
           field_value{numeric_limits<i16>::max()},
           field_value{numeric_limits<i32>::max()},
           field_value{numeric_limits<i64>::max()},
           field_value{numeric_limits<u8>::max()},
           field_value{numeric_limits<u16>::max()},
           field_value{numeric_limits<u32>::max()},
           field_value{numeric_limits<u64>::max()},
           field_value{numeric_limits<fl32>::max()},
           field_value{numeric_limits<fl64>::max()},
           field_value{"max values"},

           field_value{false},
           field_value{numeric_limits<i8>::min()},
           field_value{numeric_limits<i16>::min()},
           field_value{numeric_limits<i32>::min()},
           field_value{numeric_limits<i64>::min()},
           field_value{numeric_limits<u8>::min()},
           field_value{numeric_limits<u16>::min()},
           field_value{numeric_limits<u32>::min()},
           field_value{numeric_limits<u64>::min()},
           field_value{numeric_limits<fl32>::min()},
           field_value{numeric_limits<fl64>::min()},
           field_value{"min values"},
       }};

   SECTION("json as objects")
   {

      fmt::memory_buffer string_buf;
      json::json_buffer buffer(string_buf);
      buffer.begin_array();
      auto written_size = ds::write_data_stream_as_objects(buffer, stream, numeric_limits<std::size_t>::max());
      buffer.end_array();

      REQUIRE(written_size == 2);
      auto result = fmt::to_string(string_buf);

      string expected_string =
          "[{\"bool\":1,\"i8\":127,\"i16\":32767,\"i32\":2147483647,\"i64\":9223372036854775807,\"u8\":255,\"u16\":"
          "65535,\"u32\":4294967295,\"u64\":18446744073709551615,\"fl32\":3.4028235e+38,\"fl64\":1.7976931348623157e+"
          "308,\"string_view\":\"max "
          "values\"},{\"bool\":0,\"i8\":-128,\"i16\":-32768,\"i32\":-2147483648,\"i64\":-9223372036854775808,\"u8\":0,"
          "\"u16\":0,\"u32\":0,\"u64\":0,\"fl32\":1.1754944e-38,\"fl64\":2.2250738585072014e-308,\"string_view\":\"min "
          "values\"}]";

      REQUIRE(result == expected_string);
   }

   SECTION("json as array")
   {

      fmt::memory_buffer string_buf;
      json::json_buffer buffer(string_buf);

      buffer.begin_array();
      auto written_size = ds::write_data_stream_as_array(buffer, stream, numeric_limits<std::size_t>::max());
      buffer.end_array();

      REQUIRE(written_size == 2);
      auto result = fmt::to_string(string_buf);

      REQUIRE(result ==
              "[[1,127,32767,2147483647,9223372036854775807,255,65535,4294967295,18446744073709551615,3.4028235e+38,1."
              "7976931348623157e+308,\"max "
              "values\"],[0,-128,-32768,-2147483648,-9223372036854775808,0,0,0,0,1.1754944e-38,2.2250738585072014e-308,"
              "\"min values\"]]");
   }
}

TEST_CASE("field value limits")
{

   SECTION("i64 max/min")
   {
      field_value max{numeric_limits<i64>::max()};
      auto json = spider::serialization::json::stringify_json(max);
      REQUIRE(!json.empty());

      field_value parsed{};
      auto parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "i64");
      REQUIRE(parsed == max);

      field_value min{numeric_limits<i64>::min()};
      json = spider::serialization::json::stringify_json(min);
      REQUIRE(!json.empty());

      parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "i64");
      REQUIRE(parsed == min);
   }

   SECTION("u64 max/min")
   {
      field_value max{numeric_limits<u64>::max()};
      auto json = spider::serialization::json::stringify_json(max);
      REQUIRE(!json.empty());

      field_value parsed{};
      auto parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "u64");
      REQUIRE(parsed == max);

      field_value min{numeric_limits<u64>::min()};
      json = spider::serialization::json::stringify_json(min);
      REQUIRE(!json.empty());

      parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "u64");
      REQUIRE(parsed == min);

      field_value half{numeric_limits<u64>::max() / 2};
      json = spider::serialization::json::stringify_json(half);
      REQUIRE(!json.empty());

      parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "u64");
      REQUIRE(parsed == half);
   }

   SECTION("u64 max/min")
   {
      field_value max{numeric_limits<u64>::max()};
      auto json = spider::serialization::json::stringify_json(max);
      REQUIRE(!json.empty());

      field_value parsed{};
      auto parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "u64");
      REQUIRE(parsed == max);

      field_value min{numeric_limits<u64>::min()};
      json = spider::serialization::json::stringify_json(min);
      REQUIRE(!json.empty());

      parse_result = spider::serialization::json::parse_json(parsed, json);
      REQUIRE(parse_result == true);

      REQUIRE(field_value_type_to_string(parsed.index()) == "u64");
      REQUIRE(parsed == min);
   }
}