#include <catch.hpp>
#include <serialization/binary.h>
#include <serialization/json.h>

using namespace spider::serialization::binary::types;
using namespace spider::serialization::binary;

struct other_nested_struct
{
   u8 cool_field = 0;
};

struct binary_like_structure
{
   u8 u8_field = 0xff;
   s8 s8_field = static_cast<s8>(0xee);

   u16_be u16_be_field = 0xaabb;
   u16_le u16_le_field = 0xccdd;
   s16_be s16_be_field = static_cast<std::int16_t>(0xeeff);
   s16_le s16_le_field = 0x1122;

   u32_be u32_be_field = 0x1122aabb;
   u32_le u32_le_field = 0x3344ccdd;
   s32_be s32_be_field = 0x5566eeff;
   s32_le s32_le_field = 0x1234abcd;

   u64_be u64_be_field = 0x1122aabb1122aabb;
   u64_le u64_le_field = 0x3344ccdd3344ccdd;
   s64_be s64_be_field = 0x5566eeff5566eeff;
   s64_le s64_le_field = 0x1234abcd1234abcd;

   other_nested_struct nested{};

   fixed_array<u32_le, 10> fixed_values{};
   prefixed_array<u8, u32_le> prefixed_values{};

   fixed_array<other_nested_struct, 10> fixed_nested_value{};
   prefixed_array<u8, other_nested_struct> prefixed_nested_values{};
};

VISITABLE_STRUCT(other_nested_struct, cool_field);
VISITABLE_STRUCT(binary_like_structure, u8_field, s8_field, u16_be_field, u16_le_field, s16_be_field, s16_le_field,
                 u32_be_field, u32_le_field, s32_be_field, s32_le_field, u64_be_field, u64_le_field, s64_be_field,
                 s64_le_field, nested, fixed_values, prefixed_values, fixed_nested_value, prefixed_nested_values);

TEST_CASE("test json serialization", "[binary] [json]")
{
   binary_like_structure msg{};
   msg.prefixed_nested_values.values.push_back({10});
   msg.prefixed_values.values.push_back(100);

   auto result = spider::serialization::json::stringify_json(msg);
   REQUIRE(result.size() > 0);

   binary_like_structure parsed{};
   bool ok = spider::serialization::json::parse_json(parsed, result);

   REQUIRE(ok == true);
   REQUIRE(parsed.prefixed_values.values.size() == 1);
   REQUIRE(parsed.prefixed_nested_values.values.size() == 1);

   REQUIRE(parsed.prefixed_values.values[0].value == 100);
   REQUIRE(parsed.prefixed_nested_values.values[0].cool_field == 10);
}

const char name[] = "name";

struct struct_with_partial_byte
{
   u8 u8_before_field = 0;
   partial_byte<bit_range<0, name, 2>, bit_range<3, name, 7>> partial{};
   u8 u8_after_field = 0;
};

VISITABLE_STRUCT(struct_with_partial_byte, u8_before_field, partial, u8_after_field);

TEST_CASE("partial byte json", "[binary] [json]")
{
   struct_with_partial_byte data{};

   SECTION("BASIC")
   {
      data.u8_before_field = 0xff;
      data.partial.values[0] = 0x7;
      data.partial.values[1] = 0x1f;
      data.u8_after_field = 0xff;

      auto res = spider::serialization::json::stringify_json(data);

      REQUIRE(res.length() > 0);

      struct_with_partial_byte parsed{};

      auto ok = spider::serialization::json::parse_json(parsed, res);

      REQUIRE(ok);

      REQUIRE(parsed.u8_before_field == 0xff);
      REQUIRE(parsed.u8_after_field == 0xff);

      REQUIRE(parsed.partial.values.size() == 2);
      REQUIRE(parsed.partial.values[0] == 0x7);
      REQUIRE(parsed.partial.values[1] == 0x1f);
   }
}