#include <catch.hpp>
#include <serialization/binary.h>

using namespace spider::serialization::binary::types;
using namespace spider::serialization::binary;

struct supported_binary_types
{
   u8 u8_field;
   s8 s8_field;

   u16_be u16_be_field;
   u16_le u16_le_field;
   s16_be s16_be_field;
   s16_le s16_le_field;

   u32_be u32_be_field;
   u32_le u32_le_field;
   s32_be s32_be_field;
   s32_le s32_le_field;

   u64_be u64_be_field;
   u64_le u64_le_field;
   s64_be s64_be_field;
   s64_le s64_le_field;
};

struct init_supported_binary_types : supported_binary_types
{
   init_supported_binary_types()
   {
      u8_field = 0xff;
      s8_field = static_cast<s8>(0xee);

      u16_be_field = 0xaabb;
      u16_le_field = 0xccdd;
      s16_be_field = static_cast<std::int16_t>(0xeeff);
      s16_le_field = 0x1122;

      u32_be_field = 0x1122aabb;
      u32_le_field = 0x3344ccdd;
      s32_be_field = 0x5566eeff;
      s32_le_field = 0x1234abcd;

      u64_be_field = 0x1122aabb1122aabb;
      u64_le_field = 0x3344ccdd3344ccdd;
      s64_be_field = 0x5566eeff5566eeff;
      s64_le_field = 0x1234abcd1234abcd;
   }
};

VISITABLE_STRUCT(supported_binary_types, u8_field, s8_field, u16_be_field, u16_le_field, s16_be_field, s16_le_field,
                 u32_be_field, u32_le_field, s32_be_field, s32_le_field, u64_be_field, u64_le_field, s64_be_field,
                 s64_le_field);

VISITABLE_STRUCT(init_supported_binary_types, u8_field, s8_field, u16_be_field, u16_le_field, s16_be_field,
                 s16_le_field, u32_be_field, u32_le_field, s32_be_field, s32_le_field, u64_be_field, u64_le_field,
                 s64_be_field, s64_le_field);

void REQUIRE_BUFFER(const std::string &buffer, std::size_t idx, u8 expected_value)
{
   u8 value = buffer[idx];
   REQUIRE(value == expected_value);
}

TEST_CASE("test binary serialization", "[binary]")
{
   init_supported_binary_types instance{};

   auto buffer = to_string(instance);
   REQUIRE(buffer.size() == 2 * 1 + 4 * 2 + 4 * 4 + 4 * 8);

   // u8
   REQUIRE_BUFFER(buffer, 0, 0xff);
   // s8
   REQUIRE_BUFFER(buffer, 1, 0xee);

   // u16_be
   REQUIRE_BUFFER(buffer, 2, 0xaa);
   REQUIRE_BUFFER(buffer, 3, 0xbb);

   // u16_le
   REQUIRE_BUFFER(buffer, 4, 0xdd);
   REQUIRE_BUFFER(buffer, 5, 0xcc);

   // s16_be
   REQUIRE_BUFFER(buffer, 6, 0xee);
   REQUIRE_BUFFER(buffer, 7, 0xff);

   // s16_le
   REQUIRE_BUFFER(buffer, 8, 0x22);
   REQUIRE_BUFFER(buffer, 9, 0x11);

   // u32_be
   REQUIRE_BUFFER(buffer, 10, 0x11);
   REQUIRE_BUFFER(buffer, 11, 0x22);
   REQUIRE_BUFFER(buffer, 12, 0xaa);
   REQUIRE_BUFFER(buffer, 13, 0xbb);

   // u32_le
   REQUIRE_BUFFER(buffer, 14, 0xdd);
   REQUIRE_BUFFER(buffer, 15, 0xcc);
   REQUIRE_BUFFER(buffer, 16, 0x44);
   REQUIRE_BUFFER(buffer, 17, 0x33);

   // s32_be
   REQUIRE_BUFFER(buffer, 18, 0x55);
   REQUIRE_BUFFER(buffer, 19, 0x66);
   REQUIRE_BUFFER(buffer, 20, 0xee);
   REQUIRE_BUFFER(buffer, 21, 0xff);

   // s32_le
   REQUIRE_BUFFER(buffer, 22, 0xcd);
   REQUIRE_BUFFER(buffer, 23, 0xab);
   REQUIRE_BUFFER(buffer, 24, 0x34);
   REQUIRE_BUFFER(buffer, 25, 0x12);

   // u64_be
   REQUIRE_BUFFER(buffer, 26, 0x11);
   REQUIRE_BUFFER(buffer, 27, 0x22);
   REQUIRE_BUFFER(buffer, 28, 0xaa);
   REQUIRE_BUFFER(buffer, 29, 0xbb);
   REQUIRE_BUFFER(buffer, 30, 0x11);
   REQUIRE_BUFFER(buffer, 31, 0x22);
   REQUIRE_BUFFER(buffer, 32, 0xaa);
   REQUIRE_BUFFER(buffer, 33, 0xbb);

   // u64_le
   REQUIRE_BUFFER(buffer, 34, 0xdd);
   REQUIRE_BUFFER(buffer, 35, 0xcc);
   REQUIRE_BUFFER(buffer, 36, 0x44);
   REQUIRE_BUFFER(buffer, 37, 0x33);
   REQUIRE_BUFFER(buffer, 38, 0xdd);
   REQUIRE_BUFFER(buffer, 39, 0xcc);
   REQUIRE_BUFFER(buffer, 40, 0x44);
   REQUIRE_BUFFER(buffer, 41, 0x33);

   // s64_be
   REQUIRE_BUFFER(buffer, 42, 0x55);
   REQUIRE_BUFFER(buffer, 43, 0x66);
   REQUIRE_BUFFER(buffer, 44, 0xee);
   REQUIRE_BUFFER(buffer, 45, 0xff);
   REQUIRE_BUFFER(buffer, 46, 0x55);
   REQUIRE_BUFFER(buffer, 47, 0x66);
   REQUIRE_BUFFER(buffer, 48, 0xee);
   REQUIRE_BUFFER(buffer, 49, 0xff);

   // s64_le
   REQUIRE_BUFFER(buffer, 50, 0xcd);
   REQUIRE_BUFFER(buffer, 51, 0xab);
   REQUIRE_BUFFER(buffer, 52, 0x34);
   REQUIRE_BUFFER(buffer, 53, 0x12);
   REQUIRE_BUFFER(buffer, 54, 0xcd);
   REQUIRE_BUFFER(buffer, 55, 0xab);
   REQUIRE_BUFFER(buffer, 56, 0x34);
   REQUIRE_BUFFER(buffer, 57, 0x12);
}

TEST_CASE("test binary deserialization", "[binary]")
{
   auto initial_instance = init_supported_binary_types{};
   auto buffer = to_string(initial_instance);

   auto parsed_instance = from_string<supported_binary_types>(buffer);

   REQUIRE(initial_instance.s8_field == parsed_instance.s8_field);
   REQUIRE(initial_instance.u8_field == parsed_instance.u8_field);
   REQUIRE(initial_instance.u16_be_field == parsed_instance.u16_be_field);
   REQUIRE(initial_instance.u16_le_field == parsed_instance.u16_le_field);
   REQUIRE(initial_instance.s16_be_field == parsed_instance.s16_be_field);
   REQUIRE(initial_instance.s16_le_field == parsed_instance.s16_le_field);
   REQUIRE(initial_instance.u32_be_field == parsed_instance.u32_be_field);
   REQUIRE(initial_instance.u32_le_field == parsed_instance.u32_le_field);
   REQUIRE(initial_instance.s32_be_field == parsed_instance.s32_be_field);
   REQUIRE(initial_instance.s32_le_field == parsed_instance.s32_le_field);
   REQUIRE(initial_instance.u64_be_field == parsed_instance.u64_be_field);
   REQUIRE(initial_instance.u64_le_field == parsed_instance.u64_le_field);
   REQUIRE(initial_instance.s64_be_field == parsed_instance.s64_be_field);
   REQUIRE(initial_instance.s64_le_field == parsed_instance.s64_le_field);
}

struct nested_struct
{
   u8 u8_field = 0;
};

struct master_struct
{
   u8 u8_before_field = 0;
   nested_struct inner{};
   u8 u8_after_field = 0;
};

VISITABLE_STRUCT(nested_struct, u8_field);
VISITABLE_STRUCT(master_struct, u8_before_field, inner, u8_after_field);

TEST_CASE("test binary nested structures", "[binary]")
{
   master_struct data{};
   data.u8_before_field = 0xff;
   data.inner.u8_field = 0x01;
   data.u8_after_field = 0x02;

   auto buffer = to_string(data);
   auto parsed_instance = from_string<master_struct>(buffer);
   static_cast<void>(parsed_instance);

   REQUIRE(data.u8_before_field == 0xff);
   REQUIRE(data.u8_after_field == 0x02);
   REQUIRE(data.inner.u8_field == 0x01);
}

struct master_struct_with_vector
{
   u8 u8_before_field = 0;
   prefixed_array<u8, nested_struct> vector{};
   u8 u8_after_field = 0;
};

VISITABLE_STRUCT(master_struct_with_vector, u8_before_field, vector, u8_after_field);

TEST_CASE("test vector with nested structure", "[binary]")
{
   master_struct_with_vector data{};
   data.u8_before_field = 0xff;
   data.vector.values.push_back(nested_struct{0x01});
   data.vector.values.push_back(nested_struct{0x02});
   data.vector.values.push_back(nested_struct{0x03});

   data.u8_after_field = 0xff;

   auto buffer = to_string(data);
   auto parsed_instance = from_string<master_struct_with_vector>(buffer);

   REQUIRE(data.u8_before_field == 0xff);
   REQUIRE(data.u8_after_field == 0xff);

   REQUIRE(data.vector.values.size() == 3);
   REQUIRE(data.vector.values[0].u8_field == 0x01);
   REQUIRE(data.vector.values[1].u8_field == 0x02);
   REQUIRE(data.vector.values[2].u8_field == 0x03);
}

struct master_struct_with_fixed_array
{
   u8 u8_before_field = 0;
   fixed_array<nested_struct, 3> vector{};
   u8 u8_after_field = 0;
};

VISITABLE_STRUCT(master_struct_with_fixed_array, u8_before_field, vector, u8_after_field);

TEST_CASE("test fixed array with nested structure", "[binary]")
{
   master_struct_with_fixed_array data{};
   data.u8_before_field = 0xff;
   data.vector.values[0] = {0x01};
   data.vector.values[1] = {0x02};
   data.vector.values[2] = {0x03};

   data.u8_after_field = 0xff;

   auto buffer = to_string(data);
   auto parsed_instance = from_string<master_struct_with_fixed_array>(buffer);
   static_cast<void>(parsed_instance);

   REQUIRE(data.u8_before_field == 0xff);
   REQUIRE(data.u8_after_field == 0xff);

   REQUIRE(data.vector.values.size() == 3);
   REQUIRE(data.vector.values[0].u8_field == 0x01);
   REQUIRE(data.vector.values[1].u8_field == 0x02);
   REQUIRE(data.vector.values[2].u8_field == 0x03);
}

const char name[] = "name";

struct struct_with_partial_byte
{
   u8 u8_before_field = 0;
   partial_byte<bit_range<0, name, 2>, bit_range<3, name, 7>> partial{};
   u8 u8_after_field = 0;
};

VISITABLE_STRUCT(struct_with_partial_byte, u8_before_field, partial, u8_after_field);

struct struct_with_partial_byte_010101011
{
   u8 u8_before_field = 0;
   partial_byte<bit_range<0, name>, bit_range<1, name>, bit_range<2, name>, bit_range<3, name>, bit_range<4, name>,
                bit_range<5, name>, bit_range<6, name>, bit_range<7, name>>
       partial{};
   u8 u8_after_field = 0;
};

VISITABLE_STRUCT(struct_with_partial_byte_010101011, u8_before_field, partial, u8_after_field);

TEST_CASE("partial byte", "[binary]")
{
   struct_with_partial_byte data{};

   SECTION("BASIC")
   {
      data.u8_before_field = 0xff;
      data.partial.values[0] = 0x7;
      data.partial.values[1] = 0x1f;
      data.u8_after_field = 0xff;

      auto buffer = to_string(data);
      auto parsed_instance = from_string<struct_with_partial_byte>(buffer);
      static_cast<void>(parsed_instance);

      REQUIRE(data.u8_before_field == 0xff);
      REQUIRE(data.u8_after_field == 0xff);

      REQUIRE(data.partial.values.size() == 2);
      REQUIRE(data.partial.values[0] == 0x7);
      REQUIRE(data.partial.values[1] == 0x1f);
   }

   SECTION("10101010")
   {
      struct_with_partial_byte_010101011 partial_data{};

      partial_data.u8_before_field = 0xff;
      partial_data.partial.values[0] = 0x1;
      partial_data.partial.values[1] = 0x0;
      partial_data.partial.values[2] = 0x1;
      partial_data.partial.values[3] = 0x0;
      partial_data.partial.values[4] = 0x1;
      partial_data.partial.values[5] = 0x0;
      partial_data.partial.values[6] = 0x1;
      partial_data.partial.values[7] = 0x1;

      partial_data.u8_after_field = 0xff;

      auto buffer = to_string(partial_data);
      auto parsed_instance = from_string<struct_with_partial_byte_010101011>(buffer);
      static_cast<void>(parsed_instance);

      REQUIRE(partial_data.u8_before_field == 0xff);
      REQUIRE(partial_data.u8_after_field == 0xff);

      REQUIRE(partial_data.partial.values.size() == 8);
      REQUIRE(partial_data.partial.values[0] == 0x1);
      REQUIRE(partial_data.partial.values[1] == 0x0);
      REQUIRE(partial_data.partial.values[2] == 0x1);
      REQUIRE(partial_data.partial.values[3] == 0x0);
      REQUIRE(partial_data.partial.values[4] == 0x1);
      REQUIRE(partial_data.partial.values[5] == 0x0);
      REQUIRE(partial_data.partial.values[6] == 0x1);
      REQUIRE(partial_data.partial.values[7] == 0x1);
   }
}
