set(target serialization-tests)

if (NOT TARGET serialization-tests)
    add_executable(${target}
            serialization_test.cpp
            binary_parser_test.cpp
            binary_types_to_json_conversion.cpp
            json_data_stream_test.cpp
            logging_init.cpp)
    target_include_directories(${target} PRIVATE ${Boost_INCLUDE_DIR})
    target_link_libraries(${target} ${Boost_LIBRARIES} Catch2::Catch2WithMain ${EXE_LIBS} ${CMAKE_THREAD_LIBS_INIT} logging visit_struct commons)

    add_custom_command(
            TARGET ${target} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${CMAKE_CURRENT_SOURCE_DIR}/mstch-templates
            ${CMAKE_CURRENT_BINARY_DIR}/mstch-templates/)

    add_definitions(-DSERIALIZATION_SUPPORTS_MSGPACK)
endif ()


