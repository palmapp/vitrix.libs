#include <boost/date_time/posix_time/posix_time.hpp>
#include <serialization/mustache.h>
#include <serialization/serialization.h>
#include <streambuf>
#include <vector>

#ifdef BOOST_WINDOWS
#include <direct.h>
#define get_curr_dir _getcwd
#else
#include <unistd.h>
#define get_curr_dir getcwd
#endif

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

struct car
{
   std::string name;
   int power;
   bool broken;

   MSGPACK_DEFINE_MAP(name, power, broken)
};
VISITABLE_STRUCT(car, name, power, broken);

struct composite
{
   car a;
   car b;

   MSGPACK_DEFINE_MAP(a, b)
};
VISITABLE_STRUCT(composite, a, b);

struct simple_bool
{
   bool exists;
};
VISITABLE_STRUCT(simple_bool, exists);

struct complicated
{
   std::string name;
   std::vector<car> cars;
   std::vector<std::string> strings;
   std::vector<int> ints;
   std::vector<double> doubles;

   MSGPACK_DEFINE_MAP(name, cars, strings, ints, doubles)
};

VISITABLE_STRUCT(complicated, name, cars, strings, ints, doubles);

TEST_CASE("test parse json", "[json]")
{

   car x;
   bool result = spider::serialization::json::parse_json(x, R"({ "name" : "my cool name", "power": 10, "broken": 1})");

   REQUIRE(result == true);
   REQUIRE(x.name == "my cool name");
   REQUIRE(x.power == 10);
   REQUIRE(x.broken);

   composite c;
   result = spider::serialization::json::parse_json(c, R"({
      "a": { "name" : "my cool name", "power": 10, "broken": 0},
      "b": { "name" : "b cool name", "power": 100, "broken": 1}
   })");

   REQUIRE(result == true);
   REQUIRE(c.a.name == "my cool name");
   REQUIRE(c.a.power == 10);
   REQUIRE_FALSE(c.a.broken);

   REQUIRE(c.b.name == "b cool name");
   REQUIRE(c.b.power == 100);
   REQUIRE(c.b.broken);

   complicated comp;
   result = spider::serialization::json::parse_json(comp, R"({
      "name" : "my cars",
       "cars" : [ 
         { "name" : "my cool name", "power": 200, "broken": 0},
         { "name" : "b cool name", "power": 100, "broken": 1}
      ], 
      "strings" : [ "a", "b" ],
      "ints" : [100, 200],
      "doubles": [10.5, 30.4]
   })");

   REQUIRE(result == true);
   REQUIRE(comp.cars.size() == 2);
   REQUIRE(comp.cars[0].name == "my cool name");
   REQUIRE(comp.cars[0].power == 200);
   REQUIRE_FALSE(comp.cars[0].broken);
   REQUIRE(comp.cars[1].name == "b cool name");
   REQUIRE(comp.cars[1].power == 100);
   REQUIRE(comp.cars[1].broken);

   std::vector<std::string> strings{"a", "b"};
   std::vector<int> ints{100, 200};
   std::vector<double> doubles{10.5, 30.4};

   REQUIRE(comp.strings == strings);
   REQUIRE(comp.ints == ints);
   REQUIRE(comp.doubles == doubles);
}

void test_serialize(spider::serialization::content_type type)
{
   using namespace spider::serialization;
   using namespace std::string_literals;
   car x;
   x.name = "my name";
   x.power = 10;
   x.broken = true;

   auto string = serialize_structure(x, type);

   car y;
   bool res = parse_structure(y, string, type);

   REQUIRE(res == true);
   REQUIRE(y.name == "my name");
   REQUIRE(y.power == 10);
   REQUIRE(y.broken);

   composite c;
   c.a.name = "a name";
   c.a.power = 10;
   c.a.broken = true;

   c.b.name = "b name";
   c.b.power = 100;
   c.b.broken = false;
   string = serialize_structure(c, type);

   composite d;
   res = parse_structure(d, string, type);
   REQUIRE(res == true);
   REQUIRE(d.a.name == "a name");
   REQUIRE(d.a.power == 10);
   REQUIRE(d.a.broken);
   REQUIRE(d.b.name == "b name");
   REQUIRE(d.b.power == 100);
   REQUIRE_FALSE(d.b.broken);

   complicated out;
   out.name = "my cars";
   out.cars = {{"my cool name", 200, 1}, {"b cool name", 100, 0}};
   out.strings = {"a"s, "b"s};
   out.doubles = {10.5, 30.4};
   out.ints = {100, 200};

   string = serialize_structure(out, type);

   complicated comp;
   res = parse_structure(comp, string, type);

   REQUIRE(res == true);
   REQUIRE(comp.cars.size() == 2);
   REQUIRE(comp.cars[0].name == "my cool name");
   REQUIRE(comp.cars[0].power == 200);
   REQUIRE(comp.cars[0].broken);
   REQUIRE(comp.cars[1].name == "b cool name");
   REQUIRE(comp.cars[1].power == 100);
   REQUIRE_FALSE(comp.cars[1].broken);

   std::vector<std::string> strings{"a", "b"};
   std::vector<int> ints{100, 200};
   std::vector<double> doubles{10.5, 30.4};

   REQUIRE(comp.strings == strings);
   REQUIRE(comp.ints == ints);
   REQUIRE(comp.doubles == doubles);
}

TEST_CASE("test serialize json", "[json]")
{
   test_serialize(spider::serialization::content_type::json);
}

TEST_CASE("test parse msgpack", "[messagepack]")
{
   test_serialize(spider::serialization::content_type::msgpack);
}

TEST_CASE("test serialize url encode", "[url encode]")
{
   using namespace spider::serialization;
   car x{"my car", 100, true};

   auto my_car_string = serialize_structure(x, content_type::urlencoded);
   REQUIRE(my_car_string.empty() == false);

   car parsed;
   auto result = parse_structure(parsed, my_car_string, content_type::urlencoded);
   REQUIRE(result == true);
   REQUIRE(x.name == parsed.name);
   REQUIRE(x.power == parsed.power);
   REQUIRE(x.broken == parsed.broken);

   result = parse_structure(parsed, "?name=cool&power=1000000000000000000000&broken=0", content_type::urlencoded);
}

TEST_CASE("test parse json bools", "[json]")
{

   simple_bool x;

   SECTION("1")
   {
      bool result = spider::serialization::json::parse_json(x, R"({ "exists" : 1})");
      REQUIRE(result);
      REQUIRE(x.exists == true);
   }

   SECTION("0")
   {
      bool result = spider::serialization::json::parse_json(x, R"({ "exists" : 0})");
      REQUIRE(result);
      REQUIRE(x.exists == false);
   }

   SECTION("true")
   {
      bool result = spider::serialization::json::parse_json(x, R"({ "exists" : true})");
      REQUIRE(result);
      REQUIRE(x.exists == true);
   }

   SECTION("false")
   {
      bool result = spider::serialization::json::parse_json(x, R"({ "exists" : false})");
      REQUIRE(result);
      REQUIRE(x.exists == false);
   }
}
