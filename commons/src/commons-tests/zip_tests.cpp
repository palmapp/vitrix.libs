#include <catch.hpp>
#include <fstream>
#include <iostream>

#include <boost/dll/runtime_symbol_info.hpp>
#include <boost/filesystem/operations.hpp>
#include <commons/unzip.h>

TEST_CASE("test zip file decompression", "[zip]")
{
   const auto base_path = boost::dll::program_location().parent_path();

   boost::filesystem::remove_all(base_path / "test_zip_result");
   commons::unzip(base_path / "test_zip.zip", base_path / "test_zip_result");

   REQUIRE(boost::filesystem::is_directory(base_path / "test_zip_result") == true);
   REQUIRE(boost::filesystem::is_directory(base_path / "test_zip_result/empty_folder") == true);
   REQUIRE(boost::filesystem::is_directory(base_path / "test_zip_result/sub_folders/empty_folder") == true);
   REQUIRE(boost::filesystem::is_directory(base_path / "test_zip_result/sub_folders/full_folder") == true);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/large.txt") == 283500);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/content.txt") == 11);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/empty_file.txt") == 0);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/sub_folders/content.txt") == 11);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/sub_folders/empty_file.txt") == 0);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/sub_folders/full_folder/content.txt") == 11);
   REQUIRE(boost::filesystem::file_size(base_path / "test_zip_result/sub_folders/full_folder/empty_file.txt") == 0);
}

TEST_CASE("test zip file decompression to memory", "[zip]")
{
   const auto base_path = boost::dll::program_location().parent_path();

   auto str = commons::unzip_item_to_memory(base_path / "test_zip.zip", "content.txt");
   REQUIRE(str == "content.txt");
}