#include <catch.hpp>
#include <fstream>
#include <iostream>

#include <commons/url_fetch.h>

TEST_CASE("url fetch", "[url]")
{

   std::string body;
   auto result = commons::url::fetch_to_string(body, "https://ci.vitrix.cz/vitrix/kiosek-test/update.json");
   REQUIRE(result == 200);
   REQUIRE(body.empty() == false);
}