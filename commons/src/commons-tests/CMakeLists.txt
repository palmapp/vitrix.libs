set(target commons-tests)

if (NOT TARGET ${target})
    add_executable(${target}
            app_tests.cpp
            md5_tests.cpp
            zip_tests.cpp
            messaging_tests.cpp
            model_tests.cpp
            # process_manager_tests.cpp
            active_object_tests.cpp
            fetch_string_tests.cpp
            uri_builder_tests.cpp
            property_tests.cpp
            # data_table_tests.cpp
    )

    target_link_libraries(${target} commons Catch2::Catch2WithMain ${EXE_LIBS} logging visit_struct)

    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/test_zip.zip
            DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
endif ()