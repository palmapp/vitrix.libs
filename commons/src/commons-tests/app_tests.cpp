#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <commons/url_fetch.h>
#include <easylogging++.h>

INITIALIZE_EASYLOGGINGPP

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

TEST_CASE("test fetch_to_string", "[url_fetch]")
{

   std::string body;
   int status = commons::url::fetch_to_string(
       body, "https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date=22.12.2017");

   REQUIRE(status == 200);
   REQUIRE(body.size() > 0);
}

TEST_CASE("test fetch_to_file", "[url_fetch]")
{

   int status = commons::url::fetch_to_file(
       "tmp", "https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date=22.12.2017");

   REQUIRE(status == 200);
   REQUIRE(boost::filesystem::file_size("tmp") > 0);
}
