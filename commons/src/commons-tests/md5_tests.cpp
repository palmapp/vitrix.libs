#include <boost/dll/runtime_symbol_info.hpp>
#include <catch.hpp>
#include <commons/hex_encode.h>
#include <commons/md5.h>
#include <fstream>
#include <iostream>

TEST_CASE("test md5 file hashing", "[md5]")
{
   const auto base_path = boost::dll::program_location().parent_path();

   std::string test_string = "test string";

   commons::hash::md5 hash_calc;
   hash_calc.update(reinterpret_cast<const uint8_t *>(test_string.data()), static_cast<uint32_t>(test_string.size()));
   auto result = hash_calc.finish();
   auto hash_string = encoding::hex_encode(reinterpret_cast<const void *>(result.begin()), result.size());
   REQUIRE(hash_string == "6F8DB599DE986FAB7A21625B7916589C");

   auto test_path = base_path / "test.txt";
   {
      std::ofstream file(test_path.c_str());
      file << test_string;
   }

   auto file_result = commons::hash::calculate_file_checksum(test_path);
   auto file_hash_string =
       encoding::hex_encode(reinterpret_cast<const void *>(file_result.begin()), file_result.size());

   REQUIRE(hash_string == file_hash_string);
}