#include <catch.hpp>
#include <commons/data/data_table.h>

using namespace commons;
using namespace commons::basic_types;

TEST_CASE("test data table", "[data_table]")
{
   data_table_spec spec;
   spec.column_types = {
       data_type::boolean,
       data_type::string,
       data_type::i64,
   };

   spec.allocate = 100;

   data_table table{spec};

   REQUIRE(table.size() == 0);
}