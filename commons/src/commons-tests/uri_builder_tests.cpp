#include <catch.hpp>
#include <commons/uri_builder.h>
#include <commons/url_fetch.h>
#include <cpprest/uri.h>

TEST_CASE("uri builder", "[uri]")
{
   commons::url::uri_builder uri_builder{std::string{"https://seznam.cz?h=true"}};
   uri_builder = uri_builder.make_relative("k", {{"l", "false"}});
   std::string str = uri_builder.to_string();
   REQUIRE("https://seznam.cz/k?h=true&l=false" == str);

   uri_builder = commons::url::uri_builder{web::uri{commons::url::normalize_string("https://seznam.cz?h=true")}};
   uri_builder = uri_builder.make_relative("k");
   uri_builder = uri_builder.add_query({{"l", "false"}});
   str = uri_builder.to_string();
   REQUIRE("https://seznam.cz/k?h=true&l=false" == str);
}