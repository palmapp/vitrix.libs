#include <catch.hpp>
#include <fstream>
#include <iostream>

#include <commons/message_broker.h>
#include <condition_variable>
#include <mutex>
#include <thread>

class test_subscription : public commons::subscription
{
 public:
   struct message
   {
      std::string channel, type, message_content;
      message(std::string _channel, std::string _type, std::string _message_content)
          : channel(std::move(_channel)), type(std::move(_type)), message_content(std::move(_message_content))
      {
      }
   };

   std::vector<message> messages;
   void on_message(const std::string &channel, const std::string &type, const std::string &message) final
   {
      {
         std::unique_lock<std::mutex> lock(mutex_);
         messages.emplace_back(channel, type, message);
      }
      cnd_.notify_one();
   }

   void wait_for(size_t n)
   {
      std::unique_lock<std::mutex> lock(mutex_);
      cnd_.wait_for(lock, std::chrono::seconds{5}, [this, n]() { return messages.size() == n; });
   }

 private:
   std::mutex mutex_;
   std::condition_variable cnd_;
};

TEST_CASE("test pub sub", "[messaging]")
{
   using namespace commons;

   message_broker broker{message_broker_type::message_broker_server, "inproc://pub"};
   message_receiver receiver{message_receiver_type::message_broker_client, "inproc://pub"};

   auto subscription = std::make_shared<test_subscription>();
   receiver.subscribe_all(subscription);

   auto concrete_sub = std::make_shared<test_subscription>();
   receiver.subscribe("system/anything", concrete_sub);

   receiver.start_receive();

   std::this_thread::sleep_for(std::chrono::milliseconds{100});

   broker.publish_message("system", "type", "a");
   broker.publish_message("system", "type", "b");
   broker.publish_message("system/anything", "type", "c");

   subscription->wait_for(3);
   receiver.end_receive();

   concrete_sub->wait_for(1);

   REQUIRE(subscription->messages.size() == 3);
   REQUIRE(subscription->messages[0].message_content == "a");
   REQUIRE(subscription->messages[1].message_content == "b");
   REQUIRE(subscription->messages[2].channel == "system/anything");
   REQUIRE(subscription->messages[2].message_content == "c");
   REQUIRE(concrete_sub->messages[0].message_content == "c");
}

TEST_CASE("test driver", "[messaging]")
{
   using namespace commons;

   message_broker broker{message_broker_type::driver_client, "inproc://drivers"};
   message_receiver receiver{message_receiver_type::driver_server, "inproc://drivers"};

   auto subscription = std::make_shared<test_subscription>();
   receiver.subscribe_all(subscription);

   auto concrete_sub = std::make_shared<test_subscription>();
   receiver.subscribe("system/anything", concrete_sub);

   receiver.start_receive();

   broker.publish_message("system", "type", "a");
   broker.publish_message("system", "type", "b");
   broker.publish_message("system/anything", "type", "c");

   subscription->wait_for(3);
   receiver.end_receive();

   concrete_sub->wait_for(1);

   REQUIRE(subscription->messages.size() == 3);
   REQUIRE(subscription->messages[0].message_content == "a");
   REQUIRE(subscription->messages[1].message_content == "b");
   REQUIRE(subscription->messages[2].channel == "system/anything");
   REQUIRE(subscription->messages[2].message_content == "c");
   REQUIRE(concrete_sub->messages[0].message_content == "c");
}