#include <atomic>
#include <catch.hpp>
#include <commons/active_object.h>

class test_active_object : public commons::active_object<std::int64_t>
{
 public:
   std::atomic<std::int64_t> counter_;

   test_active_object(commons::stopping_policy policy) : active_object(policy), counter_(0) {}

 protected:
   void on_message(std::int64_t &message) override
   {
      counter_ += message;
   }
};

TEST_CASE("test stopping_policy::process_all policy", "[active_object_0]")
{

   test_active_object my_object{commons::stopping_policy::process_all};
   my_object.post_message(1);
   my_object.post_message(2);
   my_object.post_message(3);
   my_object.stop();

   REQUIRE(my_object.counter_.load() == 6);
}

TEST_CASE("test stopping_policy::stop_imediatelly policy", "[active_object_1]")
{

   test_active_object my_object{commons::stopping_policy::stop_imediatelly};
   my_object.post_message(1);
   my_object.post_message(2);
   my_object.post_message(3);
   my_object.stop();

   REQUIRE(my_object.counter_.load() <= 6);
}