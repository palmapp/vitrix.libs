#include <catch.hpp>
#include <commons/property/detail/hub_impl.h>

#include <serialization/property_list_serializer.h>

#include <easylogging++.h>

using namespace commons::property;
using namespace spider::serialization::property;
using namespace spider::property;

struct test_struct
{
   bool a;
   int b;
   std::string c;
};

VISITABLE_STRUCT(test_struct, a, b, c);

TEST_CASE("basic", "[property]")
{
   commons::property::property_hub hub{};

   int counter = 0;
   hub.value_changed.connect([&](property_context ctx, const property_list &properties) { counter++; });

   SECTION("simple")
   {
      property_context context{};
      context.id = 1000;
      context.scope_id = 100;
      context.scope = "test_scope";

      hub.set_property(context, "test_value_0", static_cast<int>(300));
      hub.set_property(context, "test_value_0", static_cast<int>(301));
      hub.set_property(context, "test_value_0", static_cast<int>(300));
      hub.set_property(context, "test_value_0", static_cast<int>(300));

      hub.set_property(context, "test_value_1", static_cast<int>(301));

      property_context context_1{};
      context_1.id = 1000;
      context_1.scope_id = 101;
      context_1.scope = "test_scope";

      hub.set_property(context_1, "test_value_1", static_cast<int>(301));

      property_context context_2{};
      context_2.id = 1000;
      context_2.scope_id = 101;
      context_2.scope = "test_scope_1";

      hub.set_property(context_2, "test_value_1", static_cast<int>(301));

      REQUIRE(counter == 6);
      REQUIRE(hub.get_properties(1000).scopes["test_scope"].items.size() == 2);
      REQUIRE(hub.get_properties(1000).scopes["test_scope_1"].items.size() == 1);

      std::string exp =
          "{\"1000\":{\"test_scope\":{\"100\":{\"test_value_0\":300,\"test_value_1\":301},\"101\":{\"test_value_1\":"
          "301}},\"test_scope_1\":{\"101\":{\"test_value_1\":301}}}}";

      REQUIRE(to_json_string(1000, hub.get_properties(1000)) == exp);
   }

   SECTION("struct")
   {
      test_struct test{};
      test.a = true;
      test.b = 300;
      test.c = "test_val";

      auto properties = to_property_list(test);

      property_context context{};
      context.id = 1000;
      context.scope_id = 100;
      context.scope = "test_scope";

      hub.set_properties(context, properties);

      REQUIRE(hub.get_properties(1000).scopes["test_scope"].items[100].size() == 3);
      REQUIRE(counter == 1);
   }
}