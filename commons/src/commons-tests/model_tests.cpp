#include "visit_struct/visit_struct.hpp"
#include <catch.hpp>
#include <commons/observable_model.h>
#include <type_traits>

struct example
{
   int id = {};
   int value_a = {};
   std::string value_b = {};
};

VISITABLE_STRUCT(example, id, value_a, value_b);

using namespace commons;

TEST_CASE("test model info retrieve", "[model]")
{
   example e{10};

   bool same_type = std::is_same<decltype(e.id), typename model_traits<example>::id_type>::value;
   REQUIRE(same_type == true);
}

struct observer : public model_observer<example>
{
   bool on_added_called = false;
   bool on_changed_called = false;
   bool on_removed_called = false;

   model_traits<example>::change_set expected_change_set;

   void on_added(const id_type &id, const example &value) override
   {
      on_added_called = true;
   }

   void on_changed(const id_type &id, const example &value, const model_traits<example>::change_set &changes) override
   {
      on_changed_called = true;
      REQUIRE(expected_change_set == changes);
   }

   void on_removed(const id_type &id) override
   {
      on_removed_called = true;
   }
};

TEST_CASE("test model diff", "[model]")
{
   observable_model<example> x;
   observer o;
   x.set_model_observer(&o);

   x.update_model(10, example{10, 100, "ahoj"});
   REQUIRE(o.on_added_called == true);

   o.expected_change_set.push_back(1);
   o.expected_change_set.push_back(2);

   x.update_model(10, example{10, 200, "beta"});
   REQUIRE(o.on_changed_called == true);

   example retrieved;
   retrieved.id = 0;

   x.try_get_model_by_id(retrieved, 10);

   REQUIRE(retrieved.id == 10);
   REQUIRE(retrieved.value_a == 200);
   REQUIRE(retrieved.value_b == "beta");

   x.remove_model(10);
   REQUIRE(o.on_removed_called == true);

   retrieved.id = 0;
   x.try_get_model_by_id(retrieved, 10);
   REQUIRE(retrieved.id == 0);
}
