#include <boost/dll/runtime_symbol_info.hpp>

#include <boost/asio/io_service.hpp>
#include <catch.hpp>
#include <chrono>
#include <commons/process_manager.h>
#include <thread>

using namespace std::chrono_literals;

class output : public commons::process_output
{
 public:
   std::string captured_output;
   bool started = false;
   bool ended = false;
   bool failed_to_start = false;

   void on_std_out(const std::int64_t id, const std::uint8_t *buffer, std::size_t len) override
   {
      captured_output += std::string(reinterpret_cast<const char *>(buffer), len);
   }

   void on_std_err(const std::int64_t id, const std::uint8_t *buffer, std::size_t len) override {}

   void on_started(const std::int64_t id, const std::int64_t pid) override
   {
      started = true;
   }

   void on_failed_to_start(const std::int64_t id, const std::string &reason) override
   {
      failed_to_start = true;
   }

   void on_ended(const std::int64_t id, const std::int64_t exit_code) override
   {
      ended = true;
   }
};

TEST_CASE("test process manager input output", "[process_manager]")
{
   const auto base_path = boost::dll::program_location().parent_path();

   boost::asio::io_service service;
   output out;
   commons::process_manager mng(service, out);
#ifdef __linux__
   mng.start_process(1, "echo ahoj");
#else
   mng.start_process(1, "cmd /C \"echo ahoj\"");
#endif
   service.run();

   std::this_thread::sleep_for(1s);

   mng.validate_processes();

   REQUIRE(out.started == true);
   REQUIRE(out.ended == true);
   REQUIRE(out.failed_to_start == false);
   REQUIRE(out.captured_output.empty() == false);
   REQUIRE(out.captured_output.find("ahoj") != std::string::npos);
}

#ifdef __linux__

class fifo_output : public output
{
 public:
   commons::process_manager *mng = nullptr;

   bool first_output = false;
   void on_std_out(const std::int64_t id, const std::uint8_t *buffer, std::size_t len) override
   {
      output::on_std_out(id, buffer, len);
      if (!first_output)
      {
         mng->feed_std_in(1, "exit\r\n");
         first_output = true;
      }
   }
};

TEST_CASE("test term process manager input output", "[process_manager]")
{
   const auto base_path = boost::dll::program_location().parent_path();

   boost::asio::io_service service;
   fifo_output out;
   commons::process_manager mng(service, out);
   out.mng = &mng;

   mng.start_process_in_term(1, "/bin/bash");

   service.run();
   std::this_thread::sleep_for(1s);
   mng.validate_processes();

   REQUIRE(out.started == true);
   REQUIRE(out.ended == true);
   REQUIRE(out.failed_to_start == false);
}

TEST_CASE("test term process scoped exit", "[process_manager]")
{
   const auto base_path = boost::dll::program_location().parent_path();

   boost::asio::io_service service;
   output out;
   {
      commons::process_manager mng(service, out);
      mng.start_process_in_term(1, "/bin/bash");
   }
   REQUIRE(out.started == true);
   REQUIRE(out.ended == true);
   REQUIRE(out.failed_to_start == false);
}
#endif