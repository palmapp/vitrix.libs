#include "hex_encode.h"
#include <algorithm>

namespace encoding
{
   const char hexdigits_up[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

   void toHex(const void *data, size_t size, std::string &tgt)
   {
      const unsigned char *bytes = reinterpret_cast<const unsigned char *>(data);

      for (size_t i = 0; i < size; ++i)
      {
         unsigned char b = bytes[i];

         tgt += hexdigits_up[b >> 4];
         tgt += hexdigits_up[b & 0xf];
      }
   }

   std::string hex_encode(std::string_view data)
   {
      return hex_encode(data.data(), data.size());
   }

   std::string hex_encode(const std::string &data)
   {
      return hex_encode(data.data(), data.size());
   }

   std::string hex_encode(const void *data, size_t len)
   {
      std::string hex;
      hex.reserve(len * 2);

      toHex(data, len, hex);

      return hex;
   }

   std::string hex_decode(const std::string &data)
   {
      std::string result;
      result.reserve(data.size() / 2);

      for (size_t i = 0; i < data.size(); i += 2)
      {
         auto byte = data.substr(i, 2);
         result.push_back(static_cast<char>(strtol(byte.c_str(), nullptr, 16)));
      }

      return result;
   }

} // namespace encoding
