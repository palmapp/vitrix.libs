#include "tx_context.h"

const std::int64_t commons::null_tx_context::required_type_id =
    tx_type_id_generator::generate_type_id<null_tx_context>();

std::int64_t commons::null_tx_context::get_context_type_id() const noexcept
{
   return null_tx_context::required_type_id;
}