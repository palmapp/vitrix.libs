#include "message_broker.h"
#include <atomic>
#include <boost/algorithm/string/predicate.hpp>
#include <mutex>
#include <thread>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <zmq.h>
#include <zmq.hpp>

#include <easylogging++.h>

namespace commons
{
   zmq::context_t &get_global_context()
   {
      static zmq::context_t global{1};
      return global;
   }

   class message_broker_impl
   {
    public:
      message_broker_impl(message_broker_type type, const std::string &connection_string)
          : socket_(get_global_context(),
                    type == message_broker_type::driver_client || type == message_broker_type::pusher ? ZMQ_PUSH
                                                                                                      : ZMQ_PUB)
      {
         if (type == message_broker_type::message_broker_server || type == message_broker_type::pusher)
            socket_.bind(connection_string);
         else
            socket_.connect(connection_string);
      }

      void publish_message(const std::string &channel, const std::string &type, const std::string &message)
      {
         std::lock_guard<std::mutex> lock{mutex_};

         socket_.send(zmq::const_buffer{channel.c_str(), channel.size()}, zmq::send_flags::sndmore);
         socket_.send(zmq::const_buffer{type.c_str(), type.size()}, zmq::send_flags::sndmore);
         socket_.send(zmq::const_buffer{message.c_str(), message.size()}, zmq::send_flags::none);
      }

      void close_and_discard_unsent_messages()
      {
         std::lock_guard<std::mutex> lock{mutex_};
         socket_.set(zmq::sockopt::linger, 0);
         socket_.close();
      }

    private:
      zmq::socket_t socket_;
      std::mutex mutex_;
   };

   class message_receiver_impl
   {
    public:
      message_receiver_impl(message_receiver_type type, const std::string &connection_string)
          : type_(type),
            socket_(get_global_context(),
                    type == message_receiver_type::driver_server || type == message_receiver_type::puller ? ZMQ_PULL
                                                                                                          : ZMQ_SUB)
      {
         if (type_ == message_receiver_type::driver_server)
         {
            socket_.bind(connection_string);
         }
         else
         {
            socket_.connect(connection_string);
         }

         socket_.set(zmq::sockopt::rcvtimeo, 100);
      }

      ~message_receiver_impl()
      {
         end_receive();
      }

      void subscribe(const std::string &channel, std::shared_ptr<subscription> subscription)
      {
         std::lock_guard<std::mutex> lock{mutex_};
         queue_.emplace_back(true, channel, std::move(subscription));
      }

      void unsubscribe(const std::string &channel, std::shared_ptr<subscription> subscription)
      {
         std::lock_guard<std::mutex> lock{mutex_};
         queue_.emplace_back(false, channel, std::move(subscription));
      }

      void subscribe_all(std::shared_ptr<subscription> subscription)
      {
         subscribe("system", std::move(subscription));
      }

      void unsubscribe_all(std::shared_ptr<subscription> subscription)
      {
         unsubscribe("", std::move(subscription));
      }

      void start_receive()
      {
         if (!running_)
         {
            LOG(TRACE) << "message receiver start requested";

            running_ = true;
            receive_thread_ = std::thread{&message_receiver_impl::run, this};
         }
      }

      void end_receive()
      {
         if (running_)
         {
            LOG(TRACE) << "message receiver stop requested";
            running_ = false;
            receive_thread_.join();
         }
      }

    private:
      void run()
      {
         LOG(TRACE) << "message receiver started";

         while (running_)
         {
            bool exception = false;
            try
            {
               zmq::message_t channel;
               zmq::message_t type;
               zmq::message_t msg;

               while (running_)
               {
                  process_command_queue();

                  bool result = false;

                  try
                  {
                     result = socket_.recv(channel).has_value();
                     result = result && socket_.recv(type).has_value();
                     result = result && socket_.recv(msg).has_value();
                  }
                  catch (...)
                  {
                     result = false;
                  }
                  
                  if (result)
                  {
                     notify(convert_message(channel), convert_message(type), convert_message(msg));
                  }
               }
            }
            catch (const std::exception &ex)
            {
               exception = true;
               LOG(ERROR) << "message_receiver thread thrown an exception : " << ex.what();
            }
            catch (...)
            {
               exception = true;
               LOG(ERROR) << "message_receiver thread thrown an unknown exception";
            }

            if (exception)
            {
               std::this_thread::sleep_for(std::chrono::seconds{1});
            }
         }

         socket_.close();

         LOG(TRACE) << "message receiver ended";
      }

      void process_command_queue()
      {
         std::lock_guard<std::mutex> lock{mutex_};
         for (auto &cmd : queue_)
         {
            auto sub = std::get<0>(cmd);
            auto &channel = std::get<1>(cmd);
            auto &subs = std::get<2>(cmd);

            if (sub)
            {
               subscribe_internal(channel, std::move(subs));
            }
            else
            {
               if (channel.empty())
               {
                  unsubscribe_all_internal(std::move(subs));
               }
               else
               {
                  unsubscribe_internal(channel, std::move(subs));
               }
            }
         }
         queue_.clear();
      }

      std::string convert_message(zmq::message_t &msg)
      {
         return {reinterpret_cast<const char *>(msg.data()), msg.size()};
      }

      void notify(const std::string &channel, const std::string &type, const std::string &msg) const
      {
         LOG(TRACE) << "msr: " << channel << " " << type << " " << msg;

         for (auto &sub : load_subscribers(channel))
         {
            sub->on_message(channel, type, msg);
         }
      }

      std::unordered_set<std::shared_ptr<subscription>> load_subscribers(const std::string &channel) const
      {
         std::unordered_set<std::shared_ptr<subscription>> invoke;
         {
            std::lock_guard<std::mutex> lock{mutex_};
            for (size_t i = 0; i != slots_.size(); ++i)
            {
               if (boost::starts_with(channel, slots_[i]))
               {
                  auto it = subscribers_.find(i);
                  if (it != subscribers_.end())
                  {
                     auto &subscribers = it->second;
                     for (auto &sub : subscribers)
                        invoke.insert(sub);
                  }
               }
            }
         }

         return invoke;
      }

      size_t find_slot(const std::string &channel, bool allow_create)
      {
         auto it = std::find(slots_.begin(), slots_.end(), channel);
         if (it != slots_.end())
         {
            return std::distance(slots_.begin(), it);
         }

         if (allow_create)
         {
            auto result = slots_.size();
            slots_.push_back(channel);
            return result;
         }

         return static_cast<size_t>(-1);
      }

      void subscribe_internal(const std::string &channel, std::shared_ptr<subscription> subscription)
      {
         LOG(INFO) << "message receiver -> subscribe " << channel;

         auto slot_idx = find_slot(channel, true);
         auto &slot_subs = subscribers_[slot_idx];

         if (type_ == message_receiver_type::message_broker_client && slot_subs.empty())
            socket_.set(zmq::sockopt::subscribe, channel);

         slot_subs.insert(std::move(subscription));
      }

      void unsubscribe_internal(const std::string &channel, std::shared_ptr<subscription> subscription)
      {
         LOG(INFO) << "message receiver -> unsubscribe " << channel;

         auto slot_idx = find_slot(channel, false);
         if (static_cast<size_t>(-1) != slot_idx)
         {
            auto &slot_subs = subscribers_[slot_idx];
            auto elements_removed = slot_subs.erase(subscription);

            if (type_ == message_receiver_type::message_broker_client && elements_removed != 0 && slot_subs.empty())
            {
               socket_.set(zmq::sockopt::unsubscribe, channel);
            }

         }
      }

      void unsubscribe_all_internal(std::shared_ptr<subscription> subscription)
      {
         LOG(INFO) << "message receiver -> unsubscribe all";

         for (auto &subs : subscribers_)
         {
            if (subs.second.erase(subscription) != 0 && subs.second.empty() &&
                type_ == message_receiver_type::message_broker_client)
            {
               const auto &channel = slots_[subs.first];
               socket_.set(zmq::sockopt::unsubscribe, channel);
            }
         }
      }

      message_receiver_type type_;
      zmq::socket_t socket_;

      mutable std::mutex mutex_;
      std::vector<std::string> slots_;
      using subscription_ptr = std::shared_ptr<subscription>;
      std::unordered_map<size_t, std::unordered_set<subscription_ptr>> subscribers_;

      std::thread receive_thread_;
      std::atomic<bool> running_ = false;

      std::vector<std::tuple<bool, std::string, subscription_ptr>> queue_;
   };

} // namespace commons

commons::message_broker::message_broker(message_broker_type type, const std::string &connection_string)
{
   impl_ = std::make_unique<message_broker_impl>(type, connection_string);
}

commons::message_broker::~message_broker() {}

void commons::message_broker::publish_message(const std::string &channel, const std::string &type,
                                              const std::string &message)
{
   impl_->publish_message(channel, type, message);
}

void commons::message_broker::close_and_discard_unsent_messages()
{
   impl_->close_and_discard_unsent_messages();
}

commons::message_receiver::message_receiver(message_receiver_type type, const std::string &connection_string)
{
   impl_ = std::make_unique<message_receiver_impl>(type, connection_string);
}

commons::message_receiver::~message_receiver() {}

void commons::message_receiver::subscribe(const std::string &channel, std::shared_ptr<subscription> subscription)
{
   impl_->subscribe(channel, subscription);
}

void commons::message_receiver::unsubscribe(const std::string &channel, std::shared_ptr<subscription> subscription)
{
   impl_->unsubscribe(channel, subscription);
}

void commons::message_receiver::subscribe_all(std::shared_ptr<subscription> subscription)
{
   impl_->subscribe_all(subscription);
}

void commons::message_receiver::unsubscribe_all(std::shared_ptr<subscription> subscription)
{
   impl_->subscribe_all(subscription);
}

void commons::message_receiver::start_receive()
{
   impl_->start_receive();
}

void commons::message_receiver::end_receive()
{
   impl_->end_receive();
}
