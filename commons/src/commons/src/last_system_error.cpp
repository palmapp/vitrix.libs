#include "last_system_error.h"

std::system_error commons::last_system_error(basic_types::optional<basic_types::string> message)
{
   return std::system_error(last_system_error_code(), message.value_or("OS Exception"));
}

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

std::error_code commons::last_system_error_code()
{
   return std::error_code(GetLastError(), std::system_category());
}

#else
#include <cerrno>

std::error_code commons::last_system_error_code()
{
   return std::error_code(errno, std::system_category());
}

#endif