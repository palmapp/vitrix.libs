#include "rc4.h"
#include <assert.h>
#include <cstring>

/******************************************************************************/
using namespace commons::crypto;
using namespace std;

rc4::rc4(const char *key)
   : rc4(key, strlen(key))
{
}

rc4::rc4(const char *key, const size_t keyLength)
{
   size_t index1;
   size_t index2;
   size_t counter;
   uint8_t temp;

   for (counter = 0; counter < 256; ++counter)
   {
      state[counter] = (uint8_t)counter;
   }

   if (keyLength != 0)
   {
      x = 0;
      y = 0;
      index1 = 0;
      index2 = 0;
      for (counter = 0; counter < 256; ++counter)
      {
         index2 = (key[index1] + state[counter] + index2) & 0xff;
         temp = state[counter];
         state[counter] = state[index2];
         state[index2] = temp;
         if (++index1 == keyLength) index1 = 0;
      }
   }
}

int rc4::next_state()
{
   uint8_t temp;
   x = (x + 1) & 0xff;
   y = (y + state[x]) & 0xff;
   temp = state[x];
   state[x] = state[y];
   state[y] = temp;
   return (state[x] + state[y]) & 0xff;
}

uint8_t rc4::crypt(uint8_t toEncrypt)
{
   auto ns = static_cast<uint8_t>(next_state());
   return static_cast<uint8_t>(toEncrypt ^ state[ns]);
}

void rc4::crypt(uint8_t *toEncrypt, std::size_t len, const bool saveState)
{
   uint8_t prevState[256];
   int prevX = 0;
   int prevY = 0;
   if (saveState)
   {
      memcpy(prevState, state, 256);
      prevX = x;
      prevY = y;
   }

   while (len--)
   {
      uint8_t byte = *toEncrypt;
      *toEncrypt = crypt(byte);

      ++toEncrypt;
   }

   if (saveState)
   {
      memcpy(state, prevState, 256);
      x = prevX;
      y = prevY;
   }
}
