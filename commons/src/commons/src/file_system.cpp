#include "file_system.h"
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <stdexcept>

using namespace boost::filesystem;

void commons::copy_directory_recursive(const path &src, const path &dst)
{
   if (exists(dst))
   {
      throw std::runtime_error(dst.generic_string() + " exists");
   }

   if (is_directory(src))
   {
      create_directories(dst);
      for (directory_entry item : directory_iterator(src))
      {
         const auto path = item.path();
         const auto dest_path = dst / item.path().filename();

         copy_directory_recursive(path, dest_path);
      }
   }
   else if (is_regular_file(src))
   {
      boost::system::error_code ec;
      copy(src, dst, ec);
      if (ec)
      {
         throw std::runtime_error(src.generic_string() + " unable to copy");
      }
   }
   else
   {
      throw std::runtime_error(dst.generic_string() + " not dir or file");
   }
}

std::string commons::get_file_extension_without_dot(const boost::filesystem::path &src)
{
   auto ext = src.extension().string();
   if (!ext.empty() && ext[0] == '.') ext = ext.substr(1);

   return ext;
}

std::string commons::read_file(const boost::filesystem::path &src)
{
   boost::filesystem::ifstream fs{src, std::ios_base::binary};
   const std::istreambuf_iterator<char> begin{fs};
   const std::istreambuf_iterator<char> end{};

   std::string result;
   boost::system::error_code ec;
   auto file_size = boost::filesystem::file_size(src, ec);
   if (!ec)
   {
      result.reserve(file_size);
      std::copy(begin, end, std::back_inserter(result));
   }

   return result;
}
