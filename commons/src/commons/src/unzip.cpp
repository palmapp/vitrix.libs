﻿#include "unzip.h"
#include <fstream>
#include <iostream>
#ifdef ANDROID

#else
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/slice.hpp>
#endif

#include "binary_utils.h"
#include "make_string.h"
#include <array>
#include <sstream>
#include <stdexcept>
namespace
{
   struct local_file_header
   {
      uint32_t signature = 0; //     4 bytes  (0x04034b50)
      uint16_t version = 0;
      uint16_t flags = 0;
      uint16_t method = 0;
      uint16_t modtime = 0;
      uint16_t moddate = 0;
      uint32_t crc32 = 0;
      uint32_t compressed_size = 0;
      uint32_t uncompressed_size = 0;
      uint16_t file_name_len = 0;
      uint16_t extrafield_len = 0;

      enum
      {
         file_size = 4 + 5 * 2 + 3 * 4 + 2 * 2
      };
   };

   local_file_header read_header(std::istream &stream)
   {
      using namespace commons::bin;
      std::array<char, local_file_header::file_size> header_buffer;
      stream.read(header_buffer.data(), header_buffer.size());

      local_file_header header;

      const uint8_t *read_ptr = reinterpret_cast<uint8_t *>(header_buffer.data());
      read_le(read_ptr, header.signature);
      read_le(read_ptr, header.version);
      read_le(read_ptr, header.flags);
      read_le(read_ptr, header.method);
      read_le(read_ptr, header.modtime);
      read_le(read_ptr, header.moddate);
      read_le(read_ptr, header.crc32);
      read_le(read_ptr, header.compressed_size);
      read_le(read_ptr, header.uncompressed_size);
      read_le(read_ptr, header.file_name_len);
      read_le(read_ptr, header.extrafield_len);

      return header;
   }

} // namespace

void commons::unzip(const boost::filesystem::path &input_zip_path, const boost::filesystem::path &output_folder)
{
#ifdef ANDROID
   commons::android_unzip(input_zip_path, output_folder);
#else
   using namespace std;

   ifstream file(input_zip_path.c_str(), ios_base::in | ios_base::binary);
   std::string file_name_buffer;
   while (!file.eof())
   {
      auto header = read_header(file);
      if (header.signature == 0x04034b50)
      {
         file_name_buffer.clear();
         file_name_buffer.resize(header.file_name_len + 1);
         if (file.read(&file_name_buffer[0], header.file_name_len))
         {
            file_name_buffer[header.file_name_len] = '\0';
            file.seekg(header.extrafield_len, ios_base::cur);

            auto output_file_path = output_folder / file_name_buffer;
            if (header.compressed_size == 0 && file_name_buffer[header.file_name_len - 1] == '/')
            {
               boost::filesystem::create_directories(output_file_path);
               continue;
            }

            boost::filesystem::create_directories(output_file_path.parent_path());
            boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
            if ((header.method & 0x8) != 0 && header.compressed_size != 0)
            {
               boost::iostreams::zlib_params params{};
               params.noheader = true;
               in.push(boost::iostreams::zlib_decompressor{params});
            }

            in.push(boost::iostreams::slice(file, 0, header.compressed_size));

            std::ofstream output_file(output_file_path.c_str(), ios_base::out | ios_base::binary);
            if (output_file)
               boost::iostreams::copy(in, output_file);
            else
               throw std::runtime_error(make_string("unable to open file : ", output_file_path.string()));
         }
      }
      else
      {
         break;
      }
   }
#endif
}

std::string commons::unzip_item_to_memory(const boost::filesystem::path &input_zip_path, const std::string &item)
{
#ifdef ANDROID
   return {};
#else
   using namespace std;
   ifstream file(input_zip_path.c_str(), ios_base::in | ios_base::binary);
   std::string file_name_buffer;
   while (!file.eof())
   {
      auto header = read_header(file);
      if (header.signature == 0x04034b50)
      {
         file_name_buffer.clear();
         file_name_buffer.resize(header.file_name_len);
         if (file.read(&file_name_buffer[0], header.file_name_len))
         {
            file.seekg(header.extrafield_len, ios_base::cur);

            if (item == file_name_buffer)
            {
               if (header.compressed_size == 0)
               {
                  return {};
               }
               else
               {
                  boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
                  if ((header.method & 0x8) != 0 && header.compressed_size != 0)
                  {
                     boost::iostreams::zlib_params params{};
                     params.noheader = true;
                     in.push(boost::iostreams::zlib_decompressor{params});
                  }

                  in.push(boost::iostreams::slice(file, 0, header.compressed_size));

                  std::stringstream result{};
                  boost::iostreams::copy(in, result);

                  return result.str();
               }
            }
            else
            {
               file.seekg(header.compressed_size, ios_base::cur);
            }
         }
      }
      else
      {
         break;
      }
   }
   return {};
#endif
}
