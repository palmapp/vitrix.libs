#include "threading/semaphore.h"

commons::semaphore::semaphore(int initial_count) : counter_(initial_count) {}
void commons::semaphore::acquire() noexcept
{
   if (--counter_ < 0)
   {
      ++counter_;

      std::unique_lock lock{mutex_};
      cv_.wait(lock, [this]() noexcept { return counter_ > 0; });
      --counter_;
   }
}

void commons::semaphore::release() noexcept
{
   ++counter_;
   cv_.notify_one();
}

bool commons::semaphore::try_acquire() noexcept
{
   if (--counter_ < 0)
   {
      ++counter_;
      return false;
   }
   else
   {
      return true;
   }
}

bool commons::semaphore::try_acquire(std::chrono::steady_clock::duration duration) noexcept
{
   if (--counter_ < 0)
   {
      ++counter_;

      std::unique_lock lock{mutex_};
      if (cv_.wait_for(lock, duration, [this]() { return counter_ > 0; }))
      {
         --counter_;
         return true;
      }

      return false;
   }
   else
   {
      return true;
   }
}
