#include "process_utils.h"
#include <boost/process/child.hpp>
#include <system_error>

bool commons::process_exists(int pid) noexcept
{
   try
   {
      auto bp_pid = static_cast<boost::process::pid_t>(pid);
      boost::process::child proc(bp_pid);
      proc.detach();

      std::error_code ec;

      return proc.running(ec);
   }
   catch (...)
   {
      return false;
   }
}