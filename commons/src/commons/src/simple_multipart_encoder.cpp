#include "simple_multipart_encoder.h"
#include "guid.h"
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <chrono>
#include <future>

using namespace commons;

simple_multipart_encoder::simple_multipart_encoder()
{
   std::stringstream boundary_ss{};
   boundary_ss << "----CppRestSdkClient";
   boundary_ss << commons::guid::generate_unsafe();
   boundary_ = boundary_ss.str();
}

const std::string simple_multipart_encoder::generate()
{
   std::stringstream content;

   for (auto &param : params_)
   {
      content << "--";
      content << boundary_;
      content << "\r\nContent-Disposition: form-data; name=\"";
      content << param.first;
      content << "\"\r\n\r\n";
      content << param.second;
      content << "\r\n";
   }

   for (auto &file : files_)
   {
      content << "--";
      content << boundary_;
      content << "\r\nContent-Disposition: form-data; name=\"";
      content << file.first;
      content << "\"; filename=\"";
      content << file.second.filename().string();
      content << "\"\r\nContent-Type: application/octet-stream";
      content << "\r\n\r\n";
      {
         boost::filesystem::ifstream ifs{file.second};
         content << std::string{std::istreambuf_iterator<char>{ifs}, std::istreambuf_iterator<char>{}};
      }
   }

   content << "\r\n--";
   content << boundary_;
   content << "--\r\n";

   return content.str();
}
