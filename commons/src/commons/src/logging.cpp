#include "logging.h"
#include "cmd_parser.h"
#include "time_utils.h"

#include <boost/filesystem/path.hpp>
#include <sstream>

#include "make_string.h"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/date_time/string_parse_tree.hpp>
#include <easylogging++.h>
#include <regex>

using namespace commons;
namespace
{

   std::string escape_regex(const std::string &input)
   {
      std::regex specialChars{R"([-[\]{}()*+?.,\^$|#\s])"};

      return std::regex_replace(input, specialChars, R"(\$&)");
   }

   int find_max_log_file(const boost::filesystem::path &log_directory, const std::string &log_name,
                         const std::string &extension, int max_rollouts)
   {
      using namespace boost::filesystem;
      auto log_name_regex = std::regex(make_string("^", escape_regex(log_name), R"(\.(\d+))"));

      int max_file = 0;
      for (auto &file : directory_iterator(log_directory))
      {
         if (file.status().type() == file_type::regular_file)
         {
            auto file_path = file.path();
            if (file_path.extension().string() == extension)
            {
               auto file_name = file_path.stem().string();

               if (auto it = std::sregex_iterator(file_name.begin(), file_name.end(), log_name_regex);
                   it != std::sregex_iterator())
               {
                  auto match = *it;
                  if (match.size() == 2)
                  {
                     auto number_string = match[1].str();

                     if (auto number = std::atoi(number_string.c_str()); number > max_file && number <= max_rollouts)
                     {
                        max_file = number;
                     }
                  }
               }
            }
         }
      }

      return max_file;
   };
   void modernLogRollCallback(const char *filename, std::size_t size)
   {
      using namespace boost::filesystem;
      if (const auto ec = commons::roll_files(filename, 5))
      {
         std::cerr << "unable to roll file " << filename << ":" << ec.message() << std::endl;
      }
   }

} // namespace

boost::system::error_code commons::roll_files(boost::filesystem::path const &log_path, int max_rollouts)
{
   using namespace boost::filesystem;

   auto log_directory = log_path.parent_path();
   auto extension = log_path.extension().string();

   const auto get_log_file_path = [&](const std::string &log_name, int i) -> path
   {
      if (i == 0) return log_directory / make_string(log_name, extension);

      return log_directory / make_string(log_name, ".", i, extension);
   };

   const auto move_existing_logs_by_one = [&]() -> boost::system::error_code
   {
      auto ec = boost::system::error_code{};
      const auto log_name = log_path.stem().string();
      const int max_log_file = find_max_log_file(log_directory, log_name, extension, max_rollouts);

      for (int i = max_log_file; i >= 0; --i)
      {
         const auto src = get_log_file_path(log_name, i);
         if (i >= max_rollouts)
         {
            remove(src, ec);
         }
         else
         {
            const auto dest = get_log_file_path(log_name, i + 1);
            rename(src, dest, ec);
         }

         if (auto exists_ec = boost::system::error_code{}; ec && !exists(src, exists_ec))
         {
            ec.clear();
         }

         if (ec)
         {
            break;
         }
      }
      return ec;
   };

   return move_existing_logs_by_one();
}

void commons::init_easylogging(int argc, const char *argv[], const char *project_name,
                               const boost::filesystem::path &app_dir, std::vector<std::string> *result_enabled_loggers,
                               bool useModernLogRolling)
{
   START_EASYLOGGINGPP(argc, argv);

   std::stringstream default_cfg_buff;
   default_cfg_buff << R"xyz(*GLOBAL:
 FORMAT = [%levshort] [%fbase:%line] %msg
 Enabled = true
 To_Standard_Output = true
 To_File = false
)xyz";
   default_cfg_buff << "Filename = /var/log/" << project_name << ".log\n";
   auto cfgStr = default_cfg_buff.str();

   el::Configurations c;
   c.setToDefault();
   c.parseFromText(cfgStr);

   auto default_config_path = app_dir / "logging.conf";
   boost::filesystem::path log_path;
   if (cmd::try_get_arg(log_path, "-lc", argc, argv))
   {
      auto file = log_path.string();
      c.parseFromFile(file);
   }
   else if (boost::filesystem::exists(default_config_path))
   {
      c.parseFromFile(default_config_path.string());
   }

   if (c.hasConfiguration(el::ConfigurationType::MaxLogFileSize))
   {
      using namespace boost::filesystem;
      if (useModernLogRolling)
      {
         el::Helpers::installPreRollOutCallback(modernLogRollCallback);
      }
      else
      {
         el::Helpers::installPreRollOutCallback(
             [](const char *filename, std::size_t size)
             {
                path old_path{filename};

                path new_path = old_path.parent_path() / old_path.stem();
                create_directories(new_path);

                std::stringstream new_name{};
                new_name << commons::time_utils::now_in_millis();
                new_name << ".log";

                boost::filesystem::rename(boost::filesystem::path{filename}, new_path / new_name.str());
             });
      }
   }

   std::vector<std::string> enabled_loggers = {"default", "websocket", "request"};

   std::string loggers;
   if (cmd::try_get_arg(loggers, "-add-loggers", argc, argv))
   {
      std::vector<std::string> loggers_to_configure;
      boost::split(loggers_to_configure, loggers, boost::is_any_of(","));

      for (auto &logger : loggers_to_configure)
      {
         enabled_loggers.push_back(logger);
      }
   }

   for (auto &logger : enabled_loggers)
   {
      auto custom_cfg = app_dir / (logger + "-logging.conf");

      if (boost::filesystem::exists(custom_cfg))
      {
         el::Configurations custom = c;
         custom.parseFromFile(custom_cfg.string());
         el::Loggers::getLogger(logger)->configure(custom);
      }
      else
      {
         el::Loggers::getLogger(logger)->configure(c);
      }
   }

   LOG(INFO) << "enabled loggers:" << boost::algorithm::join(enabled_loggers, ", ");

   if (result_enabled_loggers != nullptr)
   {
      *result_enabled_loggers = std::move(enabled_loggers);
   }
}

void commons::init_easylogging_rolling()
{
   el::Helpers::installPreRollOutCallback(modernLogRollCallback);
}
