//
// Created by jhrub on 13.03.2022.
//
#include "data/transform_data_stream.h"
#include "gsl/byte"

using namespace commons::basic_types;

commons::data::transform_data_stream::transform_data_stream(std::unique_ptr<data_stream> own_stream,
                                                            std::vector<transform_operation> operations)
   : own_stream_(std::move(own_stream)), stream_ref_(*own_stream_), operations_(std::move(operations))
{
   init();
}

commons::data::transform_data_stream::transform_data_stream(data_stream &stream_ref,
                                                            std::vector<transform_operation> operations)
   : own_stream_(nullptr), stream_ref_(stream_ref), operations_(std::move(operations))
{
   init();
}

void commons::data::transform_data_stream::init()
{
   index_columns();
   prepare_columns();
   build_stable_column_names();

   temp_row_.reserve(column_names_.size());
}

commons::basic_types::optional<commons::basic_types::array_view<commons::basic_types::string_view>>
commons::data::transform_data_stream::get_column_names()
{
   return {array_view<string_view>{column_names_}};
}

commons::basic_types::optional<commons::data::data_row> commons::data::transform_data_stream::next_row()
{
   auto row = stream_ref_.next_row();
   if (!row.has_value())
   {
      return {};
   }

   auto &current_row = row.value();
   auto &temp_row = temp_row_;
   auto &column_index = column_index_;
   temp_row.clear();

   lookup_column_func column_lookup{[&](const std::string &column) -> optional_field_value
   {
      if (auto it = column_index.find(column);
         it != column_index.end() && it->second < current_row.size())
      {
         return current_row[it->second];
      }
      return {};
   }};

   auto &operations = operations_;
   auto &source_indexes = operation_index_;
   do_prepend_columns(current_row, temp_row, operations, source_indexes, column_lookup);
   do_replace_or_copy(current_row, temp_row, operations, source_indexes, column_lookup);
   do_append_columns(current_row, temp_row, operations, source_indexes, column_lookup);

   return {data_row{temp_row_}};
}

void commons::data::transform_data_stream::do_replace_or_copy(const commons::data::data_row &current_row,
                                                              std::vector<optional_field_value> &temp_row,
                                                              const std::vector<transform_operation> &operations,
                                                              const std::vector<size_t> &source_indexes,
                                                              lookup_column_func const &lookup)
{
   for (size_t i = 0, current_row_size = current_row.size(); i != current_row_size; ++i)
   {
      bool replace = false;

      for (size_t o = 0, operation_size = operations.size(); o != operation_size; ++o)
      {
         auto &current_operation = operations[o];
         auto source_index = source_indexes[o];
         if (source_index == i)
         {
            std::visit(
               [&](auto &op) mutable
               {
                  using operation_type = std::decay_t<decltype(op)>;
                  if constexpr (std::is_same_v<operation_type, transform_operation::replace_column>)
                  {
                     auto source_index = source_indexes[o];
                     if (source_index < current_row.size())
                     {
                        temp_row.push_back(op.transformation(current_row[source_index], lookup));
                     }
                     else
                     {
                        temp_row.push_back({});
                     }
                     replace = true;
                  }

                  if constexpr (std::is_same_v<operation_type, transform_operation::remove_column>)
                  {
                     replace = true;
                  }
               },
               current_operation.operation);

            if (replace)
            {
               break;
            }
         }
      }

      if (!replace)
      {
         temp_row.push_back(current_row[i]);
      }
   }
}

void commons::data::transform_data_stream::do_prepend_columns(const commons::data::data_row &current_row,
                                                              vector<optional_field_value> &temp_row,
                                                              const std::vector<transform_operation> &operations,
                                                              const std::vector<size_t> &source_indexes,
                                                              lookup_column_func const &lookup)
{

   for (int i = 0, prepended = prepended_; i != prepended; ++i)
   {
      for (size_t o = 0, operation_size = operations.size(); o != operation_size; ++o)
      {
         auto &current_operation = operations[o];
         auto source_index = source_indexes[o];

         std::visit(
            [&](auto &op) mutable
            {
               using operation_type = std::decay_t<decltype(op)>;
               if constexpr (std::is_same_v<operation_type, transform_operation::prepend_column>)
               {
                  if (source_index < current_row.size())
                  {
                     temp_row.insert(temp_row.begin(), op.transformation(current_row[source_index], lookup));
                  }
                  else
                  {
                     temp_row.insert(temp_row.begin(), {});
                  }
               }
            },
            current_operation.operation);
      }
   }
}

void commons::data::transform_data_stream::do_append_columns(const data_row &current_row,
                                                             vector<optional_field_value> &temp_row,
                                                             const vector<transform_operation> &operations,
                                                             const vector<size_t> &source_indexes,
                                                             lookup_column_func const &lookup)
{

   for (int i = 0, appended = appended_; i != appended; ++i)
   {
      for (size_t o = 0, operation_size = operations.size(); o != operation_size; ++o)
      {
         auto &current_operation = operations[o];
         auto source_index = source_indexes[o];

         std::visit(
            [&](auto &op) mutable
            {
               using operation_type = std::decay_t<decltype(op)>;
               if constexpr (std::is_same_v<operation_type, transform_operation::add_column>)
               {
                  if (source_index < current_row.size())
                  {
                     temp_row.push_back(op.transformation(current_row[source_index], lookup));
                  }
                  else
                  {
                     temp_row.push_back({});
                  }
               }
            },
            current_operation.operation);
      }
   }
}

void commons::data::transform_data_stream::index_columns()
{
   auto names = stream_ref_.get_column_names();
   if (!names.has_value()) throw std::logic_error{"underlying data stream must support column names"};

   for (auto &column : names.value())
   {
      source_columns_.push_back(static_cast<string>(column));
      columns_.push_back(static_cast<string>(column));
   }

   for (size_t i = 0, size = columns_.size(); i != size; ++i)
   {
      column_index_[columns_[i]] = i;
   }
}

void commons::data::transform_data_stream::build_stable_column_names()
{
   for (auto &column : columns_)
   {
      column_names_.push_back(column);
   }
}

void commons::data::transform_data_stream::prepare_columns()
{
   for (auto &operation : operations_)
   {
      std::visit([this](auto &operation) mutable
      {
         operation.prepare_columns(*this);
      }, operation.operation);
   }
}

void commons::data::transform_operation::add_column::prepare_columns(commons::data::transform_data_stream &self)
{
   self.columns_.push_back(name);
   self.operation_index_.push_back(source.index(self));
   ++self.appended_;
}

void commons::data::transform_operation::prepend_column::prepare_columns(commons::data::transform_data_stream &self)
{
   self.columns_.insert(self.columns_.begin(), name);
   self.operation_index_.push_back(source.index(self));
   ++self.prepended_;
}

void commons::data::transform_operation::replace_column::prepare_columns(commons::data::transform_data_stream &self)
{
   auto source_index = source.index(self);
   auto &source_column_name = self.source_columns_.at(source_index);
   auto &dest_column_name = self.columns_.at(source_index + self.prepended_ - self.removed_);
   if (source_column_name == dest_column_name)
   {
      if (!name.empty()) self.columns_.at(source_index) = name;
   }
   else
   {
      throw std::logic_error{"unable to find column to replace"};
   }

   self.operation_index_.push_back(source_index);
}

void commons::data::transform_operation::remove_column::prepare_columns(commons::data::transform_data_stream &self)
{
   auto source_index = source.index(self);
   auto remove_index = source_index + self.prepended_ - self.removed_;
   if (remove_index < self.columns_.size())
   {
      self.columns_.erase(self.columns_.begin() + remove_index);
      ++self.removed_;
   }
   else
   {
      self.operation_index_.push_back(source_index);
   }
}

void commons::data::transform_operation::rename_column::prepare_columns(commons::data::transform_data_stream &self)
{
   auto source_index = source.index(self);
   auto &source_column_name = self.source_columns_.at(source_index);
   auto &dest_column_name = self.columns_.at(source_index + self.prepended_ - self.removed_);
   if (source_column_name == dest_column_name)
   {
      self.columns_.at(source_index) = name;
   }
   else
   {
      throw std::logic_error{"unable to find column to replace"};
   }

   self.operation_index_.push_back(source_index);
}

size_t commons::data::column_source::index(const commons::data::transform_data_stream &self)
{
   size_t result = std::numeric_limits<size_t>::max();
   std::visit(
      [&](auto &source) mutable
      {
         using source_type = std::decay_t<decltype(source)>;
         if constexpr (std::is_same_v<column_source::at_index, source_type>)
         {
            if (source.index<0)
                             {
                                // indexing from back of the row
                                result = self.source_columns_.size() + 1 + source.index;
                             }
             else
                             {
                                result = source.index;
                             }


         }
         else
         {
            if (auto it = self.column_index_.find(source.name); it != self.column_index_.end())
            {
               result = it->second;
            }
         }
      },
      source);

   if (result == std::numeric_limits<size_t>::max())
   {
      throw std::logic_error{"unable to index column"};
   }
   return result;
}
