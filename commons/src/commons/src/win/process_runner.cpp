#include "win/process_runner.h"

#include <Windows.h>

#include <charconv>
#include <codecvt>
#include <sstream>

#include <gsl/narrow>

namespace
{
   BOOL CreateProcessInJob(HANDLE hJob, LPCWSTR lpApplicationName, LPWSTR lpCommandLine,
                           LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes,
                           BOOL bInheritHandles, DWORD dwCreationFlags, LPVOID lpEnvironment,
                           LPCWSTR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo, LPPROCESS_INFORMATION ppi)
   {
      BOOL fRc =
          CreateProcessW(lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes, bInheritHandles,
                         dwCreationFlags | CREATE_SUSPENDED, lpEnvironment, lpCurrentDirectory, lpStartupInfo, ppi);
      if (fRc)
      {
         fRc = AssignProcessToJobObject(hJob, ppi->hProcess);
         if (fRc && !(dwCreationFlags & CREATE_SUSPENDED))
         {
            fRc = ResumeThread(ppi->hThread) != (DWORD)-1;
         }
         if (!fRc)
         {
            TerminateProcess(ppi->hProcess, 0);
            CloseHandle(ppi->hProcess);
            CloseHandle(ppi->hThread);
            ppi->hProcess = ppi->hThread = nullptr;
         }
      }
      return fRc;
   }
} // namespace

int commons::windows::start_process_and_wait(const commons::windows::startup_info &info)
{
   HANDLE hJob = CreateJobObject(nullptr, nullptr);
   JOBOBJECT_EXTENDED_LIMIT_INFORMATION jobInfo = {};
   jobInfo.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
   SetInformationJobObject(hJob, JobObjectExtendedLimitInformation, &jobInfo, sizeof(jobInfo));

   STARTUPINFOW startupInfo;
   ZeroMemory(&startupInfo, sizeof(STARTUPINFO));

   if (!info.ignore_stdio)
   {
      startupInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);
      startupInfo.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
      startupInfo.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
   }

   PROCESS_INFORMATION proc_info;
   ZeroMemory(&proc_info, sizeof(PROCESS_INFORMATION));

   std::wstringstream cmd_stream;
   cmd_stream << L'"' << info.executable.wstring() << L'\"';

   if (!info.w_arguments.empty())
   {
      cmd_stream << L' ' << info.w_arguments;
   }
   else if (!info.arguments.empty())
   {
      std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
      cmd_stream << conv.from_bytes(info.arguments.data(), info.arguments.data() + info.arguments.size());
   }

   auto cmd = cmd_stream.str();
   DWORD exit_code = static_cast<DWORD>(-1);
   if (CreateProcessInJob(hJob, info.executable.c_str(), &cmd[0], nullptr, nullptr, FALSE, NORMAL_PRIORITY_CLASS,
                          nullptr, info.working_directory.c_str(), &startupInfo, &proc_info) == 1)
   {
      std::int64_t milliseconds = -1;
      if (info.timeout.has_value())
         milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(info.timeout.value()).count();

      WaitForSingleObject(proc_info.hProcess, gsl::narrow<DWORD>(milliseconds));

      GetExitCodeProcess(proc_info.hProcess, &exit_code);

      CloseHandle(proc_info.hThread);
      CloseHandle(proc_info.hProcess);
   }
   CloseHandle(hJob);

   return gsl::narrow<std::int32_t>(exit_code);
}
