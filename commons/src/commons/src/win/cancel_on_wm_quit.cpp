//
// Created by jhrub on 21.02.2023.
//
#include "win/cancel_on_wm_quit.h"

#ifndef _WIN32
std::optional<std::jthread> commons::cancel_on_wm_quit(std::stop_source &source)
{
   return {};
}
#else
#include <easylogging++.h>

#include <Windows.h>
#include <vector>

namespace
{
   LRESULT WINAPI WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
   {
      return DefWindowProc(hWnd, message, wParam, lParam);
   }

   BOOL InitEmptyWindow(HWND &hwnd, __in HINSTANCE hInstance, __in_z LPCTSTR lpcszClassName)
   {
      WNDCLASSEX WindowClassEx;

      ZeroMemory(&WindowClassEx, sizeof(WNDCLASSEX));
      /*********/
      WindowClassEx.cbSize = sizeof(WNDCLASSEX);
      WindowClassEx.lpfnWndProc = WindowProc;
      WindowClassEx.hInstance = hInstance;
      WindowClassEx.lpszClassName = lpcszClassName;

      /*********/
      if (RegisterClassEx(&WindowClassEx) != 0)
      {
         // Create a _MESSAGE ONLY_ window
         hwnd = CreateWindowEx(0, lpcszClassName, NULL, 0, 0, 0, 0, 0,HWND_MESSAGE , NULL, hInstance, NULL);
         if (hwnd)
         {
            ShowWindow(hwnd, SW_SHOW);
            SetTimer(hwnd, NULL, 500, NULL);
            return TRUE;
         }
         else
            UnregisterClass(lpcszClassName, hInstance);
      }
      return FALSE;
   }

} // namespace

std::optional<std::jthread> commons::cancel_on_wm_quit(std::stop_source &source)
{
   return {std::jthread{[&](std::stop_token token)
                        {
                           auto hInstance = GetModuleHandle(NULL);
                           HWND hwnd;
                           if (InitEmptyWindow(hwnd, hInstance, "exit-window"))
                           {
                              MSG msg;
                              while ((!source.stop_requested() || !token.stop_requested()) &&
                                     GetMessage(&msg, hwnd, 0, 0))
                              {
                                 if (msg.message == 0)
                                 {
                                    LOG(INFO) << "WM_QUIT ... LOOP";
                                    break;
                                 }

                                 TranslateMessage(&msg);
                                 DispatchMessage(&msg);
                              }
                           }

                           LOG(INFO) << "WM_QUIT is requesting stop";

                           source.request_stop();
                           LOG(INFO) << "WM_QUIT stop requested";
                        }}};
}

#endif