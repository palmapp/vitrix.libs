//
// Created by jhrub on 26.07.2022.
//
#include "thread_name.h"
#include <easylogging++.h>

#include <Windows.h>
#include <codecvt>

#include <locale>

namespace
{
   std::wstring convert_to_wstring(std::string const &str)
   {
      std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
      return converter.from_bytes(str);
   }
} // namespace

void commons::set_this_thread_name(std::string const &name)
{
#if (WINVER >= NTDDI_WIN10)
   auto wname = convert_to_wstring(name);

   HRESULT r;
   r = SetThreadDescription(GetCurrentThread(), wname.data());

   if (!SUCCEEDED(r))
   {
      LOG(WARNING) << "unable to set thread name to : " << name;
   }
#endif
}
