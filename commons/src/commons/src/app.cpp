#include "app.h"
using namespace commons;

app::app(boost::application::context &ctx) : context_(ctx) {}

int app::operator()()
{
   thread_ = std::thread(&app::run, this);
   context_.find<boost::application::wait_for_termination_request>()->wait();

   thread_.join();

   return 0;
}
