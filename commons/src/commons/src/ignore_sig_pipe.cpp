#include "ignore_sig_pipe.h"
#ifndef WIN32
#include <cstring>
#include <signal.h>
#include <unistd.h>

void commons::ignore_sig_pipe() noexcept
{
   struct sigaction sa;
   std::memset(&sa, 0, sizeof(sa));
   sa.sa_handler = SIG_IGN;
   static_cast<void>(sigaction(SIGPIPE, &sa, NULL));
}
#else
void commons::ignore_sig_pipe() noexcept
{
   // not necessary to implement this on windows
}
#endif