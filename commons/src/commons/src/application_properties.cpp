#include "application_properties.h"

commons::application_properties::application_properties(commons::application_properties_settings settings)
{
   load_properties(seek_properties_files(settings));
}

std::optional<std::string_view> commons::application_properties::try_get_value(const std::string &key) const
{
   for (auto const &map : properties_chain_)
   {
      if (auto it = map.find(key); it != map.end() && !it->second.empty())
      {
         return {it->second};
      }
   }
   return {};
}
std::vector<boost::filesystem::path>
commons::application_properties::seek_properties_files(const commons::application_properties_settings &settings) const
{
   std::vector<boost::filesystem::path> result;

   auto current_path = boost::filesystem::absolute(settings.base_path);
   const auto root_path = current_path.root_path();

   for (int i = -1; i < settings.levels; ++i)
   {
      expand_properties_path_variants(result, settings, current_path);
      current_path = current_path.parent_path();

      if (current_path == root_path) break;
   }

   return result;
}

void commons::application_properties::expand_properties_path_variants(
    std::vector<boost::filesystem::path> &result, commons::application_properties_settings const &settings,
    boost::filesystem::path const &current_path) const
{
   for (auto const &variant : settings.properties_variants)
   {
      auto prop_file_path = current_path / variant;
      if (boost::filesystem::exists(prop_file_path))
      {
         result.push_back(std::move(prop_file_path));
      }
   }
}

void commons::application_properties::load_properties(const std::vector<boost::filesystem::path> &files)
{
   properties_chain_.reserve(files.size());

   for (auto const &path : files)
   {
      auto cfg = parse_config_file({path.c_str()});
      if (!cfg.empty())
      {
         properties_chain_.push_back(std::move(cfg));
      }
   }
}
