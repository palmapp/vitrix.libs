#include "process_manager.h"
#include <cstdint>
#include <vector>

#include <chrono>
#include <fstream>
#include <stdexcept>
#include <thread>

#include <boost/asio.hpp>

#ifdef __linux__
#include <boost/asio/posix/stream_descriptor.hpp>
#define VITR_TTY_SUPPORTED
#endif

#include <boost/lexical_cast.hpp>
#include <boost/process.hpp>
#include <boost/process/env.hpp>

#include "file_system.h"
#include "guid.h"

#include <easylogging++.h>
using namespace std::chrono_literals;
using namespace commons;
namespace bp = boost::process;
namespace ba = boost::asio;
using error_code = boost::system::error_code;

namespace commons
{
   struct process_base
   {
      process_output &output;
      std::int64_t id;
      std::vector<std::uint8_t> out_buffer;

      process_base(process_output &_output, std::int64_t _id) : output(_output), id(_id)
      {
         out_buffer.resize(4096);
      }
      virtual ~process_base() = default;

      virtual void write_std_in(const std::uint8_t *buffer, std::size_t size) = 0;
      virtual void end() noexcept = 0;

      virtual bool try_get_exit_code(int64_t &result) = 0;
   };

#ifdef VITR_TTY_SUPPORTED
   struct fifo_process : public process_base
   {
      ba::io_context &io;
      std::shared_ptr<boost::asio::posix::stream_descriptor> out_fifo;
      std::shared_ptr<std::ofstream> input_fifo;

      std::string pid;
      std::string pid_file;
      bool exited = false;

      fifo_process(ba::io_context &io_service, process_output &_output, std::int64_t _id, const std::string &command)
          : process_base(_output, _id), io(io_service)
      {
         auto fifo_id = guid::generate_unsafe();
         std::stringstream fifo_in_buf;
         fifo_in_buf << "/tmp/" << fifo_id << ".in";
         auto in_file = fifo_in_buf.str();

         std::stringstream fifo_out_buf;
         fifo_out_buf << "/tmp/" << fifo_id << ".out";
         auto out_file = fifo_out_buf.str();

         std::stringstream pid_buf;
         pid_buf << "/tmp/" << fifo_id << ".pid";
         pid_file = pid_buf.str();

         std::stringstream cmd_buff;
         cmd_buff << "empty -f -i " << in_file << " -o " << out_file << " -p " << pid_file << " " << command;
         auto cmd = cmd_buff.str();
         LOG(DEBUG) << cmd;

         {
            bp::env["TERM"] = "xterm-256color";
            bp::child handle(cmd);
            handle.wait();
         }

         init_empty_process_pid();

         LOG(DEBUG) << "empty process has following pid=" << pid;

         begin_read(io_service, out_file);
         input_fifo = std::make_shared<std::ofstream>(in_file.c_str());
      }

      void init_empty_process_pid()
      {
         pid = read_file(pid_file);
         boost::trim(pid);
         if (pid.empty()) throw std::runtime_error("unable to get pid of empty process");
      }

      ~fifo_process() noexcept
      {
         end();
      }

      void write_std_in(const std::uint8_t *buffer, std::size_t size) final
      {
         LOG(DEBUG) << "writing to input fifo";
         input_fifo->write(reinterpret_cast<const char *>(buffer), size);
         input_fifo->flush();
      }

      void end() noexcept final
      {
         try
         {
            try
            {
               auto ipid = boost::lexical_cast<std::int64_t>(pid);

               auto res = ::kill(ipid, SIGTERM);
               if (res == 0)
               {
                  std::this_thread::sleep_for(100ms);
                  int attempts = 50;
                  while (!exited && --attempts)
                  {
                     ::kill(ipid, 0);
                     if (errno == ESRCH)
                     {
                        exited = true;
                     }
                     else
                     {
                        std::this_thread::sleep_for(100ms);
                     }
                  }

                  if (attempts == 0)
                  {
                     ::kill(ipid, SIGKILL);
                  }
               }

               exited = true;
            }
            catch (const std::exception &ex)
            {
               LOG(ERROR) << "unable to terminate process due: " << ex.what();
            }
            catch (...)
            {
               LOG(ERROR) << "unable to terminate process due unknown exception";
            }
         }
         catch (...)
         {
            // just mega ignore this
         }
      }

      bool try_get_exit_code(int64_t &result)
      {
         if (exited || read_file(pid_file).empty())
         {
            result = 0;
            return true;
         }
         return false;
      }

    private:
      void begin_read(ba::io_context &io_service, const std::string &fifo_file)
      {
         int fd = -1;
         int attempts = 60;
         while (fd < 0 && attempts > 0)
         {
            fd = open(fifo_file.c_str(), O_RDONLY);
            if (fd < 0) std::this_thread::sleep_for(50ms);

            --attempts;
         }

         if (fd < 0 && attempts == 0)
         {
            LOG(ERROR) << "attempts to open fifo exceeded";
            throw std::runtime_error("attempts to open fifo exceeded");
         }

         LOG(DEBUG) << "file opened" << fifo_file;

         if (fd >= 0)
         {
            out_fifo = std::make_shared<boost::asio::posix::stream_descriptor>(io_service, fd);
            out_fifo->non_blocking(true);
            LOG(DEBUG) << "created stream descriptor" << fifo_file;

            read_out_fifo();
         }
         else
         {
            LOG(ERROR) << "unable to open output fifo file " << fifo_file;
         }
      }

      void read_out_fifo()
      {
         out_fifo->async_read_some(boost::asio::buffer(out_buffer), [this](const error_code &ec, std::size_t bytes) {
            if (!ec)
            {
               LOG(DEBUG) << "read " << std::string(reinterpret_cast<const char *>(out_buffer.data()), bytes);
               output.on_std_out(id, out_buffer.data(), bytes);
               read_out_fifo();
            }
            else
            {
               LOG(WARNING) << "fifo read failed: " << ec.message();
            }
         });
      }
   };
#endif

   struct stdio_process : public process_base
   {
      bp::async_pipe out_pipe;
      bp::async_pipe err_pipe;
      bp::opstream in;
      bp::child handle;
      std::vector<std::uint8_t> err_buffer;

      stdio_process(ba::io_context &io_service, process_output &_output, std::int64_t _id, const std::string &command)
          : process_base(_output, _id), out_pipe(io_service), err_pipe(io_service),
            handle(command, bp::std_out > out_pipe, bp::std_err > err_pipe, bp::std_in < in)
      {
         err_buffer.resize(4096);

         begin_read();
      }

      ~stdio_process() noexcept
      {
         end();
      }

      void end() noexcept final
      {
         try
         {
            try
            {
               if (handle.running())
               {
                  handle.terminate();
                  handle.wait();
               }
            }
            catch (const std::exception &ex)
            {
               LOG(ERROR) << "unable to terminate process due: " << ex.what();
            }
            catch (...)
            {
               LOG(ERROR) << "unable to terminate process due unknown exception";
            }
         }
         catch (...)
         {
            // just mega ignore this
         }
      }

      bool try_get_exit_code(int64_t &result)
      {
         if (!handle.running())
         {
            result = handle.exit_code();
            return true;
         }
         return false;
      }

      void write_std_in(const std::uint8_t *buffer, std::size_t size) final
      {
         in.write(reinterpret_cast<const char *>(buffer), size);
         in.flush();
      }

    private:
      void begin_read()
      {
         read_std_out();
         read_std_err();
      }

      void read_std_out()
      {
         out_pipe.async_read_some(ba::buffer(out_buffer), [this](const error_code &ec, std::size_t bytes) {
            if (!ec)
            {
               output.on_std_out(id, out_buffer.data(), bytes);
               read_std_out();
            }
         });
      }

      void read_std_err()
      {
         err_pipe.async_read_some(ba::buffer(err_buffer), [this](const error_code &ec, std::size_t bytes) {
            if (!ec)
            {
               output.on_std_err(id, err_buffer.data(), bytes);
               read_std_err();
            }
         });
      }
   };
} // namespace commons

process_manager::process_manager(boost::asio::io_context &io_service, process_output &output)
    : io_service_(io_service), output_(output)
{
}

process_manager::~process_manager()
{
   end_all();
}

void process_manager::start_process(std::int64_t id, const std::string &command)
{
   try
   {
      auto proc = std::make_unique<stdio_process>(io_service_, output_, id, command);

      if (proc->handle.running())
      {
         auto &proc_ref = *proc;
         running_processes_[id].reset(proc.release());
         output_.on_started(id, static_cast<std::int64_t>(proc_ref.handle.id()));
      }
      else
      {
         output_.on_failed_to_start(id, "process is not running");
      }
   }
   catch (const std::exception &ex)
   {
      output_.on_failed_to_start(id, ex.what());
   }
}

void process_manager::start_process_in_term(std::int64_t id, const std::string &command)
{
#ifdef VITR_TTY_SUPPORTED
   try
   {
      auto proc = std::make_unique<fifo_process>(io_service_, output_, id, command);

      auto &proc_ref = *proc;
      running_processes_[id].reset(proc.release());
      output_.on_started(id, boost::lexical_cast<std::int64_t>(proc_ref.pid));
      LOG(DEBUG) << "tty process started " << command;
   }
   catch (const std::exception &ex)
   {
      LOG(ERROR) << "tty process failed to start " << ex.what();
      output_.on_failed_to_start(id, ex.what());
   }
#else
   throw std::logic_error("pseudo terminal processes are not supported on this platform");
#endif
}

void process_manager::end_process(std::int64_t id)
{
   auto it = running_processes_.find(id);
   if (it != running_processes_.end())
   {
      it->second->end();
   }
}

void process_manager::end_not_in_list_processes(std::vector<std::int64_t> ids)
{
   std::vector<std::int64_t> term_list;
   for (auto &kv : running_processes_)
   {
      if (std::find(ids.begin(), ids.end(), kv.first) == ids.end())
      {
         term_list.push_back(kv.first);
      }
   }

   for (auto &id : term_list)
   {
      end_process(id);
   }
}

void process_manager::feed_std_in(std::int64_t id, const std::uint8_t *buffer, std::size_t len)
{
   auto it = running_processes_.find(id);
   if (it != running_processes_.end())
   {
      it->second->write_std_in(buffer, len);
   }
}

void process_manager::feed_std_in(std::int64_t id, const std::string &value)
{
   feed_std_in(id, reinterpret_cast<const std::uint8_t *>(value.data()), value.size());
}

void process_manager::validate_processes()
{
   std::vector<std::int64_t> term_list;

   for (auto &kv : running_processes_)
   {
      int64_t exit_code = 0;
      if (kv.second->try_get_exit_code(exit_code))
      {
         output_.on_ended(kv.first, exit_code);
         term_list.push_back(kv.first);
      }
   }

   for (auto &id : term_list)
   {
      running_processes_.erase(id);
   }
}

void process_manager::end_all()
{
   for (auto &kv : running_processes_)
   {
      kv.second->end();
   }

   validate_processes();
}
