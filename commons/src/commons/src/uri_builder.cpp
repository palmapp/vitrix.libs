#include "uri_builder.h"
#include "url_fetch.h"

using namespace commons::url;

uri_builder::uri_builder(const std::string &url)
{
   web::uri uri = web::uri{normalize_string(url)};
   uri_builder_ = web::uri_builder{web::uri{normalize_string(url)}};
}

uri_builder::uri_builder(const web::uri &uri)
{
   uri_builder_ = web::uri_builder{uri};
}

uri_builder::uri_builder(const uri_builder &old_uri_builder)
{
   uri_builder_ = web::uri_builder{old_uri_builder.to_uri()};
}

uri_builder::uri_builder(uri_builder &&old_uri_builder) : uri_builder_(old_uri_builder.uri_builder_) {}

const uri_builder &uri_builder::operator=(const uri_builder &new_uri_builder)
{
   uri_builder_ = web::uri_builder{new_uri_builder.uri_builder_.to_uri()};
   return *this;
}

const uri_builder &uri_builder::operator=(uri_builder &&new_uri_builder)
{
   uri_builder_ = new_uri_builder.uri_builder_;
   return *this;
}

uri_builder uri_builder::make_relative(const std::string &path, const std::map<std::string, std::string> &query,
                                       bool encode)
{
   uri_builder new_uri_builder = uri_builder{uri_builder_.to_uri()};
   new_uri_builder.uri_builder_.append_path(normalize_string(path), encode);
   append_query(new_uri_builder.uri_builder_, query, encode);
   return new_uri_builder;
}

uri_builder uri_builder::add_query(const std::map<std::string, std::string> &query, bool encode)
{
   uri_builder new_uri_builder = uri_builder{uri_builder_.to_uri()};
   append_query(new_uri_builder.uri_builder_, query, encode);
   return new_uri_builder;
}

std::string uri_builder::to_string() const
{
   return utility::conversions::to_utf8string(uri_builder_.to_string());
}

web::uri uri_builder::to_uri() const
{
   return uri_builder_.to_uri();
}

void uri_builder::append_query(web::uri_builder &uri_builder, const std::map<std::string, std::string> &query,
                               bool encode)
{
   for (auto &q : query)
   {
      uri_builder.append_query(normalize_string(q.first), normalize_string(q.second), encode);
   }
}
