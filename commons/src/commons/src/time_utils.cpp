#include "time_utils.h"
#include <fmt/chrono.h>

std::string commons::time_utils::format_utc_iso_time(std::int64_t time_in_millis, bool time_zone_offset)
{
   return format_utc_iso_time(from_millis(time_in_millis), time_zone_offset);
}

std::string commons::time_utils::format_utc_iso_time(std::chrono::system_clock::time_point time_point,
                                                     bool time_zone_offset)
{
   if (time_zone_offset)
      return fmt::format("{:%FT%TZ}", fmt::gmtime(time_point));
   else
      return fmt::format("{:%FT%T}", fmt::gmtime(time_point));
}

std::string commons::time_utils::format_local_iso_time(std::int64_t time_in_millis, bool time_zone_offset)
{
   return format_local_iso_time(from_millis(time_in_millis), time_zone_offset);
}

std::string commons::time_utils::format_local_iso_time(std::chrono::system_clock::time_point time_point,
                                                       bool time_zone_offset)
{
   if (time_zone_offset)
   {
      return fmt::format("{:%FT%T%Ez}", time_point);
   }
   else
   {
      return fmt::format("{:%FT%T}", time_point);
   }
}
