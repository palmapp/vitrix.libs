#include "url_fetch.h"
#include <codecvt>
#include <cpprest/filestream.h>
#include <cpprest/http_client.h>

using namespace utility;              // Common utilities like string conversions
using namespace web;                  // Common features like URIs.
using namespace web::http;            // Common HTTP functionality
using namespace web::http::client;    // HTTP client features
using namespace concurrency::streams; // Asynchronous streams

namespace
{

   http_request create_request(const commons::url::header_container &headers)
   {
      http_request req;
      req.set_method(methods::GET);

      for (auto &kv : headers)
      {
         auto &req_headers = req.headers();
         req_headers.add(commons::url::normalize_string(kv.first), commons::url::normalize_string(kv.second));
      }

      return req;
   }
} // namespace

#ifdef WIN32
std::wstring commons::url::normalize_string(const std::string &str)
{
   std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
   return converter.from_bytes(str);
}
#else
const std::string &commons::url::normalize_string(const std::string &str)
{
   return str;
}
#endif

int commons::url::fetch_to_string(std::string &result, const std::string &url, const header_container &headers)
{
   int status = 0;

   http_client_config cfg;
   cfg.set_validate_certificates(false);
   cfg.set_proxy(web_proxy::use_auto_discovery);

   http_client client(uri{normalize_string(url)}, cfg);

   container_buffer<std::string> buffer;
   auto task = client.request(create_request(headers)).then([&](http_response response)
   {
      status = static_cast<int>(response.status_code());
      return response.body().read_to_end(buffer);
   });

   task.wait();
   result = buffer.collection();
   return status;
}

int commons::url::send_get_request(const std::string &url, const header_container &headers)
{
   int status = 0;

   http_client_config cfg;
   cfg.set_validate_certificates(false);
   cfg.set_proxy(web_proxy::use_auto_discovery);

   http_client client(uri{normalize_string(url)}, cfg);

   auto task = client.request(create_request(headers)).then([&](http_response response)
   {
      status = static_cast<int>(response.status_code());
   });

   task.wait();
   return status;
}

int commons::url::fetch_to_file(const boost::filesystem::path &output_path, const std::string &url,
                                const header_container &headers)
{
   int status = 0;
   ostream of;
   pplx::task<void> task = fstream::open_ostream(output_path.c_str())
                           .then([&of, &url, &headers](ostream outFile)
                           {
                              of = outFile;
                              http_client_config cfg;
                              cfg.set_validate_certificates(false);
                              cfg.set_proxy(web_proxy::use_auto_discovery);

                              http_client client(uri{normalize_string(url)}, cfg);
                              return client.request(create_request(headers));
                           })
                           .then([&](http_response response)
                           {
                              status = static_cast<int>(response.status_code());
                              return response.body().read_to_end(of.streambuf());
                           })
                           .then([&](size_t)
                           {
                              return of.close();
                           });

   task.wait();

   return status;
}
