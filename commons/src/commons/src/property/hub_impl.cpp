#include "property/detail/hub_impl.h"

#include <easylogging++.h>
#include <iostream>

using namespace spider::property;

namespace
{
   template <class ContainerT, class IdT>
   decltype(typename ContainerT::value_type{}.second) &get_or_create(ContainerT &container, const IdT &id)
   {
      auto it =
         std::find_if(std::begin(container), std::end(container), [&](const auto &el)
         {
            return el.first == id;
         });

      if (it == std::end(container))
      {
         container.emplace_back();
         auto &el = container.back();
         el.first = id;

         return el.second;
      }
      else
      {
         return it->second;
      }
   }

   void update_and_track_changes(property_list &changes, property_list &data, const property_list &updates)
   {
      for (auto &prop : updates)
      {
         auto &val = get_or_create(data, prop.first);

         if (!(val == prop.second))
         {
            val = prop.second;
            changes.push_back(std::make_pair(prop.first, prop.second));
         }
      }
   }

} // namespace

commons::property::property_hub::property_hub()
{
}

property_list &commons::property::property_hub::get_for_context(const property_context &ctx)
{
   auto &scope = get_or_create(_data[ctx.scope], ctx.id);
   return get_or_create(scope, ctx.scope_id);
}

const property_list *commons::property::property_hub::try_get_for_context(const property_context &ctx) const
{

   auto scope_it = _data.find(ctx.scope);
   if (scope_it == _data.end())
   {
      return nullptr;
   }

   auto &data = scope_it->second;

   auto it = std::find_if(std::begin(data), std::end(data), [=](auto &kv)
   {
      return kv.first == ctx.scope_id;
   });

   if (it != std::end(data))
   {
      auto prop_it = std::find_if(std::begin(it->second), std::end(it->second),
                                  [=](auto &kv)
                                  {
                                     return kv.first == ctx.scope_id;
                                  });

      if (prop_it != std::end(it->second))
      {
         return &prop_it->second;
      }
   }

   return nullptr;
}

void commons::property::property_hub::set_properties(property_context context, const property_list &properties)
{
   property_list changes;
   {
      std::lock_guard<std::mutex> guard{_lock};
      auto &props = get_for_context(context);

      update_and_track_changes(changes, props, properties);
   }

   if (!changes.empty())
   {
      value_changed(context, changes);
   }
}

void commons::property::property_hub::set_property(property_context context, std::string prop, property_value value)
{
   property_list changes;
   {
      std::lock_guard<std::mutex> guard{_lock};
      auto &props = get_for_context(context);

      auto it = std::find_if(std::begin(props), std::end(props), [&](auto &data)
      {
         return data.first == prop;
      });

      if (it != std::end(props))
      {
         if (!(it->second == value))
         {
            it->second = value;
            changes.emplace_back(prop, value);
         }
      }
      else
      {
         props.emplace_back(prop, value);
         changes.emplace_back(prop, value);
      }
   }

   if (!changes.empty())
   {
      value_changed(context, changes);
   }
}

properties commons::property::property_hub::get_properties(context_id context_id) const
{
   properties props;
   {
      std::lock_guard<std::mutex> guard{_lock};

      for (auto &kv : _data)
      {
         auto it = std::find_if(std::begin(kv.second), std::end(kv.second),
                                [=](auto &item)
                                {
                                   return item.first == context_id;
                                });

         if (it != std::end(kv.second))
         {
            for (auto &item : it->second)
            {
               auto &scope = props.scopes.try_emplace(kv.first, spider::property::scope{}).first->second;
               scope.items.try_emplace(item.first, item.second);
            }
         }
      }
   }
   return props;
}

property_list commons::property::property_hub::get_properties(property_context context) const
{
   std::lock_guard<std::mutex> guard{_lock};

   property_list result;
   auto *props = try_get_for_context(context);
   if (props)
   {
      result = *props;
   }

   return result;
}

property_value commons::property::property_hub::get_property(property_context context, const std::string &key) const
{
   std::lock_guard<std::mutex> guard{_lock};

   property_value result;

   auto *properties = try_get_for_context(context);
   if (properties)
   {
      auto it = std::find_if(properties->begin(), properties->end(),
                             [&](const std::pair<std::string, property_value> &prop)
                             {
                                return prop.first == key;
                             });

      if (it != properties->end())
      {
         result = it->second;
      }
   }

   return result;
}
