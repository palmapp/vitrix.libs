#include "cancellation_token.h"
bool commons::cancellation_token::is_cancelled() const
{
   if (value_holder_)
      return value_holder_->cancelled;
   else
      return false;
}
commons::cancellation_token::cancellation_token() {}
commons::cancellation_token::cancellation_token(const commons::cancellation_token &token)
    : value_holder_(token.value_holder_)
{
}
commons::cancellation_token::cancellation_token(commons::cancellation_token &&token)
    : value_holder_(std::move(token.value_holder_))
{
}
commons::cancellation_token &commons::cancellation_token::operator=(const commons::cancellation_token &token)
{
   value_holder_ = token.value_holder_;
   return *this;
}
commons::cancellation_token &commons::cancellation_token::operator=(commons::cancellation_token &&token)
{
   value_holder_ = std::move(token.value_holder_);
   return *this;
}
commons::cancellation_token::cancellation_token(std::shared_ptr<cancellation_token_value_holder> value)
    : value_holder_(std::move(value))
{
}
bool commons::cancellation_token::wait_for(std::chrono::steady_clock::duration duration) const
{
   if (value_holder_)
   {
      std::unique_lock lock{value_holder_->mutex};
      return value_holder_->cv.wait_for(lock, duration, [this]() { return value_holder_->cancelled; });
   }
   else
   {
      return false;
   }
}

bool commons::cancellation_token::wait_until(std::chrono::steady_clock::time_point time) const
{
   if (value_holder_)
   {
      std::unique_lock lock{value_holder_->mutex};
      return value_holder_->cv.wait_until(lock, time, [this]() { return value_holder_->cancelled; });
   }
   else
   {
      return false;
   }
}

commons::cancellation_token_source::cancellation_token_source()
    : value_holder_(std::make_shared<cancellation_token_value_holder>())
{
}

commons::cancellation_token commons::cancellation_token_source::get_cancellation_token() const
{
   return {value_holder_};
}
void commons::cancellation_token_source::cancel()
{
   {
      std::lock_guard lock{value_holder_->mutex};
      value_holder_->cancelled = true;
   }

   value_holder_->cv.notify_all();
}
