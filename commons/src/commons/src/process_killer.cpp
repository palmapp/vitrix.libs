#include "process_killer.h"

#include "make_string.h"
#include <boost/filesystem.hpp>

#include <easylogging++.h>
#include <fstream>

#ifdef WIN32
#include <windows.h>
#else
#include <signal.h>
#include <sys/types.h>
#endif

#include <boost/dll/runtime_symbol_info.hpp>
#include <system_error>
#include <vector>

#include <boost/filesystem/fstream.hpp>
#include <boost/process.hpp>
#include <boost/process/extend.hpp>

namespace commons::process_killer
{
   std::mutex g_kill_mutex;
#ifdef WIN32
   void send_interrupt(boost::process::pid_t process)
   {
      std::lock_guard lk{g_kill_mutex};

      auto windows_kill_exe = boost::dll::program_location().parent_path() / "windows-kill.exe";
      if (!exists(windows_kill_exe))
      {
         LOG(WARNING) << "unable to locate windows-kill.exe at :" << windows_kill_exe << " sending kill instead";
         send_kill(process);
      }
      else
      {
         namespace bp = boost::process;

         const auto windows_creation_flags = [](auto &exec)
         {
#ifdef WIN32
            exec.creation_flags |= CREATE_NO_WINDOW;
#endif
         };

         bp::child cmd(windows_kill_exe, bp::args({"-SIGINT", commons::make_string(process)}),
                       boost::process::extend::on_setup = windows_creation_flags);
         cmd.wait();
         LOG(DEBUG) << "SIGINT sent";
      }
   }

   void send_kill(boost::process::pid_t process)
   {
      std::lock_guard lk{g_kill_mutex};

      int code = system(commons::make_string("taskkill /F /T /PID ", process).c_str());
      LOG_IF(code != 0, WARNING) << "unable to send taskkill /F /T /PID " << process << " exited " << code;
   }

   void send_term(boost::process::pid_t process)
   {
      std::lock_guard lk{g_kill_mutex};
      int code = system(commons::make_string("taskkill /T /PID ", process).c_str());
      LOG_IF(code != 0, WARNING) << "unable to send taskkill /T /PID " << process << " exited " << code;
   }
#else

   void send_interrupt(boost::process::pid_t process)
   {
      kill(process, SIGINT);
   }
   void send_kill(boost::process::pid_t process)
   {
      kill(process, SIGKILL);
   }
   void send_term(boost::process::pid_t process)
   {
      kill(process, SIGTERM);
   }
#endif

   std::optional<boost::filesystem::path> create_exit_file(std::string const &app_name, boost::process::pid_t process)
   {
      using namespace std;
      namespace fs = boost::filesystem;

      std::array<const char *, 4> directories{{getenv("TMP"), getenv("TEMP"), getenv("USERPROFILE"), "/tmp"}};
      for (auto *dir : directories)
      {
         if (dir != nullptr)
         {
            fs::path dir_path = dir;

            if (exists(dir_path))
            {
               fs::path exit_path = dir_path / commons::make_string(app_name, ".", process);
               fs::ofstream file{exit_path, std::ios::out | std::ios::trunc};
               file << process;
               return exit_path;
            }
         }
      }

      return {};
   }

} // namespace commons::process_killer