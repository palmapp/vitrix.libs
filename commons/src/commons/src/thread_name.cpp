//
// Created by jhrub on 26.07.2022.
//

#include "thread_name.h"
#ifndef WIN32
#include <pthread.h>
void commons::set_this_thread_name(std::string const &name)
{
   auto id = pthread_self();
   pthread_setname_np(id, name.data());
}

#endif