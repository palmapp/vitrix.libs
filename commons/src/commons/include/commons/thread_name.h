//
// Created by jhrub on 26.07.2022.
//

#pragma once
#include <string>
namespace commons
{
   void set_this_thread_name(std::string const &name);
}