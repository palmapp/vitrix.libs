#pragma once

#include <stdint.h>

namespace commons
{
   class byte_order
   {
    public:
      static inline bool is_little_endian()
      {
         union end_test_t {
            uint16_t val;
            uint8_t test[2];
         };
         const end_test_t un = {1};
         return un.test[0] == 1;
      }

      template <typename NumType>
      static inline void little_endian(NumType &num) noexcept
      {
         if (!is_little_endian())
         {
            swap(num);
         }
      }

      static inline void swap(uint16_t &num) noexcept
      {
         num = (num >> 8) | (num << 8); // byte 0 to byte 3
      }

      static inline void swap(uint32_t &num) noexcept
      {
         num = ((num >> 24) & 0xff) |      // move byte 3 to byte 0
               ((num << 8) & 0xff0000) |   // move byte 1 to byte 2
               ((num >> 8) & 0xff00) |     // move byte 2 to byte 1
               ((num << 24) & 0xff000000); // byte 0 to byte 3
      }

      static inline void swap(uint64_t &num) noexcept
      {
         num = ((num & 0x00000000000000FFULL) << 56) | ((num & 0x000000000000FF00ULL) << 40) |
               ((num & 0x0000000000FF0000ULL) << 24) | ((num & 0x00000000FF000000ULL) << 8) |
               ((num & 0x000000FF00000000ULL) >> 8) | ((num & 0x0000FF0000000000ULL) >> 24) |
               ((num & 0x00FF000000000000ULL) >> 40) | ((num & 0xFF00000000000000ULL) >> 56);
      }

      static inline void swap(int64_t &num) noexcept
      {
         num = ((num & 0x00000000000000FFULL) << 56) | ((num & 0x000000000000FF00ULL) << 40) |
               ((num & 0x0000000000FF0000ULL) << 24) | ((num & 0x00000000FF000000ULL) << 8) |
               ((num & 0x000000FF00000000ULL) >> 8) | ((num & 0x0000FF0000000000ULL) >> 24) |
               ((num & 0x00FF000000000000ULL) >> 40) | ((num & 0xFF00000000000000ULL) >> 56);
      }
   };

   namespace bin
   {
      template <typename TypeT>
      inline void write_le(uint8_t *&buff, const TypeT &t) noexcept
      {
         if (byte_order::is_little_endian())
         {
            *reinterpret_cast<TypeT *>(buff) = t;
            buff += sizeof(TypeT);
         }
         else
         {
            TypeT val = t;
            byte_order::swap(val);
            *reinterpret_cast<TypeT *>(buff) = t;
            buff += sizeof(TypeT);
         }
      }

      template <typename TypeT>
      inline void read_le(const uint8_t *&buff, TypeT &t) noexcept
      {
         t = *reinterpret_cast<const TypeT *>(buff);
         buff += sizeof(TypeT);

         if (!byte_order::is_little_endian())
         {
            byte_order::swap(t);
         }
      }

   } // namespace bin
} // namespace commons