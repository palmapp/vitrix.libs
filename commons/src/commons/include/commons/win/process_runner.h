//
// Created by jhrub on 10.03.2021.
//

#pragma once

#include <boost/filesystem.hpp>
#include <string_view>
#include <chrono>
#include <optional>
namespace commons::windows
{
    struct startup_info
    {
        boost::filesystem::path executable;
        boost::filesystem::path working_directory;

        ///warguments takes precendence over arguments
        std::wstring_view w_arguments;
        std::string_view arguments;

        /// If there is no timeout specified start_process_and_wait can wait forever.
        std::optional<std::chrono::steady_clock::duration> timeout;

        bool ignore_stdio = false;
    };

    /**
     * This function ensures that all created process tree is cleared after this
     * function finishes.
     *
     * It also handles the situation when program crashes.
     *
     * @param info - startup info
     * @return -1 when process run fails, otherwise the target process exit code
     */
    std::int32_t start_process_and_wait(startup_info const& info);


}