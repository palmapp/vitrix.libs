//
// Created by jhrub on 21.02.2023.
//

#pragma once

#include <stop_token>
#include <thread>
#include <optional>

namespace commons
{
   std::optional<std::jthread> cancel_on_wm_quit(std::stop_source & source);
}