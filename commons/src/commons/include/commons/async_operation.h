//
// Created by jhrub on 25.06.2021.
//

#pragma once
#include "basic_types.h"
#include <chrono>

namespace commons
{
   enum class async_operation_result
   {
      /// operation is waiting for completion
      operation_in_progress,
      /// only when it is finished it is allowed to retrieve the result by calling get_result
      finished
   };

   struct polled_async_operation
   {
      using optional_time_duration = basic_types::optional<std::chrono::steady_clock::duration>;

      virtual ~polled_async_operation() = default;

      /**
       * This method will execute poll step in an async operation
       * \param block_wait_duration - this is the maximum time the thread is allowed to block, if not set thread should
       *
       * \return async_operation_result::operation_in_progress - when we continue to do polling
       * \return async_operation_result::finished - when operation is finished and get_result is ready to be called
       *
       * \throws if this method throws an exception operation will be considered as finished without success and
       * destructor will be called
       */
      virtual async_operation_result poll_for_result(optional_time_duration block_wait_duration) noexcept = 0;

      /**
       * After calling cancel operation should be canceled
       */
      virtual void cancel() = 0;
   };

   template <class ResultT>
   struct async_operation : public polled_async_operation
   {
      /**
       * This can be called only if previous call poll_for_result returns async_operation_result::finished
       * \return it returns the result - preferred result is unique_ptr or shared_ptr of something
       */
      virtual ResultT get_result() = 0;
   };
} // namespace commons