#pragma once
#include <cstdlib>
#include <cstdint>

namespace commons
{
   namespace crypto
   {
      class rc4
      {
       public:
         rc4(const char *key);
         rc4(const char *binkey, const std::size_t len);

         std::uint8_t crypt(std::uint8_t toEncrypt);
         void crypt(std::uint8_t *toEncrypt, std::size_t len, const bool saveState);

       private:
         int next_state();
         std::uint8_t state[256];
         int x, y;
      };
   } // namespace crypto
} // namespace commons
