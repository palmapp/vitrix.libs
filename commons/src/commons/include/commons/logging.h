#pragma once
#include <boost/filesystem.hpp>

namespace commons
{

   /**
    * It searches the log_file_path parent directory for all filename.<num>.ext and moves them by one
    * all files with number equal or greater than max_rollouts are going to get deleted.
    *
    * This function eliminates a possibility of exhausting disk space by log file explosion.
    * Make sure that log_file_path is closed before calling this function.
    *
    * filename.ext is treated as a zero
    * next file will be: filename.1.ext
    *
    * @param log_file_path - log file path, it must contain extension (any)
    * @param max_rollouts - maximum file rollout
    */
   [[nodiscard]] boost::system::error_code roll_files(boost::filesystem::path const &log_file_path, int max_rollouts);
   void init_easylogging(int argc, const char *argv[], const char *project_name, const boost::filesystem::path &app_dir,
                         std::vector<std::string> *result_enabled_loggers = nullptr, bool useModernLogRolling = false);

   void init_easylogging_rolling();

} // namespace commons