#pragma once

#include <boost/process/child.hpp>
#include <map>
#include <optional>
#include <string>

namespace commons
{
   namespace process_killer
   {
      void send_interrupt(boost::process::pid_t process);
      void send_kill(boost::process::pid_t process);
      void send_term(boost::process::pid_t process);

      std::optional<boost::filesystem::path> create_exit_file(std::string const &file, boost::process::pid_t process);

   }; // namespace process_killer
} // namespace commons
