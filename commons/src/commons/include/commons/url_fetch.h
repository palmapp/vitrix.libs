#pragma once

#include <boost/filesystem.hpp>
#include <string>
#include <vector>

namespace commons
{
   namespace url
   {
#ifdef WIN32
      std::wstring normalize_string(const std::string &str);
#else
      const std::string &normalize_string(const std::string &str);
#endif

      using header_container = std::vector<std::pair<std::string, std::string>>;
      int fetch_to_string(std::string &result, const std::string &url, const header_container &headers = {});
      int send_get_request(const std::string &url, const header_container &headers = {});
      int fetch_to_file(const boost::filesystem::path &output_path, const std::string &url,
                        const header_container &headers = {});
   } // namespace url
} // namespace commons