//
// Created by jhrub on 04.09.2021.
//

#pragma once

#include "../field_value.h"
#include "data_stream.h"

namespace commons::data
{

   struct vector_data_stream : public data_stream
   {
      inline optional<array_view<string_view>> get_column_names() override
      {
         if (column_names.has_value())
         {
            return {array_view<string_view>{*column_names}};
         }
         else
         {
            return {};
         }
      }

      inline optional<data_row> next_row() override
      {
         auto data_index = index++ * column_size;
         if (data_index < data.size())
         {
            auto *begin_row = &data[data_index];
            data.at(data_index + column_size - 1);

            return {{begin_row, column_size}};
         }

         return {};
      }

      std::vector<basic_types::optional_field_value> data;
      optional<std::vector<string>> columns;
      optional<std::vector<string_view>> column_names;

      std::size_t column_size;
      std::size_t index = 0;

      inline explicit vector_data_stream(std::size_t _column_size) : column_size(_column_size) {}
      inline explicit vector_data_stream(vector<string> _columns)
          : columns(std::move(_columns)), column_size(columns->size())
      {
         auto names = column_names.emplace();
         names.reserve(column_size);

         for (auto &name : columns.value())
         {
            names.push_back(name);
         }
      }
   };

} // namespace commons::data