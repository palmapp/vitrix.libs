//
// Created by jhrub on 13.03.2022.
//

#pragma once
#include "data_stream.h"
#include <functional>
#include <memory>
namespace commons::data
{
   class transform_data_stream;
   using lookup_column_func = std::function<optional_field_value(const std::string &column)>;
   using tranform_column_func =
       std::function<optional_field_value(optional_field_value const &value, lookup_column_func const &column_getter)>;

   struct column_source
   {
      struct at_index
      {
         int index;
      };

      struct for_name
      {
         string name;
      };

      std::variant<at_index, for_name> source;

      inline column_source(at_index index) : source(index) {}
      inline column_source(for_name index) : source(index) {}

      size_t index(transform_data_stream const &self);
   };

   struct transform_operation
   {

      struct trivial_transformation
      {
         column_source source;
         tranform_column_func transformation;
         string name;

         inline trivial_transformation(column_source _source, tranform_column_func _trans, string _name = {})
             : source(std::move(_source)), transformation(std::move(_trans)), name(std::move(_name))
         {
         }
      };

      struct add_column : public trivial_transformation
      {
         using trivial_transformation::trivial_transformation;

         void prepare_columns(transform_data_stream &self);
      };

      struct prepend_column : public trivial_transformation
      {
         using trivial_transformation::trivial_transformation;

         void prepare_columns(transform_data_stream &self);
      };

      struct replace_column : public trivial_transformation
      {
         using trivial_transformation::trivial_transformation;

         void prepare_columns(transform_data_stream &self);
      };

      struct rename_column
      {
         column_source source;
         string name;

         void prepare_columns(transform_data_stream &self);
      };

      struct remove_column
      {
         column_source source;
         void prepare_columns(transform_data_stream &self);
      };

      std::variant<add_column, prepend_column, replace_column, remove_column, rename_column> operation;
   };

   class transform_data_stream : public data_stream
   {
    public:
      transform_data_stream(std::unique_ptr<data_stream> own_stream, std::vector<transform_operation> operations);
      transform_data_stream(data_stream &stream_ref, std::vector<transform_operation> operations);
      /// it is optional to support this feature
      optional<array_view<string_view>> get_column_names() override;

      /// this will return pointer to the next_data row
      virtual optional<data_row> next_row() override;

    private:
      void init();
      void index_columns();
      void prepare_columns();
      void build_stable_column_names();

      void do_prepend_columns(const data_row &current_row, vector<optional_field_value> &temp_row,
                              const vector<transform_operation> &operations, const vector<size_t> &source_indexes,
                              lookup_column_func const &);
      void do_append_columns(const data_row &current_row, vector<optional_field_value> &temp_row,
                             const vector<transform_operation> &operations, const vector<size_t> &source_indexes,
                             lookup_column_func const &);
      void do_replace_or_copy(const data_row &current_row, vector<optional_field_value> &temp_row,
                              const vector<transform_operation> &operations, const vector<size_t> &source_indexes,
                              lookup_column_func const &);

      std::unique_ptr<data_stream> own_stream_;
      data_stream &stream_ref_;

      std::vector<transform_operation> operations_;
      std::vector<size_t> operation_index_;

      std::vector<string> source_columns_;
      std::vector<string> columns_;
      std::vector<string_view> column_names_;
      std::vector<optional_field_value> temp_row_;
      std::unordered_map<string, size_t> column_index_;

      int prepended_ = 0;
      int appended_ = 0;
      int removed_ = 0;

      friend struct transform_operation::add_column;
      friend struct transform_operation::replace_column;
      friend struct transform_operation::prepend_column;
      friend struct transform_operation::remove_column;
      friend struct transform_operation::rename_column;

      friend struct column_source;
   };
} // namespace commons::data