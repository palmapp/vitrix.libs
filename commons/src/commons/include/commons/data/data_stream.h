//
// Created by jhrub on 03.06.2021.
//

#pragma once
#include "types.h"

namespace commons::data
{

   class data_stream
   {
    public:
      virtual ~data_stream() noexcept(false) = default;

      /// it is optional to support this feature
      virtual optional<array_view<string_view>> get_column_names() = 0;

      /// this will return pointer to the next_data row
      virtual optional<data_row> next_row() = 0;
   };
} // namespace commons::data