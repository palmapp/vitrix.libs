//
// Created by jhrub on 03.06.2021.
//

#pragma once

#include "../field_value.h"

namespace commons::data
{
   using namespace commons::basic_types;

   using data_row = array_view<optional_field_value>;

} // namespace commons::data