//
// Created by jhrub on 26.03.2022.
//

#pragma once
#include "data_stream.h"
#include <type_traits>
#include <visit_struct/visit_struct.hpp>
namespace commons::data
{

   template <class ContainerT>
   class visitable_data_stream : public data_stream
   {
    public:
      using value_type = typename ContainerT::value_type;
      static_assert(visit_struct::traits::is_visitable<value_type>::value,
                    "ContainerT value_type is required to be visitable");

      static_assert(std::is_default_constructible<value_type>::value,
                    "ContainerT value_type is required to be default constructible");

      visitable_data_stream(ContainerT container) : container_(std::move(container))
      {
         value_type no_val{};
         visit_struct::for_each(no_val,
                                [&](string_view name, auto &value)
                                {
                                   if constexpr (is_optional_field_value_convertible_v<std::decay_t<decltype(value)>>)
                                   {
                                      columns_.push_back(name);
                                   }
                                });

         reset_iteration();
         end_ = std::cend(container_);
      }

      /// it is optional to support this feature
      optional<array_view<string_view>> get_column_names() override
      {
         return {columns_};
      }

      /// this will return pointer to the next_data row
      optional<data_row> next_row() override
      {
         if (cursor_ != end_)
         {
            value_type const &value = *cursor_;
            fill_data_row(value);

            ++cursor_;

            return {data_row_};
         }
         else
         {
            return {};
         }
      };

      ContainerT release()
      {
         return std::move(container_);
      }

      void reset_iteration()
      {
         cursor_ = std::cbegin(container_);
      }

    private:
      using const_iterator = typename ContainerT::const_iterator;

      ContainerT container_;

      const_iterator cursor_;
      const_iterator end_;

      vector<string_view> columns_;
      vector<optional_field_value> data_row_;

      struct row_filler
      {
         template <class T>
         void operator()(string_view name, T const &value)
         {
            if constexpr (is_optional_field_value_convertible_v<T>)
            {
               if constexpr (!is_optional_field_value_convertible<T>::is_optional)
               {
                  data_row_.push_back({value});
               }

               if constexpr (is_optional_field_value_convertible<T>::is_optional)
               {
                  if (value.has_value())
                  {
                     data_row_.push_back({value.value()});
                  }
                  else
                  {
                     data_row_.push_back({});
                  }
               }
            }
         }

         vector<optional_field_value> &data_row_;
      };

      void fill_data_row(value_type const &value)
      {
         data_row_.clear();
         row_filler filler{data_row_};
         visit_struct::for_each(value, filler);
      }
   };

} // namespace commons::data