#pragma once
#include "types.h"
#include <boost/unordered_map.hpp>
#include <cstdint>
#include <vector>

#if __has_include(<compare>)
#include <compare>
#if defined(__cpp_lib_three_way_comparison) && __cpp_lib_three_way_comparison >= 201907
#define SPACESHIP_OPERATOR_IS_SUPPORTED 1
#endif
#endif

namespace commons::data
{

   struct sort_column
   {
      string name{};
      bool is_asc = true;
#ifdef SPACESHIP_OPERATOR_IS_SUPPORTED
      auto operator<=>(const sort_column &) const = default;
#endif
   };

   template <class T>
   using optional_vector = optional<vector<T>>;

   template <class K, class T>
   using optional_map = optional<boost::unordered_map<K, T>>;
   // this is a basic data_query interface to provide an unified starting point when designing data related interfaces
   struct data_query
   {
      i64 limit = -1;
      i64 offset = -1;

      string query{};

      optional_vector<string> columns{};
      optional_vector<sort_column> order_by{};
      optional_vector<optional_field_value> position_arguments{};
      optional_vector<pair<string, optional_field_value>> named_arguments{};
      optional_map<string, string> context_arguments{};

      inline static data_query make(string query, optional_vector<optional_field_value> position_arguments = {},
                                    optional_vector<sort_column> order_by = {}, optional_vector<string> columns = {},
                                    optional_map<string, string> context_arguments = {})
      {
         data_query q;
         q.query = std::move(query);
         q.position_arguments = std::move(position_arguments);
         q.order_by = std::move(order_by);
         q.columns = std::move(columns);
         q.context_arguments = std::move(context_arguments);
         return q;
      }
   };

} // namespace commons::data