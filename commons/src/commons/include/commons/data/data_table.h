//
// Created by jhrub on 19.05.2021.
//

#pragma once
#include "commons/variant_utils.h"
#include "types.h"
#include <cstdint>
#include <stdexcept>
#include <variant>
#include <vector>

namespace commons::data
{

   struct data_table_spec
   {
      std::vector<data_type> column_types;
      std::size_t allocate = 0;
   };

   struct index
   {
      std::size_t row;
      std::size_t col;

      inline index(std::size_t _row, std::size_t _col)
         : row(_row), col(_col)
      {
      }
   };

   class data_table
   {
   public:
      template <class T>
      using col_vector = std::vector<optional<T>>;

      using col_bool = col_vector<bool>;
      using col_i8 = col_vector<i8>;
      using col_i16 = col_vector<i16>;
      using col_i32 = col_vector<i32>;
      using col_i64 = col_vector<i64>;
      using col_u8 = col_vector<u8>;
      using col_u16 = col_vector<u16>;
      using col_u32 = col_vector<u32>;
      using col_u64 = col_vector<u64>;
      using col_fl32 = col_vector<fl32>;
      using col_fl64 = col_vector<fl64>;
      using col_string = col_vector<string>;

      using column_storage = std::variant<col_bool, col_i8, col_i16, col_i32, col_i64, col_u8, col_u16, col_u32,
                                          col_u64, col_fl32, col_fl64, col_string>;

      inline explicit data_table(data_table_spec const &spec)
      {
         if (!spec_.column_types.empty())
         {
            do_initial_allocation(spec);
         }
         else
         {
            throw std::logic_error("spec_column_types.empty() must be false");
         }
      }

      inline data_table(const data_table &table)
      {
         spec_ = table.spec_;
         storage_ = table.storage_;
      }

      inline data_table(data_table &&table)
      {
         spec_ = std::move(table.spec_);
         storage_ = std::move(table.storage_);
      }

      inline data_table &operator=(const data_table &table)
      {
         spec_ = table.spec_;
         storage_ = table.storage_;
         return *this;
      }

      inline data_table &operator=(data_table &&table)
      {
         spec_ = std::move(table.spec_);
         storage_ = std::move(table.storage_);
         return *this;
      }

      template <class T>
      const col_vector<T> &get_column_vector(std::size_t column)
      {
         auto &uncasted_result = storage_.at(column);

         if (spec_.column_types[column] == get_data_type<T>())
         {
            auto *result = try_variant_cast<col_vector<T>>(uncasted_result);
            if (result == nullptr)
            {
               throw std::logic_error{"bad column cast"};
            }
            else
            {
               return *result;
            }
         }
         else
         {
            throw std::logic_error{"bad column cast"};
         }
      }

      inline std::size_t size() const
      {

         if (storage_.empty())
         {
            return 0;
         }
         else
         {
            std::size_t result = 0;

            std::visit([&](auto &column_vector) mutable
            {
               result = column_vector.size();
            }, storage_.front());

            return result;
         }
      }

      inline std::size_t columns_size() const
      {
         return storage_.size();
      }

      inline optional_field_value operator[](index const &idx) const
      {
         if (idx.row < size() && idx.col < columns_size())
         {
            return get_value_unchecked(idx);
         }
         return {};
      }

      inline optional_field_value get_value(index const &idx) const
      {
         return operator[](idx);
      }

      inline void read_row(std::size_t row_idx,
                           commons::basic_types::mutable_array_view<optional_field_value> row) const
      {
         std::visit([row_idx](auto &column_vector)
         {
            static_cast<void>(column_vector.at(row_idx));
         }, storage_.at(0));
         if (row.size() >= storage_.size())
         {
            throw std::logic_error{"no space to fit row to output"};
         }

         for (index i{row_idx, 0}; i.col < storage_.size(); ++i.col)
         {
            row[i.col] = get_value_unchecked(i);
         }
      }

      inline void set_value(index const &idx, optional_field_value value)
      {
         if (idx.row < size() && idx.col < columns_size() &&
             (!value.has_value() && value.has_value() && storage_[idx.col].index() == value.value().index()))
         {
            set_value_unchecked(idx, value);
         }
         else
         {
            throw std::logic_error{"column or row index out of bounds"};
         }
      }

      inline void set_value(std::size_t row_idx, array_view<optional_field_value> row)
      {
         // row_idx bounds check
         std::visit([row_idx](auto &column_vector)
         {
            static_cast<void>(column_vector.at(row_idx));
         }, storage_.at(0));

         map_columns(row,
                     [&](optional_field_value const &fl, auto &column_vector) mutable
                     {
                        using col_storage_type = typename std::decay_t<decltype(column_vector)>::value_type;
                        using col_storage_type_without_optional = typename col_storage_type::value_type;

                        if (fl.has_value())
                        {
                           fl->visit(
                              [&](auto &value)
                              {
                                 using field_type = typename std::decay_t<decltype(value)>;
                                 if constexpr (std::is_same_v<string_view, field_type> &&
                                               std::is_same_v<col_storage_type_without_optional, string>)
                                 {
                                    column_vector[row_idx] = static_cast<string>(value);
                                 }
                                 else if constexpr (std::is_same_v<col_storage_type_without_optional, field_type>)
                                 {
                                    column_vector[row_idx] = value;
                                 }
                              });
                        }
                        else
                        {
                           column_vector[row_idx] = {};
                        }
                     });
      }

      template <class Callback>
      void map_columns(array_view<optional_field_value> row, Callback callback)
      {
         if (validate_row(row))
         {
            for (std::size_t i = 0; i != row.size(); ++i)
            {
               auto &fl = row[i];
               std::visit([&](auto &column_vector)
               {
                  callback(fl, column_vector);
               }, storage_[i]);
            }
         }
         else
         {
            throw std::logic_error("invalid row");
         }
      }

      inline void push_back(array_view<optional_field_value> row)
      {
         map_columns(row,
                     [](optional_field_value const &fl, auto &column_vector) mutable
                     {
                        using col_storage_type = typename std::decay_t<decltype(column_vector)>::value_type;
                        using col_storage_type_without_optional = typename col_storage_type::value_type;

                        if (fl.has_value())
                        {
                           fl->visit(
                              [&](auto &value)
                              {
                                 using field_type = typename std::decay_t<decltype(value)>;
                                 if constexpr (std::is_same_v<string_view, field_type> &&
                                               std::is_same_v<col_storage_type_without_optional, string>)
                                 {
                                    column_vector.push_back(static_cast<string>(value));
                                 }
                                 else if constexpr (std::is_same_v<col_storage_type_without_optional, field_type>)
                                 {
                                    column_vector.push_back(value);
                                 }
                              });
                        }
                        else
                        {
                           column_vector.push_back({});
                        }
                     });
      }

      inline bool validate_row(array_view<optional_field_value> const &row) const
      {
         if (row.size() != columns_size())
         {
            return false;
         }

         for (std::size_t i = 0; i != row.size() && i != columns_size(); ++i)
         {
            auto &fl = row[i];
            if (fl.has_value() && storage_[i].index() != fl.value().index())
            {
               return false;
            }
         }
         return true;
      }

   private:
      inline void set_value_unchecked(index const &idx, optional_field_value value)
      {
         if (value.has_value())
         {
            auto &value_to_set = value.value();
            std::visit(
               [&](auto &column_vector) mutable
               {
                  value_to_set.visit(
                     [&](auto &val_set) mutable
                     {
                        if constexpr (std::is_same_v<
                           typename std::decay_t<decltype(column_vector)>::value_type::value_type,
                           std::decay_t<decltype(val_set)>>)
                        {
                           column_vector[idx.row] = val_set;
                        }
                     });
               },
               storage_[idx.col]);
         }
         else
         {
            std::visit([&](auto &column_vector)
            {
               column_vector[idx.row].reset();
            }, storage_[idx.col]);
         }
      }

      inline optional_field_value get_value_unchecked(index const &idx) const
      {
         optional_field_value result;
         std::visit(
            [&](auto &column_vector) mutable
            {
               auto &val = column_vector[idx.row];
               if (val.has_value())
               {
                  result = val.value();
               }
            },
            storage_[idx.col]);
         return result;
      }

      template <class T>
      inline data_type get_data_type()
      {
         if constexpr (std::is_same_v<T, bool>)
         {
            return data_type::boolean;
         }
         else if constexpr (std::is_same_v<T, i8>)
         {
            return data_type::i8;
         }
         else if constexpr (std::is_same_v<T, i16>)
         {
            return data_type::i16;
         }
         else if constexpr (std::is_same_v<T, i32>)
         {
            return data_type::i32;
         }
         else if constexpr (std::is_same_v<T, i64>)
         {
            return data_type::i64;
         }
         else if constexpr (std::is_same_v<T, u8>)
         {
            return data_type::u8;
         }
         else if constexpr (std::is_same_v<T, u16>)
         {
            return data_type::u16;
         }
         else if constexpr (std::is_same_v<T, u32>)
         {
            return data_type::u32;
         }
         else if constexpr (std::is_same_v<T, u64>)
         {
            return data_type::u64;
         }
         else if constexpr (std::is_same_v<T, fl32>)
         {
            return data_type::fl32;
         }
         else if constexpr (std::is_same_v<T, fl64>)
         {
            return data_type::fl64;
         }
         else if constexpr (std::is_same_v<T, string>)
         {
            return data_type::string;
         }
         else
         {
            throw std::logic_error{"unable to map T to data_type"};
         }
      }

      inline void do_initial_allocation(data_table_spec const &spec)
      {
         storage_.reserve(spec.column_types.size());

         for (data_type col : spec.column_types)
         {
            switch (col)
            {
            case data_type::boolean:
               storage_.push_back(col_bool{});
               break;
            case data_type::i8:
               storage_.push_back(col_i8{});
               break;
            case data_type::i16:
               storage_.push_back(col_i16{});
               break;
            case data_type::i32:
               storage_.push_back(col_i32{});
               break;
            case data_type::i64:
               storage_.push_back(col_i64{});
               break;
            case data_type::u8:
               storage_.push_back(col_u8{});
               break;
            case data_type::u16:
               storage_.push_back(col_u16{});
               break;
            case data_type::u32:
               storage_.push_back(col_u32{});
               break;
            case data_type::u64:
               storage_.push_back(col_u64{});
               break;
            case data_type::fl32:
               storage_.push_back(col_fl32{});
               break;
            case data_type::fl64:
               storage_.push_back(col_fl64{});
               break;
            case data_type::string:
               storage_.push_back(col_string{});
               break;
            default:
               break;
            }
         }

         if (spec.allocate > 0)
         {
            for (auto &col : storage_)
            {
               std::visit([&](auto &vec) mutable
               {
                  vec.reserve(spec.allocate);
               }, col);
            }
         }
      }

      data_table_spec spec_;
      std::vector<column_storage> storage_;
   };
} // namespace commons::data

namespace commons
{
   using data_table = commons::data::data_table;
   using data_table_spec = commons::data::data_table_spec;
   using data_index = commons::data::index;
} // namespace commons