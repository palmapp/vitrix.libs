//
// Created by jhrub on 04.09.2021.
//

#pragma once

#include "data_stream.h"
#include "data_table.h"

namespace commons::data
{

   template <class TableT>
   struct data_table_holder
   {
      using table_reference_type = TableT const &;

      std::size_t get_size() const
      {
         return table_.size();
      }

      std::size_t get_columns_size() const
      {
         return table_.columns_size();
      }

      void read_row(std::size_t row_idx, commons::basic_types::mutable_array_view<optional_field_value> row) const
      {
         table_.read_row(row_idx, row);
      }

      data_table_holder(table_reference_type table)
         : table_(table)
      {
      }

      table_reference_type table_;
   };

   template <class TableT>
   struct data_table_holder<std::shared_ptr<TableT>>
   {
      using table_reference_type = std::shared_ptr<TableT>;

      std::size_t get_size() const
      {
         return table_->size();
      }

      std::size_t get_columns_size() const
      {
         return table_->columns_size();
      }

      void read_row(std::size_t row_idx, commons::basic_types::mutable_array_view<optional_field_value> row) const
      {
         table_->read_row(row_idx, row);
      }

      data_table_holder(table_reference_type &&table)
         : table_(std::move(table))
      {
      }

      table_reference_type table_;
   };

   /// This class can read data in circular manner just use begin_index > one_paste_end_index
   template <class TableT>
   class data_table_data_stream : public data_stream
   {
   public:
      using data_table_holder_type = data_table_holder<TableT>;
      using table_reference_type = typename data_table_holder_type::table_reference_type;

      explicit data_table_data_stream(table_reference_type table, std::optional<std::size_t> begin_index = {},
                                      std::optional<std::size_t> end_index = {})
         : begin_index_(begin_index.value_or(0)),
           one_past_end_index_(end_index.value_or(table_.get_columns_size())), current_index_(begin_index_),
           table_(std::forward<table_reference_type>(table))
      {
         table_size_ = table_.get_size();
         row_.resize(table_.get_columns_size());
      }

      optional<array_view<string_view>> get_column_names() override
      {
         return {};
      }

      optional<data_row> next_row() override
      {
         auto i = current_index_++;
         if (one_past_end_index_ <= begin_index_)
         {
            auto idx = i % table_size_;
            if (idx == one_past_end_index_)
            {
               return {};
            }

            table_.read_row(idx, {row_.data(), row_.size()});
            return {row_};
         }
         else if (i < one_past_end_index_)
         {
            table_.read_row(i, {row_.data(), row_.size()});
            return {row_};
         }

         return {};
      }

   private:
      const std::size_t begin_index_;
      const std::size_t one_past_end_index_;

      std::size_t current_index_;
      std::size_t table_size_;
      data_table_holder_type table_;
      std::vector<meteo::optional_field_value> row_;
   };
} // namespace commons::data