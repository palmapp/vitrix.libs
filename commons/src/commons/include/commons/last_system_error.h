//
// Created by jhrub on 26.03.2022.
//

#pragma once
#include "basic_types.h"
#include <system_error>

namespace commons
{
   std::system_error last_system_error(basic_types::optional<basic_types::string> message = {});
   std::error_code last_system_error_code();
} // namespace commons