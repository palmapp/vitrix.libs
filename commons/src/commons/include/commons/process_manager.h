#pragma once

#include <boost/asio/io_context.hpp>
#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace commons
{
   class process_output
   {
    public:
      virtual ~process_output() = default;

      virtual void on_std_out(const std::int64_t id, const std::uint8_t *buffer, std::size_t len) = 0;
      virtual void on_std_err(const std::int64_t id, const std::uint8_t *buffer, std::size_t len) = 0;

      virtual void on_started(const std::int64_t id, const std::int64_t pid) = 0;
      virtual void on_failed_to_start(const std::int64_t id, const std::string &reason) = 0;
      virtual void on_ended(const std::int64_t id, const std::int64_t exit_code) = 0;
   };

   struct process_base;
   class process_manager
   {
    public:
      process_manager(boost::asio::io_context &io_service, process_output &output);
      ~process_manager();

      void start_process(std::int64_t id, const std::string &command);
      void start_process_in_term(std::int64_t id, const std::string &command);

      void end_process(std::int64_t id);
      void end_not_in_list_processes(std::vector<std::int64_t> ids);
      void end_all();

      void feed_std_in(std::int64_t id, const std::uint8_t *buffer, std::size_t len);
      void feed_std_in(std::int64_t id, const std::string &value);

      void validate_processes();

    private:
      boost::asio::io_context &io_service_;
      process_output &output_;
      std::unordered_map<std::int64_t, std::unique_ptr<process_base>> running_processes_;
   };

} // namespace commons
