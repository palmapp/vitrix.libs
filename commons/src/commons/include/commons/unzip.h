#pragma once
#include <boost/filesystem.hpp>

namespace commons
{
   void unzip(const boost::filesystem::path &input_zip_path, const boost::filesystem::path &output_folder);
   std::string unzip_item_to_memory(const boost::filesystem::path &input_zip_path, const std::string &item);

#ifdef ANDROID
   void android_unzip(const boost::filesystem::path &input_zip_path, const boost::filesystem::path &output_folder);
#endif
} // namespace commons
