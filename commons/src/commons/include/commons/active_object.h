#pragma once
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

namespace commons
{
   enum class stopping_policy
   {
      process_all,
      stop_imediatelly
   };

   template <class MessageType>
   class active_object
   {
      using lock_guard = std::unique_lock<std::mutex>;

    public:
      enum class active_thread_status
      {
         initial,
         running,
         stopped,
         destroyed
      };

      explicit active_object(stopping_policy policy = stopping_policy::process_all)
          : policy_(policy), status_(active_thread_status::initial)
      {

         thread_ = std::thread(
             [this]() mutable
             {
                this->do_work();

                {
                   lock_guard lock{mutex_};
                   status_ = active_thread_status::destroyed;
                }
                condition_.notify_one();
             });
      }

      virtual ~active_object() noexcept(false)
      {
         stop();
         if (thread_.joinable()) thread_.join();
      }

      void stop() noexcept
      {
         bool initial_status = status_ == active_thread_status::initial;
         while (initial_status)
         {
            {
               lock_guard lock{mutex_};
               initial_status = status_ == active_thread_status::initial;
            }
            std::this_thread::yield();
         }

         {
            lock_guard lock{mutex_};
            if (status_ == active_thread_status::running)
            {
               status_ = active_thread_status::stopped;
               lock.unlock();
               condition_.notify_one();
            }
         }

         {
            lock_guard lock{mutex_};
            condition_.wait(lock, [this]() { return status_ == active_thread_status::destroyed; });
         }
      }

      bool post_message(MessageType &&message)
      {
         bool queued = false;
         {
            lock_guard lock{mutex_};
            if (status_ == active_thread_status::running || status_ == active_thread_status::initial)
            {
               queue_.push_back(std::forward<MessageType>(message));
               queued = true;
               ++stats_.message_counter_;
            }
         }

         if (queued) condition_.notify_one();

         return queued;
      }

    protected:
      virtual void on_message(MessageType &message) = 0;

      virtual void on_stopped() noexcept {}
      virtual void on_exception(const MessageType &, const std::exception &) noexcept {}
      virtual void on_unknown_exception(const MessageType &) noexcept {}

      lock_guard acquire_lock() const
      {
         return lock_guard(mutex_);
      }

      struct queue_stats
      {
         std::uint64_t message_counter_{0};
         std::uint64_t processed_message_counter_{0};

         inline bool is_queue_drained() const
         {
            return message_counter_ == processed_message_counter_;
         }
      };

       queue_stats get_queue_stats() const
      {
         auto lock = acquire_lock();
         return stats_;
      }

    private:


      queue_stats stats_;
      void do_work()
      {
         std::vector<MessageType> processing_queue;

         bool stopped = false;
         while (!stopped)
         {
            {
               lock_guard lock{mutex_};
               if (status_ == active_thread_status::initial)
               {
                  status_ = active_thread_status::running;
               }

               condition_.wait(lock, [this]() { return !queue_.empty() || status_ == active_thread_status::stopped; });
               stopped = status_ == active_thread_status::stopped;

               if (stopped && policy_ == stopping_policy::stop_imediatelly)
               {
                  this->on_stopped();
                  break;
               }

               std::swap(processing_queue, queue_);
            }


            for (auto &message : processing_queue)
            {
               try
               {
                  this->on_message(message);
               }
               catch (const std::exception &ex)
               {
                  this->on_exception(message, ex);
               }
               catch (...)
               {
                  this->on_unknown_exception(message);
               }
            }

            {
               auto lock = acquire_lock();
               stats_.processed_message_counter_ += processing_queue.size();
            }

            processing_queue.clear();

            if (stopped)
            {
               this->on_stopped();
            }
         }
      }

      stopping_policy policy_;
      active_thread_status status_;

      mutable std::mutex mutex_;
      std::condition_variable condition_;
      std::vector<MessageType> queue_;
      std::thread thread_;
   };

} // namespace commons
