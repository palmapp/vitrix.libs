//
// Created by jhrub on 16.10.2020.
//

#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <mutex>

#include <condition_variable>

namespace commons
{
   struct cancellation_token_value_holder
   {
      std::mutex mutex{};
      std::condition_variable cv{};
      bool cancelled = false;
   };
   class cancellation_token_source;
   class cancellation_token
   {
    public:
      bool is_cancelled() const;
      bool wait_for(std::chrono::steady_clock::duration duration) const;
      bool wait_until(std::chrono::steady_clock::time_point time) const;

      cancellation_token();
      cancellation_token(cancellation_token const &token);
      cancellation_token(cancellation_token &&token);
      cancellation_token &operator=(cancellation_token const &);
      cancellation_token &operator=(cancellation_token &&);

    private:
      friend class cancellation_token_source;
      cancellation_token(std::shared_ptr<cancellation_token_value_holder> value);
      std::shared_ptr<cancellation_token_value_holder> value_holder_;
   };

   class cancellation_token_source
   {
    public:
      cancellation_token_source();

      cancellation_token_source(cancellation_token_source const &) = delete;
      cancellation_token_source(cancellation_token_source &&) = delete;
      cancellation_token_source &operator=(cancellation_token_source const &) = delete;
      cancellation_token_source &operator=(cancellation_token_source &&) = delete;

      cancellation_token get_cancellation_token() const;
      void cancel();

    private:
      std::shared_ptr<cancellation_token_value_holder> value_holder_;
   };

} // namespace commons