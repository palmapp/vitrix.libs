#pragma once
#include <cstring>

namespace commons
{
   namespace cmd
   {
      template <class OutT>
      bool try_get_arg(OutT &result, const char *arg, const int argc, const char *argv[])
      {
         bool key_lookup = true;
         for (int i = 0; i != argc; ++i)
         {
            if (key_lookup && strcmp(argv[i], arg) == 0)
            {
               key_lookup = false;
               continue;
            }

            if (!key_lookup)
            {
               result = argv[i];
               return true;
            }
         }
         return false;
      }
   } // namespace cmd
} // namespace commons
