#pragma once
#include <memory>
#include <string>

namespace commons
{
   enum class message_broker_type
   {
      message_broker_server,
      driver_client,
      pusher
   };

   class message_broker_impl;
   class message_broker
   {
    public:
      message_broker(message_broker_type type, const std::string &connection_string);
      ~message_broker();

      void publish_message(const std::string &channel, const std::string &type, const std::string &message);
      void close_and_discard_unsent_messages();

    private:
      std::unique_ptr<message_broker_impl> impl_;
   };

   class subscription
   {
    public:
      virtual ~subscription() = default;
      virtual void on_message(const std::string &channel, const std::string &type, const std::string &message) = 0;
   };

   enum class message_receiver_type
   {
      message_broker_client,
      driver_server,
      puller
   };

   class message_receiver_impl;
   class message_receiver
   {
    public:
      message_receiver(message_receiver_type type, const std::string &connection_string);
      ~message_receiver();

      void subscribe(const std::string &channel, std::shared_ptr<subscription> subscription);
      void unsubscribe(const std::string &channel, std::shared_ptr<subscription> subscription);

      void subscribe_all(std::shared_ptr<subscription> subscription);
      void unsubscribe_all(std::shared_ptr<subscription> subscription);

      void start_receive();
      void end_receive();

    private:
      std::unique_ptr<message_receiver_impl> impl_;
   };
} // namespace commons
