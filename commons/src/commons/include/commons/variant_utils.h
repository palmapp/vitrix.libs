//
// Created by jhrub on 26.05.2021.
//

#pragma once
#include <type_traits>
#include <variant>

namespace commons
{
   template <typename VariantType, typename T, std::size_t index = 0>
   constexpr std::size_t variant_index()
   {
      if constexpr (index == std::variant_size_v<VariantType>)
      {
         return index;
      }
      else if constexpr (std::is_same_v<std::variant_alternative_t<index, VariantType>, T>)
      {
         return index;
      }
      else
      {
         return variant_index<VariantType, T, index + 1>();
      }
   }

   template <typename T, typename VariantType>
   T *try_variant_cast(VariantType &variant)
   {
      T *result = nullptr;

      std::visit(
          [&](auto &value) mutable
          {
             if constexpr (std::is_same_v<std::decay_t<decltype(value)>, T>)
             {
                result = &value;
             }
          },
          variant);

      return result;
   }

   template <typename T, typename VariantType>
   const T *try_variant_cast(VariantType const &variant)
   {
      const T *result = nullptr;

      std::visit(
          [&](auto &value) mutable
          {
             if constexpr (std::is_same_v<std::decay_t<decltype(value)>, T>)
             {
                result = &value;
             }
          },
          variant);

      return result;
   }
} // namespace commons