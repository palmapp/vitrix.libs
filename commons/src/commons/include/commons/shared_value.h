//
// Created by jhrub on 8/7/2023.
//

#pragma once
#include <mutex>
#include <optional>

namespace commons
{

   template <class V>
   class shared_value
   {
    public:
      auto get_value() const -> V
      {

         auto ret = V{};

         {
            auto lock = std::lock_guard{mutex_};
            ret = value_;
         }

         return ret;
      }

      auto exchange_value(V value) -> V
      {
         auto ret = V{};

         {
            auto lock = std::lock_guard{mutex_};
            ret = std::move(value_);
            value_ = std::move(value);
         }

         return ret;
      }

      auto set_value(V value) -> void
      {

         auto lock = std::lock_guard{mutex_};
         value_ = std::move(value);
      }

    private:
      mutable std::mutex mutex_;
      V value_ = {};
   };
} // namespace commons