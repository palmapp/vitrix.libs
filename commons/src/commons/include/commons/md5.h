#pragma once
#include <boost/filesystem.hpp>
#include <stdint.h>
#include <string.h>

namespace commons
{
   namespace hash
   {

      class md5
      {
       public:
         md5() noexcept;

         class result
         {
          public:
            uint8_t value[16];

            bool is_same(const uint8_t *data, size_t len) const;

            inline size_t size() const noexcept
            {
               return sizeof(value);
            }

            inline const uint8_t *begin() noexcept
            {
               return value;
            }

            inline const uint8_t *end() noexcept
            {
               return value + size();
            }
         };

         void update(const uint8_t *data, uint32_t len) noexcept;
         result finish() noexcept;

       private:
         uint32_t _buffer[4];
         uint32_t _bits[4];
         uint8_t _in[64];
      };

      md5::result calculate_file_checksum(const boost::filesystem::path &path);

      std::string hash(const std::string& to_hash);

   } // namespace hash
} // namespace commons