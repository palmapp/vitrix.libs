#pragma once
#include <chrono>
#include <cstdint>

namespace commons
{
   namespace time_utils
   {

      inline std::int64_t now_in_millis()
      {
         using namespace std::chrono;
         return static_cast<std::int64_t>(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
      }

      inline std::chrono::time_point<std::chrono::system_clock> from_millis(std::int64_t milliseconds)
      {
         return std::chrono::time_point<std::chrono::system_clock>(std::chrono::milliseconds(milliseconds));
      }

      inline std::int64_t to_millis(std::chrono::time_point<std::chrono::system_clock> tp)
      {
         using namespace std::chrono;
         return static_cast<std::int64_t>(duration_cast<milliseconds>(tp.time_since_epoch()).count());
      }

      inline std::int64_t to_millis(time_t tp)
      {
         return tp * 1000;
      }

      std::string format_utc_iso_time(std::int64_t time_in_millis, bool time_zone_offset = true);
      std::string format_utc_iso_time(std::chrono::system_clock::time_point time_point, bool time_zone_offset = true);

      std::string format_local_iso_time(std::int64_t time_in_millis, bool time_zone_offset = true);
      std::string format_local_iso_time(std::chrono::system_clock::time_point time_point, bool time_zone_offset = true);

   } // namespace time_utils
} // namespace commons