#pragma once

#include <boost/filesystem.hpp>

#include <vector>

namespace commons
{

   void copy_directory_recursive(const boost::filesystem::path &src, const boost::filesystem::path &dst);
   std::string get_file_extension_without_dot(const boost::filesystem::path &src);
   std::string read_file(const boost::filesystem::path &src);

   namespace fs
   {
      inline std::string get_separator()
      {
         std::string separator = "/";
         separator[0] = boost::filesystem::path::preferred_separator;

         return separator;
      }

   } // namespace fs
} // namespace commons
