#pragma once
#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include <cctype>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>

namespace commons
{
   enum properties_parser_state
   {
      pps_wait_for_key,
      pps_key,
      pps_key_end,
      pps_comment,
      pps_wait_for_value,
      pps_string_value,
      pps_string_escape,
      pps_value
   };

   template <class ItT, class PropertiesHandlerT>
   void parse_properties(ItT cursor, ItT end, PropertiesHandlerT handler)
   {
      properties_parser_state state = pps_wait_for_key;
      std::string key_buffer;
      std::string value_buffer;

      for (; cursor != end; ++cursor)
      {
         char ch = *cursor;
         switch (state)
         {
         case pps_wait_for_key:
            if (ch == '#')
            {
               state = pps_comment;
               break;
            }
            else if (std::isalpha(ch))
            {
               state = pps_key;
            }
            else
            {
               break;
            }
            [[fallthrough]];
         case pps_key:
            if (std::isspace(ch) || ch == '=')
            {
               state = pps_key_end;
               // fallthroug pps_key_end
            }
            else
            {
               key_buffer += ch;
               break;
            }
            [[fallthrough]];
         case pps_key_end:
            if (ch == '=')
            {
               state = pps_wait_for_value;
            }
            break;
         case pps_wait_for_value:
            if (std::isprint(ch) && !std::isspace(ch))
            {
               if (ch == '"')
               {
                  state = pps_string_value;
               }
               else
               {
                  state = pps_value;
                  value_buffer += ch;
               }
            }
            else if (ch == '\n')
            {
               handler(key_buffer, value_buffer);
               key_buffer.clear();
               state = pps_wait_for_key;
            }
            break;
         case pps_string_value:
            if (ch == '\\')
            {
               state = pps_string_escape;
            }
            else if (ch == '"')
            {
               handler(key_buffer, value_buffer);
               key_buffer.clear();
               value_buffer.clear();
               state = pps_wait_for_key;
            }
            else
            {
               value_buffer += ch;
            }
            break;
         case pps_value:
            if (std::isprint(ch))
               value_buffer += ch;
            else
            {
               handler(key_buffer, value_buffer);
               key_buffer.clear();
               value_buffer.clear();
               state = pps_wait_for_key;
            }
            break;
         case pps_string_escape:
            value_buffer += ch;
            state = pps_string_value;
            break;

         case pps_comment:
            if (ch == '\n') state = pps_wait_for_key;
            break;
         }
      }

      if (state >= pps_wait_for_value)
      {
         handler(key_buffer, value_buffer);
      }
   }

   template <class PropertiesHandlerT>
   void parse_properties(std::istream &is, PropertiesHandlerT &&handler)
   {
      std::istreambuf_iterator<char> end; // end-of-range iterator
      std::istreambuf_iterator<char> begin(is.rdbuf());
      parse_properties(begin, end, std::forward<PropertiesHandlerT>(handler));
   }

   using properties_map = boost::unordered_map<std::string, std::string>;

   inline properties_map parse_properties_to_map(std::istream &is)
   {
      properties_map result;
      parse_properties(is, [&](const std::string &key, const std::string &value) { result[key] = value; });

      return result;
   }

   inline properties_map parse_config_file(const boost::filesystem::path &path)
   {
      const auto *value = path.c_str();
      std::ifstream props_stream{value};
      return parse_properties_to_map(props_stream);
   }

   inline std::string get_map_value(const commons::properties_map &map, const std::string &key,
                                    const std::string &default_value = std::string{})
   {
      auto it = map.find(key);
      if (it != map.end() && !it->second.empty())
      {
         return it->second;
      }
      else
      {
         return default_value;
      }
   }

   inline int get_int_map_value(const commons::properties_map &map, const std::string &key, const int default_value = 0)
   {
      auto it = map.find(key);
      if (it != map.end() && !it->second.empty())
      {
         return std::atoi(it->second.c_str());
      }
      else
      {
         return default_value;
      }
   }

   inline bool get_bool_map_value(const commons::properties_map &map, const std::string &key,
                                  const bool default_value = false)
   {
      auto it = map.find(key);
      if (it != map.end() && !it->second.empty())
      {
         // can't transform const string, I do not really need this
         // std::transform(it->second.begin(), it->second.end(), it->second.begin(), ::tolower);
         std::istringstream is(it->second);
         bool b;
         is >> std::boolalpha >> b;
         return b;
      }
      else
      {
         return default_value;
      }
   }

} // namespace commons
