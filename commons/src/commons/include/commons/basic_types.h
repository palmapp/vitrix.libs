//
// Created by jhrub on 19.05.2021.
//

#pragma once
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <cstddef>
#include <cstdint>
#include <deque>
#include <gsl/span>
#include <map>
#include <optional>
#include <set>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <boost/utility/string_view.hpp>

namespace commons::basic_types
{
   enum class data_type
   {
      null,
      boolean,
      i8,
      i16,
      i32,
      i64,
      u8,
      u16,
      u32,
      u64,
      fl32,
      fl64,
      string
   };

   using null = std::nullptr_t;
   using i8 = std::int8_t;
   using i16 = std::int16_t;
   using i32 = std::int32_t;
   using i64 = std::int64_t;

   using u8 = std::uint8_t;
   using u16 = std::uint16_t;
   using u32 = std::uint32_t;
   using u64 = std::uint64_t;

   using fl32 = float;
   using fl64 = double;
   using string = std::string;
   using string_view = std::string_view;

   template <class T>
   using optional = std::optional<T>;

   template <class T>
   using array_view = gsl::span<const T>;

   template <class T>
   using mutable_array_view = gsl::span<T>;

   // basic containers

   template <class T>
   using vector = std::vector<T>;

   template <class T>
   using deque = std::deque<T>;

   template <class Key, class T, class Compare = std::less<Key>,
             class Allocator = std::allocator<std::pair<const Key, T>>>
   using map = std::map<Key, T, Compare, Allocator>;

   template <class Key, class T, class Hash = boost::hash<Key>, class KeyEqual = std::equal_to<Key>,
             class Allocator = std::allocator<std::pair<const Key, T>>>
   using unordered_map = boost::unordered_map<Key, T, Hash, KeyEqual, Allocator>;

   template <class Key, class Compare = std::less<Key>, class Allocator = std::allocator<Key>>
   using set = std::set<Key, Compare, Allocator>;

   template <class Key, class Hash = boost::hash<Key>, class KeyEqual = std::equal_to<Key>,
             class Allocator = std::allocator<Key>>
   using unordered_set = boost::unordered_set<Key, Hash, KeyEqual, Allocator>;

   template <class First, class Second>
   using pair = std::pair<First, Second>;

   template <class... T>
   using tuple = std::tuple<T...>;

   inline string_view to_our_string_view(boost::string_view view)
   {
      return {view.data(), view.size()};
   }

   inline boost::string_view to_boost_string_view(string_view view)
   {
      return {view.data(), view.size()};
   }
} // namespace commons::basic_types