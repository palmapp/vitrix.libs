#pragma once
#include <string>
#include <string_view>

namespace encoding
{
   std::string hex_encode(const std::string &data);
   std::string hex_encode(const void *data, size_t len);
   std::string hex_decode(const std::string &data);
   std::string hex_encode(std::string_view data);
} // namespace encoding
