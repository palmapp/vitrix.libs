#include <cpprest/uri_builder.h>
#include <map>
#include <string>

namespace commons
{
   namespace url
   {
      class uri_builder
      {
       public:
         uri_builder(const std::string &url);
         uri_builder(const web::uri &uri);

         uri_builder(const uri_builder &);
         uri_builder(uri_builder &&);

         const uri_builder &operator=(const uri_builder &);
         const uri_builder &operator=(uri_builder &&);

         uri_builder make_relative(const std::string &path, const std::map<std::string, std::string> &query = {},
                                   bool encode = false);
         uri_builder add_query(const std::map<std::string, std::string> &query, bool encode = false);
         std::string to_string() const;
         web::uri to_uri() const;

       private:
         void append_query(web::uri_builder &uri_builder, const std::map<std::string, std::string> &query, bool encode);

         web::uri_builder uri_builder_;
      };
   } // namespace url
} // namespace commons