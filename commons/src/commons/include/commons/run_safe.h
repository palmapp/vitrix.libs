#pragma once
#include <easylogging++.h>
#include <stdexcept>

namespace commons
{
   template <class Operation>
   bool run_safe(Operation op, const char *operation_id = "unknown operation") noexcept
   {
      bool result = true;
      try
      {
         op();
      }
      catch (const std::exception &ex)
      {
         LOG(ERROR) << operation_id << " failed with exception " << ex.what();
         result = false;
      }
      catch (...)
      {
         LOG(ERROR) << operation_id << " failed with unknown exception";
         result = false;
      }

      return result;
   }

   template <class Operation, class ErrorInfoProvider>
   bool run_safe(Operation op, ErrorInfoProvider error_info_provider,
                 const char *operation_id = "unknown operation") noexcept
   {
      bool result = true;
      try
      {
         op();
      }
      catch (const std::exception &ex)
      {
         LOG(ERROR) << operation_id << " failed with exception " << ex.what() << " : \n\t" << error_info_provider(ex);
         result = false;
      }
      catch (...)
      {
         LOG(ERROR) << operation_id << " failed with unknown exception";
         result = false;
      }

      return result;
   }

} // namespace commons