//
// Created by jhrub on 11.11.2022.
//

#pragma once
#include <array>
#include <chrono>
#include <easylogging++.h>
#include <fmt/chrono.h>
#include <fmt/format.h>

#include <string_view>

namespace commons
{
   template <size_t N>
   class perf_scope
   {
   public:
      using clock = std::chrono::steady_clock;
      using time_point = clock::time_point;
      using ms_duration = std::chrono::duration<double, std::milli>;

      perf_scope(std::string_view name)
      {
         logger_ = el::Loggers::getLogger("perf");
         position_ = logger_->enabled(el::Level::Debug) ? 0 : -1;

         push_measure(name);
      }

      ~perf_scope()
      {
         if (position_ > 0)
         {
            auto finish = clock::now();
            auto &[start, start_name] = checkpoints_[0];

            auto buffer = fmt::memory_buffer{};
            buffer.reserve(1000);

            fmt::format_to(std::back_inserter(buffer), "Perf scope: {}\n", start_name);
            fmt::format_to(std::back_inserter(buffer), "{:30}\t{:>10}\t{:>10}\n", "Name", "From start", "From prev");

            for (size_t i = 1; i != static_cast<size_t>(position_); ++i)
            {
               auto &[point, name] = checkpoints_[i];
               auto diff_from_start = point - start;
               auto diff_from_previous = point - checkpoints_[i - 1].first;

               fmt::format_to(std::back_inserter(buffer), "{:30}\t{:>10}\t{:>10}\n", name,
                              std::chrono::duration_cast<ms_duration>(diff_from_start),
                              std::chrono::duration_cast<ms_duration>(diff_from_previous));
            }

            auto diff_from_start = finish - start;
            auto diff_from_previous = finish - checkpoints_[position_ - 1].first;

            fmt::format_to(std::back_inserter(buffer), "{:30}\t{:>10}\t{:>10}\n", "End scope",
                           std::chrono::duration_cast<ms_duration>(diff_from_start),
                           std::chrono::duration_cast<ms_duration>(diff_from_previous));
            logger_->debug(std::string_view{buffer.data(), buffer.size()});
         }
      }

      void checkpoint(std::string_view name)
      {
         push_measure(name);
      }

   private:
      void push_measure(std::string_view name)
      {
         if (position_ >= 0 && (static_cast<std::size_t>(position_) < checkpoints_.size()))
         {
            checkpoints_[position_++] = {clock::now(), name};
         }
      }

      int position_;
      std::array<std::pair<time_point, std::string_view>, N + 1> checkpoints_;
      el::Logger *logger_;
   };

} // namespace commons
