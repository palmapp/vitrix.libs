#pragma once
#include <boost/container/small_vector.hpp>
#include <boost/unordered_map.hpp>
#include <visit_struct/visit_struct.hpp>

namespace commons
{

   template <class Model>
   struct model_traits
   {
      using id_type = decltype(Model::id);
      using field = uint8_t;
      using change_set = boost::container::small_vector<field, 16>;

      struct eq_visitor
      {
         change_set &changes;
         field idx = 0;

         template <typename T>
         void operator()(const char *, T &t1, const T &t2)
         {
            if (t1 != t2)
            {
               changes.push_back(idx);
               t1 = t2;
            }
            ++idx;
         }
      };

      static change_set update(Model &updating, const Model &changed)
      {
         change_set result;
         eq_visitor eq{result};

         visit_struct::for_each(updating, changed, eq);

         return result;
      }
   };

   template <class Model>
   class model_observer
   {
    public:
      using traits = model_traits<Model>;
      using id_type = typename traits::id_type;

      virtual ~model_observer() = default;

      virtual void on_added(const id_type &id, const Model &value) = 0;
      virtual void on_changed(const id_type &id, const Model &value,
                              const typename model_traits<Model>::change_set &changes) = 0;
      virtual void on_removed(const id_type &id) = 0;
   };

   enum class update_result
   {
      not_updated,
      updated,
      added
   };

   template <class Model>
   class observable_model
   {
    public:
      using traits = model_traits<Model>;
      using id_type = typename traits::id_type;

      update_result update_model(const id_type &id, const Model &m)
      {
         auto it = model_index_.find(id);
         if (it != model_index_.end())
         {
            auto change_set = traits::update(it->second, m);
            if (change_set.empty())
            {
               return update_result::not_updated;
            }
            else
            {
               notify_update_observer(id, m, change_set);
               return update_result::updated;
            }
         }
         else
         {
            model_index_.insert(typename index::value_type{id, m});
            notify_add_observer(id, m);
            return update_result::added;
         }
      }

      bool remove_model(const id_type &id)
      {
         if (model_index_.erase(id) != 0)
         {
            notify_remove_observer(id);
            return true;
         }
         return false;
      }

      bool try_get_model_by_id(Model &out, const id_type &id) const
      {
         auto it = model_index_.find(id);
         if (it != model_index_.end())
         {
            out = it->second;

            return true;
         }

         return false;
      }

      std::vector<Model> get_all_models() const
      {
         std::vector<Model> models;
         models.reserve(model_index_.size());

         std::transform(model_index_.begin(), model_index_.end(), std::back_inserter(models),
                        [](const typename index::value_type &kv) { return kv.second; });

         return models;
      }

      void set_model_observer(model_observer<Model> *observer)
      {
         model_observer_ = observer;
      }

      model_observer<Model> *get_model_observer() const
      {
         return model_observer_;
      }

    private:
      void notify_update_observer(const id_type &id, const Model &m,
                                  const typename model_traits<Model>::change_set &changes)
      {
         if (model_observer_ != nullptr)
         {
            model_observer_->on_changed(id, m, changes);
         }
      }

      void notify_add_observer(const id_type &id, const Model &m)
      {
         if (model_observer_ != nullptr)
         {
            model_observer_->on_added(id, m);
         }
      }

      void notify_remove_observer(const id_type &id)
      {
         if (model_observer_ != nullptr)
         {
            model_observer_->on_removed(id);
         }
      }
      using index = boost::unordered_map<id_type, Model>;
      index model_index_;
      model_observer<Model> *model_observer_ = nullptr;
   };
} // namespace commons
