//
// Created by jhrub on 21.08.2020.
//

#pragma once
#include "properties_parser.h"
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <optional>
#include <string>
#include <string_view>
#include <vector>
namespace commons
{

   struct application_properties_settings
   {
      boost::filesystem::path base_path{};
      std::vector<std::string> properties_variants{"env.properties", "app.properties"};
      int levels = std::numeric_limits<int>::max();
   };

   class application_properties
   {
    public:
      application_properties(application_properties_settings settings);

      template <class Type>
      std::optional<Type> try_get(std::string const &key) const;

      template <class Type>
      Type get(std::string const &key, Type &&default_value = {}) const;

      template <class Type>
      Type get_for_keys(std::vector<std::string> keys, Type &&default_value = {}) const;

    private:
      std::optional<std::string_view> try_get_value(std::string const &key) const;

      std::vector<boost::filesystem::path> seek_properties_files(application_properties_settings const &settings) const;

      void load_properties(std::vector<boost::filesystem::path> const &files);
      void expand_properties_path_variants(std::vector<boost::filesystem::path> &result,
                                           commons::application_properties_settings const &settings,
                                           boost::filesystem::path const &current_path) const;

      std::vector<properties_map> properties_chain_;
   };

   template <class Type>
   std::optional<Type> application_properties::try_get(const std::string &key) const
   {
      const auto value = this->try_get_value(key);
      if (value.has_value())
      {
         try
         {
            return {boost::lexical_cast<Type>(value.value())};
         }
         catch (...)
         {
            return {};
         }
      }
      return {};
   }

   template <class Type>
   Type application_properties::get(std::string const &key, Type &&default_value) const
   {
      return this->try_get<Type>(key).value_or(std::forward<Type>(default_value));
   }

   template <class Type>
   Type application_properties::get_for_keys(std::vector<std::string> keys, Type &&default_value) const
   {
      for (auto &key : keys)
      {
         auto value = this->try_get<Type>(key);
         if (value.has_value())
         {
            return value.value();
         }
      }
      return std::forward<Type>(default_value);
   }

} // namespace commons