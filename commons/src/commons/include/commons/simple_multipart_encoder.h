#pragma once

#include <boost/filesystem.hpp>
#include <sstream>
#include <string>
#include <vector>

namespace commons
{

   class simple_multipart_encoder
   {
    public:
      simple_multipart_encoder();

      inline const std::string &get_boundary()
      {
         return boundary_;
      }

      inline void add_param(const std::string &name, const std::string &value)
      {
         params_.emplace_back(std::pair{name, value});
      }

      inline void add_file(const std::string &name, const boost::filesystem::path &path)
      {
         files_.emplace_back(std::pair{name, path});
      }

      const std::string generate();

    private:
      static const std::string boundary_prefix_;
      static const std::string rand_chars_;
      std::string boundary_;
      std::stringstream body_content_;
      std::vector<std::pair<std::string, std::string>> params_;
      std::vector<std::pair<std::string, boost::filesystem::path>> files_;
   };

} // namespace commons
