//
// Created by jhrub on 6/30/2023.
//

#pragma once
#include <chrono>
#include <string>
#include <utility>
#include <vector>
#include <tuple>

#include <fmt/format.h>
#include <fmt/chrono.h>

namespace commons
{

   template<class...Durations, class DurationIn>
   std::tuple<Durations...> break_down_durations( DurationIn d ) {
      std::tuple<Durations...> retval;
      using discard=int[];
      (void)discard{0,(void((
                            (std::get<Durations>(retval) = std::chrono::duration_cast<Durations>(d)),
                            (d -= std::chrono::duration_cast<DurationIn>(std::get<Durations>(retval)))
                                )),0)...};
      return retval;
   }


   struct stop_watch
   {

    public:
      inline stop_watch()
      {
         reset();
      }

      [[nodiscard]] inline auto get_elapsed_duration_and_reset() -> std::chrono::steady_clock::duration
      {
         auto now = std::chrono::steady_clock::now();
         auto result = now - started_;

         started_ = now;
         lap_time_ = now;
         laps.clear();

         return result;
      }

      [[nodiscard]] inline auto get_elapsed() const -> std::chrono::steady_clock::duration
      {
         auto now = std::chrono::steady_clock::now();
         return now - started_;
      }

      inline auto reset() -> void
      {
         started_ = std::chrono::steady_clock::now();
         lap_time_ = started_;
         laps.clear();
      }

      inline auto push_lap(std::string name) -> std::chrono::steady_clock::duration
      {
         auto now = std::chrono::steady_clock::now();
         auto elapsed = now - lap_time_;
         lap_time_ = now;

         laps.emplace_back(std::move(name), elapsed);

         return elapsed;
      }



      inline auto to_string() const
      {
         auto elapsed = get_elapsed();

         auto buffer = fmt::memory_buffer{};
         buffer.reserve(1024);

         auto format_duration = [&](std::chrono::steady_clock::duration duration)
         {
            auto [minutes, seconds, millis] = break_down_durations<std::chrono::minutes, std::chrono::seconds, std::chrono::milliseconds>(duration);
            fmt::format_to(std::back_inserter(buffer), "{:02}:{:02}.{}", minutes, seconds, millis);
         };

         fmt::format_to(std::back_inserter(buffer), "elapsed:");
         format_duration(elapsed);
         fmt::format_to(std::back_inserter(buffer), "\n");

         for(auto & [key, val]: laps)
         {
            fmt::format_to(std::back_inserter(buffer), "{}: ",key);
            format_duration(val);
            fmt::format_to(std::back_inserter(buffer), "\n");
         }

         return fmt::to_string(buffer);
      }

      std::vector<std::pair<std::string, std::chrono::steady_clock::duration>> laps;

    private:
      std::chrono::steady_clock::time_point started_;
      std::chrono::steady_clock::time_point lap_time_;
   };

} // namespace commons