#pragma once
#include <cstdint>
#include <stdexcept>
#include <type_traits>

namespace commons
{
   template <typename, typename = int>
   struct has_required_type_id_static_member_field : std::false_type
   {
   };

   template <typename T>
   struct has_required_type_id_static_member_field<T, decltype(T::required_type_id, 0)>
       : std::integral_constant<bool, !std::is_member_pointer<decltype(&T::required_type_id)>::value>
   {
   };

   struct tx_context
   {
      virtual ~tx_context() noexcept(false) = default;
      virtual std::int64_t get_context_type_id() const noexcept = 0;

      template <class TxContextT>
      TxContextT *safe_cast() noexcept
      {
         static_assert(std::is_base_of<tx_context, TxContextT>::value, "TxContextT must be inherited from tx_context");
         static_assert(has_required_type_id_static_member_field<TxContextT>::value,
                       "TxContextT must define 'static const std::int64_t required_type_id = <req value>;' field. Use "
                       "tx_type_id_generator to generate type id.");

         auto type_id = this->get_context_type_id();
         if (type_id == TxContextT::required_type_id)
            return static_cast<TxContextT *>(this);
         else
            return nullptr;
      }

      template <class TxContextT>
      TxContextT &safe_cast_or_throw()
      {
         TxContextT *result = safe_cast<TxContextT>();
         if (result == nullptr)
         {
            throw std::runtime_error("bad cast: tx_context is expected to be a different type");
         }
         return *result;
      }
   };

   struct tx_type_id_generator
   {
    private:
      static std::int64_t generate()
      {
         static std::int64_t value = 0;
         return ++value;
      }

    public:
      template <class T>
      static std::int64_t generate_type_id()
      {
         static std::int64_t type_id = generate();
         return type_id;
      }
   };

   struct null_tx_context : public tx_context
   {
      static const std::int64_t required_type_id;
      std::int64_t get_context_type_id() const noexcept override;
   };
} // namespace commons