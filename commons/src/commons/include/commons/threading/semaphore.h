#pragma once
#include <atomic>
#include <mutex>

#include <condition_variable>

namespace commons
{
   class semaphore
   {
    public:
      semaphore(int initial_count = 0);

      void acquire() noexcept;
      bool try_acquire() noexcept;
      bool try_acquire(std::chrono::steady_clock::duration duration) noexcept;

      void release() noexcept;

    private:
      std::atomic<int> counter_;
      std::mutex mutex_;
      std::condition_variable cv_;
   };
} // namespace commons
