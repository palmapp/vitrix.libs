//
// Created by jhrub on 03.08.2021.
//

#pragma once
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <type_traits>

namespace commons
{
   template <class ContainerT>
   std::string join_container(ContainerT const &cnt, std::string delimiter)
   {
      using value_type = typename ContainerT::value_type;
      if constexpr (std::is_same_v<std::string, value_type>)
      {
         return boost::join(cnt, delimiter);
      }
      else
      {
         std::vector<std::string> str_cnt;
         for (auto &val : cnt)
         {
            str_cnt.push_back(boost::lexical_cast<std::string>(val));
         }

         return boost::join(str_cnt, delimiter);
      }
   }

} // namespace commons