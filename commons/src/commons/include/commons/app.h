#pragma once
#include <boost/application.hpp>
#include <thread>

namespace commons
{
   class app
   {
    public:
      app(boost::application::context &ctx);
      virtual ~app() = default;
      virtual int operator()();
      virtual bool stop() = 0;

      // windows specific (ignored on posix)
      inline bool pause()
      {
         return true; // return true to pause, false to ignore
      }

      inline bool resume()
      {
         return true; // return true to resume, false to ignore
      }

    private:
      std::thread thread_;

    protected:
      virtual void run() = 0;

      boost::application::context &context_;
   };

} // namespace commons
