#pragma once

#include "../../serialization/property.h"
#include "../../serialization/property_list_serializer.h"
#include <boost/signals2.hpp>
#include <string>

namespace commons::property
{
   class hub
   {
    public:
      virtual ~hub() = default;

      boost::signals2::signal<void(spider::property::property_context, const spider::property::property_list &)>
          value_changed;

      virtual void set_properties(spider::property::property_context context,
                                  const spider::property::property_list &properties) = 0;

      virtual void set_property(spider::property::property_context context, std::string prop,
                                spider::property::property_value value) = 0;

      virtual spider::property::properties get_properties(spider::property::context_id context_id) const = 0;

      virtual spider::property::property_list get_properties(spider::property::property_context context) const = 0;

      virtual spider::property::property_value get_property(spider::property::property_context context,
                                                            const std::string &key) const = 0;
   };

} // namespace commons::property
