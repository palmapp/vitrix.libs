#pragma once

#include "../hub.h"
#include <array>
#include <mutex>
#include <thread>

namespace commons::property
{

   class property_hub : public hub
   {
    public:
      property_hub();

      // Inherited via hub
      void set_properties(spider::property::property_context context,
                          const spider::property::property_list &properties) final;

      void set_property(spider::property::property_context context, std::string prop,
                        spider::property::property_value value) final;

      spider::property::properties get_properties(spider::property::context_id context_id) const final;

      spider::property::property_list get_properties(spider::property::property_context context) const final;

      spider::property::property_value get_property(spider::property::property_context context,
                                                    const std::string &key) const final;

    private:
      using property_vec =
          std::vector<std::pair<spider::property::context_id,
                                std::vector<std::pair<spider::property::scope_id, spider::property::property_list>>>>;
      std::map<spider::property::scope_type, property_vec> _data;

      spider::property::property_list &get_for_context(const spider::property::property_context &ctx);

      const spider::property::property_list *try_get_for_context(const spider::property::property_context &ctx) const;

      mutable std::mutex _lock;
   };
} // namespace commons::property