//
// Created by jhrub on 27.05.2021.
//

#pragma once
#include "basic_types.h"
#include "variant_utils.h"
#include <variant>

namespace commons::basic_types
{

   using field_value_variant =
   std::variant<bool, i8, i16, i32, i64, u8, u16, u32, u64, fl32, fl64, string_view, string>;

   enum class field_value_type
   {
      type_bool = variant_index<field_value_variant, bool>(),
      type_i8 = variant_index<field_value_variant, i8>(),
      type_i16 = variant_index<field_value_variant, i16>(),
      type_i32 = variant_index<field_value_variant, i32>(),
      type_i64 = variant_index<field_value_variant, i64>(),
      type_u8 = variant_index<field_value_variant, u8>(),
      type_u16 = variant_index<field_value_variant, u16>(),
      type_u32 = variant_index<field_value_variant, u32>(),
      type_u64 = variant_index<field_value_variant, u64>(),
      type_fl32 = variant_index<field_value_variant, fl32>(),
      type_fl64 = variant_index<field_value_variant, fl64>(),
      type_string_view = variant_index<field_value_variant, string_view>(),
      type_string = variant_index<field_value_variant, string>(),
      type_notfound
   };

   constexpr std::string_view field_value_type_strings[] = {"bool", "i8", "i16", "i32", "i64",
                                                            "u8", "u16", "u32", "u64", "fl32",
                                                            "fl64", "string_view", "string", "invalid"};

   inline field_value_type get_field_value_type(string_view key)
   {
      for (int i = 0; i < static_cast<int>(field_value_type::type_notfound); ++i)
      {
         auto current_key = field_value_type_strings[i];
         if (key == current_key)
         {
            return static_cast<field_value_type>(i);
         }
      }

      return field_value_type::type_notfound;
   }

   constexpr std::string_view field_value_type_to_string(field_value_type key)
   {
      return field_value_type_strings[static_cast<int>(key)];
   }

   constexpr std::string_view field_value_type_to_string(std::size_t key)
   {
      if (key >= static_cast<std::size_t>(field_value_type::type_notfound))
      {
         return field_value_type_strings[key];
      }
      else
      {
         return field_value_type_to_string(static_cast<field_value_type>(key));
      }
   }

   struct field_value
   {
      static constexpr auto safe_char_ptr_to_string_view(const char *str) -> string_view
      {
         return str == nullptr ? string_view{} : string_view{str};
      };

      constexpr field_value()
      {
      }

      constexpr field_value(bool val)
         : data_(val)
      {
      }

      constexpr field_value(i8 val)
         : data_(val)
      {
      }

      constexpr field_value(i16 val)
         : data_(val)
      {
      }

      constexpr field_value(i32 val)
         : data_(val)
      {
      }

      constexpr field_value(i64 val)
         : data_(val)
      {
      }

      constexpr field_value(u8 val)
         : data_(val)
      {
      }

      constexpr field_value(u16 val)
         : data_(val)
      {
      }

      constexpr field_value(u32 val)
         : data_(val)
      {
      }

      constexpr field_value(u64 val)
         : data_(val)
      {
      }

      constexpr field_value(fl32 val)
         : data_(val)
      {
      }

      constexpr field_value(fl64 val)
         : data_(val)
      {
      }

      field_value(string val)
         : data_(std::move(val))
      {
      }

      constexpr field_value(string_view val)
         : data_(val)
      {
      }

      constexpr field_value(const char *str)
         : data_(safe_char_ptr_to_string_view(str))
      {
      }

      field_value(field_value &&value)
         : data_(std::move(value.data_))
      {
      }

      field_value(field_value const &value)
         : data_(value.data_)
      {
      }

      field_value &operator=(field_value &&value)
      {
         data_ = std::move(value.data_);
         return *this;
      }

      field_value &operator=(field_value const &value)
      {
         data_ = value.data_;
         return *this;
      }

      inline field_value &operator=(const char *str)
      {
         data_ = safe_char_ptr_to_string_view(str);
         return *this;
      }

      template <class T>
      inline field_value &operator=(T &&val)
      {
         data_ = field_value_variant{std::forward<T>(val)};
         return *this;
      }

      inline operator field_value_variant &()
      {
         return data_;
      }

      constexpr std::size_t index() const
      {
         return data_.index();
      }

      constexpr field_value_type type() const
      {
         return static_cast<field_value_type>(index());
      }

      constexpr string_view type_string() const
      {
         return field_value_type_to_string(type());
      }

      constexpr inline operator field_value_variant const &() const
      {
         return data_;
      }

      constexpr inline bool operator==(field_value const &other) const
      {
         return data_ == other.data_;
      }

      constexpr inline bool operator!=(field_value const &other) const
      {
         return data_ != other.data_;
      }

      template <class T>
      void visit(T &&functor)
      {
         std::visit(std::forward<T>(functor), data_);
      }

      template <class T>
      T *try_cast()
      {

         return try_variant_cast<T>(data_);
      }

      template <class T>
      T const *try_cast() const
      {

         return try_variant_cast<T>(data_);
      }

      const optional<fl64> get_number_value() const
      {
         optional<fl64> result = {};

         visit(
            [&](auto &val) mutable
            {
               if constexpr (std::is_same_v<fl64, std::decay_t<decltype(val)>> ||
                             std::is_same_v<fl32, std::decay_t<decltype(val)>> ||
                             std::is_same_v<u8, std::decay_t<decltype(val)>> ||
                             std::is_same_v<i16, std::decay_t<decltype(val)>> ||
                             std::is_same_v<i32, std::decay_t<decltype(val)>> ||
                             std::is_same_v<u16, std::decay_t<decltype(val)>> ||
                             std::is_same_v<u32, std::decay_t<decltype(val)>>)
               {
                  result = gsl::narrow_cast<fl64>(val);
               }
            });

         return result;
      };

      template <class T>
      void visit(T &&functor) const
      {
         std::visit(std::forward<T>(functor), data_);
      }

   private:
      field_value_variant data_;
   };

   using optional_field_value = optional<field_value>;

   template <class T>
   struct is_optional_field_value_convertible
   {
      static const bool value = std::is_constructible_v<field_value, T>;
      static const bool is_optional = false;
   };

   template <class T>
   struct is_optional_field_value_convertible<std::optional<T>>
   {
      static const bool value = std::is_constructible_v<field_value, T>;
      static const bool is_optional = true;
   };

   template <class T>
   constexpr bool is_optional_field_value_convertible_v = is_optional_field_value_convertible<T>::value;

} // namespace commons::basic_types
