//
// Created by jhrub on 25.04.2023.
//

#pragma once

//
// Created by jhrub on 17.07.2021.
//

#pragma once
#include "overload.h"
#include <boost/hana.hpp>
#include <stdexcept>
#include <string_view>
#include <tuple>
#include <type_traits>

namespace commons
{

   template <class BaseDialectT, class... ImplementationT>
   struct registry
   {
      static_assert((std::is_base_of_v<BaseDialectT, ImplementationT> && ...),
                    "BaseDialectT must be derived from implementation_base");

      std::tuple<ImplementationT...> implementation_;
      constexpr registry() : implementation_{ImplementationT{}...} {}

      constexpr BaseDialectT const &get_implementation(std::string_view id) const
      {
         const auto &dialect = std::apply(
             [&](auto const &...impl) -> BaseDialectT const & { return find_by_id(id, impl...); }, implementation_);

         return dialect;
      }

    private:
      constexpr BaseDialectT const &find_by_id(std::string_view id, auto const &impl) const
      {
         if (impl.id == id) return impl;

         throw std::logic_error{"Implementation could not be found."};
      }
      constexpr BaseDialectT const &find_by_id(std::string_view id, auto const &impl, auto const &...rest) const
      {
         if (impl.id == id) return impl;

         return find_by_id(id, rest...);
      }
   };

   template <class BaseDialectT, class... DialectT>
   constexpr registry<BaseDialectT, DialectT...> make_registry()
   {
      return {};
   }
} // namespace commons