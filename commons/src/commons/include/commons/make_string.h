#pragma once
#include <sstream>
#include <string>

namespace commons
{

   template <class T>
   std::string make_string(const T &arg)
   {
      std::stringstream buffer;

      buffer << arg;

      return buffer.str();
   }

   template <class T, class T1>
   std::string make_string(const T &arg, const T1 &arg1)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;

      return buffer.str();
   }

   template <class T, class T1, class T2>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;

      std::string result = buffer.str();
      return result;
   }

   template <class T, class T1, class T2, class T3>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5, class T6>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5,
                           const T6 &arg6)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;
      buffer << arg6;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5, class T6, class T7>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5,
                           const T6 &arg6, const T7 &arg7)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;
      buffer << arg6;
      buffer << arg7;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5,
                           const T6 &arg6, const T7 &arg7, const T8 &arg8)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;
      buffer << arg6;
      buffer << arg7;
      buffer << arg8;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5,
                           const T6 &arg6, const T7 &arg7, const T8 &arg8, const T9 &arg9)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;
      buffer << arg6;
      buffer << arg7;
      buffer << arg8;
      buffer << arg9;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9,
             class T10>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5,
                           const T6 &arg6, const T7 &arg7, const T8 &arg8, const T9 &arg9, const T10 &arg10)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;
      buffer << arg6;
      buffer << arg7;
      buffer << arg8;
      buffer << arg9;
      buffer << arg10;

      return buffer.str();
   }

   template <class T, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9,
             class T10, class T11>
   std::string make_string(const T &arg, const T1 &arg1, const T2 &arg2, const T3 &arg3, const T4 &arg4, const T5 &arg5,
                           const T6 &arg6, const T7 &arg7, const T8 &arg8, const T9 &arg9, const T10 &arg10,
                           const T11 &arg11)
   {
      std::stringstream buffer;

      buffer << arg;
      buffer << arg1;
      buffer << arg2;
      buffer << arg3;
      buffer << arg4;
      buffer << arg5;
      buffer << arg6;
      buffer << arg7;
      buffer << arg8;
      buffer << arg9;
      buffer << arg10;
      buffer << arg11;
      return buffer.str();
   }
} // namespace commons