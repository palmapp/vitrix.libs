#pragma once
#include "binary_types.h"

namespace spider
{
   namespace serialization
   {
      namespace binary
      {
         enum class field_parse_result
         {
            ok,
            ignored,
            failed
         };

         template <class FieldT, class OutputIt>
         bool convert_field_to_output(FieldT const & /*field*/, OutputIt &it)
         {
            return false;
         }

         template <class ByteIt>
         struct binary_member_visitor
         {
            ByteIt &cursor;
            ByteIt end;

            binary_member_visitor(ByteIt &begin, ByteIt e) : cursor(begin), end(e) {}

            bool error = false;

            template <typename T>
            void operator()(const std::string &, T &t)
            {
               if (!error)
               {
                  error = types::field_binary_io<T>::read_binary(t, cursor, end);
               }
            }
         };

         template <class OutputIt>
         struct output_binary_member_visitor
         {
            OutputIt &output_it;

            output_binary_member_visitor(OutputIt &output_it) : output_it(output_it) {}

            template <typename T>
            void operator()(const std::string &, T const &t)
            {
               types::field_binary_io<T>::write_binary(t, output_it);
            }
         };

         struct binary_size_visitor
         {
            std::size_t message_size = 0;

            template <typename T>
            void operator()(const std::string &, T const &field)
            {
               message_size += types::field_binary_io<T>::get_size(field);
            }
         };

         namespace types
         {
            template <class FieldT>
            struct binary_io<FieldT, std::true_type>
            {

               template <class InputIt>
               static bool read_binary(FieldT &field, InputIt &cursor, InputIt end)
               {
                  binary_member_visitor<InputIt> visitor(cursor, end);
                  visit_struct::for_each(field, visitor);

                  return visitor.error;
               }

               template <class OutputIt>
               static void write_binary(FieldT const &field, OutputIt &output_it)
               {
                  output_binary_member_visitor<OutputIt> visitor{output_it};
                  visit_struct::for_each(field, visitor);
               }

               static std::size_t get_size(FieldT const &d)
               {
                  binary_size_visitor visitor{};
                  visit_struct::for_each(d, visitor);

                  return visitor.message_size;
               }
            };
         } // namespace types

         template <class Data>
         std::size_t message_size(Data const &d)
         {
            binary_size_visitor visitor{};
            visit_struct::for_each(d, visitor);

            return visitor.message_size;
         }
         template <class Data>
         std::size_t message_size()
         {
            Data d{};
            return message_size(d);
         }

         template <class Data, class It>
         bool parse_binary(Data &d, It begin, It end)
         {
            binary_member_visitor<It> visitor(begin, end);
            visit_struct::for_each(d, visitor);

            return !visitor.error;
         }

         template <class Data>
         Data from_string(const std::string &buffer)
         {
            Data d{};

            if (!parse_binary(d, buffer.begin(), buffer.end()))
            {
               throw std::domain_error("invalid binary data");
            }

            return d;
         }

         template <class Data, class OutIt>
         OutIt to_bytes(Data const &data, OutIt output)
         {
            output_binary_member_visitor<OutIt> visitor{output};
            visit_struct::for_each(data, visitor);

            return visitor.output_it;
         }

         template <class Data>
         std::string to_string(Data const &data)
         {
            std::string output;
            output.resize(message_size<Data>(data));

            auto it = output.begin();
            output_binary_member_visitor<decltype(it)> visitor{it};
            visit_struct::for_each(data, visitor);

            return output;
         }
      } // namespace binary
   }    // namespace serialization
} // namespace spider
