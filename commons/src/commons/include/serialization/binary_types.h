#pragma once
#include <array>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/list.hpp>
#include <cstdint>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <span>
#include <visit_struct/visit_struct.hpp>

namespace spider
{
   namespace serialization
   {
      namespace binary
      {
         namespace types
         {
            using s8 = std::int8_t;
            using u8 = std::uint8_t;

            namespace endianess
            {
               struct little_endian
               {
               };
               struct big_endian
               {
               };

               class Endian
               {
                private:
                  static constexpr std::uint32_t uint32_ = 0x01020304;
                  static constexpr std::uint8_t magic_ = static_cast<std::uint8_t>(uint32_);

                public:
                  static constexpr bool little = magic_ == 0x04;
                  static constexpr bool middle = magic_ == 0x02;
                  static constexpr bool big = magic_ == 0x01;
                  static_assert(little || middle || big, "Cannot determine endianness!");

                private:
                  Endian() = delete;
               };

               constexpr bool is_platform_big_endian()
               {
                  return Endian::big;
               }

               constexpr std::uint16_t swap(std::uint16_t a) noexcept
               {
                  a = ((a & 0x00FF) << 8) | ((a & 0xFF00) >> 8);
                  return a;
               }
               constexpr std::int16_t swap(std::int16_t a) noexcept
               {
                  return static_cast<std::int16_t>(swap(static_cast<std::uint16_t>(a)));
               }

               constexpr std::uint32_t swap(std::uint32_t a) noexcept
               {
                  a = ((a & 0x000000FF) << 24) | ((a & 0x0000FF00) << 8) | ((a & 0x00FF0000) >> 8) |
                      ((a & 0xFF000000) >> 24);
                  return a;
               }

               constexpr std::int32_t swap(std::int32_t a) noexcept
               {
                  return static_cast<std::int32_t>(swap(static_cast<std::uint32_t>(a)));
               }

               constexpr std::uint64_t swap(std::uint64_t a) noexcept
               {
                  a = ((a & 0x00000000000000FFULL) << 56) | ((a & 0x000000000000FF00ULL) << 40) |
                      ((a & 0x0000000000FF0000ULL) << 24) | ((a & 0x00000000FF000000ULL) << 8) |
                      ((a & 0x000000FF00000000ULL) >> 8) | ((a & 0x0000FF0000000000ULL) >> 24) |
                      ((a & 0x00FF000000000000ULL) >> 40) | ((a & 0xFF00000000000000ULL) >> 56);
                  return a;
               }

               constexpr std::int64_t swap(std::int64_t a) noexcept
               {
                  return static_cast<std::int64_t>(swap(static_cast<std::uint64_t>(a)));
               }
            } // namespace endianess

            template <class FieldT, class BoolT>
            struct binary_io
            {
            };

            template <>
            struct binary_io<u8, std::false_type>
            {
               template <class InputIt>
               static bool read_binary(u8 &field, InputIt &cursor, InputIt end)
               {
                  bool error = false;
                  if (cursor != end)
                  {
                     field = static_cast<u8>(*cursor++);
                  }
                  else
                  {
                     error = true;
                  }

                  return error;
               }

               template <class OutputIt>
               static void write_binary(u8 const &field, OutputIt &output_it)
               {
                  *output_it++ = field;
               }

               static std::size_t get_size(u8 const &)
               {
                  return 1;
               }
            };

            template <>
            struct binary_io<s8, std::false_type>
            {
               template <class InputIt>
               static bool read_binary(s8 &field, InputIt &cursor, InputIt end)
               {
                  bool error = false;
                  if (cursor != end)
                  {
                     field = static_cast<s8>(*cursor++);
                  }
                  else
                  {
                     error = true;
                  }

                  return error;
               }

               template <class OutputIt>
               static void write_binary(s8 const &field, OutputIt &output_it)
               {
                  *output_it++ = field;
               }

               static std::size_t get_size(s8 const &)
               {
                  return 1;
               }
            };

            template <class FieldT>
            struct binary_io<FieldT, std::false_type>
            {
               template <class InputIt>
               static bool read_binary(FieldT &field, InputIt &cursor, InputIt end)
               {
                  return field.read_binary(cursor, end);
               }

               template <class OutputIt>
               static void write_binary(FieldT const &field, OutputIt &output_it)
               {
                  field.write_binary(output_it);
               }

               static std::size_t get_size(FieldT const &field)
               {
                  return field.size();
               }
            };

            template <class FieldT>
            using field_binary_io = binary_io<FieldT, typename visit_struct::traits::is_visitable<FieldT>::type>;

            template <class StorageT, class EndianessT>
            struct multibyte_integer
            {
               using value_type = StorageT;
               using endianess_type = EndianessT;
               using self_type = multibyte_integer<StorageT, EndianessT>;

               value_type value;
               multibyte_integer(value_type val = 0) : value(val) {}

               multibyte_integer(const self_type &other) : value(other.value) {}

               self_type &operator=(const self_type &other)
               {
                  value = other.value;
                  return *this;
               }

               self_type &operator=(const value_type &_value)
               {
                  value = _value;
                  return *this;
               }

               bool operator==(const self_type &other) const
               {
                  return value == other.value;
               }

               bool operator!=(const self_type &other) const
               {
                  return value != other.value;
               }

               bool operator>=(const self_type &other) const
               {
                  return value >= other.value;
               }

               bool operator<=(const self_type &other) const
               {
                  return value <= other.value;
               }

               bool operator>(const self_type &other) const
               {
                  return value > other.value;
               }

               bool operator<(const self_type &other) const
               {
                  return value < other.value;
               }

               u8 *begin()
               {
                  return reinterpret_cast<u8 *>(&value);
               }

               u8 *end()
               {
                  return reinterpret_cast<u8 *>(&value) + sizeof(value_type);
               }

               const u8 *cbegin() const
               {
                  return reinterpret_cast<const u8 *>(&value);
               }

               const u8 *cend() const
               {
                  return reinterpret_cast<const u8 *>(&value) + sizeof(value_type);
               }

               std::size_t size() const
               {
                  return sizeof(value_type);
               }

               operator value_type() const
               {
                  return value;
               }

               void to_output_byte_order()
               {
                  if constexpr (sizeof(decltype(value)) > 1 && std::is_same<endianess_type, endianess::big_endian>::value &&
                      !endianess::is_platform_big_endian())
                  {
                     value = endianess::swap(value);
                  }
               }

               void from_input_byte_order()
               {
                  if constexpr (
                     sizeof(decltype(value)) > 1 &&
                     std::is_same<endianess_type, endianess::big_endian>::value &&
                      !endianess::is_platform_big_endian())
                  {
                     value = endianess::swap(value);
                  }
               }

               template <class InputIt>
               bool read_binary(InputIt &cursor, InputIt end)
               {
                  const std::size_t field_size = types::multibyte_integer<StorageT, EndianessT>::size();

                  bool error = false;
                  std::size_t available_input = std::distance(cursor, end);
                  if (available_input >= field_size)
                  {
                     auto end_cursor = cursor + field_size;
                     std::copy(cursor, end_cursor, begin());
                     cursor = end_cursor;

                     from_input_byte_order();
                  }
                  else
                  {
                     error = true;
                  }
                  return error;
               }

               template <class OutputIt>
               void write_binary(OutputIt &output_it) const
               {
                  self_type value = *this;
                  value.to_output_byte_order();

                  output_it = std::copy(value.cbegin(), value.cend(), output_it);
               }
            };

            template <class PrefixT, class ElementT>
            struct prefixed_array
            {
               using prefix_type = PrefixT;
               using element_type = ElementT;

               std::vector<element_type> values;

               auto begin()
               {
                  return values.begin();
               }

               auto end()
               {
                  return values.end();
               }

               template <class InputIt>
               bool read_binary(InputIt &cursor, InputIt end)
               {
                  prefix_type size;
                  if (!field_binary_io<prefix_type>::read_binary(size, cursor, end))
                  {
                     std::size_t length = size;

                     values.reserve(length);

                     element_type element{};
                     for (std::size_t i = 0; cursor != end && i < length; ++i)
                     {
                        if (!field_binary_io<element_type>::read_binary(element, cursor, end))
                        {
                           values.push_back(element);
                        }
                        else
                        {
                           return true;
                        }
                     }

                     return false;
                  }
                  else
                  {
                     return true;
                  }
               }

               void convert_size(u8 &result, std::size_t value) const
               {
                  if (std::numeric_limits<u8>::max() >= value)
                  {
                     result = static_cast<u8>(value);
                  }
                  else
                  {
                     std::stringstream ss;
                     ss << "prefix with size = " << sizeof(u8) << " could not hold value = " << value;

                     throw std::overflow_error(ss.str());
                  }
               }

               void convert_size(s8 &result, std::size_t value) const
               {
                  if (std::numeric_limits<s8>::max() >= value)
                  {
                     result = static_cast<s8>(value);
                  }
                  else
                  {
                     std::stringstream ss;
                     ss << "prefix with size = " << sizeof(s8) << " could not hold value = " << value;

                     throw std::overflow_error(ss.str());
                  }
               }

               template <class StorageT, class EndT>
               void convert_size(multibyte_integer<StorageT, EndT> &result, std::size_t value) const
               {
                  if (std::numeric_limits<typename multibyte_integer<StorageT, EndT>::value_type>::max() >= value)
                  {
                     result.value = static_cast<typename multibyte_integer<StorageT, EndT>::value_type>(value);
                  }
                  else
                  {
                     std::stringstream ss;
                     ss << "prefix with size = " << sizeof(typename multibyte_integer<StorageT, EndT>::value_type)
                        << " max_value = "
                        << std::numeric_limits<typename multibyte_integer<StorageT, EndT>::value_type>::max()
                        << " could not hold value = " << value;

                     throw std::overflow_error(ss.str());
                  }
               }

               template <class OutputIt>
               void write_binary(OutputIt &output_it) const
               {
                  prefix_type value = 0;
                  this->convert_size(value, values.size());

                  field_binary_io<prefix_type>::write_binary(value, output_it);

                  for (auto const &element : values)
                  {
                     field_binary_io<element_type>::write_binary(element, output_it);
                  }
               }

               std::size_t size() const
               {
                  prefix_type pref{};
                  std::size_t size = field_binary_io<prefix_type>::get_size(pref);

                  for (auto const &element : values)
                  {
                     size += field_binary_io<element_type>::get_size(element);
                  }

                  return size;
               }
            };

            template <class ElementT, std::size_t Size>
            struct fixed_array
            {
               using element_type = ElementT;

               std::array<element_type, Size> values;

               auto begin()
               {
                  return values.begin();
               }

               auto end()
               {
                  return values.end();
               }

               template <class InputIt>
               bool read_binary(InputIt &cursor, InputIt end)
               {
                  for (std::size_t i = 0; cursor != end && i < Size; ++i)
                  {
                     if (field_binary_io<element_type>::read_binary(values[i], cursor, end))
                     {
                        return true;
                     }
                  }

                  return false;
               }

               template <class OutputIt>
               void write_binary(OutputIt &output_it) const
               {
                  for (auto const &element : values)
                  {
                     field_binary_io<element_type>::write_binary(element, output_it);
                  }
               }

               std::size_t size() const
               {
                  std::size_t size = 0;

                  for (auto const &element : values)
                  {
                     size += field_binary_io<element_type>::get_size(element);
                  }

                  return size;
               }
            };


            template <class PrefixT>
            struct non_owning_byte_array
            {
               using prefix_type = PrefixT;
               using element_type = std::byte;

               std::span<element_type> data;

               auto begin()
               {
                  return data.begin();
               }

               auto end()
               {
                  return data.end();
               }

               template <class InputIt>
               bool read_binary(InputIt &cursor, InputIt end)
               {
                  auto size = prefix_type {};
                  if (!field_binary_io<prefix_type>::read_binary(size, cursor, end))
                  {
                     if (cursor + size <= end)
                     {
                        data = std::span<std::byte>{
                            const_cast<std::byte *>(reinterpret_cast<const std::byte *>(&cursor)), size};
                        return false;
                     }
                     else
                     {
                        return true;
                     }
                  }
                  else
                  {
                     return true;
                  }
               }

               void convert_size(u8 &result, std::size_t value) const
               {
                  if (std::numeric_limits<u8>::max() >= value)
                  {
                     result = static_cast<u8>(value);
                  }
                  else
                  {
                     std::stringstream ss;
                     ss << "prefix with size = " << sizeof(u8) << " could not hold value = " << value;

                     throw std::overflow_error(ss.str());
                  }
               }

               void convert_size(s8 &result, std::size_t value) const
               {
                  if (std::numeric_limits<s8>::max() >= value)
                  {
                     result = static_cast<s8>(value);
                  }
                  else
                  {
                     std::stringstream ss;
                     ss << "prefix with size = " << sizeof(s8) << " could not hold value = " << value;

                     throw std::overflow_error(ss.str());
                  }
               }

               template <class StorageT, class EndT>
               void convert_size(multibyte_integer<StorageT, EndT> &result, std::size_t value) const
               {
                  if (std::numeric_limits<typename multibyte_integer<StorageT, EndT>::value_type>::max() >= value)
                  {
                     result.value = static_cast<typename multibyte_integer<StorageT, EndT>::value_type>(value);
                  }
                  else
                  {
                     std::stringstream ss;
                     ss << "prefix with size = " << sizeof(typename multibyte_integer<StorageT, EndT>::value_type)
                        << " max_value = "
                        << std::numeric_limits<typename multibyte_integer<StorageT, EndT>::value_type>::max()
                        << " could not hold value = " << value;

                     throw std::overflow_error(ss.str());
                  }
               }

               template <class OutputIt>
               void write_binary(OutputIt &output_it) const
               {
                  prefix_type value = 0;
                  this->convert_size(value, data.size());

                  field_binary_io<prefix_type>::write_binary(value, output_it);

                  for (auto const &element : data)
                  {
                     field_binary_io<element_type>::write_binary(element, output_it);
                  }
               }

               std::size_t size() const
               {
                  prefix_type pref{};
                  std::size_t size = field_binary_io<prefix_type>::get_size(pref);

                  return size;
               }
            };


            template <size_t Begin, const char *Name, size_t End = Begin>
            struct bit_range
            {
               constexpr static size_t begin = Begin;
               constexpr static size_t end = End;
               constexpr static const char *name = Name;
            };

            template <class... BitRanges>
            struct partial_byte
            {
               std::array<u8, sizeof...(BitRanges)> values;

               template <class InputIt>
               bool read_binary(InputIt &cursor, InputIt end)
               {
                  u8 val = 0;
                  if (field_binary_io<u8>::read_binary(val, cursor, end))
                  {
                     return true;
                  }

                  int i = 0;
                  using ValidTypes = boost::mpl::list<BitRanges...>;

                  boost::mpl::for_each<ValidTypes>(
                      [&, this](auto arg)
                      {
                         using T = decltype(arg);
                         values[i] = ((val) >> (T::begin)) & ((1 << (T::end - T::begin + 1)) - 1);
                         ++i;
                      });

                  return false;
               }

               template <class OutputIt>
               void write_binary(OutputIt &output_it) const
               {
                  u8 res = 0;
                  int i = 0;
                  using ValidTypes = boost::mpl::list<BitRanges...>;

                  boost::mpl::for_each<ValidTypes>(
                      [&, this](auto arg)
                      {
                         using T = decltype(arg);
                         res |= (values[i] << (T::begin));
                         ++i;
                      });

                  field_binary_io<u8>::write_binary(res, output_it);
               }

               std::size_t size() const
               {
                  return 1;
               }
            };

            using s16_be = multibyte_integer<std::int16_t, endianess::big_endian>;
            using s16_le = multibyte_integer<std::int16_t, endianess::little_endian>;
            using u16_be = multibyte_integer<std::uint16_t, endianess::big_endian>;
            using u16_le = multibyte_integer<std::uint16_t, endianess::little_endian>;

            using s32_be = multibyte_integer<std::int32_t, endianess::big_endian>;
            using s32_le = multibyte_integer<std::int32_t, endianess::little_endian>;
            using u32_be = multibyte_integer<std::uint32_t, endianess::big_endian>;
            using u32_le = multibyte_integer<std::uint32_t, endianess::little_endian>;

            using s64_be = multibyte_integer<std::int64_t, endianess::big_endian>;
            using s64_le = multibyte_integer<std::int64_t, endianess::little_endian>;
            using u64_be = multibyte_integer<std::uint64_t, endianess::big_endian>;
            using u64_le = multibyte_integer<std::uint64_t, endianess::little_endian>;

            template <class EnumT>
            using enum_be = multibyte_integer<EnumT, endianess::big_endian>;

            template <class EnumT>
            using enum_le = multibyte_integer<EnumT, endianess::little_endian>;

            template <class EndianessT>
            std::int64_t safe_convert_to_int64(const multibyte_integer<std::uint64_t, EndianessT> &value)
            {
               if (std::numeric_limits<std::int64_t>::max() > value)
               {
                  return static_cast<std::int64_t>(value);
               }

               std::stringstream ss;
               ss << "int64 could not hold value = " << value.value;

               throw std::overflow_error(ss.str());
            }

            template <class ValueT>
            std::int64_t safe_convert_to_int64(const ValueT &value)
            {
               return static_cast<std::int64_t>(value);
            }
         } // namespace types
      }    // namespace binary
   }       // namespace serialization
} // namespace spider