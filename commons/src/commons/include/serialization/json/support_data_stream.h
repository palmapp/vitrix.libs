//
// Created by jhrub on 21.06.2021.
//

#pragma once

#include "../../commons/data/data_stream.h"
#include "support.h"

namespace spider::serialization::json_data_stream
{
   using namespace commons::basic_types;

   inline void write_field_value(json::json_buffer &buffer, commons::data::field_value const &value)
   {
      using namespace commons::basic_types;
      using namespace std;
      value.visit(
          [&](auto const &real_value)
          {
             using real_value_type = decay_t<decltype(real_value)>;
             if constexpr (is_same_v<bool, real_value_type>)
             {
                buffer.boolean(real_value);
             }
             else if constexpr (is_integral_v<real_value_type> || is_floating_point_v<real_value_type>)
             {
                buffer.number(real_value);
             }
             else if constexpr (is_same_v<string_view, real_value_type> ||
                                is_convertible_v<real_value_type, string_view>)
             {
                buffer.string(real_value);
             }
             else
             {
                static_assert(is_same_v<bool, real_value_type> || is_integral_v<real_value_type> ||
                                  is_floating_point_v<real_value_type> || is_same_v<string_view, real_value_type> ||
                                  is_convertible_v<real_value_type, string_view>,
                              "please update write_field_value : you have extended field_value type");
             }
          });
   }

   template <class CallbackT>
   void write_row_as_object(json::json_buffer &buffer, array_view<string_view> keys, commons::data::data_row row,
                            CallbackT before_object_end)
   {
      buffer.object(
          [&]()
          {
             for (std::size_t i = 0; i < keys.size(); ++i)
             {
                auto &col = row[i];
                if (col.has_value())
                {
                   buffer.string(keys[i]);
                   write_field_value(buffer, col.value());
                }
                else
                {
                   buffer.kv_pair_null(keys[i]);
                }
             }

             before_object_end();
          });
   }

   inline void write_row_as_object(json::json_buffer &buffer, array_view<string_view> keys, commons::data::data_row row)
   {
      write_row_as_object(buffer, keys, row, []() {});
   }

   template <class CallbackT>
   void write_row_as_array(json::json_buffer &buffer, commons::data::data_row row, CallbackT before_object_end)
   {
      buffer.array(
          [&]()
          {
             for (std::size_t i = 0; i < row.size(); ++i)
             {
                auto &col = row[i];
                if (col.has_value())
                {
                   write_field_value(buffer, col.value());
                }
                else
                {
                   buffer.null();
                }
             }

             before_object_end();
          });
   }

   inline void write_row_as_array(json::json_buffer &buffer, commons::data::data_row row)
   {
      write_row_as_array(buffer, row, []() {});
   }

   inline std::size_t write_data_stream_as_objects(json::json_buffer &buffer, commons::data::data_stream &stream,
                                                   std::size_t num_rows = -1)
   {
      auto columns = stream.get_column_names();
      if (!columns.has_value())
      {
         throw std::logic_error{"data stream is not supporting get_column_names call"};
      }

      std::size_t written = 0;

      for (; written < num_rows; ++written)
      {
         auto row = stream.next_row();
         if (!row.has_value())
         {
            break;
         }
         else
         {
            write_row_as_object(buffer, columns.value(), row.value());
         }
      }

      return written;
   }

   inline std::size_t write_data_stream_as_array(json::json_buffer &buffer, commons::data::data_stream &stream,
                                                 std::size_t num_rows = -1)
   {
      std::size_t written = 0;

      for (; written < num_rows; ++written)
      {
         auto row = stream.next_row();
         if (!row.has_value())
         {
            break;
         }
         else
         {
            write_row_as_array(buffer, row.value());
         }
      }

      return written;
   }
} // namespace spider::serialization::json_data_stream

namespace spider::serialization::json::support
{
   /// data stream is only meant to be serialized to end users for JS parsing
   template <>
   struct element_writer<commons::data::data_stream>
   {
      static void write_element(json_buffer &out, commons::data::data_stream &stream)
      {
         spider::serialization::json_data_stream::write_data_stream_as_objects(out, stream);
      }
   };

   template <>
   struct element_writer<commons::basic_types::field_value>
   {
      static void write_element(json_buffer &out, commons::basic_types::field_value const &field)
      {
         using namespace commons::basic_types;

         out.object(
             [&]
             {
                out.kv_pair("type", field_value_type_to_string(field.index()));

                if (static_cast<field_value_type>(field.index()) == commons::basic_types::field_value_type::type_i64 ||
                    static_cast<field_value_type>(field.index()) == commons::basic_types::field_value_type::type_u64)
                {

                   u32 low;
                   u32 high;

                   field.visit(
                       [&](auto &val) mutable
                       {
                          if constexpr (std::is_same_v<commons::basic_types::i64, std::decay_t<decltype(val)>> ||
                                        std::is_same_v<commons::basic_types::u64, std::decay_t<decltype(val)>>)
                          {
                             low = static_cast<commons::basic_types::u32>(val & 0xffffffffUL);
                             high = static_cast<commons::basic_types::u32>(val >> 32);
                          }
                       });

                   out.string("low");
                   out.number(low);

                   out.string("high");
                   out.number(high);
                }
                else
                {
                   out.string("value");
                   spider::serialization::json_data_stream::write_field_value(out, field);
                }
             });
      }
   };

   template <>
   struct element_writer<commons::basic_types::optional_field_value>
   {
      static void write_element(json_buffer &out, commons::basic_types::optional_field_value const &field)
      {
         if (field.has_value())
         {
            element_writer<commons::basic_types::field_value>::write_element(out, field.value());
         }
         else
         {
            out.null();
         }
      }
   };

   template <>
   struct element_parser<commons::basic_types::field_value>
   {
      static void clear_element(commons::basic_types::field_value &el)
      {
         el = {};
      }

      static bool parse_element(commons::basic_types::field_value &el, picojson::value &value)
      {
         using namespace commons::basic_types;
         if (value.is<double>())
         {
            el = field_value{value.get<double>()};
            return true;
         }
         if (value.is<bool>())
         {
            el = field_value{value.get<bool>()};
            return true;
         }
         else if (value.is<std::string>())
         {
            el = field_value{value.to_str()};
         }
         else if (auto *obj = value.evaluate_as_object(); obj != nullptr)
         {
            if (auto type_it = obj->find("type"); type_it != obj->end())
            {
               auto type = type_it->second.to_str();
               auto value_it = obj->find("value");

               switch (get_field_value_type(type))
               {
               case field_value_type::type_bool:
                  if (value_it != obj->end())
                  {
                     bool val = false;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_i8:
                  if (value_it != obj->end())
                  {
                     i8 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_u8:
                  if (value_it != obj->end())
                  {
                     u8 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_i16:
                  if (value_it != obj->end())
                  {
                     i16 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_u16:
                  if (value_it != obj->end())
                  {
                     u16 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_i32:
                  if (value_it != obj->end())
                  {
                     i32 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_u32:
                  if (value_it != obj->end())
                  {
                     u32 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_i64:
                  if (auto low_it = obj->find("low"), high_it = obj->find("high");
                      low_it != obj->end() && high_it != obj->end())
                  {
                     u32 low = 0, high = 0;
                     auto result0 = support::parse_element(low, low_it->second);
                     auto result1 = support::parse_element(high, high_it->second);

                     el = field_value{static_cast<i64>((static_cast<u64>(high) << 32) | low)};
                     return result0 && result1;
                  }
                  break;
               case field_value_type::type_u64:
                  if (auto low_it = obj->find("low"), high_it = obj->find("high");
                      low_it != obj->end() && high_it != obj->end())
                  {
                     u32 low = 0, high = 0;
                     auto result0 = support::parse_element(low, low_it->second);
                     auto result1 = support::parse_element(high, high_it->second);

                     el = field_value{(static_cast<u64>(high) << 32) | low};
                     return result0 && result1;
                  }
                  break;
               case field_value_type::type_fl32:
                  if (value_it != obj->end())
                  {
                     fl32 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_fl64:
                  if (value_it != obj->end())
                  {
                     fl64 val = 0;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               case field_value_type::type_string:
               case field_value_type::type_string_view:
                  if (value_it != obj->end())
                  {
                     string val;
                     auto result = support::parse_element(val, value_it->second);
                     el = field_value{val};
                     return result;
                  }
                  break;
               default:
                  break;
               }
            }
         }

         element_parser<field_value>::clear_element(el);
         return false;
      }
   };

   template <>
   struct element_parser<commons::basic_types::optional_field_value>
   {
      static void clear_element(commons::basic_types::optional_field_value &el)
      {
         el.reset();
      }

      static bool parse_element(commons::basic_types::optional_field_value &el, picojson::value &value)
      {
         if (value.is<picojson::null>())
         {
            el.reset();
            return true;
         }
         else
         {
            commons::basic_types::field_value fl;
            auto result = element_parser<commons::basic_types::field_value>::parse_element(fl, value);
            if (result)
            {
               el = std::move(fl);
            }
            else
            {
               el.reset();
            }
            return result;
         }
      }
   };
} // namespace spider::serialization::json::support