//
// Created by jhrub on 02.07.2021.
//

#pragma once
#include "buffer.h"
namespace spider::serialization::json
{
   template <class Data>
   void write_json(json_buffer &writer, Data const &d);
}

namespace spider::serialization::json::support
{

   template <class Element>
   struct element_writer
   {
      using implemented = std::false_type;

      template <class O>
      static void write_element(json_buffer &out, O const &el)
      {
         // throw std::logic_error("there is no json serialization implemented for O type");
         static_assert(!std::is_same_v<O, O>, "there is no json serialization implemented for O type");
      }
   };

   template <class Element>
   struct element_parser
   {
      using implemented = std::false_type;

      template <class O>
      static bool parse_element(Element &el, picojson::value &value)
      {
         // throw std::logic_error("there is no json parser implemented for O type");
         static_assert(!std::is_same_v<O, O>, "there is no json parser implemented for O type");
         return false;
      }
   };

   template <class Element>
   bool parse_element(Element &el, picojson::value &value);

} // namespace spider::serialization::json::support
