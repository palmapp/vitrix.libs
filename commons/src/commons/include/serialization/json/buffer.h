#pragma once
#include "common.h"
#include <cstdint>
#include <fmt/core.h>
#include <fmt/format.h>
#include <variant>

namespace spider
{
   namespace serialization
   {
      namespace json
      {
         namespace writer
         {
            struct array_state
            {
               bool has_first_record = false;

               template <class T>
               void operator()(T &buffer)
               {
                  if (has_first_record)
                  {
                     buffer.value_separator();
                  }
                  else
                  {
                     has_first_record = true;
                  }
               }
            };

            struct object_state
            {
               std::int64_t position = 0;

               template <class T>
               void operator()(T &buffer)
               {
                  if (position > 0)
                  {
                     if (++position % 2 != 0)
                     {
                        buffer.value_separator();
                     }
                     else
                     {
                        buffer.key_separator();
                     }
                  }
                  else
                  {
                     ++position;
                  }
               }
            };

            struct root_state
            {
               template <class T>

               inline void operator()(T &buffer)
               {
               }
            };

            using state = std::variant<root_state, array_state, object_state>;

         } // namespace writer

         class json_buffer
         {
            fmt::memory_buffer &buffer_;
            std::vector<char> string_buffer_;

            std::vector<writer::state> writer_stack_;

          public:
            bool compress_booleans = true;

            inline json_buffer(fmt::memory_buffer &buffer) : buffer_(buffer)
            {
               buffer_.reserve(8192);
               push_state(writer::root_state{});
            }
            inline std::string to_string() const
            {
               return {buffer_.data(), buffer_.size()};
            }

            inline void begin_array()
            {
               apply_state();
              fmt::format_to(std::back_inserter(buffer_), "[");
               push_state(writer::array_state{});
            }

            inline void end_array()
            {
              fmt::format_to(std::back_inserter(buffer_), "]");
               pop_state();
            }

            inline void begin_object()
            {
               apply_state();
              fmt::format_to(std::back_inserter(buffer_), "{}", "{");
               push_state(writer::object_state{});
            }

            inline void end_object()
            {
              fmt::format_to(std::back_inserter(buffer_), "{}", "}");
               pop_state();
            }

            inline void key_separator()
            {
              fmt::format_to(std::back_inserter(buffer_), ":");
            }
            inline void value_separator()
            {
              fmt::format_to(std::back_inserter(buffer_), ",");
            }

            inline void string(std::string_view string)
            {
               apply_state();

               string_buffer_.reserve(string.size() * 2);
               picojson::serialize_str(string, std::back_inserter(string_buffer_));
              fmt::format_to(std::back_inserter(buffer_), "{}", std::string_view{string_buffer_.data(), string_buffer_.size()});

               string_buffer_.clear();
            }

            inline void kv_pair_null(std::string_view key)
            {
               string(key);
               null();
            }

            inline void kv_pair(std::string_view key, std::string_view value)
            {
               string(key);
               string(value);
            }

            inline void kv_pair_bool(std::string_view key, bool value)
            {
               string(key);
               boolean(value);
            }

            template <class T>
            void kv_pair_num(std::string_view key, T value)
            {
               string(key);
               number(value);
            }

            template <class T>
            void kv_pair_obj(std::string_view key, T lambda)
            {
               string(key);
               object(lambda);
            }

            template <class T>
            void kv_pair_arr(std::string_view key, T lambda)
            {
               string(key);
               array(lambda);
            }

            template <class T>
            void object(T lambda)
            {
               begin_object();
               lambda();
               end_object();
            }

            template <class T>
            void array(T lambda, bool is_first = true)
            {
               if (!is_first) value_separator();

               begin_array();
               lambda();
               end_array();
            }

            inline void boolean(bool value)
            {
               apply_state();
               if (compress_booleans)
               {
                  if (value)
                    fmt::format_to(std::back_inserter(buffer_), "1");
                  else
                    fmt::format_to(std::back_inserter(buffer_), "0");
               }
               else
               {
                  if (value)
                    fmt::format_to(std::back_inserter(buffer_), "true");
                  else
                    fmt::format_to(std::back_inserter(buffer_), "false");
               }
            }

            template <class T>
            inline void number(T value)
            {
               apply_state();

              fmt::format_to(std::back_inserter(buffer_), "{}", value);
            }

            inline void null()
            {
               apply_state();
              fmt::format_to(std::back_inserter(buffer_), "null");
            }

            inline void unsafe_write_data(std::string_view data)
            {
               apply_state();
               buffer_.append(data);
            }

          private:
            inline void apply_state()
            {
               auto &current = writer_stack_.back();
               std::visit([&, this](auto &state) mutable { state(*this); }, current);
            }

            template <class State>
            void push_state(State &&state)
            {
               writer_stack_.emplace_back(std::forward<State>(state));
            }

            inline void pop_state()
            {
               writer_stack_.pop_back();
            }
         };
      } // namespace json
   }    // namespace serialization
} // namespace spider