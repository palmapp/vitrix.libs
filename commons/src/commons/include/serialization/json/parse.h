#pragma once
#include "common.h"
#include "support.h"
#include "type_traits.h"
#include <gsl/narrow>
#include <optional>

namespace spider
{
   namespace serialization
   {
      namespace json
      {
         template <class Data>
         class parse_json_member_visitor;

         namespace support
         {

            template <class Element>
            bool parse_element(Element &el, picojson::value &value);

            template <class Element>
            struct element_parser<std::optional<Element>>
            {
               static bool parse_element(std::optional<Element> &optional_el, picojson::value &value)
               {
                  if (value.is<picojson::null>())
                  {
                     optional_el.reset();
                  }
                  else
                  {
                     Element el;
                     if (!support::parse_element(el, value))
                     {
                        optional_el.reset();
                        return false;
                     }
                     else
                     {
                        optional_el = std::optional<Element>{std::move(el)};
                     }
                  }
                  return true;
               }
            };

            template <typename T>
            class has_clear_member
            {
               typedef char yes_type;
               typedef long no_type;
               template <typename U>
               static yes_type test(decltype(&U::clear));
               template <typename U>
               static no_type test(...);

             public:
               static constexpr bool value = sizeof(test<T>(0)) == sizeof(yes_type);
            };
            template <typename T>
            class has_begin_member
            {
               typedef char yes_type;
               typedef long no_type;
               template <typename U>
               static yes_type test(decltype(&U::begin));
               template <typename U>
               static no_type test(...);

             public:
               static constexpr bool value = sizeof(test<T>(0)) == sizeof(yes_type);
            };

            template <typename T>
            struct is_our_binary_number
            {
               static constexpr bool value = false;
            };

            template <typename S, typename E>
            struct is_our_binary_number<binary::types::multibyte_integer<S, E>>
            {
               static constexpr bool value = true;
            };

            template <class Element>
            bool parse_struct(Element &el, picojson::value &value)
            {
               parse_json_member_visitor<Element> visitor{value};
               visit_struct::for_each(el, visitor);

               return true;
            }

            template <class MapType>
            bool parse_map(MapType &cnt, picojson::value &value)
            {
               cnt.clear();

               if (auto *obj = value.evaluate_as_object(); obj != nullptr)
               {
                  for (auto &[key, map_val] : *obj)
                  {
                     parse_element(cnt[key], map_val);
                  }

                  return true;
               }

               return false;
            }

            template <class Element>
            bool parse_container(std::vector<Element> &cnt, picojson::value &value)
            {
               cnt.clear();

               if (auto *array = value.evaluate_as_array(); array != nullptr)
               {
                  cnt.reserve(array->size());

                  for (auto &val : *array)
                  {
                     Element el;
                     parse_element(el, val);
                     cnt.push_back(std::move(el));
                  }

                  return true;
               }
               return false;
            }

            template <class Element>
            bool parse_container(std::map<std::string, Element> &cnt, picojson::value &value)
            {
               return parse_map(cnt, value);
            }

            template <class Element>
            bool parse_container(boost::unordered_map<std::string, Element> &cnt, picojson::value &value)
            {
               return parse_map(cnt, value);
            }

            template <class Element>
            bool parse_container(std::unordered_map<std::string, Element> &cnt, picojson::value &value)
            {
               return parse_map(cnt, value);
            }

            template <class Prefix, class Element>
            bool parse_container(binary::types::prefixed_array<Prefix, Element> &data, picojson::value &value)
            {
               return parse_container(data.values, value);
            }

            template <class ElementT, std::size_t Size>
            bool parse_container(binary::types::fixed_array<ElementT, Size> &data, picojson::value &value)
            {
               std::vector<ElementT> temp;
               bool result = parse_container(temp, value);

               auto it = temp.end();
               if (temp.size() > data.values.size())
               {
                  it = temp.begin() + data.values.size();
               }

               std::copy(temp.begin(), it, data.values.begin());

               return result;
            }

            template <class Element>
            bool parse_element(Element &el, picojson::value &value)
            {
               if constexpr (std::is_integral_v<Element> || std::is_floating_point_v<Element> ||
                             std::is_enum_v<Element>)
               {
                  if (value.template is<double>())
                  {
                     el = static_cast<Element>(value.template get<double>());
                     return true;
                  }
                  else if (value.template is<bool>())
                  {
                     el = gsl::narrow<Element>(value.template get<bool>());
                     return true;
                  }
                  else
                  {
                     el = gsl::narrow<Element>(0);
                  }

                  return false;
               }
               else if constexpr (is_our_binary_number<Element>::value)
               {
                  typename Element::value_type result;
                  if (parse_element(result, value))
                  {
                     el = Element{result};
                     return true;
                  }
                  else
                  {
                     el = Element{0};
                     return false;
                  }
               }

               else if constexpr (std::is_same_v<std::string, Element>)
               {
                  if (value.is<picojson::null>() || value.is<picojson::array>() || value.is<picojson::object>())
                  {
                     el.clear();
                     return false;
                  }
                  else
                  {
                     el = value.to_str();
                     return true;
                  }
               }
               else if constexpr (visit_struct::traits::is_visitable<Element>::value)
               {
                  return parse_struct(el, value);
               }
               else if constexpr (type_traits::is_iterable<Element>::value || has_begin_member<Element>::value)
               {
                  return parse_container(el, value);
               }
               else
               {
                  return element_parser<Element>::parse_element(el, value);
               }
            }

            template <class Element>
            void clear(Element &element)
            {
               if constexpr (std::is_integral_v<Element> || std::is_floating_point_v<Element>)
               {
                  element = gsl::narrow<Element>(0);
               }
               else if constexpr (is_our_binary_number<Element>::value)
               {
                  element = Element{0};
               }
               else if constexpr (has_clear_member<Element>::value)
               {
                  element.clear();
               }
               else
               {
                  element = {};
               }
            }

            template <class... BitRanges>
            bool parse_element(binary::types::partial_byte<BitRanges...> &el, picojson::value &value)
            {
               double json_val = 0.0;
               if (parse_element(json_val, value))
               {
                  auto val = static_cast<binary::types::u8>(json_val);

                  int i = 0;
                  using ValidTypes = boost::mpl::list<BitRanges...>;

                  boost::mpl::for_each<ValidTypes>(
                      [&](auto arg)
                      {
                         using T = decltype(arg);
                         el.values[i] = ((val) >> (T::begin)) & ((1 << (T::end - T::begin + 1)) - 1);
                         ++i;
                      });

                  return true;
               }
               else
               {
                  return false;
               }
            }

         } // namespace support

         template <class Data>
         class parse_json_member_visitor
         {
            picojson::value &object_;

          public:
            parse_json_member_visitor(picojson::value &object) : object_(object) {}

            template <class Element>
            void operator()(const std::string &field, Element &data)
            {
               if (!try_get_json_value(field, data))
               {
                  clear(data);
               }
            }

            template <class Element>
            void clear(Element &element)
            {
               support::clear(element);
            }

            template <class Val>
            bool try_get_json_value(const std::string &field, Val &data)
            {
               if (object_.contains(field))
               {
                  auto &value = object_.get(field);
                  return support::parse_element(data, value);
               }

               return false;
            }
         };

         template <class Data>
         bool parse_json(Data &d, std::string_view json)
         {
            picojson::value parsed;
            auto it = json.begin();
            auto end = json.end();
            if (picojson::parse(parsed, it, end).empty())
            {
               support::parse_element(d, parsed);

               return true;
            }
            return false;
         }

         template <class Data>
         Data parse_json_throw_on_error(std::string_view json)
         {
            auto d = Data{};
            picojson::value parsed;
            auto it = json.begin();
            auto end = json.end();
            auto error = picojson::parse(parsed, it, end);
            if (error.empty())
            {
               support::parse_element(d, parsed);
               return d;
            }

            throw std::logic_error{error.c_str()};
         }
      } // namespace json
   } // namespace serialization
} // namespace spider