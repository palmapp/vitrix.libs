#pragma once
#include <boost/unordered_map.hpp>
#include <map>
#include <type_traits>
#include <unordered_map>
namespace type_traits
{

   namespace is_iterable_impl
   {
      template <class T>
      using check_specs = std::void_t<std::enable_if_t<std::is_same_v<decltype(begin(std::declval<T>())), // has begin()
                                                                      decltype(end(std::declval<T>()))    // has end()
                                                                      >>, // ... begin() and end() are the same type ...
                                      decltype(*begin(std::declval<T>())) // ... which can be dereferenced
                                      >;

      template <class T, class = void>
      struct is_iterable : std::false_type
      {
      };

      template <class T>
      struct is_iterable<T, check_specs<T>> : std::true_type
      {
      };
   } // namespace is_iterable_impl

   template <class T>
   using is_iterable = is_iterable_impl::is_iterable<T>;

   template <class T>
   constexpr bool is_iterable_v = is_iterable<T>::value;

   template <typename T>
   struct is_associative_map : std::false_type
   {
   };

   template <typename... Args>
   struct is_associative_map<std::map<Args...>> : std::true_type
   {
   };

   template <typename... Args>
   struct is_associative_map<std::unordered_map<Args...>> : std::true_type
   {
   };

   template <typename... Args>
   struct is_associative_map<boost::unordered_map<Args...>> : std::true_type
   {
   };

   template <class T>
   constexpr bool is_associative_map_v = is_associative_map<T>::value;

} // namespace type_traits