#pragma once
#include "../binary_types.h"
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/list.hpp>
#include <boost/unordered_map.hpp>
#include <formats/picojson.h>
#include <visit_struct/visit_struct.hpp>
