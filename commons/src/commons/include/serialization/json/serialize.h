#pragma once
#include "buffer.h"
#include "common.h"
#include "type_traits.h"

#include <iterator>
#include <string_view>

#include "support.h"
#include <vector>

namespace spider
{
   namespace serialization
   {
      namespace json
      {

         namespace support
         {
            template <class T>
            struct element_writer<std::optional<T>>
            {
               static void write_element(json_buffer &out, std::optional<T> const &optional_value)
               {
                  if (optional_value.has_value())
                  {
                     const T &val = optional_value.value();
                     write_json(out, val);
                  }
                  else
                  {
                     out.null();
                  }
               }
            };

            template <class StorageT, class EndianessT>
            struct element_writer<binary::types::multibyte_integer<StorageT, EndianessT>>
            {
               static void write_element(json_buffer &out,
                                         binary::types::multibyte_integer<StorageT, EndianessT> const &data)
               {
                  out.number(data.value);
               }
            };

            template <class PrefixT, class ElementT>
            struct element_writer<binary::types::prefixed_array<PrefixT, ElementT>>
            {
               static void write_element(json_buffer &out, binary::types::prefixed_array<PrefixT, ElementT> const &data)
               {
                  out.array(
                      [&]()
                      {
                         for (auto &val : data.values)
                         {
                            write_json(out, val);
                         }
                      });
               }
            };

            template <class ElementT, std::size_t Size>
            struct element_writer<binary::types::fixed_array<ElementT, Size>>
            {
               static void write_element(json_buffer &out, binary::types::fixed_array<ElementT, Size> const &data)
               {
                  out.array(
                      [&]()
                      {
                         for (auto &val : data.values)
                         {
                            write_json(out, val);
                         }
                      });
               }
            };

            template <class... BitRanges>
            struct element_writer<binary::types::partial_byte<BitRanges...>>
            {
               static void write_element(json_buffer &out, binary::types::partial_byte<BitRanges...> const &data)
               {
                  binary::types::u8 res = 0;
                  int i = 0;
                  using ValidTypes = boost::mpl::list<BitRanges...>;

                  boost::mpl::for_each<ValidTypes>(
                      [&](auto arg) mutable
                      {
                         using T = decltype(arg);
                         res |= (data.values[i] << (T::begin));
                         ++i;
                      });

                  out.number(res);
               }
            };
         } // namespace support

         template <class T>
         constexpr bool is_visitable_v = visit_struct::traits::is_visitable<T>::value;

         class struct_to_json_member_visitor
         {
            json_buffer &out_;

          public:
            inline struct_to_json_member_visitor(json_buffer &out) : out_(out) {}

            template <class T>
            void write_enum(T v)
            {
               const auto temp = static_cast<std::int64_t>(v);
               out_.number(temp);
            }

            template <class T>
            void write_associative_container(T const &value)
            {
               out_.begin_object();
               for (auto const &kv : value)
               {
                  out_.string(kv.first);
                  write_value(kv.second);
               }
               out_.end_object();
            }

            template <class T>
            void write_iterable(T const &value)
            {
               out_.begin_array();
               for (auto &&v : value)
               {
                  write_value(v);
               }

               out_.end_array();
            }

            template <class T>
            void write_object(T const &value)
            {
               out_.begin_object();
               visit_struct::for_each(value, *this);
               out_.end_object();
            }

            inline void write_value(std::string_view value)
            {
               out_.string(value);
            }
            inline void write_value(std::string const &value)
            {
               out_.string(value);
            }

            template <class T>
            void write_value(T const &v)
            {
               using namespace std;
               using D = decay_t<T>;
               if constexpr (is_integral_v<D> || is_floating_point_v<D>)
               {
                  out_.number(v);
               }
               else if constexpr (is_same_v<bool, D>)
               {
                  out_.boolean(v);
               }
               else if constexpr (is_enum_v<D>)
               {
                  write_enum(v);
               }
               else if constexpr (is_visitable_v<D>)
               {
                  write_object(v);
               }
               else if constexpr (type_traits::is_associative_map_v<D>)
               {
                  write_associative_container(v);
               }
               else if constexpr (type_traits::is_iterable_v<D>)
               {
                  write_iterable(v);
               }
               else
               {
                  support::element_writer<T>::write_element(out_, v);
               }
            }

            template <class T>
            void operator()(std::string_view name, T const &value)
            {
               write_value(name);
               write_value(value);
            }
         };

         template <class Data>
         std::string stringify_json(Data const &d)
         {
            fmt::memory_buffer buffer;
            json_buffer writer{buffer};

            write_json(writer, d);

            return writer.to_string();
         }

         template <class Data>
         void write_json(json_buffer &writer, Data const &d)
         {
            struct_to_json_member_visitor visitor{writer};
            visitor.write_value(d);
         }

      } // namespace json
   } // namespace serialization
} // namespace spider