#pragma once

#include "property.h"
#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/variant/static_visitor.hpp>
#include <chrono>
#include <cmath>
#include <formats/picojson.h>
#include <sstream>
#include <visit_struct/visit_struct.hpp>

namespace spider::serialization::property
{
   struct visitable_struct_property_serializer
   {
      inline void operator()(const char* name, const std::vector<double>& val)
      {
         spider::property::property_vector vect;
         vect.reserve(val.size());

         for (const auto& elem : val)
         {
            vect.push_back(spider::property::property_value{elem});
         }

         add_property(name, {vect});
      }

      inline void operator()(const char* name, const std::vector<std::int64_t>& val)
      {
         spider::property::property_vector vect;
         vect.reserve(val.size());

         for (const auto& elem : val)
         {
            vect.push_back(spider::property::property_value{elem});
         }

         add_property(name, {vect});
      }

      template <class T>
      void operator()(const char* name, const std::vector<T>& val)
      {
         spider::property::property_vector vect;
         vect.reserve(val.size());

         for (const auto& elem : val)
         {
            visitable_struct_property_serializer ser;
            visit_struct::for_each(elem, ser);

            vect.push_back(std::move(ser.result));
         }

         add_property(name, {vect});
      }

      template <class T>
      void add_enum_property(const char* name, const T& s)
      {
         add_property(name, {static_cast<std::int64_t>(s)});
      }

      template <class T>
      void add_complex_property(const char* name, const T& s)
      {
         visitable_struct_property_serializer ser;
         visit_struct::for_each(s, ser);

         add_property(name, {std::move(ser.result)});
      }

      template <class T>
      void operator()(const char* name, const T& s)
      {
         if constexpr (std::is_enum_v<T>)
         {
            add_enum_property(name, s);
         }
         else
         {
            add_complex_property(name, s);
         }
      }

      inline void operator()(const char* name, const std::int32_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::int8_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::int16_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::int64_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::uint8_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::uint16_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::uint32_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const std::uint64_t& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const bool& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const double& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const binary::types::u16_le& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const binary::types::u32_le& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const binary::types::u64_le& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const binary::types::s16_le& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const binary::types::s32_le& val)
      {
         add_property(name, {val});
      }

      inline void operator()(const char* name, const binary::types::s64_le& val)
      {
         add_property(name, {val});
      }

      template <class ElementT, std::size_t Size>
      void operator()(const char* name, const binary::types::fixed_array<ElementT, Size>& val)
      {
         spider::property::property_vector vect;
         vect.reserve(Size);

         for (const auto& elem : val.values)
         {
            vect.push_back(spider::property::property_value{elem});
         }

         add_property(name, {vect});
      }

      template <class... BitRanges>
      void operator()(const char* name, const binary::types::partial_byte<BitRanges...>& data)
      {
         binary::types::u8 res = 0;
         int i = 0;
         using ValidTypes = boost::mpl::list<BitRanges...>;

         boost::mpl::for_each<ValidTypes>(
            [&, this](auto arg)
            {
               using T = decltype(arg);
               add_property(T::name, data.values[i]);
               ++i;
            });
      }

      inline void operator()(const char* name, const std::chrono::system_clock::time_point& val)
      {
         using namespace boost::posix_time;
         add_property(name, {to_iso_extended_string(from_time_t(std::chrono::system_clock::to_time_t(val)))});
      }

      inline void operator()(const char* name, const std::string& val)
      {
         add_property(name, {val});
      }

      void add_property(const char* name, spider::property::property_value value)
      {
         result.emplace_back(name, std::move(value));
      }

      spider::property::property_list result;
   };

   struct prop_to_json_visitor : public boost::static_visitor<void>
   {
   public:
      void operator()(const int& data)
      {
         root_ = picojson::value(static_cast<double>(data));
      }

      void operator()(const std::string& data)
      {
         root_ = picojson::value(data);
      }

      void operator()(const std::int8_t& val)
      {
         root_ = picojson::value(static_cast<std::int64_t>(val));
      }

      void operator()(const std::int16_t& val)
      {
         root_ = picojson::value(static_cast<std::int64_t>(val));
      }

      void operator()(const std::int64_t& val)
      {
         root_ = picojson::value(val);
      }

      void operator()(const std::uint8_t& val)
      {
         root_ = picojson::value(static_cast<std::int64_t>(val));
      }

      void operator()(const std::uint16_t& val)
      {
         root_ = picojson::value(static_cast<std::int64_t>(val));
      }

      void operator()(const std::uint32_t& val)
      {
         root_ = picojson::value(static_cast<std::int64_t>(val));
      }

      void operator()(const std::uint64_t& val)
      {
         root_ = picojson::value(static_cast<std::int64_t>(val));
      }

      void operator()(const bool& val)
      {
         root_ = picojson::value(val ? 1.0 : 0.0);
      }

      void operator()(double v)
      {
         if (std::isinf(v) || std::isnan(v))
         {
            root_ = picojson::value();
         }
         else
         {
            root_ = picojson::value(v);
         }
      }

      void operator()(const spider::property::empty_value&)
      {
         root_ = picojson::value();
      }

      void operator()(const spider::property::property_vector& vect)
      {
         picojson::array value_list;

         for (auto& val : vect)
         {
            prop_to_json_visitor visitor;
            boost::apply_visitor(visitor, val);

            value_list.push_back(std::move(visitor.root_));
         }

         root_ = picojson::value(value_list);
      }

      void operator()(const spider::property::property_list& vect)
      {
         picojson::object obj;
         for (auto& val : vect)
         {
            prop_to_json_visitor visitor;
            boost::apply_visitor(visitor, val.second);
            obj.insert_or_assign(val.first, std::move(visitor.root_));
         }

         root_ = picojson::value(obj);
      }

      picojson::value root_;
   };

   template <class T>
   spider::property::property_list to_property_list(T& t)
   {
      visitable_struct_property_serializer ser;
      visit_struct::for_each(t, ser);

      return std::move(ser.result);
   }

   inline picojson::object to_json(const spider::property::property_list& properties)
   {
      picojson::object obj;
      for (auto& val : properties)
      {
         prop_to_json_visitor visitor;
         boost::apply_visitor(visitor, val.second);
         obj.insert_or_assign(val.first, std::move(visitor.root_));
      }
      return obj;
   }

   inline picojson::object scope_to_json(const spider::property::scope_id& scope_id,
                                         const spider::property::property_list& properties)
   {
      picojson::object scope;
      scope.insert_or_assign(std::to_string(scope_id), picojson::value(to_json(properties)));
      return scope;
   }

   inline std::string to_json_string(const spider::property::property_context& ctx,
                                     const spider::property::property_list& properties)
   {
      picojson::object scope;
      scope.insert_or_assign(ctx.scope, picojson::value(scope_to_json(ctx.scope_id, properties)));

      picojson::object root;
      root.insert_or_assign(std::to_string(ctx.id), picojson::value(scope));

      return picojson::value(root).serialize();
   }

   inline std::string to_json_string(const spider::property::context_id ctx_id,
                                     const spider::property::properties& props)
   {
      using namespace picojson;
      object root;
      object ctx;

      for (auto& scope : props.scopes)
      {
         picojson::object scope_json;
         for (auto& scope_item : scope.second.items)
         {
            scope_json.insert_or_assign(std::to_string(scope_item.first), picojson::value(to_json(scope_item.second)));
         }
         ctx.insert_or_assign(scope.first, value(scope_json));
      }

      root.insert_or_assign(std::to_string(ctx_id), value(ctx));

      return value(root).serialize();
   }
} // namespace spider::serialization::property
