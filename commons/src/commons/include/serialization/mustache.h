#pragma once
#include "std_types.h"
#include <mstch/mstch.hpp>
#include <visit_struct/visit_struct.hpp>

namespace spider
{

   namespace serialization
   {

      namespace mustache
      {
         template <class Data>
         class struct_mustache_member_visitor
         {
            mstch::map &map_;

          public:
            struct_mustache_member_visitor(mstch::map &map) : map_(map) {}

            void operator()(const std::string &field, int data)
            {
               map_[field] = data;
            }

            void operator()(const std::string &field, std::string data)
            {
               map_[field] = data;
            }

            void operator()(const std::string &field, double data)
            {
               map_[field] = data;
            }

            void operator()(const std::string &field, bool data)
            {
               map_[field] = data;
            }

            void operator()(const std::string &field, std::vector<std::string> &data)
            {
               to_mstch_array(field, data);
            }

            void operator()(const std::string &field, std::vector<int> &data)
            {
               to_mstch_array(field, data);
            }

            void operator()(const std::string &field, std::vector<double> &data)
            {
               to_mstch_array(field, data);
            }

            void operator()(const std::string &field, std::vector<bool> &data)
            {
               to_mstch_array(field, data);
            }

            template <class ComplexStruct>
            void operator()(const std::string &field, std::vector<ComplexStruct> &data)
            {
               mstch::array arr;
               arr.reserve(data.size());

               for (auto &value : data)
               {
                  mstch::map map;
                  struct_mustache_member_visitor<ComplexStruct> visitor{map};
                  visit_struct::for_each(value, visitor);

                  arr.push_back(std::move(map));
               }

               map_[field] = std::move(arr);
            }

            template <class ComplexStruct>
            void operator()(const std::string &field, ComplexStruct &data)
            {
               mstch::map map;
               struct_mustache_member_visitor<ComplexStruct> visitor{map};
               visit_struct::for_each(data, visitor);

               map_[field] = std::move(map);
            }

            template <class Val>
            void to_mstch_array(const std::string &field, std::vector<Val> &data)
            {
               mstch::array arr;
               arr.reserve(data.size());

               for (auto &value : data)
               {
                  arr.push_back(value);
               }

               map_[field] = std::move(arr);
            }
         };

         template <typename Data>
         std::string
         render_template(Data &data, const std::string &tmpl,
                         const std::map<std::string, std::string> &partials = std::map<std::string, std::string>())
         {
            mstch::map map;
            struct_mustache_member_visitor<Data> visitor{map};
            visit_struct::for_each(data, visitor);

            return mstch::render(tmpl, map, partials);
         }

      } // namespace mustache
   }    // namespace serialization
} // namespace spider