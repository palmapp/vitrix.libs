#pragma once
#include <stdexcept>
#include <string>

namespace spider
{
   class exception : public std::domain_error
   {
    public:
      using domain_error::domain_error;
   };
} // namespace spider