#pragma once

#include "binary_types.h"
#include <boost/variant.hpp>
#include <cstdint>
#include <map>
#include <ostream>
#include <string>
#include <utility>
#include <vector>

namespace spider::property
{
   struct empty_value
   {
      inline bool operator==(const spider::property::empty_value &) const
      {
         return true;
      }

      inline bool operator!=(const spider::property::empty_value &) const
      {
         return false;
      }
   };

   inline std::ostream &operator<<(std::ostream &out, const empty_value &)
   {
      out << "{empty value}";
      return out;
   }

   struct property_value;
   using property_vector = std::vector<property_value>;
   using property_list = std::vector<std::pair<std::string, property_value>>;
   namespace binary = spider::serialization::binary::types;

   struct property_value
       : public boost::variant<empty_value, double, std::int32_t, std::int8_t, std::int16_t, std::int64_t, std::uint8_t,
                               std::uint16_t, std::uint32_t, std::uint64_t, bool, std::string,
                               boost::recursive_wrapper<property_vector>, boost::recursive_wrapper<property_list>,
                               binary::u16_le, binary::u32_le, binary::u64_le, binary::s16_le, binary::s32_le,
                               binary::s64_le>
   {
      using self_t =
          variant<empty_value, double, std::int32_t, std::int8_t, std::int16_t, std::int64_t, std::uint8_t,
                  std::uint16_t, std::uint32_t, std::uint64_t, bool, std::string,
                  boost::recursive_wrapper<property_vector>, boost::recursive_wrapper<property_list>, binary::u16_le,
                  binary::u32_le, binary::u64_le, binary::s16_le, binary::s32_le, binary::s64_le>;

      using variant<empty_value, double, std::int32_t, std::int8_t, std::int16_t, std::int64_t, std::uint8_t,
                    std::uint16_t, std::uint32_t, std::uint64_t, bool, std::string,
                    boost::recursive_wrapper<property_vector>, boost::recursive_wrapper<property_list>, binary::u16_le,
                    binary::u32_le, binary::u64_le, binary::s16_le, binary::s32_le, binary::s64_le>::variant;

      bool operator==(const property_value &val) const
      {
         const self_t &self = *this;
         return self.operator==(static_cast<const self_t &>(val));
      }

      bool operator!=(const property_value &val) const
      {
         const self_t &self = *this;
         return self.operator!=(static_cast<const self_t &>(val));
      }
   };

   template <class ReqT>
   class cast_visitor : public boost::static_visitor<const ReqT *>
   {
    public:
      cast_visitor() = default;
      const ReqT *operator()(const ReqT &dict) const
      {
         return &dict;
      }

      template <class T>
      const ReqT *operator()(const T &) const
      {
         return nullptr;
      }
   };

   template <class ReqT, class VariantT>
   const ReqT *variant_cast(const VariantT &variant)
   {
      cast_visitor<ReqT> visitor{};
      return boost::apply_visitor(visitor, variant);
   }

   template <class ReqT, class VariantT>
   const ReqT &safe_variant_cast(const VariantT &variant)
   {
      auto ptr = variant_cast<ReqT>(variant);
      if (ptr == nullptr)
      {
         throw new std::runtime_error("invalid variant cast exception");
      }
      else
      {
         return *ptr;
      }
   }

   using context_id = std::uint64_t;
   using scope_type = std::string;
   using scope_id = std::uint64_t;

   struct property_context
   {
      context_id id;
      scope_type scope;
      std::uint64_t scope_id;

      inline bool operator==(const property_context &o) const
      {
         return id == o.id && scope == o.scope && scope_id == o.scope_id;
      }

      inline bool operator!=(const property_context &o) const
      {
         return !(operator==(o));
      }
   };

   struct scope
   {
      std::map<scope_id, property_list> items;
   };

   struct properties
   {
      std::map<scope_type, scope> scopes;
   };

   inline std::ostream &operator<<(std::ostream &out, const property_vector &vector)
   {
      bool first = false;
      for (auto &val : vector)
      {
         if (!first)
         {
            out << ", ";
         }
         else
         {
            first = true;
         }

         out << val;
      }
      return out;
   }

   inline std::ostream &operator<<(std::ostream &out, const property_list &props)
   {
      bool first = false;
      for (auto &val : props)
      {
         if (!first)
         {
            out << ", ";
         }
         else
         {
            first = true;
         }

         out << val.first << " => " << val.second;
      }
      return out;
   }
} // namespace spider::property
