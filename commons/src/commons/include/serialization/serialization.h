#pragma once
#include "exception.h"
#include "json.h"
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
#include "msgpack.h"
#endif
#include "url_query.h"
#include <boost/utility/string_view.hpp>
#include <string>

namespace spider
{
   namespace serialization
   {
      enum class content_type
      {
         json,
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
         msgpack,
#endif
         urlencoded,
         unknown
      };


      constexpr std::array content_types = {
         "application/json",
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
                                                             "application/msgpack",
#endif
         "application/x-www-form-urlencoded"
      };

      inline content_type parse_content_type(const boost::string_view& content_type_string)
      {
         for (std::size_t i = 0; i != content_types.size(); ++i)
         {
            if (content_type_string == content_types[i]) return static_cast<content_type>(i);
         }

         return content_type::unknown;
      }

      template <class Data>
      bool parse_structure(Data& output, const std::string& data, content_type content_type)
      {
         if constexpr (visit_struct::traits::is_visitable<Data>::value)
         {
            switch (content_type)
            {
            case content_type::json:
               return json::parse_json(output, data);
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
            case content_type::msgpack:
               return messagepack::parse_msgpack(output, data);
#endif
            case content_type::urlencoded:
               return url_query::parse_url_query(output, data);
            default:
               throw exception("invalid content type");
            }
         }
         else
         {
            switch (content_type)
            {
            case content_type::json:
               return json::parse_json(output, data);
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
            case content_type::msgpack:
               return messagepack::parse_msgpack(output, data);
#endif

            default:
               throw exception("invalid content type");
            }
         }

         return false;
      }

      template <class Data>
      std::string serialize_structure(const Data& input, content_type content_type)
      {
         switch (content_type)
         {
         case content_type::json:
            return json::stringify_json(input);
#ifdef SERIALIZATION_SUPPORTS_MSGPACK
         case content_type::msgpack:
            return messagepack::stringify_msgpack(input);
#endif
         case content_type::urlencoded:
            return url_query::stringify_url_query(input);
         default:
            throw exception("invalid content type");
         }
      }
   } // namespace serialization
} // namespace spider
