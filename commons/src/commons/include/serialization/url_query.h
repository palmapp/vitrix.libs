#pragma once
#include "url.h"
#include <boost/convert.hpp>
#include <boost/convert/lexical_cast.hpp>
#include <boost/convert/stream.hpp>
#include <string>
#include <visit_struct/visit_struct.hpp>

#include <boost/algorithm/string.hpp>

struct boost::cnv::by_default : public boost::cnv::cstream
{
};

namespace spider
{
   namespace serialization
   {
      namespace url_query
      {
         template <class Data>
         class parse_visitor
         {
            const std::string &key_;
            std::string &value_;

          public:
            parse_visitor(const std::string &key, std::string &value) : key_(key), value_(value) {}

            template <class T>
            void operator()(const std::string &field, std::vector<T> &field_value)
            {
               if constexpr (std::is_same_v<std::string, T> || std::is_integral_v<T> || std::is_floating_point_v<T>)
               {
                  parse_vector(field, field_value);
               }
            }

            template <class FieldValue>
            void operator()(const std::string &field, FieldValue &field_value)
            {
               if constexpr (std::is_integral_v<FieldValue> || std::is_floating_point_v<FieldValue>)
               {
                  parse(field, field_value);
               }
            }

            void operator()(const std::string &field, std::string &field_value)
            {
               if (key_ == field) field_value = std::move(value_);
            }

          private:
            template <class FieldValue>
            void parse(const std::string &field, FieldValue &field_value)
            {
               if (key_ == field)
               {
                  auto converted_value = boost::convert<FieldValue>(value_);

                  if (converted_value)
                  {
                     field_value = converted_value.value();
                  }
               }
            }

            template <class FieldValue>
            void parse_vector(const std::string &field, std::vector<FieldValue> &field_value)
            {
               if (key_ == field)
               {
                  if (value_.find(',') != std::string::npos)
                  {
                     std::vector<std::string> parts;
                     boost::split(parts, value_, boost::is_any_of(","));

                     for (auto &part : parts)
                     {
                        auto converted_value = boost::convert<FieldValue>(part);
                        if (converted_value) field_value.push_back(converted_value.value());
                     }
                  }
                  else
                  {
                     auto cv = boost::convert<FieldValue>(value_);
                     if (cv) field_value.push_back(cv.value());
                  }
               }
            }
         };

         template <class Data>
         bool parse_url_query(Data &data, const std::string &uri)
         {
            return url::parse_query(uri.begin(), uri.end(),
                                    [&](const std::string &key, std::string &value)
                                    {
                                       parse_visitor<Data> visitor{key, value};
                                       visit_struct::for_each(data, visitor);
                                    });
         }

         template <class Data>
         bool parse_url_query(Data &data, std::string_view uri)
         {
            return url::parse_query(uri.begin(), uri.end(),
                                    [&](const std::string &key, std::string &value)
                                    {
                                       parse_visitor<Data> visitor{key, value};
                                       visit_struct::for_each(data, visitor);
                                    });
         }

         template <class Data>
         class stringify_visitor
         {
          public:
            std::stringstream buffer;
            bool first_value = true;

            void operator()(const std::string &field, const std::vector<std::string> &field_value)
            {
               buffer_vector(field, field_value);
            }

            void operator()(const std::string &field, const std::vector<bool> &field_value)
            {
               buffer_vector(field, field_value);
            }

            void operator()(const std::string &field, const std::vector<int> &field_value)
            {
               buffer_vector(field, field_value);
            }

            void operator()(const std::string &field, const std::vector<double> &field_value)
            {
               buffer_vector(field, field_value);
            }

            void operator()(const std::string &field, const bool &field_value)
            {
               buffer_value(field, field_value);
            }

            void operator()(const std::string &field, const int &field_value)
            {
               buffer_value(field, field_value);
            }

            void operator()(const std::string &field, const std::string &field_value)
            {
               buffer_value(field, field_value);
            }

            void operator()(const std::string &field, const double &field_value)
            {
               buffer_value(field, field_value);
            }

            template <class FieldValue>
            void operator()(const std::string &, const FieldValue &)
            {
            }

          private:
            void buffer_field(const std::string &field)
            {
               if (first_value)
                  first_value = false;
               else
                  buffer << '&';

               buffer << url::encode(field);
               buffer << '=';
            }

            void buffer_value(const std::string &field, const std::string &field_value)
            {
               buffer_field(field);
               buffer << url::encode(field_value);
            }

            template <class FieldValue>
            void buffer_value(const std::string &field, const FieldValue &field_value)
            {
               buffer_field(field);
               buffer << url::encode(boost::convert<std::string>(field_value).value());
            }

            template <class FieldValue>
            void buffer_vector(const std::string &field, const FieldValue &field_value)
            {
               buffer_field(field);

               bool first_record = true;
               for (auto &val : field_value)
               {
                  if (first_record)
                     first_record = false;
                  else
                     buffer << ',';

                  buffer << url::encode(boost::convert<std::string>(val).value());
               }
            }
         };

         template <class Data>
         std::string stringify_url_query(const std::vector<Data> &data)
         {
            // not supported
            return {};
         }

         template <class Data>
         std::string stringify_url_query(const Data &data)
         {
            stringify_visitor<Data> visitor;
            if constexpr (visit_struct::traits::is_visitable<Data>::value)
            {
               visit_struct::for_each(data, visitor);
            }
            return visitor.buffer.str();
         }

      } // namespace url_query
   } // namespace serialization
} // namespace spider