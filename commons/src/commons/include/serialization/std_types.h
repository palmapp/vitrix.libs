#pragma once
#include "exception.h"
#include <boost/any.hpp>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <commons/basic_types.h>

namespace spider
{
   using namespace commons::basic_types;

   using filesystem_path = boost::filesystem::path;
   using socket_stream = boost::asio::ip::tcp::socket;

   using timer = boost::asio::steady_timer;

   inline string_view to_string_view(boost::string_view view)
   {
      return {view.data(), view.size()};
   }

} // namespace spider