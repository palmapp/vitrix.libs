#pragma once
#include "std_types.h"
#include <formats/msgpack-ref.hpp>

namespace spider
{
   namespace serialization
   {
      namespace messagepack
      {
         template <class Data>
         bool parse_msgpack(Data& d, const std::string& msgpack)
         {
            try
            {
               msgpack::unpack(msgpack.data(), msgpack.size()).get().convert(d);
               return true;
            }
            catch (...)
            {
               return false;
            }
         }

         template <class Data>
         bool parse_msgpack(Data& d, std::string_view msgpack)
         {
            try
            {
               msgpack::unpack(msgpack.data(), msgpack.size()).get().convert(d);
               return true;
            }
            catch (...)
            {
               return false;
            }
         }

         template <class Data>
         bool parse_msgpack(Data& d, const char* msgpack, size_t size)
         {
            try
            {
               msgpack::unpack(msgpack, size).get().convert(d);
               return true;
            }
            catch (...)
            {
               return false;
            }
         }

         template <class Data>
         std::string stringify_msgpack(const Data& d)
         {
            std::stringstream buf;
            msgpack::pack(buf, d);
            return buf.str();
         }
      } // namespace messagepack
   } // namespace serialization
} // namespace spider
