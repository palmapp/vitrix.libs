cmake_minimum_required(VERSION 3.16)
project(commons CXX)

include(../../modern_env.cmake)

find_package(fmt CONFIG REQUIRED)
find_package(ZLIB REQUIRED)
find_package(ZeroMQ CONFIG REQUIRED)
find_package(cpprestsdk CONFIG REQUIRED)
find_package(Boost 1.79.0 REQUIRED COMPONENTS random system thread filesystem chrono atomic date_time regex iostreams)
find_package(Catch2 CONFIG REQUIRED)
find_package(msgpack-cxx CONFIG REQUIRED)

set(CPPREST_LIB cpprestsdk::cpprest)

add_subdirectory(../../libs libs)

add_subdirectory(commons)

if (NOT CMAKE_SYSTEM_NAME STREQUAL "Android")
    add_subdirectory(commons-tests)
    add_subdirectory(serialization-lib-tests)
    enable_testing()
    add_test(NAME commons COMMAND commons-tests)
endif ()
